RAYLEIGH
--------
Experimental source code for computer generated holography


This pack of ANSI C source code provides tools for computer generated holography, mainly for coherent light propagation. The source code is a permanent "work in progress", it mainly serves to explore and test such algorithms. Thus, it does not have a structure to be expected from a regular library as it is not known what functionality will be implemented next week.

If you find a bug (which is quite likely), contact the main developer:
Petr Lobaz 
lobaz@kiv.zcu.cz
Department of Computer Science and Engineering 
University of West Bohemia 
Univerzitni 8 
306 14 Plzen 
Czech Republic




QUICK START FOR MSVC USERS
------------------------------

Unpack the rayleighXXX.zip.

Open the solution rayleigh.sln in the MSVC (Microsoft Visual C++ / Visual Studio). You can see four projects:
1. rayleigh - the static library with all the functionality
2. example_propagation - a simple example how to use the library
3. example_hologram - a simple example how to use the library
4. propag - a simple command-line utility for hologram reconstruction

Switch to x64 platform, Release and build the solution. All the libraries should be located where they should be.
This configuration generates a DLL file. If you prefer static linking (which is now default in Debug mode), edit rayleigh.h file and uncomment RAYLEIGH_STATIC definition (or define it using compiler directive).
Read the example_propagation.c source code, run the program and explore the results.
Read the example_hologram.c source code, run the program and explore the results.
Build your own programs and enjoy!




SLOW START WITHOUT MSVC SOLUTION
--------------------------------

1. Unpack the rayleighXXX.zip.
The project needs libraries libpng (PNG image manipulation) and FFTW (fast Fourier transform) to build. You will find binary libraries that work with MSVC in rayleigh\_PLATFORM_\_CONFIGURATION_ directories, where _PLATFORM_ is either Win32 or x64, _CONFIGURATION_ is either Debug or Release. You can try them but it is likely you will need to prepare them for your particular compiler.


2. FFTW library is distributed as a source code or a compiled library, see http://www.fftw.org/. You can use version 3.2.2 from the zip file fftw322.zip which is known to work properly.
As it is not easy to build FFTW, it is the best to use the binary distribution. It comes in DLL files for Windows. 
For MSVC, it is necessary to prepare .lib files out of them. This can be done using
  lib.exe /def:libfftw3-3.def
  lib.exe /def:libfftw3f-3.def
  lib.exe /def:libfftw3l-3.def
or
  lib.exe /machine:x64 /def:libfftw3-3.def
  lib.exe /machine:x64 /def:libfftw3f-3.def
  lib.exe /machine:x64 /def:libfftw3l-3.def
depending on architecture you are going to use. X64 is the preferred one.
Once you have your LIB and DLL files prepared, copy them to the desired locations. For example, in the rayleigh project and x64 platform these are
  rayleigh\x64\Debug
  rayleigh\x64\Release
for DLL files and
  rayleigh\x64\Debug\lib
  rayleigh\x64\Release\lib
for LIB files. In the FFTW case, the Debug and Release versions would be the same.


3. LIBPNG library comes as a source code; you must compile both Debug and Release versions if you use MSVC; compile them for your desired platform, Win32 or x64 (the preferred one). It is recommended to use libpng 1.6.6 available in the zip file lpng166_rayleigh.zip.
Unpack it. You will see lpng166 and zlib folders; these must stay in the same directory.
If you use MSVC, you can use solutions located in lpng166\projects\visualc80 (for MSVC 2008) or lpng166\projects\vstudio2012 (for MSVC 2012). Open the solution, choose the platform (x64 or Win32) and the configuration (Debug or Release) and compile DLL file. If you cannot use these two solutions, you can use the other ones provided in the lpng166\projects directory, but these do not have x64 configuration ready; you must create your own if you wish to compile rayleigh for x64 platform. Once you are ready, copy DLL and LIB files (created automatically by MSVC) to the desired location. The files are libpng16.dll, libpng16.lib andzlib.lib. In the rayleigh project and x64 platform, the locations are
  rayleigh\x64\Debug
  rayleigh\x64\Release
for DLL files and
  rayleigh\x64\Debug\lib
  rayleigh\x64\Release\lib
for LIB files. In the LIBPNG case, the Debug and Release versions are different.


4. The rayleigh\rayleigh directory contains *.c and *.h files needed to build the rayleigh library. Build it as a dynamic library (.dll) to the
rayleigh\_PLATFORM_\_CONFIGURATION_\ directory. For MSVC, you will need import library as well; this should be located in the 
rayleigh\_PLATFORM_\_CONFIGURATION_\lib directory
If you prefer static library (.lib), uncomment RAYLEIGH_STATIC definition in the file rayleigh\rayleigh.h (or #define it using compiler directive) and compile to the directoryrayleigh\_PLATFORM_\_CONFIGURATION_\lib directory. 
The library functions require FFTW, LIBPNG16 and ZLIB libraries, see above. All the additional *.h files for these libraries are located in the rayleigh\include directory, do not forget to include them in the search path. The LIB files are located in the rayleigh\_PLATFORM_\_CONFIGURATION_\lib directories.


5. The other directories contain just one .c file that wants to include rayleigh.h. To compile such a "project" (e.g. example_propagation.c) to make a EXE file, make sure that rayleigh\include and rayleigh\rayleigh directories are in the search paths. To link it, make sure you add the rayleigh.lib and the additional libraries (FFTW, LIBPNG16, ZLIB) to "additional dependencies" of the linker.
To run the EXE file, make sure that the correct DLL files are located in the same directory as the EXE file. In the rayleigh project, this is rayleigh\_PLATFORM_\_CONFIGURATION_ directory.


6. Continue with the Quick start section of this file as you should have running version of the project now. Enjoy!




CHANGELOG
--------- 
rayleigh20140130.zip
- it is possible to compile the library as DLL
- many #defines (such as PICTURE_REALIMAG) are now available as enums (such as PictureIORealImagIntensity)
- changed timers functionality
- acceleration using double LUT starts to be stable...
- added lightsArray for point light clouds
- many small changes :) - it is work in progress!

rayleigh20131029_MSVC2012.zip
- the solution converted to MSVC 2012
- upgrade to libpng1.6.6
- slight cleaning of the code, mainly making Warnings to disappear
- expanded README.TXT file


rayleigh20131018.zip
- the first release