/*
 * An example for hologram recording and illumination.
 *
 * Create two lines composeed of many point light sources.
 * The horizontal line is at z = -300 mm, the vertical at z = +300 mm.
 * The lines form a cross 2x2 mm when viewed from the hologram 
 * 2.5x2.5 mm located at z=0, centered at the origin.
 *
 * The hologram is created by the object wave and the reference wave
 * coming at an angle 1 degree in the xz plane.
 *
 * After recording, the hologram is illuminated with the illumination
 * wave parallel to the z axis.
 * That is, the real image (diffraction order m=-1) of the horizontal line
 * should be at an angle
 *   theta_out,-1 = asin [ -1 * [sin(0) - sin(1)] + sin(0)]
 *                = asin [ sin(1) ]
 *                = 1 degree
 * The distance of the real image should be approximately 300 mm from the
 * hologram, i.e. at
 *   y = 300 * tan(1) = 5.24 mm
 * We will put a screen 12.8x12.8 mm to the plane z = 300 mm.
 * As the sampling distance of the screen will be 25 um,
 * the real image should be located 209 samples above the image
 * center. In the screen center, the 0-th diffraction maximum
 * occurs.
 * The vertical line should be out of focus, of course.
 * As the "rays" from the hologram try to form its real image at
 * (approximately) z = 350 mm, i.e. 50 mm from the screen, 
 * it is clear that the defocus should be
 *   d = hologramSize * 50 / 350
 *     = 2.5 * 0.36 mm
 * which equals approximately 0.36 mm / 25 um = 14 pixels.
 *
 */

#include "rayleigh.h"

int main(int argc, char **argv)
{
	t_lightsArray *lights = NULL;
	t_optfield *hologram = NULL, *screen = NULL, *illumination = NULL;
	int numLights = 50; /* number of light sources per line */
	double maxIntensity;
		
	/* Set the optical and sampling environment. */
	rayleighSetEnvironment(532 NM, 5 UM);

	/* Create big enough array for point sources storage. */
	lightsArrayNew(&lights, 1000);

	/* Generate a horizontal line composed of point light sources. */
	lightsArrayGenerateLine(&lights, /* the array to store lights to */
		-1 MM, 0, -300 MM,     /* starting point of the line */
		1.0 / numLights, 0,    /* amplitude and phase of the starting point */
		1 MM, 0, -300 MM,      /* ending point of the line */
		1.0 / numLights, 0,    /* amplitude and phase of the ending point */
		                       /* amplitude and phase of the generated points are linearly interpolated */
		1,                     /* we may want to randomize the (interpolated) phase;
							    * this is the seed for random number generator */
		0.5,                     /* amplitude of the phase randomization; 0 = no randomization,
							    * 1 = full randomization (phase +- pi) */
		numLights,             /* number of lights per line */
		1                      /* 1 = generate end points, 0 = do not generate them */
		);

	/* Generate a vertical line composed of point light sources. */
	lightsArrayGenerateLine(&lights, 
		0, -1 MM, -350 MM,
		1.0 / numLights, 0,
		0, 1 MM, -350 MM,
		1.0 / numLights, 0,
		1, 
		0.5,
		numLights,
		1
		);

	/* Create optfields for the hologram, the illumination wave and the screen. */
	optfieldNew(0, 0, 0, 512, 512, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &hologram);
	optfieldCopy(&illumination, hologram);

	/* Note that the screen has different sampling. */
	optfieldNew(0, 0, 300 MM, 512, 512, 5, SampleTypeFFTWDouble, BufferTypeMain, NULL, &screen);

	/*
	 * HOLOGRAM RECORDING
	 *
	 */

	/* Propagate lights to the hologram. */
	lightsArrayPropagate(lights, hologram);

	/* Now we have object wave in the hologram optfield; save it. */
	optfieldSavePNG(hologram, "01_object_wave", PictureIORealImagIntensity, 1.0, 0, "The object wave.");

	/* We need to normalize the optical field; or at least the reference wave
	 * we are going to add should have the amplitude proportional to the maximum
	 * amplitude of the object wave */
	optfieldFindMinmax(hologram, NULL, NULL, NULL, &maxIntensity, NULL);
	optfieldScale(&hologram, hologram, 1/sqrt(maxIntensity));

	/* Add the reference wave - a simple approximation of the gaussian beam
	 * by a spherical wave with gaussian intensity profile. */
	optfieldLightSimple(hologram, /* optfield to add the light */
		5 MM,                     /* radius of the beam */
		100 M,                    /* curvature of the wavefront; 100 m = big enough, almost plane wave */
		0, 0,                     /* center of the beam (x,y) */
		1*PI/180.0, 0,            /* the beam is normally parallel to the z axis;
								   * we will rotate it 1 degree around x axis,
								   * 0 degrees around y axis, i.e. it will lie in 
								   * the yz */
		1                         /* amplitude of the beam; as the maximum amplitude of the 
								   * object wave is 1, we have the same intensity of the
								   * reference and the object wave */
		);
	optfieldSavePNG(hologram, "02_hologramOF", PictureIORealImagIntensity, 1.0, 0, "The optical field at the plane of the hologram.");

	/* Calculate intensity of (objectWave + referenceWave) */
	optfieldIntensity(&hologram, hologram, 1, NULL);
	
	/* The optfield now has just real part only based on intensity.
	 * It would be possible to save it as PICTURE_REAL; however, this type
	 * represents 0 as a mid-grey tone (128 in 8bit picture) because it is
	 * reasonable to do so.
	 * Here we would like to represent 0 as black and maximum as white;
	 * let's use PictureIOIntensity instead.
	 * To save amplitide instead of intensity, we can use gamma correction 2.0
	 * (because amplitude = intensity**1/2.0). Additionally, we can add the
	 * common 2.2 gamma correction to prepare a image to be viewed directly.
	 */
	optfieldSavePNG(hologram, "03_hologramT", PictureIOIntensity, 2.0*2.2, 0, "The hologram (transmittance pattern). Gamma 2.2 for direct display.");

	/*
	 * HOLOGRAM RECONSTRUCTION
	 *
	 */

	/*
	 * Here we have the hologram in the memory. If we did not,
	 * it is possible to load it. Be careful with gamma!

		optfieldDispose(&hologram);
		optfieldLoadPNG(&hologram, "03_hologramT_i.16.png", 1, SampleTypeFFTWDouble, BufferTypeMain, PictureIOIntensity, 2.0*2.2);

	 */

	/* Illuminate the hologram. Note that optfieldLightSimple ADDS the light,
	 * so it is necessary to evaluate the phasor of the illumination light
	 * elsewhere and multiply it with the hologram. */
	optfieldLightSimple(illumination, 5 MM, 100 M, 0, 0, 0, 0, 1);
	optfieldMul(&hologram, hologram, illumination);

	/* As the illumination wave is parallel to the z axis, nothing special
	 * will be visible in the saved optical field; try to rotate the illumination
	 * wave somehow. */
	optfieldSavePNG(hologram, "04_hologramILL", PictureIORealImagIntensity, 1.0, 0, "The hologram illuminated.");

	/* Finally: propagate the illuminated hologram to the screen. */
	optfieldPropagate(hologram, screen, NULL, NULL, PropagationRayleighSommerfeld, VariablePrecisionOn, 0.2, FilterTypeTriangular, NULL, NULL);

	/* Save the real image (hopefully). */
	optfieldSavePNG(screen, "05_realimage", PictureIOIntensity, 2.2, 0, "The hologram reconstruction, real image.");

	/* Done! */
	optfieldDispose(&hologram);
	optfieldDispose(&screen);
	optfieldDispose(&illumination);

	return 0;
}