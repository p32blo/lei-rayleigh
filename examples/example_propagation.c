/*
 * A simple example of rayleigh usage.
 *
 * Create a diffraction grating, illuminate it,
 * propagate the light to a certain distance,
 * focus the light and propagate further.
 *
 * propagation scheme:
 * light -> grating -> lens -> screen
 *
 * a)
 * Light lambda = 532 nm illuminates the grating, period of slits = 30 um.
 * That is, m-th diffraction maxium should occur at an angle
 * sin theta_out,m = m lambda f + sin theta_in
 *                 = m * 0.017733 [radians]
 *
 * b)
 * Light leaves the grating and illuminates an intermediate plane 
 * at a distance d1 = 50 mm.
 * This means that m-th diffraction maximum should occur at
 * x = d1 * tan theta_out,m
 *   = 50e-3 * tan ( asin ( m * 0.017733 ) )
 * That is, for m = +-1 we should get a bright spot at
 * x = +- 0.89 mm
 * As sampling of the intermediate plane will be 5 um,
 * the centers of bright spots should appear 0.89e-3/5e-6 = 177 pixels
 * from the center of the central spot.
 *
 * c)
 * We would like to know how the diffraction pattern looks like 
 * at an infinite distance. This is certainly hard to calculate
 * numerically! However, there is a trick that is used in optics:
 * to "move" the infinite distance to a finite one using a lens.
 * If we want to discover what happens at the infinite distance,
 * we must be interested in angular distribution of light rather 
 * than spatial (xy) distribution; i.e. instead of asking "what 
 * intensity is at the point [x,y]", we should ask "what intensity
 * can be seen at an angle [theta_x,theta_y]".
 * Light coming from the infinite distance (i.e. plane waves) can
 * be focused. For example, a lens f=100 mm focuses plane waves
 * to bright spots at the plane (screen) at a distance 100 mm from the lens.
 * A plane wave that hits such a lens at an angle 45 degrees will
 * be then focused to a point 100 mm from the "center" of the plane,
 * i.e. a point of the plane at the optical axis. In this way,
 * every point of the screen describes an angular intensity of 
 * the light.
 *
 * So, we will put a lens to the intermediate plane, f=100 mm
 * (for example), and we should explore the image propagated to
 * the "screen" at a distance 100 mm.
 * The light left the diffraction grating at an angle theta_out,m;
 * it follows that such a "ray" should create a bright spot
 * on the screen at a distance 
 *   x = 100e-3 * tan theta_out,m
 * from the center of the screen. In our case, this would be
 *   x = 1.7736 mm
 * As the sampling of the screen will be 10 um, it should be
 * located 1.7736e-3/10e-6 = 177.4 pixels from the center
 * of the screen.
 */

/*
 * Main header with all the functionality
 */

#include "rayleigh.h"

int main(int argc, char **argv)
{
	/* optical fields, i.e. 2-D complex images */
	t_optfield *grating = NULL, *light = NULL, *lens = NULL, *screen = NULL;

	/* auxiliary variables */
	int gratingSamplesX, gratingSamplesY;
	double lensCenterX, lensCenterY, lensCenterZ;
	double screenCenterX, screenCenterY, screenCenterZ;


	/* Set wavelength of the optical environment - wavelength
	 * Set "common sampling distance" - sampling of all optical 
	 * fields will be an integer multiple of this value.
	 */
	rayleighSetEnvironment(532 NM, 1 UM);

	/* create optical field for grating */
	optfieldNew(0, 0, -50 MM,    /* center of the grating */
		100, 100,                /* number of samples */
		5,                       /* sampling distance will be 5 * common_sampling_distance = 5 UM */
		SampleTypeFFTWDouble,    /* double precision; 
								    however, nothing else is supported now */
		BufferTypeMain,          /* the data should be stored in the main memory; 
								    however, nothing else is supported now */
		NULL,                    /* we could provide our own buffer for data storage; 
								    NULL indicates that the function should allocate its own */
		&grating);               /* variable to store the pointer */

	/* create optical field for illumination light complex amplitudes */
	optfieldCopy(&light, grating);

	/* create optical field for lens plane */
	optfieldNew(0, 0, 0 MM,      /* center of the grating */
		512, 512,                /* number of samples */
		5,                       /* sampling distance will be 5 * common_sampling_distance = 5 UM */
		SampleTypeFFTWDouble,    /* double precision; 
								    however, nothing else is supported now */
		BufferTypeMain,          /* the data should be stored in the main memory; 
								    however, nothing else is supported now */
		NULL,                    /* we could provide our own buffer for data storage; 
								    NULL indicates that the function should allocate its own */
		&lens);                  /* variable to store the pointer */

	/* create optical field for screen */
	optfieldNew(0, 0, 100 MM,    /* center of the grating */
		512, 512,                /* number of samples */
		10,                      /* sampling distance will be 50 * common_sampling_distance = 50 UM */
		SampleTypeFFTWDouble,    /* double precision; 
								    however, nothing else is supported now */
		BufferTypeMain,          /* the data should be stored in the main memory; 
								    however, nothing else is supported now */
		NULL,                    /* we could provide our own buffer for data storage; 
								    NULL indicates that the function should allocate its own */
		&screen);                /* variable to store the pointer */

	/* Create a pattern composed of 0's and 1's, i.e. completely
	 * opaque and transparent dots.
	 * In this case, every row of the grating will be the same with the 
	 * transmittance pattern 0101010...
	 * i.e. we are creating a vertical diffraction grating.
	 */
	gratingInitPattern(grating, "x.", 3);

	/* Save the pattern so we can see what did we create. */
	optfieldSavePNG(grating, /* Optfield to save */
		"01_grating_itself", /* Filename; this string is modified by an postfix to indicate image type. */
		PictureIOIntensity,  /* It is necessary to store complex numbers to the (RGB) image
							  * somehow; this is for greyscale image, pixel = intensity */
		2.2,                 /* Gamma correction for colour encoding. For the images intended for 
							  * viewing, 2.2 is recommended. For images that will be numerically
							  * explored further, 1.0 is necessary. */
		0,                   /* All the numbers to be stored have to be normalized so that
							  * maximum value gets white (for example). Here we can provide our
							  * own value for normalization, which is necessary if we want to
							  * save and compare several images. 0 is for automatic maximum
							  * value detection. */
		"The diffraction grating pattern."               
		                     /* Among the PNG file, a text file with information is saved as well.
							  * Text file can contain a user comment. */
    );

	/* Generate complex amplitudes of the illumination light.
	 * Simple light spot with gaussian intensity profile.
	 */
	optfieldLightSimple(light,  /* optfield */
		1 MM,                   /* radius of gaussian intensity profile; small = small spot */
		1000,                   /* radius of curvature; high number approaches plane wave */
		0, 0,                   /* x, y of the center of the spot */
		0, 0,                   /* angles (x, y) of illumination; 0,0 = parallel to z axis */
		1.0						/* multiply amplitude with this value */	
		);


	/* Save the pattern so we can see what did we create. */
	optfieldSavePNG(light,   /* Optfield to save */
		"02_light",          /* Filename; this string is modified by an postfix to indicate image type. */
		PictureIORealImagIntensity,    /* It is necessary to store complex numbers to the (RGB) image
							  * somehow; this is for complex numbers structure exploration,
							  * red channel = real part, green channel = imaginary part */
		1.0,                 /* Gamma correction for colour encoding. For the images intended for 
							  * viewing, 2.2 is recommended. For images that will be numerically
							  * explored further, 1.0 is necessary. */
		0,                   /* All the numbers to be stored have to be normalized so that
							  * maximum value gets white (for example). Here we can provide our
							  * own value for normalization, which is necessary if we want to
							  * save and compare several images. 0 is for automatic maximum
							  * value detection. */
		"The complex amplitude of the light."               
		                     /* Among the PNG file, a text file with information is saved as well.
							  * Text file can contain a user comment. */
    );

	/* prepare for grating illumination:
	 * get sample count of the optfield */
	optfieldGetSamples(&gratingSamplesX, &gratingSamplesY, grating);

	/* illuminate the grating by multiplication, i.e.
	 * grating := grating .* light
	 */
	optfieldMul(&grating, /* output */
		grating, light		 /* inputs */
		);

	optfieldSavePNG(grating, /* Optfield to save */
		"03_grating_illuminated", /* Filename */
		PictureIORealImagIntensity,    /* It is necessary to store complex numbers to the (RGB) image
							  * somehow; this is for complex numbers structure exploration,
							  * red channel = real part, green channel = imaginary part */
		1.0,                 /* Gamma correction for colour encoding. For the images intended for 
							  * viewing, 2.2 is recommended. For images that will be numerically
							  * explored further, 1.0 is necessary. */
		0,                   /* All the numbers to be stored have to be normalized so that
							  * maximum value gets white (for example). Here we can provide our
							  * own value for normalization, which is necessary if we want to
							  * save and compare several images. 0 is for automatic maximum
							  * value detection. */
		"The diffraction grating illuminated."               
		                     /* Among the PNG file, a text file with information is saved as well.
							  * Text file can contain a user comment. */
    );

	/* Now propagate the light from the grating to the intermediate plane where we will
	 * place the lens.
	 */
	optfieldPropagate(grating, /* Optfield to propagate from (source) */
		lens,                  /* Optfield to propagate to (target) */
		NULL, NULL,	           /* Propagation needs auxiliary memory buffers for e.g. convolution.
							    * If necessary (when memory management is critical), we can
								* provide our own buffers; if not, NULL means that optfieldPropagate
								* should allocate (and dispose) its own buffers.
								*/
		PropagationRayleighSommerfeld, /* Type of algorithm for the light propagation;
										 * see propagate_rayleigh_fft.h for further algorithms.
										 */
		VariablePrecisionOn,    /* Propagation algorithm should check for its parameters validity
							    * and to improve precision in case of coarse sampling.
								* It can happen that the task of propagation is automatically split 
								* to several simpler propagation tasks. VariablePrecisionOn states
								* that the algorithm should improve precision for each of the tasks
								* independently to each other. This is fast but may lead to artifacts
								* as different tasks operate with slightly different parameters.
								*/
	    0.2,                   /* This tells that precision of the optical field sampling should
							    * be internally improved so that critical variables are sampled
								* (in this case) at least 1/0.2-times per wavelength, i.e.
								* 5x per wavelength. In most situations this is OK.
								*/
		FilterTypeTriangular, /* If the propagation algorithm decides it is necessary to improve
							    * precision of the optical field sampling, it upsamples the 
								* optical fields using this filter; for other filter types, see
								* filter.h
								*/
		NULL,                  /* Some filters may require additional parameters, here is a pointer
							    * for them. Currently no filter requires any parameter. */
		NULL				   /* The propagation algorithm can split the task to several simpler sub-tasks.
							    * This is described in a "propagation plan". If necessary, it is possible
								* to provide our own propagation plan; NULL means that optfieldPropagate
								* should derive its own plan. */
		);

	/* Note that the filenames are the same; this is not an error. */
	optfieldSavePNG(lens, "04_right_before_lens", PictureIOIntensity, 2.2, 0, "The diffraction pattern.");
	optfieldSavePNG(lens, "04_right_before_lens", PictureIORealImagIntensity, 1.0, 0, "The diffraction pattern upstream the lens.");

	/* Get positions of the lens and the screen to calculate focal length of the lens. */
	optfieldGetCenter(&lensCenterX, &lensCenterY, &lensCenterZ, lens);
	optfieldGetCenter(&screenCenterX, &screenCenterY, &screenCenterZ, screen);

	/* Apply a lens function; an ideal lens is assumed here.
	 * We want to simulate a lens with focal length f (parallel rays are
	 * focused at a distance f), f = screenCenterZ - lensCenterZ.
	 */
	lensIdeal(0, 0, -100 KM, /* Rays from this point (100 KM far is effectively at infinity, i.e.
							  * the rays are parallel to the z axis)... */
		0, 0, lensCenterZ,   /* are focused by a lens located here... */
		0, 0, screenCenterZ, /* to a point located here. */
		lens,                /* Optfield to apply the lens function to. */
		0, 0,                /* The lens may have a circular aperture centered here (x,y). */
		5 MM,                /* The radius of the aperture; try 0.5 MM. */
		LENS_NO_APODIZE      /* Apodization is a gradual transmittance falloff to the edge of the aperture. */
		);

	optfieldSavePNG(lens, "05_right_after_lens", PictureIORealImagIntensity, 1.0, 0, "The diffraction pattern downstream the lens.");

	/* Note that we use VariablePrecisionOff here, i.e.
	 * the precision of the propagation task is assumed the same for each of 
	 * its parts. */
	optfieldPropagate(lens, screen, NULL, NULL, PropagationRayleighSommerfeld, VariablePrecisionOff, 0.2, FilterTypeTriangular, NULL, NULL);

	optfieldSavePNG(screen, "06_screen", PictureIOIntensity, 2.2, 0, "The focused difraction pattern.");

	/* Done! */
	optfieldDispose(&grating);
	optfieldDispose(&lens);
	optfieldDispose(&screen);

	return 0;
}

