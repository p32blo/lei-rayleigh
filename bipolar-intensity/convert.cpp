#include <rayleigh.h>

#include "convert.h"

void lights2lightsArray (t_lightsArray **la, lights_t *l)
{
	int i;
	/* Create big enough array for point sources storage. */
	lightsArrayNew(la, l->len);

	for (i = 0; i < l->len; ++i) {
		/* XXX - amplitude and phase values? */
#ifdef SOA
		lightsArrayGeneratePoint(la, l->x[i], l->y[i], l->z[i], 1/l->len, 0);
#else
		lightsArrayGeneratePoint(la, l->buff[i].x, l->buff[i].y, l->buff[i].z, 1/l->len, 0);
#endif
	}
}

void holo2optfield (t_optfield **opt, holo_t *h)
{
	optfieldNew(0, 0, 0,h->samples.x, h->samples.y, h->sampling, SampleTypeFFTWDouble, BufferTypeMain, NULL, opt);
	optfieldSetCorner(h->corner.x, h->corner.y, h->corner.z, *opt);

	fftw_complex *out = (fftw_complex*) (*opt)->buffer->data;

	int i;
	for (i = 0; i < h->len; ++i) {
		out[i][RE] = h->intensity[i];
		out[i][IM] = 0;
	}
}


void construct(holo_t holo, lights_t lights, char* filename)
{
	/* Set the optical and sampling environment. */
	rayleighSetEnvironment(532 NM, 5 UM);

	t_lightsArray *l = NULL;
	t_optfield *h = NULL;

	printf("Converting to LightsArray...\n");
	lights2lightsArray(&l, &lights);

	printf("Converting to Optfield...\n");
	holo2optfield(&h, &holo);

	{
		t_optfield *screen = NULL, *illumination = NULL;
		optfieldNew(0, 0, 0, 512, 512, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &illumination);

		/* Note that the screen has different sampling. */
		optfieldNew(0, 0, 300 MM, 512, 512, 5, SampleTypeFFTWDouble, BufferTypeMain, NULL, &screen);

		/* HOLOGRAM RECONSTRUCTION */
		printf("Reconstructing Hologram...\n");

		/* Illuminate the hologram. Note that optfieldLightSimple ADDS the light,
	 * so it is necessary to evaluate the phasor of the illumination light
	 * elsewhere and multiply it with the hologram. */
		optfieldLightSimple(illumination, 5 MM, 100 M, 0, 0, 0, 0, 1);
		optfieldMul(&h, h, illumination);

		/* As the illumination wave is parallel to the z axis, nothing special
	 * will be visible in the saved optical field; try to rotate the illumination
	 * wave somehow. */
		optfieldSavePNG(h, "04_hologramILL", PictureIORealImagIntensity, 1.0, 0, "The hologram illuminated.");

		/* Finally: propagate the illuminated hologram to the screen. */
		optfieldPropagate(h, screen, NULL, NULL, PropagationRayleighSommerfeld, VariablePrecisionOn, 0.2, FilterTypeTriangular, NULL, NULL);

		optfieldSavePNG(screen, filename, PictureIOIntensity, 2.2, 0, "The hologram reconstruction, real image.");

		optfieldDispose(&h);
		optfieldDispose(&screen);
		optfieldDispose(&illumination);
	}
}
