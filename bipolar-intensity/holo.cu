#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include <math.h>

// time measuring resolution (us)
#define TIME_RESOLUTION 1000000
#include <sys/time.h>

#include "holo.h"
#include "parser.h"

double sampling_distance;
double lambda;
double k_number;

void Usage(char *str) {
	printf("\n");
	printf("%s <angle x> <angle y> <n lights | o> <filename>\n", str);
	printf("| angle x -> reference light angle in the X axis\n");
	printf("| angle y -> reference light angle in the Y axis\n");
	printf("| n lights | o -> number of lights or 'o' if input is form a file\n");
	printf("| filename -> input/output file depending on previous options\n");
	printf("\n");
}

void Get_args(int argc, char*argv[],float *angX, float *angY, int *nLights, char* filename[])
{
	if (argc != 5) {
		Usage(argv[0]);
		exit(0);
	}

	*angX = atof(argv[1]) DEG;
	*angY = atof(argv[2]) DEG;
	*filename = argv[4];

	if (*argv[3] == 'o') {
		*nLights = -1;
	} else if (!isalpha (*argv[3])){
		*nLights = atoi(argv[3]);
	} else {
		Usage(argv[0]);
		exit(0);
	}
}

void checkCUDAError (const char *msg)
{
	cudaError_t err = cudaGetLastError();
	if (cudaSuccess != err) {
		fprintf(stderr, "Cuda error: %s\n%s\n", msg, cudaGetErrorString(err));
		exit(-1);
	}
}

void init_lights (lights_t *lights, int n)
{
	n *= 2;
#ifdef SOA
	lights->x = (double*) malloc(n * sizeof(double));
	lights->y = (double*) malloc(n * sizeof(double));
	lights->z = (double*) malloc(n * sizeof(double));
#else
	lights->buff = (point_t *) malloc(n * sizeof(point_t));
#endif
	lights->len = n;
	lights->size = n;
}

void set_lights(lights_t *lights, int n)
{
	int index = 0, i = 0;
	double t;

	point_t p11 = { -1 MM, 0, -300 MM };
	point_t p12 = {  1 MM, 0, -300 MM };

	point_t diff1 = { p12.x-p11.x, p12.y-p11.y, p12.z-p11.z };

	double N = n-1;

	for (i = 0; i < n; ++i) {
		t = i / N;

#ifdef SOA
		lights->x[index] = p11.x + diff1.x * t;
		lights->y[index] = p11.y + diff1.y * t;
		lights->z[index] = p11.z + diff1.z * t;
#else
		lights->buff[index].x = p11.x + diff1.x * t;
		lights->buff[index].y = p11.y + diff1.y * t;
		lights->buff[index].z = p11.z + diff1.z * t;

#endif
		index++;
	}

	point_t p21 = { 0, -1 MM, -350 MM };
	point_t p22 = { 0,  1 MM, -350 MM };

	point_t diff2 = { p22.x-p21.x, p22.y-p21.y, p22.z-p21.z };

	for (i = 0; i < n; ++i) {
		t = i / N;
#ifdef SOA
		lights->x[index] = p21.x + diff2.x * t;
		lights->y[index] = p21.y + diff2.y * t;
		lights->z[index] = p21.z + diff2.z * t;
#else
		lights->buff[index].x = p21.x + diff2.x * t;
		lights->buff[index].y = p21.y + diff2.y * t;
		lights->buff[index].z = p21.z + diff2.z * t;
#endif
		index++;
	}

	lights->len = index;
#if DEBUGS
	for (i = 0; i < lights->len; ++i) {
		printf("(%f,%f,%f)\n", lights->x[i], lights->y[i], lights->z[i]);
	}
#endif
}

point_t* set_lights_gpu (lights_t *lights)
{
	point_t *res = lights->buff;
	point_t *l = NULL;

	cudaMalloc((void**) &l, lights->len * sizeof(point_t));
	checkCUDAError("mem allocation lights");

	cudaMemcpy(l, lights->buff, lights->len * sizeof(point_t), cudaMemcpyHostToDevice);
	checkCUDAError("memcpy h->d lights");

	lights->buff = l;

	return res;
}

void set_lights_gpu_buff (lights_t *lights, point_t *l)
{
	cudaFree(lights->buff);
	lights->buff = l;

	checkCUDAError("mem free lights");
}

void init_holo (holo_t *holo, point_int_t samples)
{
	holo->corner.x = 0;
	holo->corner.y = 0;

	holo->samples = samples;

	holo->len = holo->samples.x * holo->samples.y;
	holo->size = holo->samples.x * holo->samples.y;

	//printf("%d,%d,%d\n", holo->samples.x, holo->samples.y, holo->len);

	holo->intensity = (double*) malloc( holo->len * sizeof(double));
}

void set_holo (holo_t *holo)
{
	int index = 0, i, j;
#ifdef DEBUG
	printf("<<<%f>>>\n", sampling_distance * 1.0);
#endif
	point_t center = {0, 0, 0};

	point_t corner  = {
		center.x - sampling_distance *(holo->samples.x - 1) / 2.0,
		center.y - sampling_distance *(holo->samples.y - 1) / 2.0,
		center.z
	};

#if DEBUGS
	int size = holo->samples.x * holo->samples.y;

	printf(">>>%d<<<\n", size);
#endif
	for (i = 0; i < holo->samples.x; ++i) {
		for (j = 0; j < holo->samples.y; ++j) {
			holo->intensity[index] = 0;
#if DEBUGS
			printf("%d: (%f,%f,0)-%f\n", index, holo->x[index], holo->y[index], sampling_distance);
#endif
			index++;
		}
	}

	holo->corner = corner;
}

double* set_holo_gpu (holo_t *holo)
{
	double *res = holo->intensity;
	double *buff = NULL;

	cudaMalloc((void**) &buff, holo->len * sizeof(double));
	checkCUDAError("mem allocation holo");

	holo->intensity = buff;

	return res;
}

void set_holo_gpu_buff (holo_t *holo, double *buff)
{
	cudaMemcpy(buff, holo->intensity, holo->len * sizeof(double), cudaMemcpyDeviceToHost);
	checkCUDAError("memcpy d->h holo");

	cudaFree(holo->intensity);
	checkCUDAError("mem free holo");

	holo->intensity = buff;
}

__global__ void test_holo (point_t *lights_buff, int l_size,
						   double *holo_buff, point_t corner, point_int_t samples,
						   point_t ref, double sampling_distance, double k_number)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	int j = blockIdx.y * blockDim.y + threadIdx.y;

	point_t target  = {
		corner.x + i * sampling_distance,
		corner.y + j * sampling_distance,
		0
	};

	float phase_ref = (target.x * ref.x) + (target.y * ref.y);
	uint index = j * samples.x + i;

	if (i < samples.x && j < samples.y ) {
		for (int li = 0; li < l_size; ++li) {

			point_t p = lights_buff[li];

			point_t r = {
				target.x - p.x,
				target.y - p.y,
				- p.z
			};

			float phase_n = sqrt((r.x*r.x) + (r.y*r.y) + (r.z*r.z));

			holo_buff[index] += cos(k_number * (phase_ref - phase_n));
		}
	}
}

void print_hologram (holo_t *holo)
{
	int i, j, index = 0;

	for (j = 0; j < holo->samples.y; ++j) {
		for (i = 0; i < holo->samples.x; ++i) {
			printf("(%f)\n", holo->intensity[index]);
			index++;
		}
	}
}

void test ()
{
	int nDevices;

	cudaGetDeviceCount(&nDevices);
	for (int i = 0; i < nDevices; i++) {
		cudaDeviceProp prop;
		cudaGetDeviceProperties(&prop, i);
		printf("Device Number: %d\n", i);
		printf("  Device name: %s\n", prop.name);
		printf("  Memory Clock Rate (KHz): %d\n",
			   prop.memoryClockRate);
		printf("  Memory Bus Width (bits): %d\n",
			   prop.memoryBusWidth);
		printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
			   2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
	}
}

int main (int argc, char *argv[])
{
	float angle_x, angle_y;
	int numLights;
	char* filename;

	struct timeval t1, t2;
	long long unsigned begin, end;

	Get_args(argc, argv, &angle_x, &angle_y, &numLights, &filename);
#if DEBUGS
	printf("%f,%f,%d,%s\n", angle_x, angle_y, numLights, filename);
#endif

	lights_t lights;
	holo_t holo;
	FILE *f;

	point_int_t sample = { 512, 512 };

	sampling_distance = 5 UM;
	lambda =  532 NM;
	k_number = PI2 / lambda;

	//test();

	lights = *init_buff(numLights * 2);
	//init_lights(&lights, numLights);
	init_holo(&holo, sample);

	if (numLights < 0 ) {
		if (f = fopen (filename, "r"))
			parse_obj_vertices(&lights, f);
		else {
			printf("ERROR: File does not exists\n");
			exit(-1);
		}
	} else {
		set_lights(&lights, numLights);
	}

	set_holo(&holo);

	// pointer in the device
	point_t *l;
	double *buff;

	l = set_lights_gpu(&lights);
	buff = set_holo_gpu (&holo);

	point_t ref = {
		cos(angle_x) * sin(angle_y),
		sin(angle_x),
		cos(angle_x) * cos(angle_y)
	};

	//printf("n:%d\n", omp_get_max_threads());
	//printf("%d,%d, %d\n", holo->samples.x, holo->samples.y, holo->len);

	gettimeofday(&t1, NULL);

	printf("%d\n", lights.len);

	point_t block_size = { 16, 16 };

	dim3 dimBlock(block_size.x, block_size.y);
	dim3 dimGrid(holo.samples.x /  block_size.x,
				 holo.samples.y / block_size.y);

	checkCUDAError("before");
	test_holo<<<dimGrid,dimBlock>>>(lights.buff, lights.len, holo.intensity, holo.corner, holo.samples, ref, sampling_distance, k_number);
	checkCUDAError("after");

	cudaDeviceSynchronize();
	checkCUDAError("sync");

	gettimeofday(&t2, NULL);

	set_holo_gpu_buff(&holo, buff);
	set_lights_gpu_buff(&lights, l);

	begin = t1.tv_sec * TIME_RESOLUTION + t1.tv_usec;
	end = t2.tv_sec * TIME_RESOLUTION + t2.tv_usec;

	printf("\n%llu\n", end - begin);


	return 0;
}
