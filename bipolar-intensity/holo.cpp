#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include <math.h>

#define MAX_FREQ_ANGLE 60

// time measuring resolution (us)
#define TIME_RESOLUTION 1000000
#include <sys/time.h>

#include <omp.h>

#include "convert.h"
#include "holo.h"
#include "parser.h"

#include <embree2/rtcore.h>
#include <embree2/rtcore_ray.h>

#ifdef OCCLUSION
#include <xmmintrin.h>
#include <pmmintrin.h>
#endif

double HOLO_SAMPLING_DISTANCE;
double HOLO_LAMBDA;
double HOLO_K_NUMBER;

void Usage(char *str) {
	printf("\n");
	printf("%s <angle x> <angle y> <n lights | o> <filename>\n", str);
	printf("| angle x -> reference light angle in the X axis\n");
	printf("| angle y -> reference light angle in the Y axis\n");
	printf("| n lights | o -> number of lights or 'o' if input is form a file\n");
	printf("| filename -> input/output file depending on previous options\n");
	printf("\n");
}

void Get_args(int argc, char*argv[],float *angX, float *angY, int *nLights, char* filename[])
{
	if (argc != 5) {
		Usage(argv[0]);
		exit(0);
	}

	*angX = atof(argv[1]) DEG;
	*angY = atof(argv[2]) DEG;
	*filename = argv[4];

	if (*argv[3] == 'o') {
		*nLights = -1;
	} else if (!isalpha (*argv[3])){
		*nLights = atoi(argv[3]);
	} else {
		Usage(argv[0]);
		exit(0);
	}
}

void init_lights (lights_t *lights, int n)
{
	n *= 2;
#ifdef SOA
	lights->x = (double*) malloc(n * sizeof(double));
	lights->y = (double*) malloc(n * sizeof(double));
	lights->z = (double*) malloc(n * sizeof(double));
#else
	lights->buff = (point_t*) malloc(n * sizeof(point_t));
#endif
	lights->len = n;
	lights->size = n;
}

void set_lights(lights_t *lights, int n)
{
	int index = 0, i = 0;
	double t;

	point_t p11 = { -1 MM, 0, -300 MM };
	point_t p12 = {  1 MM, 0, -300 MM };

	point_t diff1 = { p12.x-p11.x, p12.y-p11.y, p12.z-p11.z };

	double N = n-1;

	for (i = 0; i < n; ++i) {
		t = i / N;

#ifdef SOA
		lights->x[index] = p11.x + diff1.x * t;
		lights->y[index] = p11.y + diff1.y * t;
		lights->z[index] = p11.z + diff1.z * t;
#else
		lights->buff[index].x = p11.x + diff1.x * t;
		lights->buff[index].y = p11.y + diff1.y * t;
		lights->buff[index].z = p11.z + diff1.z * t;

#endif
		index++;
	}

	point_t p21 = { 0, -1 MM, -350 MM };
	point_t p22 = { 0,  1 MM, -350 MM };

	point_t diff2 = { p22.x-p21.x, p22.y-p21.y, p22.z-p21.z };

	for (i = 0; i < n; ++i) {
		t = i / N;
#ifdef SOA
		lights->x[index] = p21.x + diff2.x * t;
		lights->y[index] = p21.y + diff2.y * t;
		lights->z[index] = p21.z + diff2.z * t;
#else
		lights->buff[index].x = p21.x + diff2.x * t;
		lights->buff[index].y = p21.y + diff2.y * t;
		lights->buff[index].z = p21.z + diff2.z * t;
#endif
		index++;
	}

	lights->len = index;
#if DEBUGS
	for (i = 0; i < lights->len; ++i) {
		printf("(%f,%f,%f)\n", lights->x[i], lights->y[i], lights->z[i]);
	}
#endif
}

void init_holo (holo_t *holo, point_int_t samples, int sampling)
{
	holo->corner.x = 0;
	holo->corner.y = 0;

	holo->samples = samples;
	holo->sampling = sampling;

	holo->len = holo->samples.x * holo->samples.y;
	holo->size = holo->samples.x * holo->samples.y;

	//printf("%d,%d,%d\n", holo->samples.x, holo->samples.y, holo->len);

	holo->intensity = (double*) malloc (holo->size * sizeof(double));
}

void set_holo (holo_t *holo)
{
	int index = 0, i, j;
#ifdef DEBUG
	printf("<<<%f>>>\n", HOLO_SAMPLING_DISTANCE * 1.0);
#endif
	point_t center = {0, 0, 0};

	double sample_dist = HOLO_SAMPLING_DISTANCE * holo->sampling;

	point_t corner  = {
		center.x - sample_dist * (holo->samples.x - 1) / 2.0,
		center.y - sample_dist * (holo->samples.y - 1) / 2.0,
		center.z
	};

#if DEBUGS
	int size = holo->samples.x * holo->samples.y;

	printf(">>>%d<<<\n", size);
#endif
	for (i = 0; i < holo->samples.x; ++i) {
		for (j = 0; j < holo->samples.y; ++j) {

			holo->intensity[index] = 0;
#if DEBUGS
			printf("%d: (%f,%f,0)-%f\n", index, holo->x[index], holo->y[index], sampling_distance);
#endif
			index++;
		}
	}

	holo->corner = corner;
}

static inline double angle(point_t p1, point_t p2, point_t v1)
{
	point_t v2 = {
		p2.x - p1.x,
		p2.y - p1.y,
		p2.z - p1.z
	};

	//Get the dot product
	double dot = v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;

	// Divide the dot by the product of the magnitudes of the vectors
	dot = dot / (sqrt(v1.x*v1.x + v1.y*v1.y + v1.z*v1.z) * sqrt(v2.x*v2.x + v2.y*v2.y + v2.z*v2.z));

	//Get the arc cosin of the angle, you now have your angle in radians
	double val_acos = acos(dot);

	//Multiply by 180/Mathf.PI to convert to degrees
	double angle = val_acos * 180/ PI;

	//Congrats, you made it really hard on yourself.
	return angle;
}

void test_holo (lights_t * lights, holo_t * holo, float angle_x, float angle_y, RTCScene *scene)
{
	int samples_x = holo->samples.x;
	int samples_y = holo->samples.y;
	int l_size = lights->len;

	point_t ref  = {
		cos(angle_x) * sin(angle_y),
		sin(angle_x),
		cos(angle_x) * cos(angle_y)
	};

	double sample_dist = HOLO_SAMPLING_DISTANCE * holo->sampling;

	//printf("%d,%d, %d\n", holo->samples.x, holo->samples.y, holo->len);
	int li, i, j;

	//printf("\r             Light %6d of %6d (%6.2f%% done)", li+1, lights->len, (100.0*li) / lights->len);

	#pragma omp parallel for private(li, i, j)
	for (j = 0; j < samples_y; ++j) {

		//printf("\rLine %6d /", i);
		point_t target = {0};

		target.y = holo->corner.y + j * sample_dist;

		for (i = 0; i < samples_x; ++i) {

			target.x = holo->corner.x + i * sample_dist;

			int index = j * samples_x + i;

			float phase_ref = (target.x * ref.x) + (target.y * ref.y);

			#pragma simd
			for (li = 0; li < l_size; ++li) {

#ifdef SOA
				point_t p = {
					lights->x[li],
					lights->y[li],
					lights->z[li]
				};
#else
				point_t p = lights->buff[li];
#endif

#ifdef ANTIALIAS
				if (angle(p, target, ref) <= MAX_FREQ_ANGLE)
#endif
				{
					point_t r = {
						target.x - p.x,
						target.y - p.y,
						- p.z
					};

#ifdef OCCLUSION
					RTCRay ray;

					ray.org[0] = p.x; ray.org[1] = p.y; ray.org[2] = p.z;
					ray.dir[0] = r.x; ray.dir[1] = r.y; ray.dir[2] = r.z;
					ray.tnear = 0.0001f;
					ray.tfar = INFINITY;
					ray.geomID = RTC_INVALID_GEOMETRY_ID;
					ray.primID = RTC_INVALID_GEOMETRY_ID;
					ray.instID = RTC_INVALID_GEOMETRY_ID;
					ray.mask = 0xFFFFFFFF;
					ray.time = 0.f;

					rtcOccluded(*scene, ray);

					// no geometry is occluding this point
					if (ray.geomID)
#endif
					{

						float phase_n = sqrt((r.x*r.x) + (r.y*r.y) + (r.z*r.z));

						holo->intensity[index] += cos(HOLO_K_NUMBER * (phase_ref - phase_n));
					}
				}
			}
		}
	}
}

void print_hologram (holo_t *holo)
{
	int i, j, index = 0;

	for (j = 0; j < holo->samples.y; ++j) {
		for (i = 0; i < holo->samples.x; ++i) {
			printf("(%f)\n", holo->intensity[index]);
			index++;
		}
	}
}


int main (int argc, char *argv[])
{
	float angle_x, angle_y;
	int numLights;
	char* filename;

	struct timeval t1, t2;
	long long unsigned begin, end;

	Get_args(argc, argv, &angle_x, &angle_y, &numLights, &filename);
#if DEBUGS
	printf("%f,%f,%d,%s\n", angle_x, angle_y, numLights, filename);
#endif

	lights_t lights;
	holo_t holo;

	float* verts = NULL; int num_verts;
	unsigned* inds = NULL; int num_inds;

	point_int_t sample = { 512, 512 };

	HOLO_SAMPLING_DISTANCE = 5 UM;
	HOLO_LAMBDA =  532 NM;
	HOLO_K_NUMBER = PI2 / HOLO_LAMBDA;

	if (numLights < 0 ) {
		lights = *init_buff_obj(filename, verts, num_verts, inds, num_inds);
	} else {
		lights = *init_buff(numLights * 2);
		set_lights(&lights, numLights);
#ifdef OCCLUSION
		num_inds = 0;
		verts = get_buff_float(&lights, num_verts);
#endif
	}

	init_holo(&holo, sample, 1);
	set_holo(&holo);

	printf("Using %d threads\n", omp_get_max_threads());

	RTCScene scene;

#ifdef OCCLUSION
	rtcInit(NULL);

	_MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
	_MM_SET_DENORMALS_ZERO_MODE(_MM_DENORMALS_ZERO_ON);

	scene = rtcNewScene(RTC_SCENE_STATIC, RTC_INTERSECT1);

	unsigned int geomID = rtcNewTriangleMesh (scene, RTC_GEOMETRY_STATIC, num_inds, num_verts);

	rtcSetBuffer(scene, geomID, RTC_VERTEX_BUFFER, verts, 0, 3 * sizeof(float));
	rtcSetBuffer(scene, geomID, RTC_INDEX_BUFFER, inds, 0, 3 * sizeof(unsigned));

	rtcCommit(scene);

#endif

	gettimeofday(&t1, NULL);
	test_holo(&lights, &holo, angle_x, angle_y, &scene);
	gettimeofday(&t2, NULL);

	begin = t1.tv_sec * TIME_RESOLUTION + t1.tv_usec;
	end = t2.tv_sec * TIME_RESOLUTION + t2.tv_usec;

	printf("Time: %llu\n", end - begin);

	construct(holo, lights, filename);

#ifdef OCCLUSION
	free(verts);
	free(inds);

	rtcDeleteScene(scene);

	rtcExit();
#endif

	end_buff(&lights);
	free(holo.intensity);

	return 0;
}
