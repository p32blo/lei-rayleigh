#ifndef __CONVERT_H__
#define __CONVERT_H__

#include "holo.h"

void construct(holo_t holo, lights_t lights, char* filename);

#endif //__CONVERT_H__
