
#include <stdlib.h>
#include <stdio.h>

#include <vector>

#include "tinyobjloader/tiny_obj_loader.h"

#include "parser.h"

lights_t *init_buff (int size)
{
	lights_t *b = (lights_t*) calloc (1, sizeof(lights_t));

	if (size < 0)
		size = 1024;

	b->len = 0;
	b->size = size;

#ifdef SOA
	b->x = (double*) calloc(size, sizeof(double));
	b->y = (double*) calloc(size, sizeof(double));
	b->z = (double*) calloc(size, sizeof(double));
#else
	b->buff = (point_t*) calloc(size, sizeof(point_t));
#endif
	return b;
}


void set_point(point_t *p, double x, double y, double z)
{
	p->x = x;
	p->y = y;
	p->z = z;
}

void set_point_p(point_t *p, point_t pt)
{
	set_point(p, pt.x, pt.y ,pt.z);
}

point_t get_point_p(lights_t *l, int i)
{
	point_t p;
#ifdef SOA
	p.x = l->x[i];
	p.y = l->y[i];
	p.z = l->z[i];
#else
	p = l->buff[i];
#endif

	return p;
}

void add_point (lights_t *b, double x, double y, double z)
{
	if (b->len >= b->size) {
		b->size *= 2;
#ifdef SOA
		b->x = (double*) realloc(b->x, b->size * sizeof(double));
		b->y = (double*) realloc(b->y, b->size * sizeof(double));
		b->z = (double*) realloc(b->z, b->size * sizeof(double));
#else
		b->buff = (point_t*)realloc(b->buff, b->size * sizeof(point_t));
#endif
	}
#ifdef SOA
	b->x[b->len] = x;
	b->y[b->len] = y;
	b->z[b->len] = z;
#else
	set_point(&b->buff[b->len], x, y, z);
#endif
	b->len++;
}

void add_point_p(lights_t *b, point_t p)
{
	add_point(b, p.x, p.y, p.z);
}

void end_buff(lights_t *b) {
#ifdef SOA
	free(b->x);
	free(b->y);
	free(b->z);
#else
	free(b->buff);
#endif
}


void print_point(double x, double y, double z) {
	printf("(%f,%f,%f)\n", x, y, z);
}

void print_point_t(point_t p) {
	print_point(p.x, p.y, p.z);
}

std::vector<tinyobj::shape_t> parse_obj_vertices(char* filename)
{
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string err = tinyobj::LoadObj(shapes, materials, filename);

	if (!err.empty()) {
		fprintf(stderr, "%s\n", err.c_str());
		exit(1);
	}

	// assume single mesh - 0
	return shapes;
}

lights_t* init_buff_obj (char* filename, float *&verts, int &num_verts, unsigned *&indexs, int &num_index)
{
	auto shapes = parse_obj_vertices(filename);

	num_verts = num_index = 0;
	for (int i = 0; i < shapes.size(); ++i) {
		num_verts += shapes[i].mesh.positions.size();
		num_index += shapes[i].mesh.indices.size();
	}
#ifdef OCCLUSION
	verts = (float*) malloc ((num_verts + 1) * sizeof(float));
	indexs = (unsigned*) malloc ((num_index + 1) * sizeof(unsigned));
#endif

	num_verts /= 3;
	num_index /= 3;

	lights_t* b = init_buff(num_verts);

	int index_v = 0;
	int index_i = 0;
	for (int i = 0; i < shapes.size(); ++i) {

		auto pos = shapes[i].mesh.positions;
		auto ind = shapes[i].mesh.indices;

		for (int j = 0; j < pos.size(); j+=3) {
			point_t p = { pos[j], pos[j+1], pos[j+2] };
			add_point_p(b, p);
#ifdef OCCLUSION
			verts[index_v++] = p.x;
			verts[index_v++] = p.y;
			verts[index_v++] = p.z;
#endif
		}

#ifdef OCCLUSION
		for (int j = 0; j < ind.size(); ++j) {
			indexs[index_i++] = ind[j];
		}
#endif
	}
	return b;
}

float *get_buff_float(lights_t *l, int &len)
{
	float *verts = (float*) malloc ((3 * l->len + 1) * sizeof(float));

	int index = 0;
	for (int i = 0; i < l->len; ++i) {

		point_t p = get_point_p(l, i);

		verts[index++] = p.x;
		verts[index++] = p.y;
		verts[index++] = p.z;
	}
	len = l->len;
	return verts;
}

void print_point_t_buff(point_t *buff, int size)
{
	int i;
	for (i = 0; i < size; ++i) {
		print_point_t(buff[i]);
	}
}

void print_lights(lights_t *l)
{
#ifdef SOA
	int i;
	for (i = 0; i < l->len; ++i) {
		print_point(l->x[i], l->y[i], l->z[i]);
	}
#else
	print_point_t_buff(l->buff,l->len);
#endif
}


void usage (int argc, char *argv[])
{
	fprintf(stderr, "%s <filename>\n", argv[0]);
}

static int test_parser (int argc, char *argv[])
{
	if (argc != 2) {
		usage(argc, argv);
		exit(-1);
	}

	float* verts; int num_verts;
	unsigned* inds; int num_inds;
	lights_t* b = init_buff_obj(argv[1], verts, num_verts, inds, num_inds);

	print_lights(b);

	end_buff(b);
	free(b);

	return 0;
}
