#ifndef __PARSER_H__
#define __PARSER_H__

#include  <vector>

#include "holo.h"

lights_t *init_buff (int size);
lights_t* init_buff_obj (char* filename, float *&verts, int &num_verts, unsigned *&indexs, int &num_index);

void end_buff(lights_t *b);

void set_point(point_t *p, double x, double y, double z);
void set_point_p(point_t *p, point_t pt);

void add_point (lights_t *b, double x, double y, double z);
void add_point_p(lights_t *b, point_t p);

void print_point(double x, double y, double z);
void print_point_t(point_t p);
void print_point_t_buff(point_t *buff, int size);

void print_lights(lights_t *l);

float *get_buff_float(lights_t *l, int &len);

#endif // __PARSER_H__
