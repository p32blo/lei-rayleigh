#ifndef __HOLO_H__
#define __HOLO_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef struct sPoint {
	double x, y, z;
} point_t;

typedef struct sPointInt {
	int x, y;
} point_int_t;

typedef struct sLights {
	int size;
	int len;
#ifdef SOA
	double *x;
	double *y;
	double *z;
#else
	point_t *buff;
#endif
} lights_t;

typedef struct sHolo {
	point_int_t samples;
	point_t corner;

	int size;
	int len;
	int sampling;

	double *intensity;
} holo_t;


/* Unit Conversion Defenitions */

#define MM		*1.0e-3
#define UM		*1.0e-6
#define NM		*1.0e-9

#define PI		3.1415926535897932384626433832795
#define PI2		6.283185307179586476925286766559

#define DEG		*(PI/180.0)

#ifdef __cplusplus
}
#endif

#endif //__HOLO_H__
