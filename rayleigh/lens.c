/*
 * Application of the lens to the optical field
 */

#include "rayleighCompile.h"

/*
 * Apply an ideal convex lens that works exactly according the imaging equation:
 * 1/s + 1/v = 1/f
 * where s and v are object and image distances, f is focal distance
 *
 * Principle: point source on axis in the distance s shines on the lens plane; every point
 * is then illuminated with phase \phi(x,y) and becomes a new point source. We need these sources to
 * sum up so that the maximal constructive interference appears on the axis in the distance v
 * and forms an image of the original point source.
 * To do this, imagine that the image of the original light is a light too. In this case, it
 * illuminates points on lens plane with phases \theta(x,y). So, if the phase on lens plane
 * is exactly -\theta(x,y), constructive interference appears exactly as we want.
 * However, actual phase is \phi(x,y). All the lens have to do is to phase modify
 * incoming light by adding phase shift -\phi(x,y)-\theta(x,y).
 *
 * PARAMETERS
 * f (IN): focal length
 * v (IN): object distance (ie. lens is focused to the distance v)
 * image (INOUT): image in the lens plane
 * xmin, xmax, ymin, ymax (IN): an area where the circle with the lens has to appear
 *   if xmax, ymax == 0, it is assumed that they should reach the border of the image
 *   typical use may be xmin, xmax, ymin, ymax = 0
 * apodize (IN): 0 = do not apodize lens, 1 = do it
 */
int lensIdeal(double object_x, double object_y, double object_z,
			   double lens_center_x, double lens_center_y, double lens_center_z,
			   double image_x, double image_y, double image_z,
			   t_optfield *image,
			   double mask_center_x, double mask_center_y, double mask_radius,
			   int apodize)
{
	int index;
	int resolution_x, resolution_y;
	int index_x, index_y;
	t_point r_before, r_after, lens_point, image_corner;
	double samplingDistance;
	double r_before_final, r_before_z2, r_before_yz2;
	double r_after_final, r_after_z2, r_after_yz2;
	double mask_r_final, mask_x2, mask_y2;
	double phase_shift, central_phase_shift;
	fftw_complex *data;

	optfieldGetCorner(&image_corner.x, &image_corner.y, &image_corner.z, image);
	optfieldGetSamples(&resolution_x, &resolution_y, image);
	samplingDistance = optfieldGetSamplingdistance(image);
	data = (fftw_complex*) image->buffer->data;


	lens_point.z = image_corner.z - lens_center_z;

	r_before.z = lens_point.z - object_z;
	r_after.z = image_z - lens_point.z;

	r_before_z2 = r_before.z * r_before.z;
	r_after_z2  = r_after.z * r_after.z;

	central_phase_shift = sqrt(
		(object_x - lens_center_x)*(object_x - lens_center_x)+
		(object_y - lens_center_y)*(object_y - lens_center_y)+
		(object_z - lens_center_z)*(object_z - lens_center_z))
		+ sqrt(
		(image_x - lens_center_x)*(image_x - lens_center_x)+
		(image_y - lens_center_y)*(image_y - lens_center_y)+
		(image_z - lens_center_z)*(image_z - lens_center_z));
	central_phase_shift *= k_number;

	for (index_y=0, index=0; index_y<resolution_y; index_y++)
	{
		lens_point.y = index_y * samplingDistance + image_corner.y - lens_center_y;

		r_before.y = lens_point.y - object_y;
		r_after.y  = image_y - lens_point.y;

		r_before_yz2 = r_before_z2 + r_before.y * r_before.y;
		r_after_yz2  = r_after_z2  + r_after.y * r_after.y;

		mask_y2 = lens_point.y - mask_center_y;
		mask_y2 *= mask_y2;

		for (index_x=0; index_x<resolution_x; index_x++, index++)
		{
			lens_point.x = index_x * samplingDistance + image_corner.x - lens_center_x;

			mask_x2 = lens_point.x - mask_center_x;
			mask_x2 *= mask_x2;

			mask_r_final = sqrt(mask_x2 + mask_y2);

			if (mask_radius != 0 && mask_r_final > mask_radius)
			{
				data[index][RE] = 0.0;
				data[index][IM] = 0.0;
				continue;
			}

			r_before.x = lens_point.x - object_x;
			r_after.x  = image_y - lens_point.x;

			r_before_final = sqrt(r_before_yz2 + r_before.x * r_before.x);
			r_after_final  = sqrt(r_after_yz2  + r_after.x * r_after.x);

			//r_before_final = r_before_final - 2*PI*(int)(r_before_final / (2*PI)) ;
			//r_after_final  = r_after_final - 2*PI*(int)(r_after_final / (2*PI));

			phase_shift = - k_number * (r_after_final + r_before_final);

			//if (index_y == 512 && index_x%128 == 0) printf("%8.3f ", phase_shift+central_phase_shift);

			{
				double cc = cos(phase_shift);
				double ss = sin(phase_shift);
				double a = data[index][RE];
				double b = data[index][IM];
				double new_a = a*cc - b*ss;
				double new_b = a*ss + b*cc;
				double apod_value;

				if (mask_radius != 0 && apodize == LENS_APODIZE)
				{
					apod_value = cos(mask_r_final / mask_radius * PI / 2);
					new_a *= apod_value;
					new_b *= apod_value;
				}

				data[index][RE] = new_a;
				data[index][IM] = new_b;
			}

		}

	}

	return RAYLEIGH_OK;
}


/*
 * Apply a thick (aspherical) lens phase distortion.
 * image: image to apply the distortion
 * tc: lens "thickness", or rather "thickness" on the z axis
 * innerPropagation: LENS_THICK_SIMPLE - calculates phase distortions on the first and the second surface only
 *                   LENS_THICK_ADDCONSTANT - adds a phase shift proportional to tc and n (see below)
 * r1: radius of the first surface; positive means the curvature center is to the right of the lens
 * r1_a4, r1_a6, r1_a8, r1_a10, r1_conic: aspheric coefficients; all zeros indicate spherical surface
 * r2, r2_a4, ..., r2_a10, r2_conic: dtto for the second surface
 * n: index of refraction of the lens material
 * lensCenterX, lensCenterY: XY coordinates of the lens center
 * maskCenterX, maskCenterY: XY coordinates of the lens mask (aperture) center
 * maskRadius: aperture radius
 * apodize: LENS_NO_APODIZE - hard clip (lens unmasked/lens masked)
 *          LENS_APODIZE - gradual change between masked and unmasked
 */
int lensThickAspheric(t_optfield *image,
					  int innerPropagation,
					  double tc,
					  double r1, double r1_a4, double r1_a6, double r1_a8, double r1_a10, double r1_conic,
					  double r2, double r2_a4, double r2_a6, double r2_a8, double r2_a10, double r2_conic,
					  double n,
					  double lensCenterX, double lensCenterY,
					  double maskCenterX, double maskCenterY, double maskRadius,
					  int apodize)
{
	int index;
	int resolution_x, resolution_y;
	int index_x, index_y;
	t_point image_corner;
	double lensPointX, lensPointY, lensPointY2, lensRadial2, lensRadial4;
	double samplingDistance;
	double mask_r_final, mask_x2, mask_y2;
	fftw_complex *data;
	double r1Curvature, r2Curvature;
	double phaseShift;
	double r1_conic_1, r2_conic_1;
	double tmp;

	/* set up temporary coefficients */
	/* curvatures of the first and the second surface */
	r1Curvature = 1.0 / r1;
	r2Curvature = 1.0 / r2;

	/* in calculations, (conic+1) is actually needed */
	r1_conic_1 = r1_conic + 1.0;
	r2_conic_1 = r2_conic + 1.0;

	/* aspheric coefficients are usually given in mm-based units
	 * a4 in mm^-3, a6 in mm^-5, a8 in mm^-7, ...
	 * in this software, lenghts are measured in meters, therefore
	 * conversion is necessary
	 */
	r1_a4  *= 1e9;
	r1_a6  *= 1e15;
	r1_a8  *= 1e21;
	r1_a10 *= 1e27;

	r2_a4  *= 1e9;
	r2_a6  *= 1e15;
	r2_a8  *= 1e21;
	r2_a10 *= 1e27;

	/* prepare common lengths etc. necesarry in computation */
	optfieldGetCorner(&image_corner.x, &image_corner.y, &image_corner.z, image);
	optfieldGetSamples(&resolution_x, &resolution_y, image);
	samplingDistance = optfieldGetSamplingdistance(image);
	data = (fftw_complex*) image->buffer->data;

	for (index_y=0, index=0; index_y<resolution_y; index_y++)
	{
		lensPointY = index_y * samplingDistance + image_corner.y - lensCenterY;
		lensPointY2 = lensPointY * lensPointY;

		mask_y2 = lensPointY - maskCenterY;
		mask_y2 *= mask_y2;

		for (index_x=0; index_x<resolution_x; index_x++, index++)
		{
			lensPointX = index_x * samplingDistance + image_corner.x - lensCenterX;
			lensRadial2 = lensPointX*lensPointX + lensPointY2;
			lensRadial4 = lensRadial2*lensRadial2;

			mask_x2 = lensPointX - maskCenterX;
			mask_x2 *= mask_x2;

			mask_r_final = sqrt(mask_x2 + mask_y2);

			if (maskRadius != 0 && mask_r_final > maskRadius)
			{
				data[index][RE] = 0.0;
				data[index][IM] = 0.0;
				continue;
			}

			tmp = r1Curvature * lensRadial2;
			tmp =  tmp / (1.0 + sqrt(1.0 - r1_conic_1*r1Curvature*tmp));
			tmp	+=(r1_a4 +
					 (r1_a6 +
					   (r1_a8 + r1_a10*lensRadial2)*lensRadial2
					 )*lensRadial2
				   )*lensRadial4;
			phaseShift = tmp;

			tmp = r2Curvature * lensRadial2;
			tmp =  tmp / (1.0 + sqrt(1.0 - r2_conic_1*r2Curvature*tmp)) +
				   (r2_a4 +
					 (r2_a6 +
					   (r2_a8 + r2_a10*lensRadial2)*lensRadial2
					 )*lensRadial2
				   )*lensRadial4;

			phaseShift -= tmp;

			//if (innerPropagation == LENS_THICK_ADDCONSTANT) phaseShift += tc;

			phaseShift *= (1.0 - n)*k_number;
			//phaseShift = -phaseShift;
			//if (index_y == 512 && index_x%128 == 0) printf("%8.3f ", phaseShift);

			{
				double cc = cos(phaseShift);
				double ss = sin(phaseShift);
				double a = data[index][RE];
				double b = data[index][IM];
				double new_a = a*cc - b*ss;
				double new_b = a*ss + b*cc;
				double apod_value;

				if (maskRadius != 0 && apodize == LENS_APODIZE)
				{
					apod_value = cos(mask_r_final / maskRadius * PI / 2);
					new_a *= apod_value;
					new_b *= apod_value;
				}

				data[index][RE] = new_a;
				data[index][IM] = new_b;
			}

//				data[index][RE] = phaseShift;
//				data[index][IM] = 0;

		}

	}

	return RAYLEIGH_OK;
}


/*
 * Apply a thick (aspherical) lens doublet phase distortion.
 * image: image to apply the distortion
 * tc: lens "thickness", or rather "thickness" on the z axis
 * innerPropagation: LENS_THICK_SIMPLE - calculates phase distortions on the first and the second surface only
 *                   LENS_THICK_ADDCONSTANT - adds a phase shift proportional to tc and n (see below)
 * r1: radius of the first surface; positive means the curvature center is to the right of the lens
 * r1_a4, r1_a6, r1_a8, r1_a10, r1_conic: aspheric coefficients; all zeros indicate spherical surface
 * r2, r2_a4, ..., r2_a10, r2_conic: dtto for the second surface
 * n: index of refraction of the lens material
 * lensCenterX, lensCenterY: XY coordinates of the lens center
 * maskCenterX, maskCenterY: XY coordinates of the lens mask (aperture) center
 * maskRadius: aperture radius
 * apodize: LENS_NO_APODIZE - hard clip (lens unmasked/lens masked)
 *          LENS_APODIZE - gradual change between masked and unmasked
 */
int lensThickAsphericDoublet(t_optfield *image,
					  int innerPropagation,
					  double tc1, double tc2,
					  double r1, double r1_a4, double r1_a6, double r1_a8, double r1_a10, double r1_conic,
					  double r2, double r2_a4, double r2_a6, double r2_a8, double r2_a10, double r2_conic,
					  double r3, double r3_a4, double r3_a6, double r3_a8, double r3_a10, double r3_conic,
					  double n1, double n2,
					  double lensCenterX, double lensCenterY,
					  double maskCenterX, double maskCenterY, double maskRadius,
					  int apodize)
{
	int index;
	int resolution_x, resolution_y;
	int index_x, index_y;
	t_point image_corner;
	double lensPointX, lensPointY, lensPointY2, lensRadial2, lensRadial4;
	double samplingDistance;
	double mask_r_final, mask_x2, mask_y2;
	fftw_complex *data;
	double r1Curvature, r2Curvature, r3Curvature;
	double phaseShift;
	double r1_conic_1, r2_conic_1, r3_conic_1;
	double tmp;

	/* set up temporary coefficients */
	/* curvatures of the first and the second surface */
	r1Curvature = 1.0 / r1;
	r2Curvature = 1.0 / r2;
	r3Curvature = 1.0 / r3;

	/* in calculations, (conic+1) is actually needed */
	r1_conic_1 = r1_conic + 1.0;
	r2_conic_1 = r2_conic + 1.0;
	r3_conic_1 = r3_conic + 1.0;

	/* aspheric coefficients are usually given in mm-based units
	 * a4 in mm^-3, a6 in mm^-5, a8 in mm^-7, ...
	 * in this software, lenghts are measured in meters, therefore
	 * conversion is necessary
	 */
	r1_a4  *= 1e9;
	r1_a6  *= 1e15;
	r1_a8  *= 1e21;
	r1_a10 *= 1e27;

	r2_a4  *= 1e9;
	r2_a6  *= 1e15;
	r2_a8  *= 1e21;
	r2_a10 *= 1e27;

	r3_a4  *= 1e9;
	r3_a6  *= 1e15;
	r3_a8  *= 1e21;
	r3_a10 *= 1e27;

	/* prepare common lengths etc. necesarry in computation */
	optfieldGetCorner(&image_corner.x, &image_corner.y, &image_corner.z, image);
	optfieldGetSamples(&resolution_x, &resolution_y, image);
	samplingDistance = optfieldGetSamplingdistance(image);
	data = (fftw_complex*) image->buffer->data;

	for (index_y=0, index=0; index_y<resolution_y; index_y++)
	{
		lensPointY = index_y * samplingDistance + image_corner.y - lensCenterY;
		lensPointY2 = lensPointY * lensPointY;

		mask_y2 = lensPointY - maskCenterY;
		mask_y2 *= mask_y2;

		for (index_x=0; index_x<resolution_x; index_x++, index++)
		{
			lensPointX = index_x * samplingDistance + image_corner.x - lensCenterX;
			lensRadial2 = lensPointX*lensPointX + lensPointY2;
			lensRadial4 = lensRadial2*lensRadial2;

			mask_x2 = lensPointX - maskCenterX;
			mask_x2 *= mask_x2;

			mask_r_final = sqrt(mask_x2 + mask_y2);

			if (maskRadius != 0 && mask_r_final > maskRadius)
			{
				data[index][RE] = 0.0;
				data[index][IM] = 0.0;
				continue;
			}

			tmp = r1Curvature * lensRadial2;
			tmp =  tmp / (1.0 + sqrt(1.0 - r1_conic_1*r1Curvature*tmp));
			tmp	+=(r1_a4 +
					 (r1_a6 +
					   (r1_a8 + r1_a10*lensRadial2)*lensRadial2
					 )*lensRadial2
				   )*lensRadial4;
			tmp *= (1.0 - n1)*k_number;
			phaseShift = tmp;

			tmp = r2Curvature * lensRadial2;
			tmp =  tmp / (1.0 + sqrt(1.0 - r2_conic_1*r2Curvature*tmp)) +
				   (r2_a4 +
					 (r2_a6 +
					   (r2_a8 + r2_a10*lensRadial2)*lensRadial2
					 )*lensRadial2
				   )*lensRadial4;
			tmp *= (n1 - n2)*k_number;
			phaseShift += tmp;

			tmp = r3Curvature * lensRadial2;
			tmp =  tmp / (1.0 + sqrt(1.0 - r3_conic_1*r3Curvature*tmp)) +
				   (r3_a4 +
					 (r3_a6 +
					   (r3_a8 + r3_a10*lensRadial2)*lensRadial2
					 )*lensRadial2
				   )*lensRadial4;
			tmp *= (n2 - 1.0)*k_number;
			phaseShift += tmp;

			//if (innerPropagation == LENS_THICK_ADDCONSTANT) phaseShift += tc;

			//phaseShift = -phaseShift;
			//if (index_y == 512 && index_x%128 == 0) printf("%8.3f ", phaseShift);

			{
				double cc = cos(phaseShift);
				double ss = sin(phaseShift);
				double a = data[index][RE];
				double b = data[index][IM];
				double new_a = a*cc - b*ss;
				double new_b = a*ss + b*cc;
				double apod_value;

				if (maskRadius != 0 && apodize == LENS_APODIZE)
				{
					apod_value = cos(mask_r_final / maskRadius * PI / 2);
					new_a *= apod_value;
					new_b *= apod_value;
				}

				data[index][RE] = new_a;
				data[index][IM] = new_b;
			}

//				data[index][RE] = phaseShift;
//				data[index][IM] = 0;

		}

	}

	return RAYLEIGH_OK;
}


void lens_test_ideal(void)
{
	t_optfield *image = NULL, *screen = NULL;
	double z1, z2;

	z1 = -10000;
	z2 = 20 MM;

	optfieldNew(0, 0, 0, 1024, 1024, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &image);
	optfieldNew(0, 0, z2, 4096, 4096, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &screen);
	optfieldLightSimple(image, 10000, 1e6, 0, 0, 0, 0, 1);
	optfieldSavePNG(image, "simple_input", PictureIORealImagIntensity, 1.0, 0, NULL);
	lensIdeal(0, 0, z1,
			 0, 0, 0,
			 0, 0, z2,
			 image,
			 0, 0,
			 0, LENS_NO_APODIZE);
	optfieldSavePNG(image, "simple_output", PictureIORealImagIntensity, 1.0, 0, NULL);
	optfieldPropagate(image, screen, NULL, NULL, PropagationRayleighSommerfeld, VariablePrecisionOn, 0.2, FilterTypeTriangular, NULL, NULL);
	optfieldSavePNG(screen, "simple_screen", PictureIORealImagIntensity, 1.0, 0, NULL);

}





void lens_test_aspheric(void)
{
	t_optfield *image = NULL, *screen = NULL;
	double tc, n; // on-axis thickness, index of refraction
	double r1, r1_a4=0, r1_a6=0, r1_a8=0, r1_a10=0, r1_conic=0; // first surface
	double r2, r2_a4=0, r2_a6=0, r2_a8=0, r2_a10=0, r2_conic=0; // second surface
	double efl, bfl, tfl; // effective - back - theoretical focal length
	double f;
	char outfilename[100];

	/* Newport KPA10 aspheric planoconvex lens */
	r1 = 13.255 MM;
	/*
	r1_a4 = +9.557E-5;
	r1_a6 = -2.610E-7;
	r1_a8 = +1.125E-9;
	r1_a10 = -2.999E-12;
	r1_conic = -2.364;
	*/
	r2 = HUGE_VAL;
	tc = 4 MM;
	n = 1.59466;    /* for lambda=500 nm */
	efl = 22.5 MM;	/* effective focal length */
	bfl = 19.98 MM; /* back focal length */


	/* calculated focal length for spheric lens */
	tfl = 1.0 /
		((n-1.0)*(1/r1 - 1/r2)+(n-1.0)*tc/(n*r1*r2));

	optfieldNew(0, 0, 0, 7*1024, 7*1024, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &image);
	optfieldNew(0, 0, 0, 1024, 1024, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &screen);

	printf("spheric: f=%f\naspheric: f=%f\n\n", tfl, efl);
	optfieldLightSimple(image, 10000, 1e6, 0, 0, 0, 0, 1);
	optfieldLightSimple(image, 10000, 1e6, 0, 0, -0.4/20.0, 0, 1);
	optfieldLightSimple(image, 10000, 1e6, 0, 0, 0, -0.3/20.0, 1);
	optfieldLightSimple(image, 10000, 1e6, 0, 0, 0.2/20.0, 0, 1);
	optfieldLightSimple(image, 10000, 1e6, 0, 0, 0, 0.1/20.0, 1);
	//optfieldSavePNG(image, "KPA10_input", PictureIORealImagIntensity, 1.0, 0, NULL);
	lensThickAspheric(image,
		LENS_THICK_ADDCONSTANT, tc,
		r1, r1_a4, r1_a6, r1_a8, r1_a10, r1_conic,
		r2, r2_a4, r2_a6, r2_a8, r2_a10, r2_conic,
		n,
		0, 0,
		0, 0,
		(7.15/2) MM, LENS_NO_APODIZE);

	//optfieldSavePNG(image, "KPA10s_output", PictureIORealImagIntensity, 1.0, 0, NULL);

	optfieldSetCenter(0, 0, tfl, screen);
	optfieldZeroData(screen);
	optfieldPropagate(image, screen, NULL, NULL, PropagationRayleighSommerfeld, VariablePrecisionOn, 0.2, FilterTypeTriangular, NULL, NULL);
	sprintf(outfilename, "KPA10s_screen_%04d", 100+(int)((tfl-efl)*10000));
	optfieldSavePNG(screen, outfilename, PictureIOIntensity, 2.2, 0, NULL);

	for (f=-1.0 MM; f<1 MM; f+=0.5e-3)
	{
		optfieldSetCenter(0, 0, efl + f, screen);
		optfieldZeroData(screen);
		optfieldPropagate(image, screen, NULL, NULL, PropagationRayleighSommerfeld, VariablePrecisionOn, 0.2, FilterTypeTriangular, NULL, NULL);
		sprintf(outfilename, "KPA10s_screen_%04d", 100+(int)(f*10000));
		optfieldSavePNG(screen, outfilename, PictureIOIntensity, 2.2, 0, NULL);
	}

	optfieldDispose(&screen);
	optfieldDispose(&image);
}


void lens_test_asphericDoublet(void)
{
	t_optfield *image = NULL, *screen = NULL;
	double tc1, n1; // on-axis thickness, index of refraction of the first lens
	double tc2, n2; // on-axis thickness, index of refraction of the second lens
	double r1, r1_a4=0, r1_a6=0, r1_a8=0, r1_a10=0, r1_conic=0; // first surface
	double r2, r2_a4=0, r2_a6=0, r2_a8=0, r2_a10=0, r2_conic=0; // second surface
	double r3, r3_a4=0, r3_a6=0, r3_a8=0, r3_a10=0, r3_conic=0; // third surface
	double efl, bfl, tfl; // effective - back - theoretical focal length
	double f;
	char outfilename[100];

	/* Newport APAC10 achromatic doublet lens */
	r1 = 7.56 MM;
	r2 = -12.8 MM;
	r3 = -61.6 MM;
	/*
	r3_a4 = +7.019E-4;
	r3_a6 = -9.193E-6;
	r3_a8 = 0;
	r3_a10 = 0;
	r3_conic = 0;
	*/
	tc1 = 4.5 MM;
	tc2 = 1.5 MM;
	n1 = 1.718;     /* N-LaK8 for lambda=532 nm */
	n2 = 1.851;     /* SF57   for lambda=532 nm */
	efl = 12.0 MM;	/* effective focal length */
	bfl = 8.16 MM;  /* back focal length */


	/* calculated focal length for spheric lens doublet
	 * 1/f = 1/f1 + 1/f2
	 * where f1 and f2 are focal lenghts of the lenses
	 */

	/* 1/f1 */
	tfl  = (n1-1.0)*(1/r1 - 1/r2)+(n1-1.0)*tc1/(n1*r1*r2);
	/* 1/f2 */
	tfl += (n2-1.0)*(1/r2 - 1/r3)+(n2-1.0)*tc2/(n2*r2*r3);
	/* inverse */
	tfl = 1/tfl;

	optfieldNew(0, 0, 0, 7*1024, 7*1024, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &image);
	optfieldNew(0, 0, 0, 1024, 1024, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &screen);

	printf("theoretic: f=%f\ncatalogue: f=%f\n\n", tfl, efl);
	optfieldLightSimple(image, 10000, 1e6, 0, 0, 0, 0, 1);
	optfieldLightSimple(image, 10000, 1e6, 0, 0, -0.20/10.0, 0, 1);
	optfieldLightSimple(image, 10000, 1e6, 0, 0, 0, -0.15/10.0, 1);
	optfieldLightSimple(image, 10000, 1e6, 0, 0, 0.10/10.0, 0, 1);
	optfieldLightSimple(image, 10000, 1e6, 0, 0, 0, 0.05/10.0, 1);
	//optfieldSavePNG(image, "APAC10_input", PictureIORealImagIntensity, 1.0, 0, NULL);
	lensThickAsphericDoublet(image,
		LENS_THICK_ADDCONSTANT,
		tc1, tc2,
		r1, r1_a4, r1_a6, r1_a8, r1_a10, r1_conic,
		r2, r2_a4, r2_a6, r2_a8, r2_a10, r2_conic,
		r3, r3_a4, r3_a6, r3_a8, r3_a10, r3_conic,
		n1, n2,
		0, 0,
		0, 0,
		(7.15/2) MM, LENS_NO_APODIZE);

	//optfieldSavePNG(image, "APAC10_output", PictureIORealImagIntensity, 1.0, 0, NULL);
/*
	optfieldSetCenter(0, 0, tfl, screen);
	optfieldZeroData(screen);
	optfieldPropagate(image, screen, NULL, NULL, PropagationRayleighSommerfeld, VariablePrecisionOn, 0.2, FilterTypeTriangular, NULL, NULL);
	sprintf(outfilename, "APAC10_screen_%05d", (int)(tfl TOUM));
	optfieldSavePNG(screen, outfilename, PictureIOIntensity, 2.2, 0, NULL);
*/

	efl = 10.2 MM;
	for (f=-0.3 MM; f<-0.1 MM; f+=0.1e-3)
	{
		optfieldSetCenter(0, 0, efl + f, screen);
		optfieldZeroData(screen);
		optfieldPropagate(image, screen, NULL, NULL, PropagationRayleighSommerfeld, VariablePrecisionOn, 0.2, FilterTypeTriangular, NULL, NULL);
		sprintf(outfilename, "APAC10s_screen_%05d", (int)((efl + f) TOUM + 0.5));
		optfieldSavePNG(screen, outfilename, PictureIOIntensity, 2.2, 0, NULL);
	}

	optfieldDispose(&screen);
	optfieldDispose(&image);
}
