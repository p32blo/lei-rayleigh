/*
 * Optfield -> optfield propagation.
 *
 * Simple methods that do not use propagation plan,
 * i.e. optfields are not splitted for the calculation to fit
 * to the memory. Either you have a lot of memory, or you don't :)
 */
#ifndef __OPTFIELDPROPAGATESIMPLE_H
#define __OPTFIELDPROPAGATESIMPLE_H

#include "rayleigh.h"

/* When propagating, large optical filed may be split to
 * several pieces. You can define if all the propagations
 * between pieces should share the same precision of kernel,
 * or this precision shoud be derived from the particular
 * pieces geometry.
 */

typedef enum {
	VariablePrecisionOff = 0,
	VariablePrecisionOn = 1
} t_PropagationPrecision;

/* Propagation kernel can be evalueted in several regimes:
 * - full Rayleigh-Sommerfeld kernel:        FT((jk-1/r) z exp(jkr)/r^2)
 * - simplified Rayleigh-Sommerfeld kernel:  FT((jk) z exp(jkr)/r^2)
 * - angular spectrum decomposition:         exp(j2pi z sqrt(1/lambda^2 - fx^2 - fy^2))
 * - Fresnel kernel
 * - etc.
 *
 * Please note that only several methods are implemented now.
 */
typedef enum {
	PropagationRayleighSommerfeld = 0,
	PropagationAngular = 1
} t_PropagationMethod;

RAYLEIGH_EXPORT char* propagationMethodName[];

RAYLEIGH_EXPORT char *_debug_prefix;
RAYLEIGH_EXPORT char *_debug_postfix;
RAYLEIGH_EXPORT char _debug_filename[];

/*
 * Propagate source to target in one pass, no propagation plan
 */
RAYLEIGH_EXPORT int optfieldPropagateSimple(t_optfield *source, t_optfield *target,
				  double propagationPrecision, t_FilterType filterType, void *filterTypeData,
				  t_PropagationMethod propagationMethod, t_PropagationAcceleration propagationAcceleration,
				  t_rhoLUT **externRhoLUT, int rhoLUTFilterType, int rhoLUTsubsampling, double rhoLUTPrecision,
				  t_waveLUT **externWaveLUT, int waveLUTFilterType, int waveLUToversampling);

/*
 * Propagate source to target
 */
RAYLEIGH_EXPORT int optfieldPropagate(t_optfield *source, t_optfield *target,
					   t_buffer *buffer1, t_buffer *buffer2,
					   t_PropagationMethod propagationMethod,
					   t_PropagationPrecision variablePrecision, double propagationPrecision,
					   t_FilterType filterType, void *filterTypeData,
					   t_propagPlan **provided_propag_list);

RAYLEIGH_EXPORT int optfieldFindLargestShift(t_optfield *source, t_optfield *target, t_point *largestShift);

#endif
