#include "rayleighCompile.h"

#define MAX_PIECES	16

/* Various internal functions.
 */
void computeSamplingNumbers(int orig_samples, int samplingcount, int *big_samples, int *big_count, int *small_samples, int *small_count);
int findBestDataPartition(int lights_offset_x, int lights_offset_y,
			int lights_step_x, int lights_step_y,
			int lights_resolution_x, int lights_resolution_y,
			int lights_count_x, int lights_count_y,
			int screen_offset_x, int screen_offset_y,
			int screen_step_x, int screen_step_y,
			int screen_resolution_x, int screen_resolution_y,
			int screen_count_x, int screen_count_y,
			RAYLEIGH_INT64 maximum_FFT_size,
			t_propagPlan **propag_list);
void calculate_counts_resolutions(int lights_resolution_x, int lights_resolution_y,
	int screen_resolution_x, int screen_resolution_y,
	int piecesD_lights_count_x, int piecesD_lights_count_y,
	int piecesD_screen_count_x, int piecesD_screen_count_y,
	int *piecesD_lights_bsresolution_x, int *piecesD_lights_bsresolution_y,
	int *piecesD_screen_bsresolution_x, int *piecesD_screen_bsresolution_y,
	int *piecesD_lights_bscount_x, int *piecesD_lights_bscount_y,
	int *piecesD_screen_bscount_x, int *piecesD_screen_bscount_y);
int estimateOperations(int sourceSamples_x, int sourceSamples_y,
					   int targetSamples_x, int targetSamples_y,
					   int sourcePieces_x, int sourcePieces_y,
					   int targetPieces_x, int targetPieces_y,
					   RAYLEIGH_INT64 maximum_FFT_size,
					   double *total_operations);

/*
 * When splitting an array, "big" and "small" pieces arise. For simplicity, we will store counts and resolutions
 * of both in an array if size 2; these constants just tell what index is for big/small piece.
 */
#define BIG	0
#define SMALL 1

/*
 * add a piece to a propagation plan
 */
int propagPlanAdd(t_propagPlan **propagPlan,
	int source_start_x, int source_start_y,
	int source_step_x, int source_step_y,
	int source_samples_x, int source_samples_y,
	int target_start_x, int target_start_y,
	int target_step_x, int target_step_y,
	int target_samples_x, int target_samples_y,
	int convolution_samples_x, int convolution_samples_y)
{
	t_propagPlan *piece;

	if (propagPlan == NULL)
		return RAYLEIGH_ERROR;

	if ((piece = (t_propagPlan*) malloc(sizeof(t_propagPlan))) == NULL)
		return RAYLEIGH_ERROR;

	propagPieceSet(piece,
		source_start_x, source_start_y,
		source_step_x, source_step_y,
		source_samples_x, source_samples_y,
		target_start_x, target_start_y,
		target_step_x, target_step_y,
		target_samples_x, target_samples_y,
		convolution_samples_x, convolution_samples_y,
		*propagPlan);

	*propagPlan = piece;

	return RAYLEIGH_OK;
}


/*
 * set a piece in a propagation plan
 */
int propagPieceSet(t_propagPlan *piece,
	int source_start_x, int source_start_y,
	int source_step_x, int source_step_y,
	int source_samples_x, int source_samples_y,
	int target_start_x, int target_start_y,
	int target_step_x, int target_step_y,
	int target_samples_x, int target_samples_y,
	int convolution_samples_x, int convolution_samples_y,
	t_propagPlan *pieceNext)
{
	INITEXCEPTION;

	if (piece == NULL)
		GOEXCEPTION0("NULL piece pointer.\n");

	piece->convolutionSamples_x = convolution_samples_x;
	piece->convolutionSamples_y = convolution_samples_y;

	piece->sourceSamples_x = source_samples_x;
	piece->sourceSamples_y = source_samples_y;
	piece->sourceStart_x = source_start_x;
	piece->sourceStart_y = source_start_y;
	piece->sourceStep_x = source_step_x;
	piece->sourceStep_y = source_step_y;

	piece->targetSamples_x = target_samples_x;
	piece->targetSamples_y = target_samples_y;
	piece->targetStart_x = target_start_x;
	piece->targetStart_y = target_start_y;
	piece->targetStep_x = target_step_x;
	piece->targetStep_y = target_step_y;

	piece->next = pieceNext;

EXCEPTION0("propagPieceSet");

ENDEXCEPTION;
}


/*
 * free propagation plan
 */
int propagPlanDispose(t_propagPlan **propagPlan)
{
	t_propagPlan *piece1, *piece2;

	if (propagPlan == NULL)
		return RAYLEIGH_OK;

	piece1 = *propagPlan;
	while (piece1 != NULL)
	{
		piece2 = piece1->next;
		free(piece1);
		piece1 = piece2;
	}

	*propagPlan = NULL;
	return RAYLEIGH_OK;
}


/*
 * find largest samples count in x and y directions
 * find largest buffer in term of total samples number
 */
int propagPlanFindLargestBuffers(t_propagPlan *propagPlan,
								 int *maxConvolutionSamples_x, int *maxConvolutionSamples_y,
								 RAYLEIGH_INT64 *maxConvolutionSamples_xy)
{
	RAYLEIGH_INT64 tmp;

	if (propagPlan == NULL) return RAYLEIGH_ERROR;

	*maxConvolutionSamples_x = propagPlan->convolutionSamples_x;
	*maxConvolutionSamples_y = propagPlan->convolutionSamples_y;
	*maxConvolutionSamples_xy = (RAYLEIGH_INT64)(*maxConvolutionSamples_x) * (RAYLEIGH_INT64)(*maxConvolutionSamples_y);

	while (propagPlan->next != NULL)
	{
		propagPlan = propagPlan->next;
		if (*maxConvolutionSamples_x < propagPlan->convolutionSamples_x)
			*maxConvolutionSamples_x = propagPlan->convolutionSamples_x;
		if (*maxConvolutionSamples_y < propagPlan->convolutionSamples_y)
			*maxConvolutionSamples_y = propagPlan->convolutionSamples_y;
		tmp = (RAYLEIGH_INT64)propagPlan->convolutionSamples_x * (RAYLEIGH_INT64)propagPlan->convolutionSamples_y;
		if (*maxConvolutionSamples_xy < tmp)
			*maxConvolutionSamples_xy = tmp;
	}

	return RAYLEIGH_OK;
}


/*
 * count number of propagation operations on propagPlan
 */
int propagPlanPropagationCount(t_propagPlan *propagPlan)
{
	int i = 0;
	t_propagPlan *piece = propagPlan;

	while (piece != NULL)
	{
		i++;
		piece = piece->next;
	}

	return i;
}

/*
 * creates propagation plan source->target using buffers of limited size
 */
int propagPlanNew(t_optfield *source, t_optfield *target, RAYLEIGH_INT64 maximumFFTByteSize, t_propagPlan **propagPlan)
{
	/* these need a lot of explanation:
	 * Due to sampling issues, source or target data splits into interleaved arrays.
	 * Let's talk about source only, the same applied for target.
	 * If in luck, all of these interleaved arrays will share the same samples count.
	 * If not, some of them will have samples count R ("small"), the rest R+1 ("big").
	 * This is the meaning of samplingPiece.
	 * Concerning samplingPiece only: due to memory constraints, it can be split to several big dataPieces and
	 * 0 or 1 small dataPieces. They have their own samples counts again.
	 */
	t_piecesInfo sourceInfo, targetInfo;
	int commonSamples_x, commonSamples_y;
	int sourceSamples_x, sourceSamples_y;
	int targetSamples_x, targetSamples_y;
	int i;
	int sourcePieceOffset_x, sourcePieceSamples_x, sourcePieceCount_x;
	int sourcePieceOffset_y, sourcePieceSamples_y, sourcePieceCount_y;
	int targetPieceOffset_x, targetPieceSamples_x, targetPieceCount_x;
	int targetPieceOffset_y, targetPieceSamples_y, targetPieceCount_y;

	*propagPlan = NULL;

	CHECK(optfieldGetSamples(&sourceSamples_x, &sourceSamples_y, source));
	CHECK(optfieldGetSamples(&targetSamples_x, &targetSamples_y, target));

	/*
	 * source and target have sampling in some (simple) ratio
	 * therefore,  (sourceInfo.samplesCount_x) sampling distances in source
	 * is equal to (targetInfo.samplesCount_x) sampling distances in target
	 *
	 * example: sampling distance of source = 1 um, sampling distance of target = 0.5 um
	 *          => sourceInfo.samplesCount_x = 1, targetInfo.samplesCount_x = 2
	 *
	 * as the sampling distances are integer multiples of common_sampling_distance, to calculate
	 * these numbers is easy:
	 */

	sourceInfo.samplingCount_x = optfieldGetSampling(target);
	targetInfo.samplingCount_x = optfieldGetSampling(source);

	/*
	 * e.g., for previous example source->sampling = 2, target->sampling = 1,
	 * therefore sourceInfo.samplesCount_x has to be equal to target->sampling
	 *
	 * of course, we should divide both with the greatest common divisor
	 */

	{
		int d;
		d = gcd(sourceInfo.samplingCount_x, targetInfo.samplingCount_x);
		targetInfo.samplingCount_x /= d;
		sourceInfo.samplingCount_x /= d;
	}

	/*
	 * currently, sampling is the same in x and y directions
	 * TODO: allow different
	 */

	targetInfo.samplingCount_y = targetInfo.samplingCount_x;
	sourceInfo.samplingCount_y = sourceInfo.samplingCount_x;

	/* We can imagine that different sampling leads to splitting of arrays into interleaved pieces with
	 * the same sampling; number of these pieces is given by sourceInfo.samplesCount_x etc.
	 * Samples counts of these pieces is stored in sourceInfo.samplingPiece_big.samples_x and targetInfo.samplingPiece_small.samples_x
	 * Let's explain it more precisely.
	 *
	 * Example as above, samples count of source is 3x3 samples, samples count of target is 6x6 samples:
	 * source: . . .      target: a b a b a b  (ie. target is split to pieces a, b, c, d with sampling distance 1 um)
	 *         . . .              c d c d c d
	 *		   . . .              a b a b a b
	 *		                      c d c d c d
	 *                            a b a b a b
	 *							  c d c d c d
	 *
	 * However, imagine that last row and last column of the target array are missing; therefore, still four arrays
	 * are interleaved, but they have different samples counts, big and small.
	 * We need to keep in mind how many big pieces do we have (count of small is just complete count minus big count).
	 * For simplicity, we will remember both big and small samples count,
	 * although samples count of small is just samples count of big - 1.
	 */


	/*
	 * imagine target.samplesCount_x = 4, ie. array of resolution eg. 19 is split to four interleaved arrays a, b, c, d:
	 * a b c d a b c d a b c d a b c d a b c
	 * It is easy to see that there are 19 % 4 = 3 big arrays (a, b, c) of resolution 19/4 + 1 = 5,
	 * and 4 - 19 % 4 = 1 small arrays (d) of resolution 19/4 = 4.
	 * If in luck, there won't be any big arrays.
	 */
	computeSamplingNumbers(sourceSamples_x, sourceInfo.samplingCount_x,
		&sourceInfo.samplingPiece_big.samples_x,
		&sourceInfo.samplingPiece_big.count_x,
		&sourceInfo.samplingPiece_small.samples_x,
		&sourceInfo.samplingPiece_small.count_x);
	computeSamplingNumbers(sourceSamples_y, sourceInfo.samplingCount_y,
		&sourceInfo.samplingPiece_big.samples_y,
		&sourceInfo.samplingPiece_big.count_y,
		&sourceInfo.samplingPiece_small.samples_y,
		&sourceInfo.samplingPiece_small.count_y);
	computeSamplingNumbers(targetSamples_x, targetInfo.samplingCount_x,
		&targetInfo.samplingPiece_big.samples_x,
		&targetInfo.samplingPiece_big.count_x,
		&targetInfo.samplingPiece_small.samples_x,
		&targetInfo.samplingPiece_small.count_x);
	computeSamplingNumbers(targetSamples_y, targetInfo.samplingCount_y,
		&targetInfo.samplingPiece_big.samples_y,
		&targetInfo.samplingPiece_big.count_y,
		&targetInfo.samplingPiece_small.samples_y,
		&targetInfo.samplingPiece_small.count_y);

#ifdef _DEBUG_LISTING
	printf("due to sampling, lights array is split to (%d + %d) x (%d + %d) interleaved arrays of resolutions %d(%d) x %d(%d)\n",
		lights.samplingpiece_big.count_x, lights.samplingpiece_small.count_x,
		lights.samplingpiece_big.count_y, lights.samplingpiece_small.count_y,
		lights.samplingpiece_big.resolution_x, lights.samplingpiece_small.resolution_x,
		lights.samplingpiece_big.resolution_y, lights.samplingpiece_small.resolution_y);
	printf("due to sampling, screen array is split to (%d + %d) x (%d + %d) interleaved arrays of resolutions %d(%d) x %d(%d)\n",
		screen.samplingpiece_big.count_x, screen.samplingpiece_small.count_x,
		screen.samplingpiece_big.count_y, screen.samplingpiece_small.count_y,
		screen.samplingpiece_big.resolution_x, screen.samplingpiece_small.resolution_x,
		screen.samplingpiece_big.resolution_y, screen.samplingpiece_small.resolution_y);
#endif

	/*
	 * For cyclic convolution of input N and output M, we need an array of size M+N-1,
	 * or better its fft-friendly version
	 * As M and N, we place samplingPiece_?.resolution, where ? stands for "big" in case there are any big pieces
	 */
	calculateConvolutionSamples(
		sourceInfo.samplingPiece_big.count_x == 0 ? sourceInfo.samplingPiece_small.samples_x : sourceInfo.samplingPiece_big.samples_x,
		sourceInfo.samplingPiece_big.count_y == 0 ? sourceInfo.samplingPiece_small.samples_y : sourceInfo.samplingPiece_big.samples_y,
		targetInfo.samplingPiece_big.count_x == 0 ? targetInfo.samplingPiece_small.samples_x : targetInfo.samplingPiece_big.samples_x,
		targetInfo.samplingPiece_big.count_y == 0 ? targetInfo.samplingPiece_small.samples_y : targetInfo.samplingPiece_big.samples_y,
		&commonSamples_x,
		&commonSamples_y);

	/* However, commonSamples can lead to bigger space requirements than those
	 * allowed by maximum_FFT_size; in this case we need to partition.
	 * In case source and target resolutions are very different, in may be convenient to split computation anyway.
	 * In both cases, we need to find the best partition.
	 *
	 * The situaltion is a little bit complicated due to fact that big->big, big->small etc. partitions may differ.
	 */

	/*
	 * there are 16 possible combinations: [big_x, big_y] -> [big_x, big_y], [big_x, big_y] -> [big_x, small_y]
	 * etc.
	 * cycle through all of them, by binary arithmetic decide what situation is to be solved
	 */
	for (i=0; i<16; i++)
	{
		if ((i & 8) == 0) /* big or small piece of source in x direction */
		{
			sourcePieceOffset_x = 0;
			sourcePieceSamples_x = sourceInfo.samplingPiece_big.samples_x;
			sourcePieceCount_x = sourceInfo.samplingPiece_big.count_x;
		}
		else
		{
			sourcePieceOffset_x = sourceInfo.samplingPiece_big.count_x;
			sourcePieceSamples_x = sourceInfo.samplingPiece_small.samples_x;
			sourcePieceCount_x = sourceInfo.samplingPiece_small.count_x;
		}

		if ((i & 4) == 0) /* big or small piece of lights in y direction */
		{
			sourcePieceOffset_y = 0;
			sourcePieceSamples_y = sourceInfo.samplingPiece_big.samples_y;
			sourcePieceCount_y = sourceInfo.samplingPiece_big.count_y;
		}
		else
		{
			sourcePieceOffset_y = sourceInfo.samplingPiece_big.count_y;
			sourcePieceSamples_y = sourceInfo.samplingPiece_small.samples_y;
			sourcePieceCount_y = sourceInfo.samplingPiece_small.count_y;
		}

		if ((i & 2) == 0) /* big or small piece of screen in x direction */
		{
			targetPieceOffset_x = 0;
			targetPieceSamples_x = targetInfo.samplingPiece_big.samples_x;
			targetPieceCount_x = targetInfo.samplingPiece_big.count_x;
		}
		else
		{
			targetPieceOffset_x = targetInfo.samplingPiece_big.count_x;
			targetPieceSamples_x = targetInfo.samplingPiece_small.samples_x;
			targetPieceCount_x = targetInfo.samplingPiece_small.count_x;
		}

		if ((i & 1) == 0) /* big or small piece of screen in y direction */
		{
			targetPieceOffset_y = 0;
			targetPieceSamples_y = targetInfo.samplingPiece_big.samples_y;
			targetPieceCount_y = targetInfo.samplingPiece_big.count_y;
		}
		else
		{
			targetPieceOffset_y = targetInfo.samplingPiece_big.count_y;
			targetPieceSamples_y = targetInfo.samplingPiece_small.samples_y;
			targetPieceCount_y = targetInfo.samplingPiece_small.count_y;
		}

		findBestDataPartition(
			sourcePieceOffset_x, sourcePieceOffset_y,
			sourceInfo.samplingCount_x, sourceInfo.samplingCount_y,
			sourcePieceSamples_x, sourcePieceSamples_y,
			sourcePieceCount_x, sourcePieceCount_y,
			targetPieceOffset_x, targetPieceOffset_y,
			targetInfo.samplingCount_x, targetInfo.samplingCount_y,
			targetPieceSamples_x, targetPieceSamples_y,
			targetPieceCount_x, targetPieceCount_y,
			maximumFFTByteSize,
			propagPlan);
	}

#ifdef _DEBUG_LISTING
	write_propaglist(propag_list, orig_image_lights, orig_image_screen);
#endif

	return RAYLEIGH_OK;

EXCEPTION
	if (propagPlan != NULL)
		if (*propagPlan != NULL)
			propagPlanDispose(propagPlan);

	return RAYLEIGH_ERROR;
}

/*
 * given original samples and number of parts to split to,
 * calculates number of big and small interleaved parts and their resolution
 *
 * example: orig_samples = 10, samplingcount = 3, ie. array to be split looks like "abcabcabca"
 * array is split to 1 "big" array (a) of with 4 samples and 2 "small" arrays (b, c) with 3 samples
 */
void computeSamplingNumbers(int orig_samples, int samplingcount, int *big_samples, int *big_count, int *small_samples, int *small_count)
{
	*big_samples   = orig_samples / samplingcount + 1;
	*small_samples = orig_samples / samplingcount;
	*big_count        = orig_samples % samplingcount;
	*small_count      = samplingcount - orig_samples % samplingcount;
}


int findBestDataPartition(int lights_offset_x, int lights_offset_y,
			int lights_step_x, int lights_step_y,
			int lights_resolution_x, int lights_resolution_y,
			int lights_count_x, int lights_count_y,
			int screen_offset_x, int screen_offset_y,
			int screen_step_x, int screen_step_y,
			int screen_resolution_x, int screen_resolution_y,
			int screen_count_x, int screen_count_y,
			RAYLEIGH_INT64 maximum_FFT_size,
			t_propagPlan **propag_list)
{
	int piecesS_lights_count_x, piecesS_lights_count_y, piecesS_screen_count_x, piecesS_screen_count_y;
	int piecesD_lights_count_x, piecesD_lights_count_y;
	int piecesD_screen_count_x, piecesD_screen_count_y;
	int piecesD_lights_bsresolution_x[2], piecesD_lights_bsresolution_y[2];
	int piecesD_screen_bsresolution_x[2], piecesD_screen_bsresolution_y[2];
	int piecesD_lights_bscount_x[2], piecesD_lights_bscount_y[2];
	int piecesD_screen_bscount_x[2], piecesD_screen_bscount_y[2];
	double pieces_best_operations = 1e+99;
	int	pieces_best_lights_x, pieces_best_lights_y;
	int	pieces_best_screen_x, pieces_best_screen_y;
	int i, result;
	t_propagPlan *piece;

	if (lights_count_x == 0 || lights_count_y == 0 || screen_count_x == 0 || screen_count_y == 0)
		return RAYLEIGH_OK;

	for (piecesD_lights_count_x = 1; piecesD_lights_count_x <= MAX_PIECES; piecesD_lights_count_x++)
	for (piecesD_lights_count_y = 1; piecesD_lights_count_y <= MAX_PIECES; piecesD_lights_count_y++)
	for (piecesD_screen_count_x = 1; piecesD_screen_count_x <= MAX_PIECES; piecesD_screen_count_x++)
	for (piecesD_screen_count_y = 1; piecesD_screen_count_y <= MAX_PIECES; piecesD_screen_count_y++)
	{
		double piece_aux_operations = 0;

		calculate_counts_resolutions(
			lights_resolution_x, lights_resolution_y,
			screen_resolution_x, screen_resolution_y,
			piecesD_lights_count_x, piecesD_lights_count_y,
			piecesD_screen_count_x, piecesD_screen_count_y,
			piecesD_lights_bsresolution_x, piecesD_lights_bsresolution_y,
			piecesD_screen_bsresolution_x, piecesD_screen_bsresolution_y,
			piecesD_lights_bscount_x, piecesD_lights_bscount_y,
			piecesD_screen_bscount_x, piecesD_screen_bscount_y);

		for (i = 0; i < 16; i++)
		{
			result = estimateOperations(
				piecesD_lights_bsresolution_x[i        % 2], piecesD_lights_bsresolution_y[(i >> 1) % 2],
				piecesD_screen_bsresolution_x[(i >> 2) % 2], piecesD_screen_bsresolution_y[(i >> 3) % 2],
				piecesD_lights_bscount_x[i        % 2], piecesD_lights_bscount_y[(i >> 1) % 2],
				piecesD_screen_bscount_x[(i >> 2) % 2], piecesD_screen_bscount_y[(i >> 3) % 2],
				maximum_FFT_size, &piece_aux_operations);
			if (result != RAYLEIGH_OK) break;
		}

		if (result != RAYLEIGH_OK) continue;


		if (piece_aux_operations < pieces_best_operations)
		{
			pieces_best_operations = piece_aux_operations;
			pieces_best_lights_x = piecesD_lights_count_x;
			pieces_best_lights_y = piecesD_lights_count_y;
			pieces_best_screen_x = piecesD_screen_count_x;
			pieces_best_screen_y = piecesD_screen_count_y;
		}
	}


	/* re-calculate best found resolutions */
	calculate_counts_resolutions(
		lights_resolution_x, lights_resolution_y,
		screen_resolution_x, screen_resolution_y,
		pieces_best_lights_x, pieces_best_lights_y,
		pieces_best_screen_x, pieces_best_screen_y,
		piecesD_lights_bsresolution_x, piecesD_lights_bsresolution_y,
		piecesD_screen_bsresolution_x, piecesD_screen_bsresolution_y,
		piecesD_lights_bscount_x, piecesD_lights_bscount_y,
		piecesD_screen_bscount_x, piecesD_screen_bscount_y);

#ifdef _DEBUG_LISTING
	printf("best found data partition:\n");
	printf("split lights to %d x %d pieces of resolution %d(%d) x %d(%d)\n",
		pieces_best_lights_x, pieces_best_lights_y,
		piecesD_lights_bsresolution_x[BIG], piecesD_lights_bsresolution_x[SMALL],
		piecesD_lights_bsresolution_y[BIG], piecesD_lights_bsresolution_y[SMALL]);
	printf("split screen to %d x %d pieces of resolution %d(%d) x %d(%d)\n",
		pieces_best_screen_x, pieces_best_screen_y,
		piecesD_screen_bsresolution_x[BIG], piecesD_screen_bsresolution_x[SMALL],
		piecesD_screen_bsresolution_y[BIG], piecesD_screen_bsresolution_y[SMALL]);
	printf("best found partition leads to approx. %.0f operations\n", pieces_best_operations);
#endif

	/* let's assign best found values */
	for (piecesS_lights_count_x = 0; piecesS_lights_count_x < lights_count_x; piecesS_lights_count_x++)
	for (piecesS_lights_count_y = 0; piecesS_lights_count_y < lights_count_y; piecesS_lights_count_y++)
	for (piecesS_screen_count_x = 0; piecesS_screen_count_x < screen_count_x; piecesS_screen_count_x++)
	for (piecesS_screen_count_y = 0; piecesS_screen_count_y < screen_count_y; piecesS_screen_count_y++)
	for (piecesD_lights_count_x = 0; piecesD_lights_count_x < pieces_best_lights_x; piecesD_lights_count_x++)
	for (piecesD_lights_count_y = 0; piecesD_lights_count_y < pieces_best_lights_y; piecesD_lights_count_y++)
	for (piecesD_screen_count_x = 0; piecesD_screen_count_x < pieces_best_screen_x; piecesD_screen_count_x++)
	for (piecesD_screen_count_y = 0; piecesD_screen_count_y < pieces_best_screen_y; piecesD_screen_count_y++)
	{
		if ((piece = (t_propagPlan*) malloc(sizeof(t_propagPlan))) == NULL)
		{
			fprintf(stderr, "error while allocating propagation list\n");
			exit(1);
		}

		if (piecesD_lights_count_x == pieces_best_lights_x - 1 && piecesD_lights_bscount_x[SMALL] == 1)
			piece->sourceSamples_x = piecesD_lights_bsresolution_x[SMALL];
		else
			piece->sourceSamples_x = piecesD_lights_bsresolution_x[BIG];

		if (piecesD_lights_count_y == pieces_best_lights_y - 1 && piecesD_lights_bscount_y[SMALL] == 1)
			piece->sourceSamples_y = piecesD_lights_bsresolution_y[SMALL];
		else
			piece->sourceSamples_y = piecesD_lights_bsresolution_y[BIG];

		piece->sourceStart_x = piecesD_lights_count_x * piecesD_lights_bsresolution_x[BIG] * lights_step_x + lights_offset_x + piecesS_lights_count_x;
		piece->sourceStart_y = piecesD_lights_count_y * piecesD_lights_bsresolution_y[BIG] * lights_step_y + lights_offset_y + piecesS_lights_count_y;
		piece->sourceStep_x = lights_step_x;
		piece->sourceStep_y = lights_step_y;

		if (piecesD_screen_count_x == pieces_best_screen_x - 1 && piecesD_screen_bscount_x[SMALL] == 1)
			piece->targetSamples_x = piecesD_screen_bsresolution_x[SMALL];
		else
			piece->targetSamples_x = piecesD_screen_bsresolution_x[BIG];

		if (piecesD_screen_count_y == pieces_best_screen_y - 1 && piecesD_screen_bscount_y[SMALL] == 1)
			piece->targetSamples_y = piecesD_screen_bsresolution_y[SMALL];
		else
			piece->targetSamples_y = piecesD_screen_bsresolution_y[BIG];

		piece->targetStart_x = piecesD_screen_count_x * piecesD_screen_bsresolution_x[BIG] * screen_step_x + screen_offset_x + piecesS_screen_count_x;
		piece->targetStart_y = piecesD_screen_count_y * piecesD_screen_bsresolution_y[BIG] * screen_step_y + screen_offset_y + piecesS_screen_count_y;
		piece->targetStep_x = screen_step_x;
		piece->targetStep_y = screen_step_y;

		calculateConvolutionSamples(piece->sourceSamples_x, piece->sourceSamples_y,
			piece->targetSamples_x, piece->targetSamples_y,
			&(piece->convolutionSamples_x), &(piece->convolutionSamples_y));

		piece->next = *propag_list;
		*propag_list = piece;
	}

	return RAYLEIGH_OK;
}


/*
 * For given resolution and count of pieces,
 * it calculates how many big and small pieces arises when splitting.
 * Example: array of resolution 15 is split to 4 pieces => 3 big pieces of resolution 4, 1 small piece of resolution 3
 */
void calculate_counts_resolutions(int lights_resolution_x, int lights_resolution_y,
	int screen_resolution_x, int screen_resolution_y,
	int piecesD_lights_count_x, int piecesD_lights_count_y,
	int piecesD_screen_count_x, int piecesD_screen_count_y,
	int *piecesD_lights_bsresolution_x, int *piecesD_lights_bsresolution_y,
	int *piecesD_screen_bsresolution_x, int *piecesD_screen_bsresolution_y,
	int *piecesD_lights_bscount_x, int *piecesD_lights_bscount_y,
	int *piecesD_screen_bscount_x, int *piecesD_screen_bscount_y)
{
	piecesD_lights_bsresolution_x[BIG]   = div_roundup(lights_resolution_x, piecesD_lights_count_x);
	piecesD_lights_bsresolution_y[BIG]   = div_roundup(lights_resolution_y, piecesD_lights_count_y);
	piecesD_screen_bsresolution_x[BIG]   = div_roundup(screen_resolution_x, piecesD_screen_count_x);
	piecesD_screen_bsresolution_y[BIG]   = div_roundup(screen_resolution_y, piecesD_screen_count_y);

	if (lights_resolution_x % piecesD_lights_bsresolution_x[BIG] == 0)
	{
		piecesD_lights_bscount_x[BIG]        = piecesD_lights_count_x;
		piecesD_lights_bsresolution_x[SMALL] = 0;
		piecesD_lights_bscount_x[SMALL]      = 0;
	}
	else
	{
		piecesD_lights_bscount_x[BIG]        = piecesD_lights_count_x - 1;
		piecesD_lights_bsresolution_x[SMALL] = lights_resolution_x - piecesD_lights_bscount_x[BIG] * piecesD_lights_bsresolution_x[BIG];
		piecesD_lights_bscount_x[SMALL]      = 1;
	}


	if (lights_resolution_y % piecesD_lights_bsresolution_y[BIG] == 0)
	{
		piecesD_lights_bscount_y[BIG]        = piecesD_lights_count_y;
		piecesD_lights_bsresolution_y[SMALL] = 0;
		piecesD_lights_bscount_y[SMALL]      = 0;
	}
	else
	{
		piecesD_lights_bscount_y[BIG]        = piecesD_lights_count_y - 1;
		piecesD_lights_bsresolution_y[SMALL] = lights_resolution_y - piecesD_lights_bscount_y[BIG] * piecesD_lights_bsresolution_y[BIG];
		piecesD_lights_bscount_y[SMALL]      = 1;
	}

	if (screen_resolution_x % piecesD_screen_bsresolution_x[BIG] == 0)
	{
		piecesD_screen_bscount_x[BIG]        = piecesD_screen_count_x;
		piecesD_screen_bsresolution_x[SMALL] = 0;
		piecesD_screen_bscount_x[SMALL]      = 0;
	}
	else
	{
		piecesD_screen_bscount_x[BIG]        = piecesD_screen_count_x - 1;
		piecesD_screen_bsresolution_x[SMALL] = screen_resolution_x - piecesD_screen_bscount_x[BIG] * piecesD_screen_bsresolution_x[BIG];
		piecesD_screen_bscount_x[SMALL]      = 1;
	}


	if (screen_resolution_y % piecesD_screen_bsresolution_y[BIG] == 0)
	{
		piecesD_screen_bscount_y[BIG]        = piecesD_screen_count_y;
		piecesD_screen_bsresolution_y[SMALL] = 0;
		piecesD_screen_bscount_y[SMALL]      = 0;
	}
	else
	{
		piecesD_screen_bscount_y[BIG]        = piecesD_screen_count_y - 1;
		piecesD_screen_bsresolution_y[SMALL] = screen_resolution_y - piecesD_screen_bscount_y[BIG] * piecesD_screen_bsresolution_y[BIG];
		piecesD_screen_bscount_y[SMALL]      = 1;
	}

}

/*
 * Estimate number of operations when propagating
 * (sourcePieces_x * sourcePieces_y) pieces of (sourceSamples_x * sourceSamples_y) samples
 * to
 * (targetPieces_x * targetPieces_y) pieces of (targetSamples_x * targetSamples_y) samples.
 *
 * In case fft-friendly space needed is bigger than maximum space for convolution (maximum_FFT_size),
 * an ERROR is returned.
 * Otherwise, OK is returned and an estimate number of operations is added to *total_operations.
 */
int estimateOperations(int sourceSamples_x, int sourceSamples_y,
					   int targetSamples_x, int targetSamples_y,
					   int sourcePieces_x, int sourcePieces_y,
					   int targetPieces_x, int targetPieces_y,
					   RAYLEIGH_INT64 maximum_FFT_size,
					   double *total_operations)
{
	int convolutionSamples_x, convolutionSamples_y;
	RAYLEIGH_INT64 convolution_size;

	if (sourcePieces_x == 0 || sourcePieces_y == 0 || targetPieces_x == 0 || targetPieces_y == 0)
		return RAYLEIGH_OK;
	if (sourceSamples_x == 0 || sourceSamples_y == 0 || targetSamples_x == 0 || targetSamples_y == 0)
		return RAYLEIGH_OK;

	calculateConvolutionSamples(sourceSamples_x, sourceSamples_y,
			targetSamples_x, targetSamples_y,
			&convolutionSamples_x, &convolutionSamples_y);

	convolution_size = (RAYLEIGH_INT64) convolutionSamples_x * (RAYLEIGH_INT64) convolutionSamples_y;

	if (convolution_size > maximum_FFT_size)
		return RAYLEIGH_ERROR;

	*total_operations += sourcePieces_x * sourcePieces_y *
			(double) convolution_size*log((double)convolution_size) * (1 + 2*targetPieces_x * targetPieces_y);

	return RAYLEIGH_OK;
}
