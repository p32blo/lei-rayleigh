/*
 * buffer.h
 *
 * Memory buffers management.
 *
 * This version just adds "buffers" functionality, although just one buffer type
 * (in the main memory) is actually supported. It is meant for future use, e.g.
 * to support distributed memory or graphics card memory.
 *
 */

#ifndef __BUFFER_H
#define __BUFFER_H

/*
 * buffer for data processing, data storage etc.
 */

typedef enum {
	BufferTypeMain = 1,
	BufferTypeGPU = 2
} t_BufferType;

extern char *bufferTypeName[];

/* struct holding pointer to the buffer data as well as
 * its characteristics.
 */
typedef struct {
	int id;	    /* buffer identifier; not used yet */
	t_BufferType type;	/* memory space type; not used yet */
	RAYLEIGH_INT64 byteSize; /* size of the buffer in bytes */
	void* data;
} t_buffer;

/*
 * tries to allocate space for buffer structure as well as its data
 */
RAYLEIGH_EXPORT int bufferNew(RAYLEIGH_INT64 byteSize, t_BufferType bufferType, t_buffer **buffer);

/*
 * tries to free buffer resources
 */
RAYLEIGH_EXPORT int bufferDispose(t_buffer **buffer);

/*
 * getter for buffer->type
 */
RAYLEIGH_EXPORT int bufferGetType(t_buffer *buffer);

/*
 * getter for buffer->type
 */
RAYLEIGH_EXPORT RAYLEIGH_INT64 bufferGetByteSize(t_buffer *buffer);

/*
 * getter for buffer->byteSize
 */
RAYLEIGH_EXPORT RAYLEIGH_INT64 bufferGetMaxByteSize(t_BufferType bufferType);



/*****************
 * debug functions
 */
#ifdef _RAYLEIGH_DEBUG
/*
 * test basic buffer functionality
 * when any problem appears, exits
 */
void bufferTest(void);
#endif

#endif
