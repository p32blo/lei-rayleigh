#include "rayleighCompile.h"

char *filterTypeName[] = {"None", "Constant", "Triangular", "Lanczos2", "Lanczos3", "CubicStd"};

/*
 * Calculate filter taps.
 * Input:
 * fwh: half of the filter width; number of filter taps is then 2*fwh+1
 * upsampling: filter to be calculated is used for this particular upsampling;
 *    important for filter normalization
 * filterType: type of the filter, see t_FilterType enum definitions
 * filterTypeParams: unused; some filter types in the future may need additional information, e.g. bicubic filter
 *
 * Output:
 * filterData: actual filter taps
 *
 * For examples of input parameters see filterFindUpsampling function.
 * The function is not meant to be called by a common user.
 */
void fillFilterData(int fwh, int upsampling, double *filterData, t_FilterType filterType, void *filterTypeParams)
{
	int i, j;
	double sum, val = 1, x;

	/* fill in preliminary (unnormalized) filter taps */
	for (i = -fwh; i <= fwh; i++)
	{
		switch (filterType)
		{
			case FilterTypeConstant:
				val = 1.0;
				break;
			case FilterTypeTriangular:
				val = 1.0 - fabs(i)/(double)(fwh + 1);
				break;
			case FilterTypeLanczos2:
				if (i != 0)
				{
					x = 2.0 * (double) i / (double) (fwh + 1);
					val = 2.0 * sin(PI*x) * sin(PI*x/2.0) / (PI*PI*x*x);
				}
				else
					val = 1;
				break;
			case FilterTypeLanczos3:
				if (i != 0)
				{
					x = 3.0 * (double) i / (double) (fwh + 1);
					val = 3.0 * sin(PI*x) * sin(PI*x/3.0) / (PI*PI*x*x);
				}
				else
					val = 1;
				break;
		}
		filterData[i+fwh] = val;
	}

	/* normalize or do whatever postprocessing, if needed */
	switch (filterType)
	{
		case FilterTypeLanczos2:
		case FilterTypeLanczos3:
			for (i=0; i<upsampling; i++)
			{
				sum = 0;
				for (j=i; j<2*fwh+1; j += upsampling)
					sum += filterData[j];
				for (j=i; j<2*fwh+1; j += upsampling)
					filterData[j] /= sum;
			}
		break;
	}

	/* the sum of the filter data has to be 1 to conserve the energy */
	sum = 0;
	for (i = 0; i < 2*fwh+1; i++)
		sum += filterData[i];
	for (i = 0; i < 2*fwh+1; i++)
		filterData[i] /= sum;

}

/*
 * Check if points "shift" and "shift+(samplingDistance_x, samplingDistance_y, 0)"
 * are "close together" for propagation calculation. If not, the function
 * looks for numbers finalUpsampling_x, finalUpsampling_y so that the distance between
 * points "shift" and
 * "shift+(samplingDistance_x/finalUpsampling_x, samplingDistance_y/finalUpsampling_y, 0)"
 * is less than propagationPrecision * lambda.
 *
 * Numbers finalUpsampling_x and finalUpsampling_x may vary depending on filter type
 * used for further resampling of the optical field, therefore its parameters need to
 * be supplied as well.
 */
void filterFindUpsampling(t_point shift,
			double samplingDistance_x, double samplingDistance_y,
			t_FilterType filterType, void *filterTypeParams,
			int *finalUpsampling_x, int *finalUpsampling_y,
			double *filterSamplingDistance_x, double *filterSamplingDistance_y,
			double propagationPrecision)
{
	int upsampling_x, upsampling_y;
	int upsamplingStep_x = 1, upsamplingStep_y = 1;
	double shiftSampleLength, shiftSampleLength_x, shiftSampleLength_y;
	double shiftLength;
	t_point shiftSample;

	/* for certain filters, upsamplingStep has to be different */
	switch (filterType)
	{
		case FilterTypeConstant:
			/* this type of filter (piecewise constant) needs odd upsampling:
			 * original samples: ABC    upsampled samples: .abc
			 * after upsampling:
			 * . . . A . . . . . . B . . . . . . C . . .
			 * after upsampling and convolution:
			 * a a a A a a a b b b B b b b c c c C c c c
			 * in this case, upsampling is 7
			 */
			upsamplingStep_x = 2;
			upsamplingStep_y = 2;
			break;
	}

	/* for precision decision, direction (sign) of the vector is irrelevant */
	shift.x = fabs(shift.x);
	shift.y = fabs(shift.y);
	shift.z = fabs(shift.z);
	shiftLength = vectorLength(shift);

	upsampling_x = 1;
	upsampling_y = 1;

	/*
	 * First we could try to increase upsampling_x and upsampling_y separately
	 * and then to check for diagonal upsampling.
	 * However, it seems that diagonal search is fine without these preliminary searches.
	 */

	shiftSample.y = shift.y;
	shiftSample.z = shift.z;

	do
	{
		shiftSample.x = shift.x + samplingDistance_x / upsampling_x;
		shiftSampleLength = vectorLength(shiftSample);
		if ((shiftSampleLength - shiftLength)/lambda < propagationPrecision) break;
		upsampling_x += upsamplingStep_x;
#pragma warning(push)
#pragma warning(disable : 4127)  /* yes, the while condition is constant, do not generate a warning */
	} while(1);
#pragma warning(pop)

	/*
	printf("x: %f\n", (shiftSampleLength - shiftLength)/lambda);
	*/

	shiftSample.x = shift.x;
	shiftSample.z = shift.z;

	do
	{
		shiftSample.y = shift.y + samplingDistance_y / upsampling_y;
		shiftSampleLength = vectorLength(shiftSample);
		if ((shiftSampleLength - shiftLength)/lambda < propagationPrecision) break;
		upsampling_y += upsamplingStep_y;
#pragma warning(push)
#pragma warning(disable : 4127)  /* yes, the while condition is constant, do not generate a warning */
	} while(1);
#pragma warning(pop)

	/*
	printf("y: %f\n", (shiftSampleLength - shiftLength)/lambda);
	printf("\npreliminary upsampling %d %d \n", upsampling_x, upsampling_y);
	*/

	/* Calculate length of the shift for the sample diagonaly
	 * from the maximum
	 */
	shiftSample.z = shift.z;
	shiftSample.x = shift.x + samplingDistance_x / upsampling_x;
	shiftSample.y = shift.y + samplingDistance_y / upsampling_y;
	shiftSampleLength = vectorLength(shiftSample);

	/* printf("xy: %f\n", (shiftSampleLength - shiftLength)/lambda); */

	/*
	 * Check if the difference of lengths of shift and shift in the next
	 * sample diagonaly are in the propagationPrecision limit.
	 * If not, increase upsampling so that both are as close as possible.
	*/
	while ((shiftSampleLength - shiftLength)/lambda > propagationPrecision)
	{
		shiftSample.x = shift.x + samplingDistance_x / (upsampling_x + upsamplingStep_x);
		shiftSample.y = shift.y + samplingDistance_y / upsampling_y;
		shiftSampleLength_x = vectorLength(shiftSample);

		shiftSample.x = shift.x + samplingDistance_x / upsampling_x;
		shiftSample.y = shift.y + samplingDistance_y / (upsampling_y + upsamplingStep_y);
		shiftSampleLength_y = vectorLength(shiftSample);

		if (shiftSampleLength_x < shiftSampleLength_y)
		{
			upsampling_x += upsamplingStep_x;
			shiftSampleLength = shiftSampleLength_x;
			/* printf("step x: %f\n", (shiftSampleLength - shiftLength)/lambda); */

		}
		else
		{
			upsampling_y += upsamplingStep_y;
			shiftSampleLength = shiftSampleLength_y;
			/* printf("step y: %f\n", (shiftSampleLength - shiftLength)/lambda); */
		}
	}

	*filterSamplingDistance_x = samplingDistance_x / upsampling_x;
	*filterSamplingDistance_y = samplingDistance_y / upsampling_y;

	*finalUpsampling_x = upsampling_x;
	*finalUpsampling_y = upsampling_y;

	/* printf("final: %f\n", (shiftSampleLength - shiftLength)/lambda); */
}


/*
 * Calculate an interpolation filter for a given upsampling.
 * Input:  filterType - type of filter to be calculated, see FILTERTYPE constants in filter.h
 *         filterTypeParams - additional parameters for a particular filter type; NULL for parameterless types
 *         upsampling - desired upsampling of the optical field, 2 = twice
 * Output: filterShift - filter shifting between successive interpolations
 *         filterData - filter taps
 *         filterWidthHalf - filter tap count = 2*filterWidthHalf + 1
 */
void filterCalculate(t_FilterType filterType, void *filterTypeParams,
			int *filterShift, int upsampling, double *filterData, int *filterWidthHalf)
{
	int filterWidth;
	int fwh = 0;

	switch (filterType)
	{
		case FilterTypeConstant:
			/* example: upsampling = 5
			 * (ABC = original samples, 0 = kernel centre)
			 *            . . A . . . . B . . . . C . .
			 * kernel 1:  . . 0 . .
			 * kernel 2:            . . 0 . .
			 *
			 * => fwh = 2, filterShift = 5
			 */
			fwh = (upsampling)/2;
			*filterShift = 2*fwh + 1;
			break;
		case FilterTypeTriangular:
			/* example: upsampling = 5
			 *            . . . . A . . . . B . . . . C . . . .
			 * kernel 1:  . . . . 0 . . . .
			 * kernel 2:            . . . . 0 . . . .
			 *
			 * => fwh = 4, filterShift = 5
			 */
			fwh = upsampling - 1;
			*filterShift = upsampling;
			break;
		case FilterTypeLanczos2:
			/* example: upsampling = 5
			 *            . . . . A . . . . B . . . . C . . . . D . . . .
			 * kernel 1:  . . . . . . . . . 0 . . . . . . . . .
			 * kernel 2:            . . . . . . . . . 0 . . . . . . . . .
			 *
			 * => fwh = 9, filterShift = 5
			 */
			fwh = 2*upsampling - 1;
			*filterShift = upsampling;
			break;
		case FilterTypeLanczos3:
			/* example: upsampling = 5
			 *            . . . . A . . . . B . . . . C . . . . D . . . . E . . . . F . . . .
			 * kernel 1:  . . . . . . . . . . . . . . 0 . . . . . . . . . . . . . .
			 * kernel 2:            . . . . . . . . . . . . . . 0 . . . . . . . . . . . . . .
			 *
			 * => fwh = 14, filterShift = 5
			 */
			fwh = 3*upsampling - 1;
			*filterShift = upsampling;
			break;
	}

	*filterWidthHalf = fwh;

	filterWidth = 2*fwh + 1;

	fillFilterData(fwh, upsampling, filterData, filterType, filterTypeParams);

	return;
}


/*
 * Auxiliary function:
 * Convolve a 1-D array linein using a filter to a 1-D array lineout.
 * Used in 2-D convolution functions.
 */
void filterUpsampleConvLine(T_SAMPLE_TYPE_FFTWDOUBLE *lineout, T_SAMPLE_TYPE_FFTWDOUBLE *linein,
							int upsampling, int upsSamples, int filterWidthHalf, double *filterData)
{
	int dst, i, j;
	int filterWidth;
	T_SAMPLE_TYPE_FFTWDOUBLE val;

	filterWidth = 2*filterWidthHalf + 1;

	/*
	 * convolve first part from [0]
	 * to [filterWidthHalf - 1] (just before the first nonzero one)
	 */

	dst = 0; /* index of a sample to be calculated */
	j = 0;   /* in the beginning, result is just filter[j=0] * linein[filterWidthHalf] */

	while (dst < filterWidthHalf)
	{
		i = j;
		val[RE] = val[IM] = 0;
		while (i < filterWidthHalf)
		{
			val[RE] += linein[dst + filterWidthHalf - i][RE] * filterData[i];
			val[IM] += linein[dst + filterWidthHalf - i][IM] * filterData[i];
			i += upsampling;
		}
		lineout[dst][RE] = val[RE];
		lineout[dst][IM] = val[IM];

		j++;
		if (j == upsampling) j = 0;

		dst++;
	}

	/* now do the regular part:
	 * dst_x in now filterWidthHalf
	 * j is now filterWidthHalf % upsampling
	 */
	while (dst < upsSamples - filterWidthHalf)
	{
		i = j;
		val[RE] = val[IM] = 0;
		while (i < filterWidth)
		{
			val[RE] += linein[dst + filterWidthHalf - i][RE] * filterData[i];
			val[IM] += linein[dst + filterWidthHalf - i][IM] * filterData[i];
			i += upsampling;
		}
		lineout[dst][RE] = val[RE];
		lineout[dst][IM] = val[IM];

		j++;
		if (j == upsampling) j = 0;

		dst++;
	}

	/* do the rest:
	 * dst_x is now upsSamples - filterWidthHalf
	 *
	 * before preceding loop, j was filterWidthHalf % upsampling
	 * the loop ran (upsSamples - 2*filterWidthHalf) times
	 * == (srcSamples-1)*(upsampling-1) + srcSamples + 2*filterWidthHalf - 2*filterWidthHalf
	 * == (srcSamples-1)*(upsampling-1) + srcSamples
	 * == srcSamples*upsampling - srcSamples - upsampling + 1 + srcSamples
	 * == srcSamples*upsampling - upsampling + 1
	 *
	 * that is, value of j is now one more bigger in (mod upsampling) arithmetic
	 */

	while (dst < upsSamples)
	{
		/* for dst_x = upsSamples - filterWidthHalf,
		 * value of the index is dst_x + filterWidthHalf_x - i == upsSamples - i
		 * that could be bigger than upsSamples
		 *
		 * therefore we will proceed the summation in the opposite direction
		 */
		i = (filterWidth - j - 1) / upsampling * upsampling + j;

		val[RE] = val[IM] = 0;
		while (i > filterWidthHalf)
		{
			val[RE] += linein[dst + filterWidthHalf - i][RE] * filterData[i];
			val[IM] += linein[dst + filterWidthHalf - i][IM] * filterData[i];
			i -= upsampling;
		}
		lineout[dst][RE] = val[RE];
		lineout[dst][IM] = val[IM];

		j++;
		if (j == upsampling) j = 0;

		dst++;
	}
}

/*
 * Resample an optical field.
 * Input:  src - optfield to be resampled
 *         upsampling_x, upsampling_y - upsampling factors in xy directions
 *         filterType - filter type to be used, see FILTERTYPE constants in filter.h
 *         filterTypeParams - additional parameters for a filter; NULL for parameterless filters
 * Output: dst - resampled optfield
 *
 * TODO: extend to nonuniform upsampling
 */

int optfieldResample(t_optfield **dst, t_optfield *src, int upsampling_x, int upsampling_y, t_FilterType filterType, void *filterTypeParams)
{
	int _retval = RAYLEIGH_ERROR;
	int srcSamples_x, srcSamples_y;
	double srcCenter_x, srcCenter_y, srcCenter_z;
	int upsSamples_x, upsSamples_y;
	int filterShift_x, filterShift_y;
	int filterWidthHalf_x, filterWidthHalf_y;
	int filterWidth_x, filterWidth_y;
	double *filterData_x = NULL, *filterData_y = NULL;
	int src_x, src_y, dst_x, dst_y;
	int indexSrc_y, indexDst_y;
	T_SAMPLE_TYPE_FFTWDOUBLE *linein = NULL, *lineout = NULL;
	int tmp;

	if (upsampling_x != upsampling_y)
	{
		ELOG0("Upsampling has to be equal for x and y.\n");
		GOEXCEPTION;
	}

	CHECK(optfieldGetSamples(&srcSamples_x, &srcSamples_y, src));
	CHECK(optfieldGetCenter(&srcCenter_x, &srcCenter_y, &srcCenter_z, src));

	/*
	 * allocate kernel filter data
	 */
	if ((filterData_x = (double*) malloc(MAX_FILTER_WIDTH * sizeof(double))) == NULL)
		GOEXCEPTION;

	if ((filterData_y = (double*) malloc(MAX_FILTER_WIDTH * sizeof(double))) == NULL)
		GOEXCEPTION;


	if (dst == NULL)
		GOEXCEPTION;

	filterCalculate(filterType, filterTypeParams,
		&filterShift_x, upsampling_x, filterData_x, &filterWidthHalf_x);

	filterCalculate(filterType, filterTypeParams,
		&filterShift_y, upsampling_y, filterData_y, &filterWidthHalf_y);

	filterWidth_x = 2*filterWidthHalf_x + 1;
	filterWidth_y = 2*filterWidthHalf_y + 1;

	upsSamples_x = srcSamples_x + (srcSamples_x - 1)*(upsampling_x - 1) + 2*filterWidthHalf_x;
	upsSamples_y = srcSamples_y + (srcSamples_y - 1)*(upsampling_y - 1) + 2*filterWidthHalf_y;

	tmp = upsSamples_x > upsSamples_y ? upsSamples_x : upsSamples_y;
	linein = (T_SAMPLE_TYPE_FFTWDOUBLE*) malloc(tmp * sizeof(T_SAMPLE_TYPE_FFTWDOUBLE));
	if (linein == NULL)
		GOEXCEPTION;

	lineout = (T_SAMPLE_TYPE_FFTWDOUBLE*) malloc(tmp * sizeof(T_SAMPLE_TYPE_FFTWDOUBLE));
	if (lineout == NULL)
		GOEXCEPTION;


	if (dst == NULL)
		GOEXCEPTION;

	if (*dst == NULL)
	{
		CHECK(optfieldNew(srcCenter_x, srcCenter_y, srcCenter_z,
			upsSamples_x, upsSamples_y,
			optfieldGetSampling(src) / upsampling_x,
			optfieldGetSampleType(src), bufferGetType(src->buffer), NULL, dst));
	}
	else
	{
		CHECK(optfieldSetSamples(upsSamples_x, upsSamples_y, *dst));
		optfieldSetSampling(optfieldGetSampling(src) / upsampling_x, *dst);
		optfieldSetCenter(srcCenter_x, srcCenter_y, srcCenter_z, *dst);
	}

	for (src_y = 0, indexSrc_y = 0, dst_y = 0 /* 1 */, indexDst_y = dst_y * upsSamples_x;
		 src_y < srcSamples_y;
		 src_y++, indexSrc_y += srcSamples_x, dst_y += upsampling_y, indexDst_y += upsSamples_x /* 1 */)

		 /* 1:
		  * There should be "dst_y = filterWidthHalf_y" and "indexDst_y += upsampling_y * upsSamples_x"
		  * to put samples after line convolution into a proper position in dst.
		  * However, the column convolution will not run in place (but in separate buffer like line convolution).
		  * Therefore dst is now used as a buffer only and the samples can lie at anywhere.
		  */
	{
		memset(linein, 0, tmp * sizeof(T_SAMPLE_TYPE_FFTWDOUBLE));

		for (src_x = 0, dst_x = filterWidthHalf_x;
			 src_x < srcSamples_x;
			 src_x++, dst_x += upsampling_x)
		{
			linein[dst_x][RE] = ((T_SAMPLE_TYPE_FFTWDOUBLE*)src->buffer->data)[indexSrc_y + src_x][RE];
			linein[dst_x][IM] = ((T_SAMPLE_TYPE_FFTWDOUBLE*)src->buffer->data)[indexSrc_y + src_x][IM];
		}

		filterUpsampleConvLine(lineout, linein, upsampling_x, upsSamples_x, filterWidthHalf_x, filterData_x);

		memcpy(((T_SAMPLE_TYPE_FFTWDOUBLE*)(*dst)->buffer->data) + indexDst_y,
			lineout,
			upsSamples_x * sizeof(T_SAMPLE_TYPE_FFTWDOUBLE));

	}


	for (src_x = 0;
		 src_x < upsSamples_x;
		 src_x++)
	{
		memset(linein, 0, tmp * sizeof(T_SAMPLE_TYPE_FFTWDOUBLE));

		for (src_y = 0, indexSrc_y = src_x, dst_y = filterWidthHalf_y;
			 src_y < srcSamples_y;
			 src_y++, indexSrc_y += upsSamples_x, dst_y += upsampling_y)
		{
			linein[dst_y][RE] = ((T_SAMPLE_TYPE_FFTWDOUBLE*)(*dst)->buffer->data)[indexSrc_y][RE];
			linein[dst_y][IM] = ((T_SAMPLE_TYPE_FFTWDOUBLE*)(*dst)->buffer->data)[indexSrc_y][IM];
		}

		filterUpsampleConvLine(lineout, linein, upsampling_y, upsSamples_y, filterWidthHalf_y, filterData_y);

		for (dst_y = 0, indexDst_y = src_x;
			 dst_y < upsSamples_y;
			 dst_y++, indexDst_y += upsSamples_x)
		{
			((T_SAMPLE_TYPE_FFTWDOUBLE*)(*dst)->buffer->data)[indexDst_y][RE] = lineout[dst_y][RE];
			((T_SAMPLE_TYPE_FFTWDOUBLE*)(*dst)->buffer->data)[indexDst_y][IM] = lineout[dst_y][IM];
		}

	}


	_retval = RAYLEIGH_OK;

EXCEPTION
	if (filterData_x != NULL) free(filterData_x);
	if (filterData_y != NULL) free(filterData_y);
	if (linein != NULL) free(linein);
	if (lineout != NULL) free(lineout);

	return _retval;
}


/* Filter a line of complex numbers.
 * The filter taps are given in filterData, number of taps is 2*filterWidthHalf+1
 * All the filter taps must overlap the input line during filtering, i.e. number of output (filtered)
 *   values is samples - 2*filterWidthHalf. Example:
 *   input (10 samples):   . . . . . . . . . .
 *   filter (3 taps):      x x x
 *   filtered value:         o
 *   last filter position:               x x x
 *   last filtered value:                  o
 * It is also possible to calculate every n-th filtered value only, i.e. to perform
 *   both filtering and downsampling at once. This "n" is given as "downsampling"
 */
int filterComplex1D(fftw_complex *input, int inSamples,
					double *filterData, int filterWidthHalf,
					int downsampling,
					fftw_complex *output)
{
	int i, o, f, iStart, filterWidth;
	int outSamples;
	double tmp_re, tmp_im;

	if (input == NULL || output == NULL || inSamples < 0)
		GOEXCEPTION;

	outSamples = (inSamples - 2*filterWidthHalf)/downsampling;
	filterWidth = 2*filterWidthHalf + 1;

	if (outSamples < 0)
		GOEXCEPTION;

	if (filterWidth < 1)
		GOEXCEPTION;

	if (outSamples == 0)
		return RAYLEIGH_OK;

	for (o = 0, iStart = 0; o < outSamples; o++, iStart += downsampling)
	{
		tmp_re = 0;
		tmp_im = 0;
		for (f = 0, i = iStart; f < filterWidth; f++, i++)
		{
			tmp_re += filterData[f] * input[i][RE];
			tmp_im += filterData[f] * input[i][IM];
		}
		output[o][RE] = tmp_re;
		output[o][IM] = tmp_im;
	}

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}



/*
 * Calculate the frequency response of a filter.
 * Output: freqResponce - a 1-D comples array, length "samples * upsampling"
 * Input:  filterData - filter taps
 *         filterWidthHalf - filter tap count = 2*filterWidthHalf + 1
 *		   cutdown - 0 = just output FFT of the filter taps, 1 = reorder data for easier readability
 *		   samples, upsampling - determine frequency response length and cutdown data reordering
 */
int filterCalcFreqResponse(fftw_complex **freqResponse,
						   double *filterData, int filterWidthHalf, int upsampling,
						   int samples, int cutdown)
{
	int fftLength, i, filterWidth, shift;
	fftw_plan p;

	if (freqResponse==NULL)
	{
		ELOG0("Error when calling filterCalcFreqResponse.\n");
		GOEXCEPTION
	}

	filterWidth = 2*filterWidthHalf + 1;

	fftLength = samples * upsampling;
	*freqResponse = fftw_malloc(fftLength * sizeof(fftw_complex));

	if (*freqResponse==NULL)
	{
		ELOG0("Error allocating frequency response data.\n");
		GOEXCEPTION
	}

	memset(*freqResponse, 0, fftLength * sizeof(fftw_complex));

	(*freqResponse)[0][RE] = filterData[filterWidthHalf];

	for (i=0; i<filterWidthHalf; i++)
	{
		(*freqResponse)[i+1][RE] = filterData[filterWidthHalf-1-i];
		(*freqResponse)[fftLength-1-i][RE] = filterData[i+filterWidthHalf+1];
	}

	/*
	filterComplexSave(*freqResponse, fftLength, "kernelw.txt");
	filterRealSave(filterData, filterWidth, "kernel.txt");
	*/

	p = fftw_plan_dft_1d(fftLength, *freqResponse, *freqResponse, -1, FFTW_ESTIMATE);
	fftw_execute(p);
	fftw_destroy_plan(p);

	if (cutdown == 1)
	{
		shift = (samples+1)/2;

		for (i=shift; i<samples; i++)
		{
			(*freqResponse)[i][RE] = (*freqResponse)[fftLength-samples+i][RE];
			(*freqResponse)[i][IM] = (*freqResponse)[fftLength-samples+i][IM];
		}
	}

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}





/*
 * Multiply filter taps with a real number
 */
int filterScale(double *infilter, int filterWidth, double scale, double *outfilter)
{
	int i;
	if (infilter == NULL || outfilter == NULL)
		return RAYLEIGH_ERROR;

	for (i=0; i<filterWidth; i++)
		outfilter[i] = scale * infilter[i];

	return RAYLEIGH_OK;
}


/*
 * Save filter taps (real part only) to a text file.
 */
int filterRealSave(double *filter, int samples, char *filename)
{
	FILE *fout = NULL;
	int i;

	fout = fopen(filename, "w");
	if (fout == NULL)
	{
		ELOG1("Error saving real filter data to file %s.\n", filename);
		GOEXCEPTION
	}

	for (i=0; i<samples; i++)
		fprintf(fout, "%.10g\n", filter[i]);

	fclose(fout);
	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}


/*
 * Save filter taps to a text file.
 */
int filterComplexSave(fftw_complex *filter, int samples, char *filename)
{
	FILE *fout = NULL;
	int i;

	fout = fopen(filename, "w");
	if (fout == NULL)
	{
		ELOG1("Error saving complex filter data to file %s.\n", filename);
		GOEXCEPTION
	}

	for (i=0; i<samples; i++)
		fprintf(fout, "%.10g %.10g\n", filter[i][RE], filter[i][IM]);

	fclose(fout);
	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}


/*
 * Testing filter.c
 */
void optfieldResampleTest(void)
{
	t_optfield *src = NULL, *dst = NULL;
	int samplesX, samplesY;
	int y, x;
	int resample;
	char fname[100];

	samplesX = samplesY = 60;

	CHECK(optfieldNew(0, 0, 0, samplesX, samplesY, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &src));

	((T_SAMPLE_TYPE_FFTWDOUBLE*)(src->buffer->data))[5*samplesX + 5][RE] = 10;

	y = 5;
	for (x = 20; x < 30; x++)
			((T_SAMPLE_TYPE_FFTWDOUBLE*)src->buffer->data)[y*samplesX + x][RE] = 10;

	for (x = 40; x < 50; x++)
			((T_SAMPLE_TYPE_FFTWDOUBLE*)src->buffer->data)[y*samplesX + x][RE] = x-39;

	y = 10;
	for (x = 20; x < 50; x++)
			((T_SAMPLE_TYPE_FFTWDOUBLE*)src->buffer->data)[y*samplesX + x][RE] = 5*sin((x-20.0)/2.0)+5;

	x = 5;

	for (y = 20; y < 30; y++)
			((T_SAMPLE_TYPE_FFTWDOUBLE*)src->buffer->data)[y*samplesX + x][RE] = 10;

	for (y = 40; y < 50; y++)
			((T_SAMPLE_TYPE_FFTWDOUBLE*)src->buffer->data)[y*samplesX + x][RE] = y-39;

	x = 10;
	for (y = 20; y < 50; y++)
			((T_SAMPLE_TYPE_FFTWDOUBLE*)src->buffer->data)[y*samplesX + x][RE] = 5*sin((y-20.0)/2.0)+5;

	for (y = 20; y < 50; y++)
	{
		for (x = 20; x < 35; x++)
			((T_SAMPLE_TYPE_FFTWDOUBLE*)src->buffer->data)[y*samplesX + x][RE] = 10*(((x/3)%2) ^ ((y/3)%2));
		for (x = 35; x < 50; x++)
			((T_SAMPLE_TYPE_FFTWDOUBLE*)src->buffer->data)[y*samplesX + x][RE] = 10*(0.5*sin(x)*sin(y)+0.5);
	}

	optfieldSavePNG(src, "resampleSrc", PictureIOIntensity, 2, 0, NULL);

	resample = 5;

	CHECK(optfieldResample(&dst, src, resample, resample, FilterTypeConstant, NULL));
	sprintf(fname, "resampleUps_%02d_constant", resample);
	optfieldSavePNG(dst, fname, PictureIOIntensity, 2, 0, NULL);
	optfieldDispose(&dst);

	CHECK(optfieldResample(&dst, src, resample, resample, FilterTypeTriangular, NULL));
	sprintf(fname, "resampleUps_%02d_triangle", resample);
	optfieldSavePNG(dst, fname, PictureIOIntensity, 2, 0, NULL);
	optfieldDispose(&dst);

	CHECK(optfieldResample(&dst, src, resample, resample, FilterTypeLanczos2, NULL));
	sprintf(fname, "resampleUps_%02d_lanczos2", resample);
	optfieldSavePNG(dst, fname, PictureIOIntensity, 2, 0, NULL);
	optfieldDispose(&dst);

	CHECK(optfieldResample(&dst, src, resample, resample, FilterTypeLanczos3, NULL));
	sprintf(fname, "resampleUps_%02d_lanszos3", resample);
	optfieldSavePNG(dst, fname, PictureIOIntensity, 2, 0, NULL);
	optfieldDispose(&dst);

	resample = 6;

	CHECK(optfieldResample(&dst, src, resample, resample, FilterTypeTriangular, NULL));
	sprintf(fname, "resampleUps_%02d_triangle", resample);
	optfieldSavePNG(dst, fname, PictureIOIntensity, 2, 0, NULL);
	optfieldDispose(&dst);

	CHECK(optfieldResample(&dst, src, resample, resample, FilterTypeLanczos2, NULL));
	sprintf(fname, "resampleUps_%02d_lanczos2", resample);
	optfieldSavePNG(dst, fname, PictureIOIntensity, 2, 0, NULL);
	optfieldDispose(&dst);

	CHECK(optfieldResample(&dst, src, resample, resample, FilterTypeLanczos3, NULL));
	sprintf(fname, "resampleUps_%02d_lanszos3", resample);
	optfieldSavePNG(dst, fname, PictureIOIntensity, 2, 0, NULL);
	optfieldDispose(&dst);

	resample = 15;

	CHECK(optfieldResample(&dst, src, resample, resample, FilterTypeTriangular, NULL));
	sprintf(fname, "resampleUps_%02d_triangle", resample);
	optfieldSavePNG(dst, fname, PictureIOIntensity, 2, 0, NULL);
	optfieldDispose(&dst);

	CHECK(optfieldResample(&dst, src, resample, resample, FilterTypeLanczos2, NULL));
	sprintf(fname, "resampleUps_%02d_lanczos2", resample);
	optfieldSavePNG(dst, fname, PictureIOIntensity, 2, 0, NULL);
	optfieldDispose(&dst);

	CHECK(optfieldResample(&dst, src, resample, resample, FilterTypeLanczos3, NULL));
	sprintf(fname, "resampleUps_%02d_lanszos3", resample);
	optfieldSavePNG(dst, fname, PictureIOIntensity, 2, 0, NULL);
	optfieldDispose(&dst);

EXCEPTION
	if (src != NULL) optfieldDispose(&src);
	if (dst != NULL) optfieldDispose(&dst);
}
