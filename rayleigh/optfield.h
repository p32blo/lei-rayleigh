/*
 * General work with optical fields on planes parallell with xy axes.
 */

#ifndef __OPTFIELD_H
#define __OPTFIELD_H

#include "rayleigh.h"

typedef enum {
	SampleTypeFFTWDouble = 1
} t_SampleType;

#define T_SAMPLE_TYPE_FFTWDOUBLE	fftw_complex
extern char *SampleTypeName[];

/*
 * optical field on a plane parallell with xy plane
 * can be used for an image of convolution kernel etc. just as the image with complex valued "pixels"
 */
typedef struct {
										/* values needed to init an image */
										/* ------------------------------ */
	t_point corner;						/* position of the [0, 0] sample */
	t_SampleType sampleType;			/* type of sample, such as 2x double etc. See SAMPLE_TYPE_* list */
	int sampleTypeSize;					/* size of one sample in bytes */
	int sampling;						/* sampling distance is sampling * common_sampling_distance */
	int samples_x, samples_y;           /* sampling of the image
										 * First sample is on the position corner.x (x direction only)
										 * Last sample is on the position corner.x + (resolution_x-1)*sampling_distance.
										 */

										/* some useful values for further use */
	t_buffer *buffer;                   /* samples themselves */
} t_optfield;



/* Initialize image parameters and allocate space.
 *
 * PARAMETERS
 * center_x, center_y, center_z (IN): centre of the plane, in meters
 * samples_x, samples_y (IN): sample count in both x and y directions
 * sampling (IN): sampling distance as an integer multiple of common_sampling_distance
 * sampleType (IN): type of one sample, see SAMPLE_TYPE_* list
 * bufferType (IN): type of buffer, see BUFFER_TYPE_* list
 * buffer (IN): you can provide external buffer; if NULL, a new buffer is created
 * optfield (OUT): output image.
 */

RAYLEIGH_EXPORT int optfieldNew(double center_x, double center_y, double center_z,
				 int samples_x, int samples_y,
				 int sampling, t_SampleType sampleType,
				 t_BufferType bufferType, t_buffer *buffer,
				 t_optfield **optfield);


/*
 * creates an empty valid optfield structure with NULL buffer
 */
RAYLEIGH_EXPORT int optfieldNewEmpty(t_optfield **optfield);


/*
 * Free space occupied by both the optical field samples
 * and the optical field structure
 */
RAYLEIGH_EXPORT int optfieldDispose(t_optfield **optfield);


/*
 * set/get optical field center position
 */
RAYLEIGH_EXPORT int optfieldSetCenterP(t_point *center, t_optfield *optfield);
RAYLEIGH_EXPORT int optfieldSetCenter(double center_x, double center_y, double center_z, t_optfield *optfield);
RAYLEIGH_EXPORT int optfieldGetCenterP(t_point *center, t_optfield *optfield);
RAYLEIGH_EXPORT int optfieldGetCenter(double *center_x, double *center_y, double *center_z, t_optfield *optfield);

/*
 * set/get optical field sample [0,0] position
 */
RAYLEIGH_EXPORT int optfieldSetCornerP(t_point *corner, t_optfield *optfield);
RAYLEIGH_EXPORT int optfieldSetCorner(double corner_x, double corner_y, double corner_z, t_optfield *optfield);
RAYLEIGH_EXPORT int optfieldGetCornerP(t_point *corner, t_optfield *optfield);
RAYLEIGH_EXPORT int optfieldGetCorner(double *corner_x, double *corner_y, double *corner_z, t_optfield *optfield);

/*
 * set/get optical field sampling
 * setting sampling does not change sample count of the optical field,
 * i.e. its size is changed accordingly
 */
RAYLEIGH_EXPORT int optfieldSetSampling(int sampling, t_optfield *optfield);
RAYLEIGH_EXPORT int optfieldGetSampling(t_optfield *optfield);

/*
 * set/get optical field sample type
 * setting sampling type does not change buffer size,
 * size of buffer is tested
 */
RAYLEIGH_EXPORT t_SampleType optfieldSetSampleType(t_SampleType sampleType, t_optfield *optfield);
RAYLEIGH_EXPORT t_SampleType optfieldGetSampleType(t_optfield *optfield);
RAYLEIGH_EXPORT int optfieldGetSampleTypeSize(t_optfield *optfield);

/*
 * set/get optical field sampling distance
 * setting sampling distance does not change sample count of the optical field,
 * i.e. its size is changed accordingly
 * sampling distance is set to the nearest multiple of common_sampling_distance
 */
RAYLEIGH_EXPORT int optfieldSetSamplingdistance(double sampling_distance, t_optfield *optfield);
RAYLEIGH_EXPORT double optfieldGetSamplingdistance(t_optfield *optfield);

/*
 * set/get number of samples of the optical field
 * setting number of samples checks if the buffer inside can hold such data
 */
RAYLEIGH_EXPORT int optfieldSetSamples(int samples_x, int samples_y, t_optfield *optfield);
RAYLEIGH_EXPORT int optfieldGetSamples(int *samples_x, int *samples_y, t_optfield *optfield);

/*
 * set/get number of samples of the optical field through
 * the actual physical size
 * setting number of samples checks if the buffer inside can hold such data
 */
RAYLEIGH_EXPORT int optfieldSetSize(double size_x, double size_y, t_optfield *optfield);
RAYLEIGH_EXPORT int optfieldGetSize(double *size_x, double *size_y, t_optfield *optfield);

/*
 * Find maxima/minima in the optfield
 */
RAYLEIGH_EXPORT int optfieldFindMinmax(t_optfield *image, fftw_complex outMin, fftw_complex outMax,
									   fftw_complex outMaxAbsSqSample,
									   double *outMaxAbsSq, RAYLEIGH_INT64 *outMaxAbsSqIndex);

/*
 * Sets image samples to complex zero
 */
RAYLEIGH_EXPORT int optfieldZeroData(t_optfield *optfield);

#ifdef _RAYLEIGH_DEBUG
/*
 * Sets image data to sequence of integers for easy debugging
 */
int optfieldDebugdata(t_optfield *optfield);
#endif

/*
 * Create a deep copy of src optical field into dst optical field. Allocation included.
 */
RAYLEIGH_EXPORT int optfieldCopy(t_optfield **dst, t_optfield *src);


/* Copies optfield (samples_x * samples_y) sample values from src to dst (ie. from src->buffer to dst->buffer).
 * Data can be offset from the zero position by offset_x, offset_y.
 * Data can be interleaved, ie. can have gaps, sample distances are set by step_x and step_y.
 * Example 1: common copy from src to dst:
 *			  optfieldGetSamples(*samples_x, *samples_y, src);
 *            optfieldCopyData(dst, 0, 0, 1, 1, src, 0, 0, 1, 1, samples_x, samples_y)
 * Example 2: copy every even sample of src to odd samples of dst:
 *			  optfieldGetSamples(*samples_x, *samples_y, src);
 *            optfieldCopyData(dst, 1, 1, 2, 2, src, 0, 0, 2, 2, samples_x/2, samples_y/2)
 *            (image: . = sample left, * = sample copied, array position [0,0] is the upper left corner)
 *            * . * . * . * .          . . . . . . . .
 *            . . . . . . . .          . * . * . * . *
 *            * . * . * . * .          . . . . . . . .
 *            . . . . . . . .          . * . * . * . *
 *            * . * . * . * .          . . . . . . . .
 *            . . . . . . . .          . * . * . * . *
 */
RAYLEIGH_EXPORT int optfieldCopyData(t_optfield *dst, int dst_offset_x, int dst_offset_y, int dst_step_x, int dst_step_y,
					t_optfield *src, int src_offset_x, int src_offset_y, int src_step_x, int src_step_y,
					int samples_x, int samples_y);

/* the same function as copy_image_data; instead of overwriting original numbers, it adds to actual content
 */
RAYLEIGH_EXPORT int optfieldAddDataSparse(t_optfield *dst, int dst_offset_x, int dst_offset_y, int dst_step_x, int dst_step_y,
				   t_optfield *src, int src_offset_x, int src_offset_y, int src_step_x, int src_step_y,
				   int samples_x, int samples_y);

/* the same function as copy_image_data; instead of overwriting original numbers, it multiplies actual content
 */
RAYLEIGH_EXPORT int optfieldMulDataSparse(t_optfield *dst, int dst_offset_x, int dst_offset_y, int dst_step_x, int dst_step_y,
				   t_optfield *src, int src_offset_x, int src_offset_y, int src_step_x, int src_step_y,
				   int samples_x, int samples_y);

/* create an optical field by combining amplitude from the first and the phase from the second one
 */
RAYLEIGH_EXPORT int optfieldCreateAmpPhase(t_optfield **output, t_optfield **amplitude, double amplitude_multiplier,
						   t_optfield **phase, double phase_multiplier);

/* create an optical field by combining real part from the first one;
 * real part of the second one is considered as imag part of the output
 */
RAYLEIGH_EXPORT int optfieldCreateRealImag(t_optfield **output, t_optfield **real, double real_multiplier,
						   t_optfield **imag, double imag_multiplier);

/* Calculate real-only optfield with values based on the intensity of the other */
RAYLEIGH_EXPORT int optfieldIntensity(t_optfield **output, t_optfield *input, double maxValue, double *outMaxIntensity);

/* Multiply an optical field by a real number; output = input * factor */
RAYLEIGH_EXPORT int optfieldScale(t_optfield **output, t_optfield *input, double factor);

/* Elementwise multiplication; output = inputA .* inputB */
RAYLEIGH_EXPORT int optfieldMul(t_optfield **output, t_optfield *inputA, t_optfield *inputB);

/* Optfield summation; output = inputA + inputB */
RAYLEIGH_EXPORT int optfieldAdd(t_optfield **output, t_optfield *inputA, t_optfield *inputB);


/*
 * debug printing functions
 * print 1, 2, 3 or 4 optical fields contents
 */
void optfieldPrint1(t_optfield *optfield,
					char *form1, char *form2, char *form3);
void optfieldPrint2(t_optfield *lights, t_optfield *screen,
					char *form1, char *form2, char *form3);
void optfieldPrint3(t_optfield *lights, t_optfield *kernel, t_optfield *screen,
					char *form1, char *form2, char *form3);
void optfieldPrint4(t_optfield *orig, t_optfield *lights, t_optfield *kernel, t_optfield *screen,
					char *form1, char *form2, char *form3);

/*
 * print optical field properties
 */
void optfieldPrintProperties(t_optfield *piece, char *type);

void optfieldTestBasic(void);

#endif
