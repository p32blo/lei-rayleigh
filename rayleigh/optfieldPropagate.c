/*
 * propagate optfield->optfield
 */


#include "rayleighCompile.h"

char *propagationMethodName[] = {"Rayleigh-Sommerfeld", "angular spectrum"};

/* 
 * When testing/debugging, some files are saved.
 * To ease the process, these strings are added to the filename.
 */
char *_debug_prefix = NULL;
char *_debug_postfix = NULL; 
char _debug_filename[1000]; 

#define DEBUG_FAST_PROPAGATE


/* Auxiliary functions 
 */
void findLargestShift(t_point sourcePieceCorner, double sourcePieceEdge_x, double sourcePieceEdge_y,
			t_point targetPieceCorner, double targetPieceEdge_x, double targetPieceEdge_y,
			t_point *largestShift);


/* 
 * Propagation of the source to the target using double look-up table technique.
 * Input: source
 *        propagationPrecision - Value to determine the propagation kernel filtering.
 *           e.g. value 0.2 tells that the source will be upsampled so that the distance 
 *           between a target samples and two adjacent source samples will be less than
 *           0.2*lambda
 *        filterType, filterTypedata - filter to use for the source upsampling plus optional filtering data
 *        externRhoLUT - when NULL, the fastPropagate calculates its own rhoLUT and deletes it after completion.
 *           When *externRhoLUT==NULL, then the fastPropagate calculates its own rhoLUT and returns
 *           a pointer to it.
 *           When *externRhoLUT!=NULL, an external rhoLUT is assumed. NO SANITY CHECK IS PERFORMED!
 *        rhoFilterType - rho values should be interpolated somehow for the (x,y) pairs that are not
 *           included in the table. Only FilterTypeConstant and FilterTypeTriangular is supported.
 *        externWaveLUT - the same applies as for externRhoLUT; this time, the externWaveLUT contains
 *           the complex amplitudes for certain (rho,z) pairs.
 *        waveLUTFilterType - complex amplitudes should be interpolated for those rho that are not included
 *           in the LUT. FilterTypeConstant and FilterTypeTriangular are supported.
 *           Note that z coordinate is not interpolated.
 */
int optfieldPropagateSimple(t_optfield *source, t_optfield *target, 
				  double propagationPrecision, t_FilterType filterType, void *filterTypeData,
				  t_PropagationMethod propagationMethod, t_PropagationAcceleration propagationAcceleration,
				  t_rhoLUT **externRhoLUT, int rhoLUTFilterType, int rhoLUTsubsampling, double rhoLUTPrecision,
				  t_waveLUT **externWaveLUT, int waveLUTFilterType, int waveLUToversampling)
{
	int sourceX, sourceY, targetX, targetY;
	int cx, cy;
	t_waveLUT *waveLUT = NULL;
	t_rhoLUT *rhoLUT = NULL;
	int disposeRhoLUT = 0, disposeWaveLUT = 0;
	t_optfield *data = NULL, *kernel = NULL;
	int filterUpsamplingX, filterUpsamplingY, filterUpsampling;
	double filterSamplingDistanceX, filterSamplingDistanceY;
	double *filterDataX = NULL, *filterDataY = NULL;
	double samplingDistance;
	t_point minRelShift, maxRelShift;
	double rhoMin, rhoMax;
	fftw_plan plan;

	char test_kernel_info[1000];

	INITEXCEPTION;

	/* Make the default info text for saved kernel. */
	sprintf(test_kernel_info, "Kernel type: %s. Acceleration method: %s. Propagation precision: %lf. Filter type: %s. ",
		propagationMethodName[propagationMethod], 
		propagationMethodAccName[propagationAcceleration], 
		propagationPrecision,
		filterTypeName[filterType]);

	/* find the extremal shifts between the source and the target */
	CHECK0(rhoLUTFindMinMaxRelShift(source, target, &minRelShift, &maxRelShift), \
				"Cannot find geometrical shift for the source and target.\n");

	if (optfieldGetSampling(source) != optfieldGetSampling(target))
	{
		ELOG0("Source and target sampling distances must be the same when calling fastPropagate.\n");
		ELOG0("Use optfieldPropagate instead.\n");
		GOEXCEPTION;
	}

	samplingDistance = optfieldGetSamplingdistance(source);

	/* 
	 * Should we need the rhoLUT? 
	 */

	if (propagationAcceleration == PropagationAccDoubleLUT)
	{
		/* 
		 * Is it necessary to prepare a rhoLUT, or an external is included? 
		 */
		if (externRhoLUT == NULL ||
			*externRhoLUT == NULL)
		{
			LOG0("Creating rhoLUT...");

			/* if we are asked to propose optimal rhoLUT sampling, do so. */
			if (rhoLUTsubsampling == 0)
			{
				double rhoOptSampling;
				rhoOptSampling = rhoLUTOptSampling(rhoLUTPrecision, minRelShift.x, minRelShift.y, minRelShift.z);
				rhoLUTsubsampling = (int)floor(rhoOptSampling/samplingDistance);
				if (rhoLUTsubsampling < 1) rhoLUTsubsampling = 1;
			}


			rhoLUTPropagationNew(source, target, &rhoLUT, &minRelShift, &maxRelShift, rhoLUTsubsampling);
			LOG0(" Done!\n");

			/* if we should return a created one, provide a pointer */
			if (externRhoLUT != NULL)
			{
				*externRhoLUT = rhoLUT;
				disposeRhoLUT = 0;
			}
			else
				/* we won't return the rhoLUT, dispose it at the end */
				disposeRhoLUT = 1;
		}
		else
		{
			/* An external rhoLUT is provided; use it, do not dispose it at the end. */
			rhoLUT = *externRhoLUT;
			disposeRhoLUT = 0;
		}
		/* END of PropagationAccDoubleLUT branch for the rhoLUT */
	}


	/* Find parameters for the waveLUT/kernel filtering 
	 * First, find necessary x and y upsampling factors.
	 */
	filterFindUpsampling(maxRelShift, 
		samplingDistance, samplingDistance,
		filterType, filterTypeData,
		&filterUpsamplingX, &filterUpsamplingY,
		&filterSamplingDistanceX, &filterSamplingDistanceY,
		propagationPrecision);

	if (propagationAcceleration == PropagationAccDoubleLUT ||
		propagationAcceleration == PropagationAccWaveLUT)
	{
		/*
		 * Is it necessary to prepare a waveLUT, or an external is included? 
		 */
		if (externWaveLUT == NULL ||
			*externWaveLUT == NULL)
		{
			LOG0("Creating waveLUT...");

			/* As waveLUT has to be radially symmetric, the filter upsampling must switch to the worst case. */
			filterUpsampling = filterUpsamplingX > filterUpsamplingY ? filterUpsamplingX : filterUpsamplingY;

			/* We need to decide min and max rho values.
			 * In case we have a rhoLUT, it is quite simple:
			 */

			if (rhoLUT != NULL)
			{
				rhoMin = rhoLUT->rhoMin;
				rhoMax = rhoLUT->rhoMax;
			}
			else
			{
				rhoMin = sqrt(minRelShift.x*minRelShift.x + minRelShift.y*minRelShift.y);
				rhoMax = sqrt(maxRelShift.x*maxRelShift.x + maxRelShift.y*maxRelShift.y);
			}

			/* Create the waveLUT */
			waveLUTNew(rhoMin, rhoMax, samplingDistance/waveLUToversampling, 
				maxRelShift.z, maxRelShift.z, 1, 
				samplingDistance, 1.5, 
				filterUpsampling, 
				filterType, filterTypeData,
				&waveLUT);

			LOG4(" Done!\nCreated waveLUT %d x %d (filter upsampling %d, filter type %s)\n", 
				waveLUT->samplesRho, waveLUT->samplesZ,
				filterUpsampling,
				filterTypeName[filterType]);

			/* if we should return a created one, provide a pointer */
			if (externWaveLUT != NULL)
			{
				*externWaveLUT = waveLUT;
				disposeWaveLUT = 0;
			}
			else
				/* we won't return the waveLUT, dispose it at the end */
				disposeWaveLUT = 1;
		}
		else
		{
			/* An external waveLUT is provided; use it, do not dispose it at the end. */
			waveLUT = *externWaveLUT;
			disposeWaveLUT = 0;
		}
		/* END of PropagationAccDoubleLUT/WAVELUT branch for the waveLUT */
	}


	CHECK0(optfieldGetSamples(&sourceX, &sourceY, source), "Weird source.\n");
	CHECK0(optfieldGetSamples(&targetX, &targetY, target), "Weird target.\n");

	calculateConvolutionSamples(sourceX, sourceY, targetX, targetY, &cx, &cy);

	CHECK0(optfieldNew(0, 0, 0, cx, cy, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &data), 
		"Cannot create temporary source buffer.\n");
	CHECK0(optfieldNew(0, 0, 0, cx, cy, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &kernel),
		"Cannot create temporary kernel buffer.\n");

	if (propagationAcceleration == PropagationAccPrecise)
	{
		t_point shift, sourceCorner, targetCorner;
		t_propagPlan propagPiece;
		int filterWidthHalfX = 0, filterWidthHalfY = 0;
		int filterShiftX = 0, filterShiftY = 0;

		if ((filterDataX = (double*) malloc(MAX_FILTER_WIDTH * sizeof(double))) == NULL)
			GOEXCEPTION0("Error allocating filter for kernel calculation.\n");

		if ((filterDataY = (double*) malloc(MAX_FILTER_WIDTH * sizeof(double))) == NULL)
			GOEXCEPTION0("Error allocating filter for kernel calculation.\n");

		/* calculate the source-target shift */
		optfieldGetCornerP(&sourceCorner, source);
		optfieldGetCornerP(&targetCorner, target);
		vectorSub(&shift, &targetCorner, &sourceCorner);

		/* prepare filters for the kernel calculation */
		filterCalculate(filterType, filterTypeData,
			&filterShiftX, filterUpsamplingX, filterDataX, &filterWidthHalfX);
		filterCalculate(filterType, filterTypeData,
			&filterShiftY, filterUpsamplingY, filterDataY, &filterWidthHalfY);

		propagPieceSet(&propagPiece, 0, 0, 1, 1, sourceX, sourceY, 
			0, 0, 1, 1, targetX, targetY, cx, cy, NULL);

		sprintf(test_kernel_info, "%s Filter upsampling x, y: %d, %d.",
			test_kernel_info, filterUpsamplingX, filterUpsamplingY);
		LOG3("Calculating precise kernel, filter type %s, upsampling %d, %d...",
			filterTypeName[filterType], filterUpsamplingX, filterUpsamplingY);
		kernelCalculateRSFT(&shift, &propagPiece, 
			samplingDistance, samplingDistance,
			filterShiftX, filterDataX, filterWidthHalfX, filterSamplingDistanceX,
			filterShiftY, filterDataY, filterWidthHalfY, filterSamplingDistanceY,
			kernel, FFTApplyOff);

		/* end of PropagationAccPrecise */
	}
	else
	{
		/* Calculate the kernel in either accelerated way. */
		if (propagationAcceleration == PropagationAccDoubleLUT)
		{
			sprintf(test_kernel_info, "%s RhoLUT %dx%d filtering %s subsampling %d. WaveLUT %dx%d filtering %s.",
				test_kernel_info, 
				rhoLUTGetSamplesX(rhoLUT), rhoLUTGetSamplesY(rhoLUT), filterTypeName[rhoLUTFilterType], rhoLUTsubsampling,
				waveLUTGetSamplesRho(waveLUT), waveLUTGetSamplesZ(waveLUT), filterTypeName[waveLUTFilterType]);
			CHECK0(kernelCalculateDLUT(kernel, 
				source, target, 
				rhoLUT, rhoLUTFilterType, 
				waveLUT, waveLUTFilterType),
				"Problem when calculating kernel.\n");
		}
		else if (propagationAcceleration == PropagationAccWaveLUT)
		{
			sprintf(test_kernel_info, "%s WaveLUT %dx%d filtering %s.",
				test_kernel_info, 
				waveLUTGetSamplesRho(waveLUT), waveLUTGetSamplesZ(waveLUT), filterTypeName[waveLUTFilterType]);
			CHECK0(kernelCalculateWaveLUT(kernel, 
				source, target, 
				waveLUT, waveLUTFilterType),
				"Problem when calculating kernel.\n");
		}
		else
		{
			GOEXCEPTION0("Wrong acceleration technique.\n");
		}
	}

	CHECK0(optfieldCopyData(data, 0, 0, 1, 1, source, 0, 0, 1, 1, sourceX, sourceY),
		"Problem with data->source copying.\n");

#ifdef DEBUG_FAST_PROPAGATE
	sprintf(_debug_filename, "%s01_rholut%s", _debug_prefix, _debug_postfix);
	if (rhoLUT != NULL) rhoLUTSave(rhoLUT, _debug_filename);

	sprintf(_debug_filename, "%s02_wavelut%s", _debug_prefix, _debug_postfix);
	if (waveLUT != NULL) waveLUTSave(waveLUT, _debug_filename);

	//sprintf(_debug_filename, "%s02_wavelutInterp4%s", _debug_prefix, _debug_postfix);
	//if (waveLUT != NULL) saveWaveLUTInterp(waveLUT, _debug_filename, waveLUTFilterType, NULL, 4);

	sprintf(_debug_filename, "%s03_kernel%s", _debug_prefix, _debug_postfix);
	optfieldSavePNG(kernel, _debug_filename, PictureIORealImagIntensity, 1.0, 0, test_kernel_info);

	sprintf(_debug_filename, "%s04_sourceExt%s", _debug_prefix, _debug_postfix);
	optfieldSavePNG(data, _debug_filename, PictureIORealImagIntensity, 1.0, 0, NULL);
#endif

	plan = fftw_plan_dft_2d(cy, cx, (fftw_complex*) data->buffer->data, (fftw_complex*) data->buffer->data, +1, FFTW_ESTIMATE);
	fftw_execute(plan);
	fftw_destroy_plan(plan);

	plan = fftw_plan_dft_2d(cy, cx, (fftw_complex*) kernel->buffer->data, (fftw_complex*) kernel->buffer->data, +1, FFTW_ESTIMATE);
	fftw_execute(plan);
	fftw_destroy_plan(plan);

	CHECK0(optfieldMul(&data, data, kernel),
		/* OLD VERSION:  optfieldMulDataSparse(data, 0, 0, 1, 1, kernel, 0, 0, 1, 1, cx, cy), */
		"Problem with data*kernel multiplication.\n");

	plan = fftw_plan_dft_2d(cy, cx, (fftw_complex*) data->buffer->data, (fftw_complex*) data->buffer->data, -1, FFTW_ESTIMATE);
	fftw_execute(plan);
	fftw_destroy_plan(plan);

	CHECK0(optfieldCopyData(target, 0, 0, 1, 1, data, 0, 0, 1, 1, targetX, targetY),
		"Problem with data->target copying.\n");

	/* 
	 * 
	 * DONE ! 
	 *
	 */

EXCEPTION0("fastPropagate")
	
	if (disposeRhoLUT)
		rhoLUTDispose(&rhoLUT);

	if (disposeWaveLUT)
		waveLUTDispose(&waveLUT);

	if (data != NULL)
		optfieldDispose(&data);

	if (kernel != NULL)
		optfieldDispose(&kernel);

	if (filterDataX != NULL)
		free(filterDataX);

	if (filterDataY != NULL)
		free(filterDataY);

ENDEXCEPTION
}



/*
 * Propagate orig_image_lights to orig_image_screen.
 * It is possible to provide temporary spaces space1 and space2 for convolution calculation.
 * In case one (or both) is NULL, the function allocates its own space of size at most MAXIMUM_FFT_SIZE.
 * The last parameter is a plan how to split lights/screen to pieces.
 * There are three possibilities: in case propag_list == NULL, the function creates its own propagation plan.
 * In case *propag_list == NULL, the function creates the plan and the pointer to it is returned.
 * Otherwise (*propag_list != NULL), the provided list is used.
 */
int optfieldPropagate(t_optfield *source, t_optfield *target, 
					   t_buffer *providedBuffer1, t_buffer *providedBuffer2, 
					   t_PropagationMethod propagationMethod,
					   t_PropagationPrecision variablePrecision, double propagationPrecision,
					   t_FilterType filterType, void *filterTypeData,
					   t_propagPlan **providedPropagPlan)
{
	/* pointers to space1 and space2, or pointers to internally allocated space that is freed after calculation */
	t_optfield *image = NULL, *kernel = NULL;

	/* actual buffers used in calculation */
	t_buffer *buffer1, *buffer2;

	/* positions of a particular piece of data now being propagated */
	t_point sourcePieceCorner, targetPieceCorner;   /* position of [0,0] sample */
	double sourcePieceEdge_x, sourcePieceEdge_y;
	double targetPieceEdge_x, targetPieceEdge_y;
	t_point sourceCorner, targetCorner;
	t_point shift, largestShift;


	RAYLEIGH_INT64 maximumFFTByteSize;
	
	/* size of sample for convolution calculation */
	int sampleTypeSize;

	/* maybe we have to create our own propagation list; this pointer will point to some propagation list anyway */
	t_propagPlan *propagPlan = NULL, *propagationPiece, lastPropagatedPiece;

	/*
	 * When iterating, it is good to know resolution of actual piece; let's have a special variable for it.
	 */

	/* maximum resolution of auxiliary array for cyclic convolution 
	 * when a lot of memory is available, this is simply 
	 *  piecesS_lights_resolution_x + piecesS_screen_resolution_x (the same for y)
	 */
	RAYLEIGH_INT64 maxConvolutionSamples_xy;
	int maxConvolutionSamples_x, maxConvolutionSamples_y;

	/* common sampling */

	/* physical sizes of orig_image_lights and orig_image_screen pieces */


	/* In case of big arrays, direct application of FFT may not be possible;
	 * we need to split arrays of the same sampling further into smaller pieces.
	 * Their count and resolution is given here.
	 * As before, partition need not be perfect; original array is split into big and small arrays
	 * and we need to remember their resolutions.
	 *
	 * simple example: Lights resolution 100x100, screen resolution 200x200, they share the same sampling.
	 *          For cyclic convolution an array of (100+200-1)x(100+200-1) = 299x299 is needed.
	 *          However, we may be limited by a size of 100x100.
	 *          We can eg. split lights to 2x2=4 pieces of size 50x50, and screen to 4x4=16 pices of size 50x50.
	 *          Then we will need to propagate each of the "light" piece to every "screen" piece, yielding
	 *          4*16=64 propagations.
	 * complex example: Lights resolution 190x190, screen resolution 290x290, they share the same sampling.
	 *          When calculating cyclic concolution, we are again limited to the array of 100x100.
	 *          We can split 190 as 3*50+40, and 290 as 5*50+40.
	 *          Therefore, lights array splits to 3*3 arrays 50x50 (big), 3 arrays 40x50, 3 arrays 50x40, and 
	 *          1 array 40x40 (small ones).
	 *          Screen array splits to 5*5 arrays 50x50 (big), 5 arrays 40x50, 5 arrays 50x40, and 
	 *          1 array 40x40.
	 */

	int propagationNo, propagationCount;

	/* kernel filtering is needed for high angles of propagation 
	 * filter is symmetrical width odd number of taps
	 * filter width = 2*filterWidthHalf + 1
	 * We will assume that filterWidthHalf is small, up to say 10
	 * Otherwise, different techniques of kernel filtering should be used
	 */
	int filterWidthHalf_x = 0, filterWidthHalf_y = 0;
	double *filterData_x = NULL, *filterData_y = NULL;
	double filterSamplingDistance_x = 0, filterSamplingDistance_y = 0;
	int filterShift_x = 0, filterShift_y = 0;
	int filterUpsampling_x = 0, filterUpsampling_y = 0;
	/* */ 
	fftw_plan p;

INITEXCEPTION;

	/* --------------------------------------------------------------*/

	/* TODO
	 * better handling of free space, support for other sample types
	 */
	maximumFFTByteSize = bufferGetMaxByteSize(BufferTypeMain);
	sampleTypeSize = sizeof(T_SAMPLE_TYPE_FFTWDOUBLE);


	/* maximum FFT size is limited;
	   in case space1 or space2 are provided, we can be restricted even more
     */
	if (providedBuffer1 != NULL)
	{
		RAYLEIGH_INT64 tmp = bufferGetByteSize(providedBuffer1);
		if ( tmp < maximumFFTByteSize) 
			maximumFFTByteSize = tmp;
	}


	if (providedBuffer2 != NULL)
	{
		RAYLEIGH_INT64 tmp = bufferGetByteSize(providedBuffer2);
		if ( tmp < maximumFFTByteSize) 
			maximumFFTByteSize = tmp;
	}

	/* we have to create propagation plan if none is provided */
	if (providedPropagPlan == NULL || *providedPropagPlan == NULL)
	{
		propagPlanNew(source, target, maximumFFTByteSize, &propagPlan);
	}
	else
		propagPlan = *providedPropagPlan;


	/* it is not necessary to allocate maximumFFTByteSize buffers
	 * in fact, the largest convolution size of actual propagation plan is needed
	 */

	propagPlanFindLargestBuffers(propagPlan, &maxConvolutionSamples_x, &maxConvolutionSamples_y, &maxConvolutionSamples_xy);

	if (maximumFFTByteSize < maxConvolutionSamples_xy * sampleTypeSize)
	{
		ELOG0("Provided propagation plan requires bigger buffers than provided\n");
		GOEXCEPTION;
	}

	/* TODO
	 * try to make maximumFFTByteSize to be FFT friendly
     */
	maximumFFTByteSize = maxConvolutionSamples_xy * sampleTypeSize;



	/* do we need to allocate space for kernel and FT?
	 */
	if (providedBuffer1 == NULL)
	{
		CHECK0(bufferNew(maximumFFTByteSize, BufferTypeMain, &buffer1), 
			"Error allocating temporary buffer.\n");
		CHECK0(optfieldNew(0, 0, 0, 0, 0, 1, SampleTypeFFTWDouble, BufferTypeMain, buffer1, &image), 
			"Error allocating optfield.\n");
	}
	else
	{
		buffer1 = providedBuffer1;
		CHECK0(optfieldNew(0, 0, 0, 0, 0, 1, SampleTypeFFTWDouble, BufferTypeMain, buffer1, &image),
			"Error allocating temporary optfield.\n");
		CHECK0(optfieldZeroData(image),
			"Error accessing temporary optfield when clearing data.\n");
	}

	if (providedBuffer2 == NULL)
	{
		CHECK0(bufferNew(maximumFFTByteSize, BufferTypeMain, &buffer2), 
			"Error allocating temporary buffer.\n");
		CHECK0(optfieldNew(0, 0, 0, 0, 0, 1, SampleTypeFFTWDouble, BufferTypeMain, buffer2, &kernel),
			"Error allocating optfield.\n");
	}
	else
	{
		buffer2 = providedBuffer2;
		CHECK0(optfieldNew(0, 0, 0, 0, 0, 1, SampleTypeFFTWDouble, BufferTypeMain, buffer2, &kernel),
			"Error allocating temporary optfield.\n");
		CHECK0(optfieldZeroData(kernel),
			"Error accessing temporary optfield when clearing data.\n");
	}

	
	propagationNo = 1;
	propagationCount = propagPlanPropagationCount(propagPlan);
	propagationPiece = propagPlan;

	/* 
	 * we will just add numbers, zeroing target space is at the user's responsibility

	 optfieldZeroData(target);

	 *
	 */

	memset(&lastPropagatedPiece, 0, sizeof(t_propagPlan));

	/*
	 * allocate kernel filter data
	 */
	if ((filterData_x = (double*) malloc(MAX_FILTER_WIDTH * sizeof(double))) == NULL)
		GOEXCEPTION;

	if ((filterData_y = (double*) malloc(MAX_FILTER_WIDTH * sizeof(double))) == NULL)
		GOEXCEPTION;

	optfieldGetCorner(&sourceCorner.x, &sourceCorner.y, &sourceCorner.z, source);
	optfieldGetCorner(&targetCorner.x, &targetCorner.y, &targetCorner.z, target);

	if (variablePrecision == VariablePrecisionOff)
	{
		if (propagationPrecision > 0)
		{
			optfieldFindLargestShift(source, target, &largestShift);

			filterFindUpsampling(largestShift, 
				optfieldGetSamplingdistance(source), optfieldGetSamplingdistance(source),
				filterType, filterTypeData,
				&filterUpsampling_x, &filterUpsampling_y,
				&filterSamplingDistance_x, &filterSamplingDistance_y,
				propagationPrecision);
		}
		else
		{
			filterUpsampling_x = filterUpsampling_y = (int)(-propagationPrecision+0.5);
			filterSamplingDistance_x = filterSamplingDistance_y = 
				optfieldGetSamplingdistance(source) / filterUpsampling_x;
		}

		filterCalculate(filterType, filterTypeData,
			&filterShift_x, filterUpsampling_x, filterData_x, &filterWidthHalf_x);

		filterCalculate(filterType, filterTypeData,
			&filterShift_y, filterUpsampling_y, filterData_y, &filterWidthHalf_y);
	}

	while (propagationPiece != NULL)
	{
#ifdef PROGRESS_LISTING
		LOG2("\rpropagating piece no %d of %d", propagationNo, propagationCount);
#endif
		//calculate_piece_data(lights_piece, screen_piece, orig_image_lights, orig_image_screen, propagation_piece);

		optfieldSetSamples(propagationPiece->convolutionSamples_x, propagationPiece->convolutionSamples_y, image);
		optfieldSetSamples(propagationPiece->convolutionSamples_x, propagationPiece->convolutionSamples_y, kernel);

		/* do we need new FFT of the lights? */
		if (lastPropagatedPiece.convolutionSamples_x != propagationPiece->convolutionSamples_x ||
			lastPropagatedPiece.convolutionSamples_y != propagationPiece->convolutionSamples_y ||
			lastPropagatedPiece.sourceSamples_x	     != propagationPiece->sourceSamples_x ||
			lastPropagatedPiece.sourceSamples_y	     != propagationPiece->sourceSamples_y ||
			lastPropagatedPiece.sourceStart_x		 != propagationPiece->sourceStart_x ||
			lastPropagatedPiece.sourceStart_y		 != propagationPiece->sourceStart_y ||
			lastPropagatedPiece.sourceStep_x 		 != propagationPiece->sourceStep_x ||
			lastPropagatedPiece.sourceStep_y 		 != propagationPiece->sourceStep_y)
		{
			optfieldZeroData(image);

			CHECK0(optfieldCopyData(image, 
				0, 0, 1, 1, 
				source, 
				propagationPiece->sourceStart_x, propagationPiece->sourceStart_y,
				propagationPiece->sourceStep_x, propagationPiece->sourceStep_y,
				propagationPiece->sourceSamples_x, propagationPiece->sourceSamples_y), 
				"Error copying data from the source to the temporary optfield.\n");

#ifdef PROPAGATE_DEBUG_SAVEPNG
			optfieldSavePNG(image, "image", PictureIOIntensity, 1.0, 0, NULL);
#endif
#ifdef PROPAGATE_DEBUG_PRINTIMG
			printf("\n\nSource piece:\n");
			optfieldPrint1(image, NULL, NULL, NULL);
#endif

			p = fftw_plan_dft_2d(propagationPiece->convolutionSamples_y, propagationPiece->convolutionSamples_x,
							 (fftw_complex*) image->buffer->data, (fftw_complex*) image->buffer->data, -1, FFTW_ESTIMATE);

			fftw_execute(p);

			fftw_destroy_plan(p);

			memcpy(&lastPropagatedPiece, propagationPiece, sizeof(t_propagPlan));
		}

		sourcePieceCorner.x = propagationPiece->sourceStart_x * optfieldGetSamplingdistance(source) + sourceCorner.x;
		sourcePieceCorner.y = propagationPiece->sourceStart_y * optfieldGetSamplingdistance(source) + sourceCorner.y;
		sourcePieceCorner.z = sourceCorner.z;

		targetPieceCorner.x = propagationPiece->targetStart_x * optfieldGetSamplingdistance(target) + targetCorner.x;
		targetPieceCorner.y = propagationPiece->targetStart_y * optfieldGetSamplingdistance(target) + targetCorner.y;
		targetPieceCorner.z = targetCorner.z;

		if (variablePrecision == VariablePrecisionOn)
		{
			sourcePieceEdge_x = propagationPiece->sourceSamples_x * optfieldGetSamplingdistance(source);
			sourcePieceEdge_y = propagationPiece->sourceSamples_y * optfieldGetSamplingdistance(source);

			targetPieceEdge_x = propagationPiece->targetSamples_x * optfieldGetSamplingdistance(target);
			targetPieceEdge_y = propagationPiece->targetSamples_y * optfieldGetSamplingdistance(target);


			if (propagationPrecision > 0)
			{
				findLargestShift(sourcePieceCorner, sourcePieceEdge_x, sourcePieceEdge_y,
					targetPieceCorner, targetPieceEdge_x, targetPieceEdge_y,
					&largestShift);

				filterFindUpsampling(largestShift, 
					optfieldGetSamplingdistance(source), optfieldGetSamplingdistance(source),
					filterType, filterTypeData,
					&filterUpsampling_x, &filterUpsampling_y,
					&filterSamplingDistance_x, &filterSamplingDistance_y,
					propagationPrecision);
			}
			else
			{
				filterUpsampling_x = filterUpsampling_y = (int)(-propagationPrecision+0.5);
				filterSamplingDistance_x = filterSamplingDistance_y = 
					optfieldGetSamplingdistance(source) / filterUpsampling_x;
			}


			filterCalculate(filterType, filterTypeData,
				&filterShift_x, filterUpsampling_x, filterData_x, &filterWidthHalf_x);

			filterCalculate(filterType, filterTypeData,
				&filterShift_y, filterUpsampling_y, filterData_y, &filterWidthHalf_y);
		}

#ifdef PROGRESS_LISTING
		LOG2(" (upsampling %d %d)    ", filterUpsampling_x, filterUpsampling_y);
#endif
  
		shift.x = targetPieceCorner.x - sourcePieceCorner.x;
		shift.y = targetPieceCorner.y - sourcePieceCorner.y;
		shift.z = targetPieceCorner.z - sourcePieceCorner.z;

		if (propagationMethod == PropagationRayleighSommerfeld)
			kernelCalculateRSFT(&shift, propagationPiece,
				optfieldGetSamplingdistance(source) * propagationPiece->sourceStep_x,
				optfieldGetSamplingdistance(source) * propagationPiece->sourceStep_y,
				filterShift_x, filterData_x, filterWidthHalf_x, filterSamplingDistance_x,
				filterShift_y, filterData_y, filterWidthHalf_y, filterSamplingDistance_y,
				kernel, FFTApplyOn);
		else 		
			kernelCalculateAS(&shift, propagationPiece,
				optfieldGetSamplingdistance(source) * propagationPiece->sourceStep_x,
				optfieldGetSamplingdistance(source) * propagationPiece->sourceStep_y,
				filterData_x, filterWidthHalf_x, filterUpsampling_x,
				filterData_y, filterWidthHalf_y, filterUpsampling_y,
				kernel);

		
#ifdef PROPAGATE_DEBUG_SAVEPNG
		optfieldSavePNG(kernel, "kernelfft", PictureIOIntensityPhase, 1.0, 0, NULL);
		optfieldSavePNG(image, "imagefft", PictureIOIntensityPhase, 1.0, 0, NULL);
#endif

		optfieldMulDataSparse(kernel, 0, 0, 1, 1, 
			image, 0, 0, 1, 1, 
			propagationPiece->convolutionSamples_x, propagationPiece->convolutionSamples_y);

		p = fftw_plan_dft_2d(propagationPiece->convolutionSamples_y, propagationPiece->convolutionSamples_x,
							 (fftw_complex*) kernel->buffer->data, (fftw_complex*) kernel->buffer->data, +1, FFTW_ESTIMATE);

		fftw_execute(p);

		fftw_destroy_plan(p);

#ifdef PROPAGATE_DEBUG_SAVEPNG
		optfieldSavePNG(kernel, "kernelimage", PictureIOPhase, 1.0, 0, NULL);
#endif
#ifdef PROPAGATE_DEBUG_PRINTIMG
			printf("\n\nTarget piece:\n");
			optfieldPrint1(kernel, NULL, NULL, NULL);
#endif

		CHECK0(optfieldAddDataSparse(target,
					   propagationPiece->targetStart_x, propagationPiece->targetStart_y,
					   propagationPiece->targetStep_x, propagationPiece->targetStep_y,
					   kernel,
					   0, 0, 1, 1,
					   propagationPiece->targetSamples_x, propagationPiece->targetSamples_y),
					   "Error when adding data from the temporary optfield to the target.\n");

#ifdef _DEBUG_LISTING_COMPUTATIONS
		write_fourimages(orig_image_lights, image, kernel, orig_image_screen);
#endif

		propagationNo++;
		propagationPiece = propagationPiece->next;
	}


EXCEPTION0("optfieldPropagate")

	/* do we have to free propagation list, or return the created one? */
	if (providedPropagPlan == NULL)
		propagPlanDispose(&propagPlan);
	else
		*providedPropagPlan = propagPlan;

	if (providedBuffer1 == NULL)
			bufferDispose(&(image->buffer));
		else
			image->buffer = NULL;

		optfieldDispose(&image);

	if (providedBuffer2 == NULL)
		bufferDispose(&(kernel->buffer));
	else
		kernel->buffer = NULL;

	optfieldDispose(&kernel);

	if (filterData_x != NULL) free(filterData_x);
	if (filterData_y != NULL) free(filterData_y);


#ifdef PROGRESS_LISTING
	printf("\nDone!\n");
#endif

 return RAYLEIGH_OK;

}

/* 
 * Auxiliary function
 * Find the largest distance between a corner of the source rectangle and the target rectangle.
 */
void findLargestShift(t_point sourcePieceCorner, double sourcePieceEdge_x, double sourcePieceEdge_y,
			t_point targetPieceCorner, double targetPieceEdge_x, double targetPieceEdge_y,
			t_point *largestShift)
{
	t_point shift;
	double length, largestLength;
	int sourceCorner_x, sourceCorner_y;
	int targetCorner_x, targetCorner_y;

	largestLength = 0;

	for (sourceCorner_x = 0; sourceCorner_x <= 1; sourceCorner_x++)
	for (sourceCorner_y = 0; sourceCorner_y <= 1; sourceCorner_y++)
	for (targetCorner_x = 0; targetCorner_x <= 1; targetCorner_x++)
	for (targetCorner_y = 0; targetCorner_y <= 1; targetCorner_y++)
	{
		shift.x = targetPieceCorner.x - sourcePieceCorner.x 
			- sourceCorner_x * sourcePieceEdge_x 
			+ targetCorner_x * targetPieceEdge_x;
		shift.y = targetPieceCorner.y - sourcePieceCorner.y
			- sourceCorner_y * sourcePieceEdge_y 
			+ targetCorner_y * targetPieceEdge_y;
		shift.z = targetPieceCorner.z - sourcePieceCorner.z;

		length = vectorLength(shift);

		if (length > largestLength)
		{
			largestLength = length;
			largestShift->x = shift.x;
			largestShift->y = shift.y;
			largestShift->z = shift.z;
		}
	}
}


/* 
 * Find the largest distance between a corner of the source rectangle and the target rectangle.
 */
int optfieldFindLargestShift(t_optfield *source, t_optfield *target, t_point *largestShift)
{
	t_point sourceCorner, targetCorner;
	int sourceSamples_x, sourceSamples_y;
	int targetSamples_x, targetSamples_y;
	double sourceEdge_x, sourceEdge_y; /* edge length */
	double targetEdge_x, targetEdge_y;

	CHECK(optfieldGetCorner(&sourceCorner.x, &sourceCorner.y, &sourceCorner.z, source));
	CHECK(optfieldGetCorner(&targetCorner.x, &targetCorner.y, &targetCorner.z, target));

	CHECK(optfieldGetSamples(&sourceSamples_x, &sourceSamples_y, source));
	CHECK(optfieldGetSamples(&targetSamples_x, &targetSamples_y, target));

	sourceEdge_x = sourceSamples_x * optfieldGetSamplingdistance(source);
	sourceEdge_y = sourceSamples_y * optfieldGetSamplingdistance(source);

	targetEdge_x = targetSamples_x * optfieldGetSamplingdistance(target);
	targetEdge_y = targetSamples_y * optfieldGetSamplingdistance(target);

	findLargestShift(sourceCorner, sourceEdge_x, sourceEdge_y,
		targetCorner, targetEdge_x, targetEdge_y,
		largestShift);

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}





