/*
 * for general documentation, see .h file
 * implementation details are covered here
 */

#include "rayleighCompile.h"

char *bufferTypeName[] = {"", "BufferTypeMain", "BufferTypeGPU"};


static int bufferID = 0;

/*
 * TODO: check actual memory limits
 */
RAYLEIGH_INT64 bufferGetMaxByteSize(t_BufferType bufferType)
{
	return (RAYLEIGH_INT64)16384*32768*2*sizeof(double);
}

/*
 * tries to allocate space for buffer structure as well as its data
 */
int bufferNew(RAYLEIGH_INT64 byteSize, t_BufferType bufferType, t_buffer **buffer)
{
	/* check valid pointer */
	if (buffer == NULL)
		GOEXCEPTION0("NULL buffer pointer.\n");

	/* alloc *buffer structure */
	*buffer = (t_buffer*) malloc(sizeof(t_buffer));
	if (*buffer == NULL)
		GOEXCEPTION0("Error allocating buffer structure.\n");

	/* allocate buffer data
	 * TODO: other than main memory buffers
	 */
	if (bufferType == BufferTypeMain)
		(*buffer)->data = fftw_malloc(byteSize);
	else
	{
		free(*buffer);
		*buffer = NULL;
		GOEXCEPTION0("Unsupported buffer type.\n");
	}

	/* check data allocation success */
	if ((*buffer)->data == NULL)
	{
		free(*buffer);
		*buffer = NULL;
		GOEXCEPTION0("Error allocating buffer data.\n");
	}

	/* fill the buffer structure */
	(*buffer)->type = bufferType;
	(*buffer)->byteSize = byteSize;
	(*buffer)->id = ++bufferID;

	return RAYLEIGH_OK;

EXCEPTION
	ELOG0("Error in bufferNew.\n");
	return RAYLEIGH_ERROR;
}


/*
 * attempts to free buffer resources
 */
int bufferDispose(t_buffer **buffer)
{
	/* check valid pointer */
	if (buffer == NULL)
		GOEXCEPTION0("NULL buffer pointer.\n");

	if (*buffer == NULL)
		GOEXCEPTION("NULL *buffer pointer.\n");

	/* try to free buffer data */
	if ((*buffer)->data != NULL)
	{
		if (bufferGetType(*buffer) == BufferTypeMain)
			fftw_free((*buffer)->data);
		else
			GOEXCEPTION0("Buffer empty, nothing to free. This should not happen.\n");
	}

	/* free buffer structure */
	free(*buffer);
	*buffer = NULL;

	return RAYLEIGH_OK;

EXCEPTION
	ELOG0("Error in bufferDispose.\n");
	return RAYLEIGH_ERROR;
}


/*
 * getter for buffer->type
 */
int bufferGetType(t_buffer *buffer)
{
	return buffer->type;
}


/*
 * getter for buffer->bytesize
 */
RAYLEIGH_INT64 bufferGetByteSize(t_buffer *buffer)
{
	return buffer->byteSize;
}

#ifdef _RAYLEIGH_DEBUG
/*
 * test basic buffer functionality
 * when any problem appears, exits
 */
void bufferTest(void)
{
	t_buffer *b1 = NULL, *b2 = NULL;
	RAYLEIGH_INT64 s1 = 100, s2 = (RAYLEIGH_INT64)2 >> 32;

	CHECK(bufferNew(s1, BufferTypeMain, &b1));
	CHECK(bufferNew(s2, BufferTypeMain, &b2));
	if (bufferGetType(b1) != BufferTypeMain) GOEXCEPTION;
	if (bufferGetByteSize(b2) != s2) GOEXCEPTION;
	CHECK(bufferDispose(&b1));
	CHECK(bufferDispose(&b2));

	LOG0("Buffer test OK\n");
	return;

EXCEPTION
	ELOG0("Buffer implementation problems!\n");
	exit(0);
}
#endif
