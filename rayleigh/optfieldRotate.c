/*
 * Rotation of the optical field.
 * Not yet ready to use, although the main functionality is OK.
 */

#include "rayleighCompile.h"
#include "optfieldRotate.h"


/*
 * Shifts optical field by (x, y) samples in the frequency domain
 * i.e. multiplies its frequency domain by a complex exponential.
 * It is assumed that the optfield data are in frequency domain.
 */
int optfieldShiftFT(t_optfield *source, t_optfield *target, int shift_x, int shift_y)
{
	double fx, fy, arg, argy, s, c, tmp;
	int row, col, index;
	fftw_complex *sourceData, *targetData;

	int sourceSamplesX, sourceSamplesY;
	int targetSamplesX, targetSamplesY;

	CHECK(optfieldGetSamples(&sourceSamplesX, &sourceSamplesY, source));
	CHECK(optfieldGetSamples(&targetSamplesX, &targetSamplesY, target));

	sourceData = (fftw_complex*) source->buffer->data;
	targetData = (fftw_complex*) target->buffer->data;

	if (sourceSamplesX != targetSamplesX ||
		sourceSamplesY != targetSamplesY)
		GOEXCEPTION;

	/* spatial frequency multiplier */
	fx = -2 * PI * (double)shift_x/(double)(sourceSamplesX);
	fy = -2 * PI * (double)shift_y/(double)(sourceSamplesY);

	/* optical field modification */
	for(row = 0, index = 0; row < targetSamplesY; row++)
	{
		argy = fy * row;
		for(col = 0; col < targetSamplesX; col++, index++)
		{
			arg = fx * col + argy;
			c = cos(arg);
			s = sin(arg);
			tmp = sourceData[index][RE] * c - sourceData[index][IM] * s;
			targetData[index][IM] = sourceData[index][RE] * s + sourceData[index][IM] * c;
			targetData[index][RE] = tmp;
		}
	}

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}



int optfieldMultReal(t_optfield *source, t_optfield *target, double mult)
{
	int row, col, index;
	fftw_complex *sourceData, *targetData;

	int sourceSamplesX, sourceSamplesY;
	int targetSamplesX, targetSamplesY;

	CHECK(optfieldGetSamples(&sourceSamplesX, &sourceSamplesY, source));
	CHECK(optfieldGetSamples(&targetSamplesX, &targetSamplesY, target));

	sourceData = (fftw_complex*) source->buffer->data;
	targetData = (fftw_complex*) target->buffer->data;

	if (sourceSamplesX != targetSamplesX ||
		sourceSamplesY != targetSamplesY)
		GOEXCEPTION;

	/* optical field modification */
	for(row = 0, index = 0; row < targetSamplesY; row++)
	{
		for(col = 0; col < targetSamplesX; col++, index++)
		{
			targetData[index][RE] = sourceData[index][RE] * mult;
			targetData[index][IM] = sourceData[index][IM] * mult;
		}
	}

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}



void calculateIndex(int *index1, int *index2, double *s, double index, int fieldSamples)
{
	int i1, i2;

	i1 = (int) (floor(index));
	i2 = i1+1;
	*s = index - i1;

	if (i1 < 0) i1 += fieldSamples;
	if (i2 < 0) i2 += fieldSamples;

	*index1 = i1;
	*index2 = i2;
}


int optfieldRotate(t_optfield *source, t_optfield *target,
				   double angleX, double angleY, double angleZ,
				   int applyFFT, int applyIFFT)
{
	int sourceSamplesX, sourceSamplesY;
	int targetSamplesX, targetSamplesY;
	int halfSamplesX, halfSamplesY;
	int fieldSamplesX, fieldSamplesY;
	double *rotationMatrix = NULL, *rotationInvMatrix = NULL;
	double fx0s, fy0s, fz0s;
	double fxs, fys, fzs;
	int row, col, px, py;
	int index, indexC1, indexC2, indexR1, indexR2;
	double s, t;
	int sgn;
	double fx, fy, fz;
	double corr;
	double indexToFreqX, indexToFreqY, freqToIndexX, freqToIndexY;
	double sampleSizeX, sampleSizeY;
	double real, imag;
	t_optfield *original = NULL, *rotated = NULL;
	fftw_complex *originalData, *rotatedData;
	fftw_plan p;

	CHECK(optfieldGetSamples(&sourceSamplesX, &sourceSamplesY, source));
	CHECK(optfieldGetSamples(&targetSamplesX, &targetSamplesY, target));


	matrix33Rotate(&rotationMatrix, angleX, angleY, angleZ);
	matrix33Transpose(&rotationInvMatrix, rotationMatrix);

	fieldSamplesX = 2*sourceSamplesX;
	fieldSamplesY = 2*sourceSamplesY;
	halfSamplesX = sourceSamplesX;
	halfSamplesY = sourceSamplesY;

	sampleSizeX = optfieldGetSamplingdistance(source);
	sampleSizeY = optfieldGetSamplingdistance(source);

	freqToIndexX = fieldSamplesX * sampleSizeX;
	freqToIndexY = fieldSamplesY * sampleSizeY;
	indexToFreqX = 1/freqToIndexX;
	indexToFreqY = 1/freqToIndexY;

	CHECK(optfieldNew(0, 0, 0, fieldSamplesX, fieldSamplesY, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &original));
	CHECK(optfieldNew(0, 0, 0, fieldSamplesX, fieldSamplesY, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &rotated));

	optfieldCopyData(original, 0, 0, 1, 1, source, 0, 0, 1, 1, sourceSamplesX, sourceSamplesY);

	originalData = (fftw_complex*)original->buffer->data;
	rotatedData = (fftw_complex*)rotated->buffer->data;

	optfieldSavePNG(original, "originalXY", PictureIORealImagIntensity, 1.0, 0, NULL);

	p = fftw_plan_dft_2d(fieldSamplesY, fieldSamplesX,
		 originalData, originalData, 1, FFTW_ESTIMATE);
	fftw_execute(p);
	fftw_destroy_plan(p);

	optfieldShiftFT(original, original, halfSamplesX/2, halfSamplesY/2);

	optfieldSavePNG(original, "originalFXFY", PictureIORealImagIntensity, 1.0, 0, NULL);

	/*
	 * spatial frequencies in the origin after the rotation:
	 * [fx0s, fy0s, fz0s]T = rotationMatrix * [0, 0, 1]T
	 */

	fx0s = rotationMatrix[2] / lambda;
	fy0s = rotationMatrix[5] / lambda;
	fz0s = rotationMatrix[8] / lambda;
	if(fz0s < 0){
		fx0s *= -1;
		fy0s *= -1;
	}

	for(row = 0, index = 0; row < fieldSamplesY; row++)
	{
		/* unfold the index */
		if (row < halfSamplesY)
			py = row;
		else
			py = row - fieldSamplesY;

		/* spatial frequency */
		fy = py * indexToFreqY;

		for (col = 0; col < fieldSamplesX; col++, index++)
		{
			/* unfold the index */
			if (col < halfSamplesX)
				px = col;
			else
				px = col - fieldSamplesX;

			/* spatial frequency */
			fx = px * indexToFreqX;

			/* compute fz such that |(fx, fy, fz)| = 1/lambda */
			fz = 1.0/(lambda * lambda) - (fx * fx + fy * fy);
			if (fz <= 0)
			{
				rotatedData[index][RE] = 0.0;
				rotatedData[index][IM] = 0.0;
			}
			else
			{
				fz = sqrt(fz);

				/* rotate (fx, fy, fz) vector */
				fxs = rotationMatrix[0] * fx + rotationMatrix[1] * fy + rotationMatrix[2] * fz;
				fys = rotationMatrix[3] * fx + rotationMatrix[4] * fy + rotationMatrix[5] * fz;
				fzs = rotationMatrix[6] * fx + rotationMatrix[7] * fy + rotationMatrix[8] * fz;

				/* flip the (fx,fy,fz) vector to fz>0 */
				if(fzs < 0){
					sgn = -1;
					fxs = -fxs;
					fys = -fys;
				}
				else{
					sgn = 1;
				}

				/* convert frequency to index */
				calculateIndex(&indexC1, &indexC2, &s, fxs*freqToIndexX, fieldSamplesX);
				calculateIndex(&indexR1, &indexR2, &t, fys*freqToIndexY, fieldSamplesY);

				/* bilinear interpolation */
				real = (1 - t) * ((1 - s) * originalData[indexR1 * fieldSamplesX + indexC1][RE] +
									   s  * originalData[indexR1 * fieldSamplesX + indexC2][RE]) +
							t  * ((1 - s) * originalData[indexR2 * fieldSamplesX + indexC1][RE] +
									   s  * originalData[indexR2 * fieldSamplesX + indexC2][RE]);
				imag = (1 - t) * ((1 - s) * originalData[indexR1 * fieldSamplesX + indexC1][IM] +
									   s  * originalData[indexR1 * fieldSamplesX + indexC2][IM]) +
							t  * ((1 - s) * originalData[indexR2 * fieldSamplesX + indexC1][IM] +
									   s  * originalData[indexR2 * fieldSamplesX + indexC2][IM]);
				imag *= sgn;

				/* energy correction to accomodate non-zero area of (fx,fy) on the Ewald sphere etc. */
				corr = fz / fzs *  /* energy correction */
					   (           /* jacobian */
						(rotationMatrix[0] - rotationMatrix[2] * fx / fz) * (rotationMatrix[4] - rotationMatrix[5] * fy / fz) -
						(rotationMatrix[1] - rotationMatrix[2] * fy / fz) * (rotationMatrix[3] - rotationMatrix[5] * fx / fz)
					   );


				//vynasobeni hodnotou upravujici vystup
				rotatedData[index][RE] = real * corr;
				rotatedData[index][IM] = imag * corr;

				imag = imag;

			}
		}
	}

	optfieldSavePNG(rotated, "rotatedFXFY", PictureIORealImagIntensity, 1.0, 0, NULL);

	optfieldShiftFT(rotated, rotated, -halfSamplesX/2, -halfSamplesY/2);

	p = fftw_plan_dft_2d(fieldSamplesY, fieldSamplesX,
		 rotatedData, rotatedData, -1, FFTW_ESTIMATE);
	fftw_execute(p);
	fftw_destroy_plan(p);

	optfieldMultReal(rotated, rotated, 1/((double)fieldSamplesX * (double)fieldSamplesY));

	optfieldSavePNG(rotated, "rotatedXY", PictureIORealImagIntensity, 1.0, 0, NULL);

	optfieldCopyData(target, 0, 0, 1, 1, rotated, 0, 0, 1, 1, sourceSamplesX, sourceSamplesY);

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}

void test_optfieldRotate(void)
{
	t_optfield *source = NULL, *target = NULL;
	int rows = 300, columns = 300;
	int i, j, index;

	optfieldNew(0, 0, 0, rows, columns, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &source);
	optfieldNew(0, 0, 0, rows, columns, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &target);


	for (i=0, index=0; i<rows; i++)
		for (j=0; j<columns; j++, index++)
			((fftw_complex*)source->buffer->data)[index][0] = 1;

	((fftw_complex*)source->buffer->data)[rows/5*columns+columns/5][0] = 1;

	optfieldSavePNG(source, "src", PictureIORealImagIntensity, 1.0, 1.5, NULL);

	optfieldRotate(source, target, 0, 45*PI/180, 0, 0, 0);

	optfieldSavePNG(target, "tgt", PictureIORealImagIntensity, 1.0, 1.5, NULL);

	optfieldRotate(target, source, 0, -45*PI/180, 0, 0, 0);

	optfieldSavePNG(source, "tgt2", PictureIORealImagIntensity, 1.0, 1.5, NULL);

	/*
	p = fftw_plan_dft_2d(256, 256,
		source->buffer->data, target->buffer->data, 1, FFTW_ESTIMATE);
	fftw_execute(p);
	fftw_destroy_plan(p);

	optfieldSavePNG(target, "src_ft", PictureIORealImagIntensity, 1.0, 0, NULL);
	optfieldShiftFT(target, target, 2, 2);
	optfieldSavePNG(target, "tgt_ft", PictureIORealImagIntensity, 1.0, 0, NULL);

	p = fftw_plan_dft_2d(256, 256,
		target->buffer->data, target->buffer->data, -1, FFTW_ESTIMATE);
	fftw_execute(p);
	fftw_destroy_plan(p);

	optfieldSavePNG(target, "tgt", PictureIORealImagIntensity, 1.0, 0, NULL);
	*/

	optfieldDispose(&source);
	optfieldDispose(&target);
}
