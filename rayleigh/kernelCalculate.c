/*
 * Calculate propagation kernels for various propagation methods.
 */

#include "rayleighCompile.h"

/* Various debugging flags */
#define NO_PROPAGATE_DEBUG_SAVEPNG
#define NO_PROPAGATE_DEBUG_PRINTIMG
#define NO_DEBUG_KERNEL

#define NO_DEBUG_LISTING
#define NO_DEBUG_LISTING_COMPUTATIONS
#define NO_DEBUG_LISTING_SCHEMA
#define PROGRESS_LISTING
#define NO_DEBUG_SAVE_INTERMEDIATES
#define _DEBUG_SAVETYPE		PICTURE_TEXT, 5.2, 0.0


/*
 * Calculate kernel for source->target propagation.
 * No interpolation is evaluated.
 * The kernel shape (R-S, Fresnel, etc.) if given through waveLUT.
 */
int kernelCalculateDLUTFast(t_optfield *kernel, t_optfield *source, t_optfield *target, t_rhoLUT *rhoLUT, t_waveLUT *waveLUT)
{
	int kernelSamplesX, kernelSamplesY;
	int sourceSamplesX, sourceSamplesY;
	int targetSamplesX, targetSamplesY;
	int yIndex, xIndex, xDiff, yDiff, index;
	t_point sourceCorner, targetCorner;
	int xShift, yShift;
	// double x, y;
	double rho;
	int indexWave;
	fftw_complex* kernelData;

	optfieldGetSamples(&kernelSamplesX, &kernelSamplesY, kernel);
	optfieldGetSamples(&sourceSamplesX, &sourceSamplesY, source);
	optfieldGetSamples(&targetSamplesX, &targetSamplesY, target);

	if (optfieldGetSampling(source) != optfieldGetSampling(target))
	{
		ELOG0("Source and target sampling rates must be the same.");
		GOEXCEPTION;
	}

	optfieldGetCorner(&(sourceCorner.x), &(sourceCorner.y), &(sourceCorner.z), source);
	optfieldGetCorner(&(targetCorner.x), &(targetCorner.y), &(targetCorner.z), target);

	xShift = (int) floor((targetCorner.x - sourceCorner.x) / optfieldGetSamplingdistance(source) + 0.5);
	yShift = (int) floor((targetCorner.y - sourceCorner.y) / optfieldGetSamplingdistance(source) + 0.5);

	kernelData = (fftw_complex*)kernel->buffer->data;

	index = 0;
	for (yIndex=0; yIndex<kernelSamplesY; yIndex++)
	{
		if (yIndex < targetSamplesY)
			yDiff = yIndex + yShift;
		else if (yIndex >= kernelSamplesY - sourceSamplesY)
			yDiff = kernelSamplesY - yIndex - yShift;
		else
			yDiff = 0;

		yDiff = abs(yDiff);

		for (xIndex=0; xIndex<kernelSamplesX; xIndex++, index++)
		{
			if (xIndex < targetSamplesX)
				xDiff = xIndex + xShift;
			else if (xIndex >= kernelSamplesX - sourceSamplesX)
				xDiff = kernelSamplesX - xIndex - xShift;
			else
				xDiff = 0;

			xDiff = abs(xDiff);

			rho = rhoLUT->rho[xDiff + yDiff*rhoLUT->samplesX];
			indexWave = (int)floor((rho - waveLUT->rhoMin)/waveLUT->rhoSamplingDistance + 0.5);
			if (indexWave < waveLUT->samplesRho)
			{
				kernelData[index][RE] = waveLUT->phasor[indexWave][RE];
				kernelData[index][IM] = waveLUT->phasor[indexWave][IM];
			}
			else
			{
				kernelData[index][RE] = 0;
				kernelData[index][IM] = 0;
			}
		}
	}
	return RAYLEIGH_OK;

EXCEPTION

	ELOG0("Error in calling kernelCalculateFast.\n");
	return RAYLEIGH_ERROR;
}


/*
/*
 * Calculate kernel for source->target propagation.
 * rhoLUT/waveLUT entries are interpolated during kernel evaluation.
 * The kernel shape (R-S, Fresnel, etc.) if given through waveLUT.
 *
 * Internal function
 * ALMOST NO CHECKS OF PARAMETERS VALIDITY!
 *
 */
int kernelCalculateDLUT(t_optfield *kernel, t_optfield *source, t_optfield *target,
							 t_rhoLUT *rhoLUT, const int rhoLUTFilterType,
							 t_waveLUT *waveLUT, const int waveLUTFilterType)
{
	int kernelSamplesX, kernelSamplesY;
	int sourceSamplesX, sourceSamplesY;
	int targetSamplesX, targetSamplesY;
	int yIndex, xIndex, index;
	t_point sourceCorner, targetCorner;
	double xShift, yShift;
	double xDiff, yDiff, xDiffFrac = 0, yDiffFrac = 0;
	int xDiffInt, yDiffInt;
	// double x, y;
	double rho, rho00, rho01, rho10, rho11;
	int indexWaveInt;
	double indexWaveDbl, indexWaveFrac;
	fftw_complex* kernelData;

	if (rhoLUTFilterType != FilterTypeConstant &&
		rhoLUTFilterType != FilterTypeTriangular)
	{
		GOEXCEPTION0("Unsupported filter type for rhoLUT interpolation.\n");
	}

	if (waveLUTFilterType != FilterTypeConstant &&
		waveLUTFilterType != FilterTypeTriangular &&
		waveLUTFilterType != FilterTypeCubicStd)
	{
		GOEXCEPTION0("Unsupported filter type for rhoLUT interpolation.\n");
	}

	/* get number of samples and do a sanity check */
	CHECK0(optfieldGetSamples(&kernelSamplesX, &kernelSamplesY, kernel), "Weird kernel optfield.\n");
	CHECK0(optfieldGetSamples(&sourceSamplesX, &sourceSamplesY, source), "Weird source optfield.\n");
	CHECK0(optfieldGetSamples(&targetSamplesX, &targetSamplesY, target), "Weird target optfield.\n");

	/* test if sampling distances of the source and the target are the same
	 * in fact, sampling distance of the kernel is not important as it serves
	 * as a memory space only
	 */
	if (optfieldGetSampling(source) != optfieldGetSampling(target))
	{
		GOEXCEPTION0("Source and target sampling rates must be the same.");
	}

	if (optfieldGetSampling(source) == 0)
	{
		GOEXCEPTION0("Source/target sampling rate must be different from 0.");
	}

	/* calculate vector (xShift, yShift, ?)
	 * from the corner of the source to the corner of the target
	 *
	 * recalculate it immediately so that it is expressed in the number of sampling intervals
	 */
	optfieldGetCorner(&(sourceCorner.x), &(sourceCorner.y), &(sourceCorner.z), source);
	optfieldGetCorner(&(targetCorner.x), &(targetCorner.y), &(targetCorner.z), target);

	xShift = (targetCorner.x - sourceCorner.x) / optfieldGetSamplingdistance(source);
	yShift = (targetCorner.y - sourceCorner.y) / optfieldGetSamplingdistance(source);

	/* we will access kernelData often, store the pointer to the local variable */
	kernelData = (fftw_complex*)kernel->buffer->data;

	/* calculate the kernel row by row, colomn by column;
	 * the "index" variable incrementally traverses all the kernel samples
	 */
	index = 0;

	for (yIndex=0; yIndex<kernelSamplesY; yIndex++)
	{
		/* Decide if we are in the "positive" or the "negative" part of the kernel
		 * The biggest trick is to treat yShift correctly.
		 * Note that we can sum yIndex + yShift as the yShift is expressed in sampling distance
		 */
		if (yIndex < targetSamplesY)
		{
			yDiff = yIndex + yShift;
		}
		else if (yIndex >= kernelSamplesY - sourceSamplesY)
		{
			yDiff = kernelSamplesY - yIndex - yShift;
		}
		else
		{
			/* If the kernel size is bigger than sourceSamples + targetSamples - 1,
			 * it contains values that are actually not used in the final target due to
			 * convolution properties.
			 * Set these kernel samples to 0 to make sure there is no INF, NaN or whatever.
			 *
			 * It might be better to try to set the values so that the FFT(kernel) leads
			 * to "nicer numbers"; such values might be e.g. interpolation between valid parts
			 * of the kernel. Not implemented.
			 */

			/* This is obvious method hot to zero the whole line:
			 *
			for (xIndex=0; xIndex<kernelSamplesX; xIndex++, index++)
			{
				kernelData[index][RE] = 0;
				kernelData[index][IM] = 0;
			}
			*/

			/* This is the same: */
			memset((void*)(kernelData+index), 0, kernelSamplesX * sizeof(fftw_complex));
			index += kernelSamplesX;

			/* Continue with the next line. */
			continue;
		}

		/* We will address rhoLUT by yDiff, xDiff; as the rhoLUT saves only
		 * positive xDiff, yDiff entries due to symmetry, make is positive.
		 * Also, change the sampling rate to that of the rhoLUT.
		 */
		yDiff = fabs(yDiff) * optfieldGetSamplingdistance(kernel) / rhoLUTGetSamplingDistance(rhoLUT);


		if (rhoLUTFilterType == FilterTypeConstant)
		{
			yDiffInt = (int) floor(yDiff + 0.5);
		}
		else
		{
			/* bilinear interpolation */
			/* yDiff holds the non-integer row index to the kernel.
			 * Take the integer and the fractional part of it for interpolation.
			 */
			yDiffFrac = yDiff - floor(yDiff);
			yDiffInt = (int) floor(yDiff);
		}


		/* Iterate all columns */
		for (xIndex=0; xIndex<kernelSamplesX; xIndex++, index++)
		{
			/* The same applies as for the y part of the double loop. */
			if (xIndex < targetSamplesX)
			{
				xDiff = xIndex + xShift;
			}
			else if (xIndex >= kernelSamplesX - sourceSamplesX)
			{
				xDiff = kernelSamplesX - xIndex - xShift;
			}
			else
			{
				/* Set the particular sample to zero if we are between the "positive" and the "negative" part of the kernel.*/
				kernelData[index][RE] = 0;
				kernelData[index][IM] = 0;

				/* Continue with next column. */
				continue;
			}

			/* The same applies as for the y part of the interpolation decision.
			   Additionally, do the interpolation as we have both xDiffInt, yDiffInt.
			 */
			xDiff = fabs(xDiff) * optfieldGetSamplingdistance(kernel) / rhoLUTGetSamplingDistance(rhoLUT);

			if (rhoLUTFilterType == FilterTypeConstant)
			{
				xDiffInt = (int) floor(xDiff + 0.5);
				rho = rhoLUT->rho[xDiffInt + yDiffInt*rhoLUT->samplesX];
			}
			else
			{
				/* bilinear interpolation */

				/* xDiff holds the non-integer row index to the kernel.
				 * Make it positive and change the sampling distace to that of the rhoLUT.
				 * Take the integer and the fractional part of it for interpolation.
				 */
				xDiffFrac = xDiff - floor(xDiff);
				xDiffInt = (int) floor(xDiff);

				rho00 = rhoLUT->rho[xDiffInt + yDiffInt*rhoLUT->samplesX];
				rho01 = rhoLUT->rho[xDiffInt + (yDiffInt+1)*rhoLUT->samplesX];
				rho10 = rhoLUT->rho[xDiffInt + 1 + yDiffInt*rhoLUT->samplesX];
				rho11 = rhoLUT->rho[xDiffInt + 1 + (yDiffInt+1)*rhoLUT->samplesX];
				rho = bilinearInterp(rho00, rho01, rho10, rho11, xDiffFrac, yDiffFrac);
			}

			/* Now we have rho calculated.
			 * Use it as an index to the waveLUT.
			 */
			indexWaveDbl = (rho - waveLUT->rhoMin)/waveLUT->rhoSamplingDistance;

			/* Again, decide how to interpolate the values of the waveLUT. */
			if (waveLUTFilterType == FilterTypeConstant)
			{
				indexWaveInt = (int)floor(indexWaveDbl + 0.5);
				if (indexWaveInt < waveLUT->samplesRho)
				{
					kernelData[index][RE] = waveLUT->phasor[indexWaveInt][RE];
					kernelData[index][IM] = waveLUT->phasor[indexWaveInt][IM];
				}
				else
				{
					kernelData[index][RE] = 0;
					kernelData[index][IM] = 0;
				}
			}
			else if (waveLUTFilterType == FilterTypeTriangular)
			{
				indexWaveInt = (int)floor(indexWaveDbl);
				indexWaveFrac = indexWaveDbl - floor(indexWaveDbl);
				if (indexWaveInt < waveLUT->samplesRho - 1)
				{
					kernelData[index][RE] = waveLUT->phasor[indexWaveInt][RE]
											+ indexWaveFrac*(waveLUT->phasor[indexWaveInt+1][RE]-waveLUT->phasor[indexWaveInt][RE]);
					kernelData[index][IM] = waveLUT->phasor[indexWaveInt][IM]
											+ indexWaveFrac*(waveLUT->phasor[indexWaveInt+1][IM]-waveLUT->phasor[indexWaveInt][IM]);
				}
				else
				{
					kernelData[index][RE] = 0;
					kernelData[index][IM] = 0;
				}
			}
			else if (waveLUTFilterType == FilterTypeCubicStd)
			{
				indexWaveInt = (int)floor(indexWaveDbl);
				indexWaveFrac = indexWaveDbl - floor(indexWaveDbl);
				if (indexWaveInt == 0)
				{
					kernelData[index][RE] = cubicInterp(waveLUT->phasor[indexWaveInt+1][RE],
														waveLUT->phasor[indexWaveInt][RE],
														waveLUT->phasor[indexWaveInt+1][RE],
														waveLUT->phasor[indexWaveInt+2][RE],
														indexWaveFrac);
					kernelData[index][IM] = cubicInterp(waveLUT->phasor[indexWaveInt+1][IM],
														waveLUT->phasor[indexWaveInt][IM],
														waveLUT->phasor[indexWaveInt+1][IM],
														waveLUT->phasor[indexWaveInt+2][IM],
														indexWaveFrac);
				}
				else if (indexWaveInt < waveLUT->samplesRho - 2)
				{
					kernelData[index][RE] = cubicInterp(waveLUT->phasor[indexWaveInt-1][RE],
														waveLUT->phasor[indexWaveInt][RE],
														waveLUT->phasor[indexWaveInt+1][RE],
														waveLUT->phasor[indexWaveInt+2][RE],
														indexWaveFrac);
					kernelData[index][IM] = cubicInterp(waveLUT->phasor[indexWaveInt-1][IM],
														waveLUT->phasor[indexWaveInt][IM],
														waveLUT->phasor[indexWaveInt+1][IM],
														waveLUT->phasor[indexWaveInt+2][IM],
														indexWaveFrac);
				}
				else
				{
					kernelData[index][RE] = 0;
					kernelData[index][IM] = 0;
				}
			}
		}
	}
	return RAYLEIGH_OK;

EXCEPTION

	ELOG0("Error in calling kernelCalculateFastInterp.\n");
	return RAYLEIGH_ERROR;
}


/*
 * Calculate kernel for source->target propagation.
 * WaveLUT entries are interpolated during kernel evaluation.
 * rho (i.e. x^2+y^2) is evaluated directly, i.e. no rhoLUT is necessary.
 *
 * The kernel shape (R-S, Fresnel, etc.) if given through waveLUT.
 */
int kernelCalculateWaveLUT(t_optfield *kernel, t_optfield *source, t_optfield *target,
						   t_waveLUT *waveLUT, const int waveLUTFilterType)
{
	int kernelSamplesX, kernelSamplesY;
	int sourceSamplesX, sourceSamplesY;
	int targetSamplesX, targetSamplesY;
	int yIndex, xIndex, index;
	t_point sourceCorner, targetCorner;
	double samplingDistance;
	t_point shift;
	double rhoY, rhoY2, rhoX, rho;
	int indexWaveInt;
	double indexWaveDbl, indexWaveFrac;
	fftw_complex* kernelData;


	/* get number of samples and do a sanity check */
	CHECK0(optfieldGetSamples(&kernelSamplesX, &kernelSamplesY, kernel), "Weird kernel optfield.\n");
	CHECK0(optfieldGetSamples(&sourceSamplesX, &sourceSamplesY, source), "Weird source optfield.\n");
	CHECK0(optfieldGetSamples(&targetSamplesX, &targetSamplesY, target), "Weird target optfield.\n");

	/* test if sampling distances of the source and the target are the same
	 * in fact, sampling distance of the kernel is not important as it serves
	 * as a memory space only
	 */
	if (optfieldGetSampling(source) != optfieldGetSampling(target))
	{
		GOEXCEPTION0("Source and target sampling rates must be the same.");
	}

	if (optfieldGetSampling(source) == 0)
	{
		GOEXCEPTION0("Source/target sampling rate must be different from 0.");
	}

	samplingDistance = optfieldGetSamplingdistance(source);

	/* calculate shift vector
	 * from the corner of the source to the corner of the target
	 */
	optfieldGetCorner(&(sourceCorner.x), &(sourceCorner.y), &(sourceCorner.z), source);
	optfieldGetCorner(&(targetCorner.x), &(targetCorner.y), &(targetCorner.z), target);
	vectorSub(&shift, &targetCorner, &sourceCorner);

	/* we will access kernelData often, store the pointer to the local variable */
	kernelData = (fftw_complex*)kernel->buffer->data;

	/* calculate the kernel row by row, colomn by column;
	 * the "index" variable incrementally traverses all the kernel samples
	 */
	index = 0;

	for (yIndex=0; yIndex<kernelSamplesY; yIndex++)
	{
		/* Decide if we are in the "positive" or the "negative" part of the kernel
		 * The biggest trick is to treat yShift correctly.
		 * Note that we can sum yIndex + yShift as the yShift is expressed in sampling distance
		 */
		if (yIndex < targetSamplesY)
		{
			rhoY = yIndex * samplingDistance + shift.y;
		}
		else if (yIndex >= kernelSamplesY - sourceSamplesY)
		{
			rhoY = (yIndex - kernelSamplesY) * samplingDistance + shift.y;
		}
		else
		{
			/* If the kernel size is bigger than sourceSamples + targetSamples - 1,
			 * it contains values that are actually not used in the final target due to
			 * convolution properties.
			 * Set these kernel samples to 0 to make sure there is no INF, NaN or whatever.
			 *
			 * It might be better to try to set the values so that the FFT(kernel) leads
			 * to "nicer numbers"; such values might be e.g. interpolation between valid parts
			 * of the kernel. Not implemented.
			 */

			/* This is obvious method how to zero the whole line:
			 *
			for (xIndex=0; xIndex<kernelSamplesX; xIndex++, index++)
			{
				kernelData[index][RE] = 0;
				kernelData[index][IM] = 0;
			}
			*/

			/* This is the same: */
			memset((void*)(kernelData+index), 0, kernelSamplesX * sizeof(fftw_complex));
			index += kernelSamplesX;

			/* Continue with the next line. */
			continue;
		}

		rhoY2 = rhoY*rhoY;

		/* Iterate all columns */
		for (xIndex=0; xIndex<kernelSamplesX; xIndex++, index++)
		{
			/* The same applies as for the y part of the double loop. */
			if (xIndex < targetSamplesX)
			{
				rhoX = xIndex * samplingDistance + shift.x;
			}
			else if (xIndex >= kernelSamplesX - sourceSamplesX)
			{
				rhoX = (xIndex - kernelSamplesX) * samplingDistance + shift.x;
			}
			else
			{
				/* Set the particular sample to zero if we are between the "positive" and the "negative" part of the kernel.*/
				kernelData[index][RE] = 0;
				kernelData[index][IM] = 0;

				/* Continue with next column. */
				continue;
			}


			rho = sqrt(rhoX*rhoX + rhoY2);

			/* Now we have rho calculated.
			 * Use it as an index to the waveLUT.
			 */
			indexWaveDbl = (rho - waveLUT->rhoMin)/waveLUT->rhoSamplingDistance;

			/* Again, decide how to interpolate the values of the waveLUT. */
			if (waveLUTFilterType == FilterTypeConstant)
			{
				indexWaveInt = (int)floor(indexWaveDbl + 0.5);
				if (indexWaveInt < waveLUT->samplesRho)
				{
					kernelData[index][RE] = waveLUT->phasor[indexWaveInt][RE];
					kernelData[index][IM] = waveLUT->phasor[indexWaveInt][IM];
				}
				else
				{
					kernelData[index][RE] = 0;
					kernelData[index][IM] = 0;
				}
			}
			else if (waveLUTFilterType == FilterTypeTriangular)
			{
				indexWaveInt = (int)floor(indexWaveDbl);
				indexWaveFrac = indexWaveDbl - floor(indexWaveDbl);
				if (indexWaveInt < waveLUT->samplesRho - 1)
				{
					kernelData[index][RE] = waveLUT->phasor[indexWaveInt][RE]
											+ indexWaveFrac*(waveLUT->phasor[indexWaveInt+1][RE]-waveLUT->phasor[indexWaveInt][RE]);
					kernelData[index][IM] = waveLUT->phasor[indexWaveInt][IM]
											+ indexWaveFrac*(waveLUT->phasor[indexWaveInt+1][IM]-waveLUT->phasor[indexWaveInt][IM]);
				}
				else
				{
					kernelData[index][RE] = 0;
					kernelData[index][IM] = 0;
				}
			}
		}
	}
	return RAYLEIGH_OK;

EXCEPTION

	ELOG0("Error in calling kernelCalculateFastInterp.\n");
	return RAYLEIGH_ERROR;
}

/*
 * Calculate the filtered Raylegh-Sommerfeld convolution kernel
 * for the light propagation.
 * If desired, the function returns FFT of the kernel.
 */
int kernelCalculateRSFT(t_point *global_shift,
					t_propagPlan *propagationPiece,
					double samplingDistance_x, double samplingDistance_y,
					int filterShift_x, double *filterData_x, int filterWidthHalf_x, double filterSamplingDistance_x,
					int filterShift_y, double *filterData_y, int filterWidthHalf_y, double filterSamplingDistance_y,
					t_optfield *kernel, t_FFTApply applyFFT)
{
	t_point r_filter, r_orig, shift;
	/* int half_resolution_x, half_resolution_y; */
	int filter_x, filter_y;
	int filterWidth_x = 0, filterWidth_y = 0;
	int i, j, k, index;
	int kernelSamples_x, kernelSamples_y;
	int sourceSamples_x, sourceSamples_y;
	int targetSamples_x, targetSamples_y;
	double temp_r2z, temp_r2yz, temp_r2, temp, temp_exponent;
	double sgn;
	fftw_plan p;
	T_SAMPLE_TYPE_FFTWDOUBLE *data, filteredKernelValue;
	T_SAMPLE_TYPE_FFTWDOUBLE *upsampledLinePiece = NULL, **downsampledLine = NULL, **downsampledLineBuffer = NULL;
	int firstFilterSample_x = 0, firstFilterSample_y = 0;

	//char filename[1000];

	INITEXCEPTION;

	/* There were some debugging problems with parameter passing in VS2008.
	 * This copy is just for debug watch.
	 * There should be no problem to pass shift vector directly.
	 */
	shift.x = global_shift->x;
	shift.y = global_shift->y;
	shift.z = global_shift->z;

	CHECK0(optfieldGetSamples(&kernelSamples_x, &kernelSamples_y, kernel),
		"Error accessing kernel optfield structure.\n");
	CHECK0(optfieldZeroData(kernel),
		"Error cleaning kernel optfield data.\n");

	if (propagationPiece == NULL)
	{
		GOEXCEPTION0("NULL propagation piece.\n");
	}

	sourceSamples_x = propagationPiece->sourceSamples_x;
	sourceSamples_y = propagationPiece->sourceSamples_y;

	targetSamples_x = propagationPiece->targetSamples_x;
	targetSamples_y = propagationPiece->targetSamples_y;

	/* TODO
	 * handling of different sample types
	 */

	if (kernel->buffer != NULL &&
		kernel->buffer->data != NULL)
	{
		data = (T_SAMPLE_TYPE_FFTWDOUBLE*) kernel->buffer->data;
	}
	else
	{
		GOEXCEPTION0("No buffer in the kernel optfield.\n");
	}

	filterWidth_x = 1 + 2*filterWidthHalf_x;
	filterWidth_y = 1 + 2*filterWidthHalf_y;

	upsampledLinePiece = (T_SAMPLE_TYPE_FFTWDOUBLE*) malloc(filterWidth_x * sizeof(T_SAMPLE_TYPE_FFTWDOUBLE));
	if (upsampledLinePiece == NULL)
	{
		GOEXCEPTION0("Error allocating temporary line buffers.\n");
	}

	downsampledLineBuffer = (T_SAMPLE_TYPE_FFTWDOUBLE**) malloc(filterWidth_y * sizeof(T_SAMPLE_TYPE_FFTWDOUBLE*));
	if (downsampledLineBuffer == NULL)
	{
		GOEXCEPTION0("Error allocating temporary line buffers.\n");
	}

	/* NULL it so that further memory deallocation is safe */
	for (filter_y = 0; filter_y < filterWidth_y; filter_y++)
		downsampledLineBuffer[filter_y] = NULL;

	for (filter_y = 0; filter_y < filterWidth_y; filter_y++)
	{
		downsampledLineBuffer[filter_y] = (T_SAMPLE_TYPE_FFTWDOUBLE*) malloc(kernelSamples_x * sizeof(T_SAMPLE_TYPE_FFTWDOUBLE));
		if (downsampledLineBuffer[filter_y] == NULL)
		{
			GOEXCEPTION0("Error allocating temporary line buffers.\n");
		}
	}

	/* Once one row of the final data is processed, we are going to calculate
	 * a line (upsampling_y) samples lower. Therefore (upsampling_y) lines
	 * in downsampledLineBuffer are useless; the rest can be reused.
	 * We could copy data; instead, we will acces these buffers using pointers
	 * and then just copy pointers to particular buffers.
	 */
	downsampledLine = (T_SAMPLE_TYPE_FFTWDOUBLE**) malloc(filterWidth_y * sizeof(T_SAMPLE_TYPE_FFTWDOUBLE*));
	if (downsampledLine == NULL)
	{
		GOEXCEPTION0("Error allocating temporary line buffers.\n");
	}

	for (filter_y = 0; filter_y < filterWidth_y; filter_y++)
		downsampledLine[filter_y] = downsampledLineBuffer[filter_y];


	/*
	 * -------------------------------------------------------
	 *
	 * Time to calculate kernel data
	 *
	 */

	r_orig.z = shift.z;

	/* We can calculate backward propagation as well */
	if (r_orig.z < 0) sgn = -1;
				 else sgn = +1;

	 r_filter.z = r_orig.z;

#ifdef _DEBUG_LISTING
	printf("uncorrected shift vector (%.3f, %.3f, %.3f) [mm]\n", shift.x * 1e3, shift.y * 1e3, shift.z * 1e3);
#endif

	/* calculate the kernel; structure of positive/negative vectors is ([0,0] is upper left corner)
	 * PP | NP
	 * ---+----
	 * NP | NN
	 *
	 * [0,0] sample holds change of comlex amplitude when source[0,0] illuminates target[0,0]
	 * P-part samples are changes of comlex amplitude when source[0,0] illuminates target[x,y]
	 * N-part samples are changes of comlex amplitude when source[x,y] illuminates target[0,0]
	 */

	/* what we need is to compute vector r from a point in sourcePiece to targetPiece
	 * or, better said, we need to calculate |r|^2
	 * we will build this quantity coordinate by coordinate starting from z
	 */
	temp_r2z = r_filter.z * r_filter.z;

	index = 0;
	for (j=0; j<kernelSamples_y; j++)
	{
		/* decide between positive/negative coordinate, see above
		 */
		if (j < targetSamples_y)
			r_orig.y = j * samplingDistance_y; /* or orig_image_screen->res2size, as both share the same sampling */
		else
			r_orig.y = (j - kernelSamples_y) * samplingDistance_y;

		/* incorporate shift between [0,0] samples of source and target */
		r_orig.y += shift.y;

		if (j == 0 || j == targetSamples_y)
		{
			firstFilterSample_y = 0;
		}
		else
		{
			if (filterShift_y < filterWidth_y)
			{
				for (k=0; k<filterWidth_y; k++)
					downsampledLineBuffer[k] = downsampledLine[k];

				for (k=0; k<filterWidth_y-filterShift_y; k++)
					downsampledLine[k] = downsampledLineBuffer[k+filterShift_y];

				for (k=filterWidth_y-filterShift_y; k<filterWidth_y; k++)
					downsampledLine[k] = downsampledLineBuffer[k-filterWidth_y+filterShift_y];

				firstFilterSample_y = filterWidth_y - filterShift_y;
			}
		}

		/* cycle through all filtered kernel samples */
		for (filter_y = firstFilterSample_y; filter_y < filterWidth_y; filter_y++)
		{
			r_filter.y = r_orig.y + (filter_y-filterWidthHalf_y) * filterSamplingDistance_y;
			temp_r2yz = temp_r2z + r_filter.y * r_filter.y;



			/* calculate one line of upsampled unfiltered kernel values */
			for (i=0; i<kernelSamples_x; i++)
			{
				if (i < targetSamples_x)
					r_orig.x = i * samplingDistance_x;
				else
					r_orig.x = (i - kernelSamples_x) * samplingDistance_x;

				/* incorporate shift between [0,0] samples of source and target */
				r_orig.x += shift.x;

				if (i == 0 || i == targetSamples_x)
				{
					firstFilterSample_x = 0;
				}
				else
				{
					for (k=filterShift_x; k<filterWidth_x; k++)
					{
						upsampledLinePiece[k-filterShift_x][RE] = upsampledLinePiece[k][RE];
						upsampledLinePiece[k-filterShift_x][IM] = upsampledLinePiece[k][IM];
					}

					firstFilterSample_x = filterWidth_x - filterShift_x;
					if (firstFilterSample_x < 0) firstFilterSample_x = 0;
				}



				for (filter_x = firstFilterSample_x; filter_x < filterWidth_x; filter_x++)
				{
					r_filter.x = r_orig.x + (filter_x - filterWidthHalf_x) * filterSamplingDistance_x;

					temp_r2 = temp_r2yz + r_filter.x * r_filter.x;
					/*
					 * now we are done with computation of r and |r|^2
					 *
					 * let's compute simplified Rayleigh-Sommerfeld kernel:
					 *
					 *     1      exp(k |r|)    z
					 * -------- * ---------- * ---
					 * j lambda       |r|      |r|
					 *
					 */

					temp_exponent = sgn * k_number * sqrt(temp_r2);
					// temp_exponent = temp_exponent - (int)(temp_exponent / (2*PI)) * 2*PI; /* truncate to [0, 2pi) */

					/* notice that multiplication with filter tap values
					 * can be taken in advance
					 */
					temp = r_filter.z / (lambda * temp_r2);

					/* notice the signs and sin/cos reversal due to 1/j factor */
					upsampledLinePiece[filter_x][RE] =   temp * sin(temp_exponent);
					upsampledLinePiece[filter_x][IM] =  -temp * cos(temp_exponent);

					/*
					 * this generates some readable values for debugging work with kernel
					 */
					#ifdef DEBUG_KERNEL
								//kernel->data[index][RE] = (sqrt(temp_r2) * 1e3);
								//kernelValue[RE] = exp(-(r_filter.x*r_filter.x + r_filter.y*r_filter.y)*1e8);
								kernelValue[RE] = r_filter.x * r_filter.x * 1e9;
								/* if (r.x < 0) kernel->data[index][RE] *= -1; */
								kernelValue[IM] = 0;
					#endif
				} /* end of upsampled x traversing (index filer_x) */


				/* convolution by row */
				filteredKernelValue[RE] = 0;
				filteredKernelValue[IM] = 0;

				for (filter_x = 0; filter_x < filterWidth_x; filter_x++)
				{
					filteredKernelValue[RE] += filterData_x[filter_x] * upsampledLinePiece[filter_x][RE];
					filteredKernelValue[IM] += filterData_x[filter_x] * upsampledLinePiece[filter_x][IM];
				}

				downsampledLine[filter_y][i][RE] = filteredKernelValue[RE];
				downsampledLine[filter_y][i][IM] = filteredKernelValue[IM];

			} /* end of x traversing (index i) */

		} /* end of upsampled y traversing (index filter_y) */

		/* convolution by columns */
		for (i=0; i<kernelSamples_x; i++, index++)
		{
			filteredKernelValue[RE] = 0;
			filteredKernelValue[IM] = 0;

			for (filter_y = 0; filter_y < filterWidth_y; filter_y++)
			{
				filteredKernelValue[RE] += filterData_y[filter_y] * downsampledLine[filter_y][i][RE];
				filteredKernelValue[IM] += filterData_y[filter_y] * downsampledLine[filter_y][i][IM];
			}

			data[index][RE] = filteredKernelValue[RE];
			data[index][IM] = filteredKernelValue[IM];

		} /* end of one line of kernel (index i) */

	} /* end of y traversing (index j) */

#ifdef _DEBUG_LISTING
	printf("corrected shift vector (%.3f, %.3f, %.3f) [mm]\n", shift.x * 1e3, shift.y * 1e3, shift.z * 1e3);
#endif

#ifdef PROPAGATE_DEBUG_PRINTIMG
	printf("\nKernel shift (%.3g, %.3g, %.3g):\n", shift.x, shift.y, shift.z);
	optfieldPrint1(kernel, "%5.2f ", "%5.2f ", "     ");
#endif


#ifdef PROPAGATE_DEBUG_SAVEPNG
	sprintf(filename, "%s_kernel", debugFnamePrefix);
	optfieldSavePNG(kernel, filename, PictureIORealImagIntensity, 1.0, 0, NULL);
	optfieldSavePNG(kernel, filename, PictureIOIntensity, 1.0, 0, NULL);
#endif

	if (applyFFT == FFTApplyOn)
	{
		p = fftw_plan_dft_2d(kernelSamples_y, kernelSamples_x,
			(fftw_complex*) kernel->buffer->data, (fftw_complex*) kernel->buffer->data, -1, FFTW_ESTIMATE);

		fftw_execute(p);

#ifdef PROPAGATE_DEBUG_SAVEPNG
	sprintf(filename, "%s_kernelFT", debugFnamePrefix);
	optfieldSavePNG(kernel, filename, PictureIORealImagIntensity, 1.0, 0, NULL);
	optfieldSavePNG(kernel, filename, PictureIOIntensity, 1.0, 0, NULL);
#endif

		fftw_destroy_plan(p);
	}

EXCEPTION

	if (downsampledLineBuffer != NULL)
	{
		for (filter_y = 0; filter_y < filterWidth_y; filter_y++)
			if (downsampledLineBuffer[filter_y] != NULL)
				free(downsampledLineBuffer[filter_y]);
		free(downsampledLineBuffer);
	}

	if (upsampledLinePiece != NULL)
		free(upsampledLinePiece);

	if (downsampledLine != NULL)
		free(downsampledLine);

ENDEXCEPTION;
}


//   100 1e6 650 0 0 2 2 2540 1 1 "x." 0.2 3 1 0 50 4 4 1 1 0  _prA
//   100 1e6 650 0 0 2 2 38100 1 3 @_grating_u15_i.16.png 0.2 3 1 0 50 4 4 1 0 0  _prRimg15

int kernelCalculateAS(t_point *global_shift,
					t_propagPlan *propagationPiece,
					double samplingDistance_x, double samplingDistance_y,
					double *interpFilterData_x, int interpFilterWidthHalf_x, int interpUpsampling_x,
					double *interpFilterData_y, int interpFilterWidthHalf_y, int interpUpsampling_y,
					t_optfield *kernel)
{
	t_point f_filterInterp, f_filter, f_orig, shift;
	int filterShift_x, filterWidthHalf_x, filterShift_y, filterWidthHalf_y, filterWidth_x, filterWidth_y;
	fftw_complex *filterData_x = NULL, *filterData_y = NULL;
	int filter_x, filter_y;
	int i, j, k, index;
	int kernelSamples_x, kernelSamples_y;
	int sourceSamples_x, sourceSamples_y;
	int targetSamples_x, targetSamples_y;
	double temp_lambda, temp_y, temp_xy, temp_exponent;
	double limit_fx, limit_fy;
	double max_fx, max_fy, fc;
	double window;
	double attenuation_fx, attenuation_fy, attenuation;
	double aliasAttenuation;
	double sgn;
	T_SAMPLE_TYPE_FFTWDOUBLE *data, filteredKernelValue;
	T_SAMPLE_TYPE_FFTWDOUBLE *upsampledLinePiece = NULL, **downsampledLine = NULL, **downsampledLineBuffer = NULL;
	int firstFilterSample_x = 0, firstFilterSample_y = 0;
	double samplingDistance_fx, samplingDistance_fy;
	double interpDistance_fx, interpDistance_fy;
	double filterSamplingDistance_fx, filterSamplingDistance_fy;
	int upsampling;
	fftw_complex freqShift, shiftedValue, interpResponse;
	fftw_plan p;
	double temp_local_f, local_fx, local_fy;
	int interp_j, interp_i;

	FILE *fout;
	double linemaxre = 1, linemaxim = 1;
	int linelength = 0, linecount = 0;
	int index_i, index_j;
	fftw_complex angularSpectrum;

	/* char filename[1000]; */

	fftw_complex *interpFreqResponse_x = NULL, *interpFreqResponse_y = NULL;



	///DEBUG
	fout = fopen("_line.raw", "wb");
	if (fout == NULL) { printf("!!!\n"); exit(0); }

	/* Let us sample transfer function (angular spectrum) upsampling* finer
	 * than needed. This will allow us to perform digital lowpass to filter out
	 * unwanted frequencies. This lowpass filter will then be
	 * 2*filterWidthHalf+1 taps long.
	 * However, we should avoid aliasing due to higher frequencies than allowed by
	 * sampling frequency raised by "upsampling".
	 * This will be done using local frequency estimation. For smooth transition,
	 * the frequencies larger than max_f (maximum allowed frequency in this raised
	 * sampling) will be attenuated to 0, the frequencies lower than
	 * aliasAttenuation * max_f will be kept, and the frequencies between will
	 * be linearly attenuated.
	 */

	// correct
	upsampling = 2;
	filterWidthHalf_x = 10*upsampling;
	filterWidthHalf_y = 10*upsampling;
	aliasAttenuation = 0.75;

	/// INCORRECT
	//upsampling = 1;
	//filterWidthHalf_x = 0;
	//filterWidthHalf_y = 0;
	//aliasAttenuation = 0.9999;

	filterShift_x = upsampling;
	filterShift_y = upsampling;
	filterWidth_x = 1 + 2*filterWidthHalf_x;
	filterWidth_y = 1 + 2*filterWidthHalf_y;

	filterData_x = (fftw_complex*) malloc(filterWidth_x * sizeof(fftw_complex));
	if (filterData_x == NULL)
	{
		ELOG0("Error allocating filter in angular spectrum calculation.\n");
		GOEXCEPTION
	}

	filterData_y = (fftw_complex*) malloc(filterWidth_y * sizeof(fftw_complex));
	if (filterData_y == NULL)
	{
		ELOG0("Error allocating filter in angular spectrum calculation.\n");
		GOEXCEPTION
	}

	CHECK(optfieldGetSamples(&kernelSamples_x, &kernelSamples_y, kernel));
	CHECK(optfieldZeroData(kernel));

	sourceSamples_x = propagationPiece->sourceSamples_x;
	sourceSamples_y = propagationPiece->sourceSamples_y;

	targetSamples_x = propagationPiece->targetSamples_x;
	targetSamples_y = propagationPiece->targetSamples_y;

	shift.x = global_shift->x + (targetSamples_x - sourceSamples_x)/2.0 * samplingDistance_x;
	shift.y = global_shift->y + (targetSamples_y - sourceSamples_y)/2.0 * samplingDistance_y;
	shift.z = global_shift->z;

	filterCalcFreqResponse(&interpFreqResponse_x, interpFilterData_x, interpFilterWidthHalf_x, interpUpsampling_x, kernelSamples_x, 0);
	filterCalcFreqResponse(&interpFreqResponse_y, interpFilterData_y, interpFilterWidthHalf_y, interpUpsampling_y, kernelSamples_y, 0);

	filterComplexSave(interpFreqResponse_x, kernelSamples_x*interpUpsampling_x, "freq_x.txt");
	filterComplexSave(interpFreqResponse_y, kernelSamples_y*interpUpsampling_y, "freq_y.txt");

	/* TODO
	 * handling of different sample types
	 */

	data = (T_SAMPLE_TYPE_FFTWDOUBLE*) kernel->buffer->data;

	/// DEBUG
	samplingDistance_fx = 1/(kernelSamples_x * samplingDistance_x);
	samplingDistance_fy = 1/(kernelSamples_x * samplingDistance_x);

	interpDistance_fx = 1 / samplingDistance_x;
	interpDistance_fy = 1 / samplingDistance_y;

	filterSamplingDistance_fx = samplingDistance_fx / upsampling;
	filterSamplingDistance_fy = samplingDistance_fy / upsampling;

	max_fx = 0.5/samplingDistance_fx * upsampling;
	// max_fx = fabs(max_fx - shift.x);
	limit_fx = max_fx/(lambda*sqrt(shift.z*shift.z + max_fx*max_fx));

	max_fy = 0.5/samplingDistance_fy * upsampling;
	// max_fy = fabs(max_fy - shift.y);
	limit_fy = max_fy/(lambda*sqrt(shift.z*shift.z + max_fy*max_fy));

	/*
	  Explaining is a little bit tricky.
	  In spatial domain, R-S convolution kernel h(x,y) has to be spatially clipped within a rectangle,
	  i.e. multiplied by rect(x/w, y/w), where w is source_size + target_size.
	  It means thats its frequency counterpart, function H(fx, fy), has to be convolved with
	  function w*w*sinc(fx w)*sinc(fy w).
	  This means that function H(f_x, f_y) has to be frequency limited.

	  h(x,y) has sampling period delta => H(fx,fy) has extent 1/delta, i.e. max. frequency is 0.5/delta
	  h(x,y) has the extent (width) w => H(fx,fy) is sampled with step 1/w
		=> H(fx, fy) it is capable to sample frequency at most w/2
		=> this is exactly what we need as the "frequency limiting" clips h(x,y)
		   to the interval -w/2 to w/2

	  To perform filtering, let us upsample H(fx,fy) say 3x.
	  It follows we need to clip frequencies above 0.3333 of the current sampling rate.

	  Definition of clipping frequency is as follows:
	  fc = 0.5  <=> maximum frequency for a particular sampling rate

	  => for our example (filtering 0.3333x current sampling rate), fc = 0.5/3

	  -------

	  In spatial domain, R-S kernel can be spatially clipped within a shifted rectangle,
	  i.e. multiplied by rect((x-x0)/w, (y-y0)/w) = rect(x/w - x0/w, y/w - y0/w)
	  In frequency domain, it has to be convolved with
	  w*w*sinc(fx w)*sinc(fy w) * exp(-j * 2pi * (fx*x0 + fy*y0))

	*/

	fc = 0.5 / upsampling;
	for (i=0; i<filterWidthHalf_x; i++)
	{
		/* Blackman window, DSP Guide, page 286 */
		window = 0.42 - 0.5*cos(2*PI*i/(filterWidth_x-1)) + 0.08*cos(4*PI*i/(filterWidth_x-1));

		/* windowed sinc filter */
		filterData_x[filterWidth_x-1-i][RE] =
		filterData_x[i][RE] = sin(2*PI*fc*(i-filterWidthHalf_x))/(PI*(i-filterWidthHalf_x)) * window;

		filterData_x[filterWidth_x-1-i][IM] =
		filterData_x[i][IM] = 0;
	}
	filterData_x[filterWidthHalf_x][RE] = 2*fc;
	filterData_x[filterWidthHalf_x][IM] = 0;
/*
	for (i=0; i<filterWidthHalf_x; i++)
	{
		double f, re, im, fre, fim, e;
		f = (i-filterWidthHalf_x) * filterSamplingDistance_fx;
		e = 2*PI*f*shift.x;
		re = cos(e);
		im = sin(e);
		fre = filterData_x[i][RE];
		fim = filterData_x[i][IM];
		filterData_x[i][RE] = fre * re - fim * im;
		filterData_x[i][IM] = fre * im + fim * re;
	}
*/

	fc = 0.5 / upsampling;
	for (i=0; i<filterWidthHalf_y; i++)
	{
		/* Blackman window, DSP Guide, page 286 */
		window = 0.42 - 0.5*cos(2*PI*i/(filterWidth_y-1)) + 0.08*cos(4*PI*i/(filterWidth_y-1));

		/* windowed sinc filter */
		filterData_y[filterWidth_y-1-i][RE] =
		filterData_y[i][RE] = sin(2*PI*fc*(i-filterWidthHalf_y))/(PI*(i-filterWidthHalf_y)) * window;

		filterData_y[filterWidth_y-1-i][IM] =
		filterData_y[i][IM] = 0;
	}
	filterData_y[filterWidthHalf_y][RE] = 2*fc;
	filterData_y[filterWidthHalf_y][IM] = 0;


	upsampledLinePiece = (T_SAMPLE_TYPE_FFTWDOUBLE*) malloc(filterWidth_x * sizeof(T_SAMPLE_TYPE_FFTWDOUBLE));
	if (upsampledLinePiece == NULL)
		GOEXCEPTION;

	downsampledLineBuffer = (T_SAMPLE_TYPE_FFTWDOUBLE**) malloc(filterWidth_y * sizeof(T_SAMPLE_TYPE_FFTWDOUBLE*));
	if (downsampledLineBuffer == NULL)
		GOEXCEPTION;

	/* NULL it so that further memory deallocation is safe */
	for (filter_y = 0; filter_y < filterWidth_y; filter_y++)
		downsampledLineBuffer[filter_y] = NULL;

	for (filter_y = 0; filter_y < filterWidth_y; filter_y++)
	{
		downsampledLineBuffer[filter_y] = (T_SAMPLE_TYPE_FFTWDOUBLE*) malloc(kernelSamples_x * sizeof(T_SAMPLE_TYPE_FFTWDOUBLE));
		if (downsampledLineBuffer[filter_y] == NULL)
			GOEXCEPTION;
	}

	/* Once one row of the final data is processed, we are going to calculate
	 * a line (upsampling_y) samples lower. Therefore (upsampling_y) lines
	 * in downsampledLineBuffer are useless; the rest can be reused.
	 * We could copy data; instead, we will acces these buffers using pointers
	 * and then just copy pointers to particular buffers.
	 */
	downsampledLine = (T_SAMPLE_TYPE_FFTWDOUBLE**) malloc(filterWidth_y * sizeof(T_SAMPLE_TYPE_FFTWDOUBLE*));
	for (filter_y = 0; filter_y < filterWidth_y; filter_y++)
		downsampledLine[filter_y] = downsampledLineBuffer[filter_y];


	/*
	 * -------------------------------------------------------
	 *
	 * Time to calculate kernel data
	 *
	 */

	f_orig.z = shift.z;

	/* We can calculate backward propagation as well */
	if (f_orig.z < 0) sgn = -1;
				 else sgn = +1;

	 f_filter.z = f_orig.z;

#ifdef _DEBUG_LISTING
	printf("uncorrected shift vector (%.3f, %.3f, %.3f) [mm]\n", shift.x * 1e3, shift.y * 1e3, shift.z * 1e3);
#endif

	/* calculate the kernel; structure of positive/negative vectors is ([0,0] is upper left corner)
	 * PP | NP
	 * ---+----
	 * NP | NN
	 *
	 * [0,0] sample holds change of comlex amplitude when source[0,0] illuminates target[0,0]
	 * P-part samples are changes of comlex amplitude when source[0,0] illuminates target[x,y]
	 * N-part samples are changes of comlex amplitude when source[x,y] illuminates target[0,0]
	 */

	/* what we need is to compute vector r from a point in sourcePiece to targetPiece
	 * or, better said, we need to calculate |r|^2
	 * we will build this quantity coordinate by coordinate starting from z
	 */
	temp_lambda = 1/(lambda*lambda);

	index = 0;
	for (j=0; j<kernelSamples_y; j++)
	{
		/* decide between positive/negative coordinate, see above
		 */
		if (j < kernelSamples_y / 2)
			f_orig.y = j * samplingDistance_fy; /* or orig_image_screen->res2size, as both share the same sampling */
		else
			f_orig.y = (j - kernelSamples_y) * samplingDistance_fy;

		/* incorporate shift between [0,0] samples of source and target */
		f_orig.y += shift.y;

		if (j == 0 || j == kernelSamples_y / 2)
		{
			firstFilterSample_y = 0;
		}
		else
		{
			if (filterShift_y < filterWidth_y)
			{
				for (k=0; k<filterWidth_y; k++)
					downsampledLineBuffer[k] = downsampledLine[k];

				for (k=0; k<filterWidth_y-filterShift_y; k++)
					downsampledLine[k] = downsampledLineBuffer[k+filterShift_y];

				for (k=filterWidth_y-filterShift_y; k<filterWidth_y; k++)
					downsampledLine[k] = downsampledLineBuffer[k-filterWidth_y+filterShift_y];

				firstFilterSample_y = filterWidth_y - filterShift_y;
			}
		}

		/* cycle through all filtered kernel samples */
		for (filter_y = firstFilterSample_y; filter_y < filterWidth_y; filter_y++)
		{
			f_filter.y = f_orig.y + (filter_y-filterWidthHalf_y) * filterSamplingDistance_fy;

			/* If f_filter_y is large enough, then we should eliminate calculated values as they will be
			   aliased anyway. In simple way:
			   attenuation_fy = fabs(f_filter.y) > limit_fy ? 0 : 0;

			   However, it might be better not to hard-clip the values. Instead,
			   at 0.75*limit_fy we will start to attenuate so that at limit_fy
			   there will be full attenuation.
			*/

			attenuation_fy = fabs(f_filter.y);
			if (attenuation_fy > limit_fy)
				attenuation_fy = 0;
			else if (attenuation_fy < 0.75*limit_fy)
				attenuation_fy = 1;
			else
				attenuation_fy = 4*(1-attenuation_fy/limit_fy);


			/* calculate one line of upsampled unfiltered kernel values */
			for (i=0; i<kernelSamples_x; i++)
			{
				if (i < kernelSamples_x / 2)
					f_orig.x = i * samplingDistance_fx;
				else
					f_orig.x = (i - kernelSamples_x) * samplingDistance_fx;

				/* incorporate shift between [0,0] samples of source and target */
				f_orig.x += shift.x;

				if (i == 0 || i == kernelSamples_x / 2)
				{
					firstFilterSample_x = 0;
				}
				else
				{
					for (k=filterShift_x; k<filterWidth_x; k++)
					{
						upsampledLinePiece[k-filterShift_x][RE] = upsampledLinePiece[k][RE];
						upsampledLinePiece[k-filterShift_x][IM] = upsampledLinePiece[k][IM];
					}

					firstFilterSample_x = filterWidth_x - filterShift_x;
					if (firstFilterSample_x < 0) firstFilterSample_x = 0;
				}



				for (filter_x = firstFilterSample_x; filter_x < filterWidth_x; filter_x++)
				{
					f_filter.x = f_orig.x + (filter_x - filterWidthHalf_x) * filterSamplingDistance_fx;

					upsampledLinePiece[filter_x][RE] = 0;
					upsampledLinePiece[filter_x][IM] = 0;

					for (interp_j = 0; interp_j < interpUpsampling_y; interp_j++)
					//interp_j = 1; //interpUpsampling_y/2;
					{
						f_filterInterp.y = f_filter.y + (interp_j - interpUpsampling_y/2) * interpDistance_fy;
						temp_y = temp_lambda - f_filterInterp.y * f_filterInterp.y;

						for (interp_i = 0; interp_i < interpUpsampling_x; interp_i++)
						//interp_i = 2; //*interpUpsampling_x/2;
						{
							f_filterInterp.x = f_filter.x + (interp_i - interpUpsampling_x/2) * interpDistance_fx;

							index_i = i;
							if (i>=kernelSamples_x/2) index_i += kernelSamples_x*(interpUpsampling_x-1);
							index_i += kernelSamples_x * (interp_i - interpUpsampling_x/2);
							if (index_i < 0)
								index_i += kernelSamples_x * interpUpsampling_x;
							index_i = index_i % (kernelSamples_x * interpUpsampling_x);

							index_j = j;
							if (j>=kernelSamples_y/2) index_j += kernelSamples_y*(interpUpsampling_y-1);
							index_j += kernelSamples_y * (interp_j - interpUpsampling_y/2);
							if (index_j < 0)
								index_j += kernelSamples_y * interpUpsampling_y;
							index_j = index_j % (kernelSamples_y * interpUpsampling_y);

							interpResponse[RE] = interpFreqResponse_x[index_i][RE] * interpFreqResponse_y[index_j][RE] -
												 interpFreqResponse_x[index_i][IM] * interpFreqResponse_y[index_j][IM];
							interpResponse[IM] = interpFreqResponse_x[index_i][RE] * interpFreqResponse_y[index_j][IM] +
												 interpFreqResponse_x[index_i][IM] * interpFreqResponse_y[index_j][RE];

							//interpResponse[RE] = 1;
							//interpResponse[IM] = 0;


							/* Attenuation in fx is the same as for fy, see a comment there. Simple way:
							attenuation_fx = fabs(f_filter.x) < limit_fx ? 1 : 0;
							*/

							temp_local_f = -shift.z /
									   sqrt(1/(lambda*lambda) - f_filterInterp.y*f_filterInterp.y - f_filterInterp.x*f_filterInterp.x);

							local_fy = f_filterInterp.y * temp_local_f + shift.y;
							local_fx = f_filterInterp.x * temp_local_f + shift.x;

							attenuation_fx = fabs(local_fx);
							if (attenuation_fx > max_fx)
								attenuation_fx = 0;
							else if (attenuation_fx <= aliasAttenuation*max_fx)
								attenuation_fx = 1;
							else
								attenuation_fx = (1-attenuation_fx/max_fx)/(1-aliasAttenuation);

							attenuation_fy = fabs(local_fy);
							if (attenuation_fy > max_fy)
								attenuation_fy = 0;
							else if (attenuation_fy <= aliasAttenuation*max_fy)
								attenuation_fy = 1;
							else
								attenuation_fy = (1-attenuation_fy/max_fy)/(1-aliasAttenuation);

							attenuation = attenuation_fx * attenuation_fy;

							/// INCORRECT
							/// attenuation = 1;

							interpResponse[RE] *= attenuation;
							interpResponse[IM] *= attenuation;

							temp_xy = temp_y - f_filterInterp.x * f_filterInterp.x;

							temp_exponent = sgn * 2 * PI * (f_orig.z * sqrt(temp_xy)
								+ (f_filterInterp.x*shift.x + f_filterInterp.y*shift.y)
								);

							angularSpectrum[RE] = cos(temp_exponent);
							angularSpectrum[IM] = sin(temp_exponent);

							upsampledLinePiece[filter_x][RE] += angularSpectrum[RE] * interpResponse[RE] - angularSpectrum[IM] * interpResponse[IM];
							upsampledLinePiece[filter_x][IM] += angularSpectrum[IM] * interpResponse[RE] + angularSpectrum[RE] * interpResponse[IM];

							/*
							if (j == 0)
							{
								double val;
								int ival;
								val = upsampledLinePiece[filter_x][RE];
								if (val > linemaxre) linemaxre = val;
								val = val / linemaxre * 0.5 + 0.5;
								ival = (int)(val * 65535); ival = ival<0 ? 0 : ival > 65535 ? 65535 : ival;
								fputc((ival & 0xFF00) >> 8, fout);
								fputc(ival & 0xFF, fout);
								val = upsampledLinePiece[filter_x][IM];
								if (val > linemaxim) linemaxim = val;
								val = val / linemaxim * 0.5 + 0.5;
								ival = (int)(val * 65535); ival = ival<0 ? 0 : ival > 65535 ? 65535 : ival;
								fputc((ival & 0xFF00) >> 8, fout);
								fputc(ival & 0xFF, fout);
								fputc(0, fout);
								fputc(0, fout);
								if (filter_y == 0) linelength++;
								if (filter_x == 0) linecount++;
							}
							*/
						}
					}

				} /* end of upsampled x traversing (index filer_x) */


				/* convolution by row */
				filteredKernelValue[RE] = 0;
				filteredKernelValue[IM] = 0;

				for (filter_x = 0; filter_x < filterWidth_x; filter_x++)
				{
					filteredKernelValue[RE] += filterData_x[filter_x][RE] * upsampledLinePiece[filter_x][RE] -
											   filterData_x[filter_x][IM] * upsampledLinePiece[filter_x][IM];
					filteredKernelValue[IM] += filterData_x[filter_x][RE] * upsampledLinePiece[filter_x][IM] +
											   filterData_x[filter_x][IM] * upsampledLinePiece[filter_x][RE];
				}

				downsampledLine[filter_y][i][RE] = filteredKernelValue[RE];
				downsampledLine[filter_y][i][IM] = filteredKernelValue[IM];

			} /* end of x traversing (index i) */

		} /* end of upsampled y traversing (index filter_y) */

		/* convolution by columns */
		for (i=0; i<kernelSamples_x; i++, index++)
		{
			filteredKernelValue[RE] = 0;
			filteredKernelValue[IM] = 0;

			for (filter_y = 0; filter_y < filterWidth_y; filter_y++)
			{
				filteredKernelValue[RE] += filterData_y[filter_y][RE] * downsampledLine[filter_y][i][RE] -
										   filterData_y[filter_y][IM] * downsampledLine[filter_y][i][IM];
				filteredKernelValue[IM] += filterData_y[filter_y][RE] * downsampledLine[filter_y][i][IM] +
										   filterData_y[filter_y][IM] * downsampledLine[filter_y][i][RE];
			}

			temp_exponent = sgn * 2 * PI *
					((sourceSamples_x-0.5*kernelSamples_x)/(double)kernelSamples_x*i+
					 (sourceSamples_y-0.5*kernelSamples_y)/(double)kernelSamples_y*j);
			freqShift[RE] = cos(temp_exponent);
			freqShift[IM] = sin(temp_exponent);


			shiftedValue[RE] = filteredKernelValue[RE]*freqShift[RE] - filteredKernelValue[IM]*freqShift[IM];
			shiftedValue[IM] = filteredKernelValue[IM]*freqShift[RE] + filteredKernelValue[RE]*freqShift[IM];

/*
			interpResponse[RE] = interpFreqResponse_x[i][RE] * interpFreqResponse_y[j][RE] -
								 interpFreqResponse_x[i][IM] * interpFreqResponse_y[j][IM];
			interpResponse[IM] = interpFreqResponse_x[i][RE] * interpFreqResponse_y[j][IM] +
								 interpFreqResponse_x[i][IM] * interpFreqResponse_y[j][RE];

			data[index][RE] = interpResponse[RE] * shiftedValue[RE] -
							  interpResponse[IM] * shiftedValue[IM];
			data[index][IM] = interpResponse[RE] * shiftedValue[IM] +
							  interpResponse[IM] * shiftedValue[RE];
*/

			data[index][RE] = shiftedValue[RE];
			data[index][IM] = shiftedValue[IM];

		} /* end of one line of kernel (index i) */

	} /* end of y traversing (index j) */

	fclose(fout);

#ifdef _DEBUG_LISTING
	printf("corrected shift vector (%.3f, %.3f, %.3f) [mm]\n", shift.x * 1e3, shift.y * 1e3, shift.z * 1e3);
#endif

#ifdef PROPAGATE_DEBUG_PRINTIMG
	printf("\nKernel shift (%.3g, %.3g, %.3g):\n", shift.x, shift.y, shift.z);
	optfieldPrint1(kernel, "%5.2f ", "%5.2f ", "     ");
#endif


#ifdef PROPAGATE_DEBUG_SAVEPNG
	sprintf(filename, "%s_ASkernelFT", debugFnamePrefix);
	optfieldSavePNG(kernel, filename, PictureIORealImagIntensity, 1.0, 0, NULL);
	optfieldSavePNG(kernel, filename, PictureIOIntensity, 1.0, 0, NULL);
#endif

	p = fftw_plan_dft_2d(kernelSamples_y, kernelSamples_x,
		(fftw_complex*) kernel->buffer->data, (fftw_complex*) kernel->buffer->data, +1, FFTW_ESTIMATE);
	fftw_execute(p);
	fftw_destroy_plan(p);

#ifdef PROPAGATE_DEBUG_SAVEPNG
	sprintf(filename, "%s_ASkernel", debugFnamePrefix);
	optfieldSavePNG(kernel, filename, PictureIORealImagIntensity, 1.0, 0, NULL);
	optfieldSavePNG(kernel, filename, PictureIOIntensity, 1.0, 0, NULL);
#endif

	p = fftw_plan_dft_2d(kernelSamples_y, kernelSamples_x,
		(fftw_complex*) kernel->buffer->data, (fftw_complex*) kernel->buffer->data, -1, FFTW_ESTIMATE);
	fftw_execute(p);
	fftw_destroy_plan(p);

	printf("\n\nline length %d   line count %d\nlinemaxre %f    linemaxim %f\n\n", linelength, linecount, (float)linemaxre, (float)linemaxim);

	return RAYLEIGH_OK;

EXCEPTION

	if (filterData_x != NULL) free(filterData_x);
	if (filterData_y != NULL) free(filterData_y);

	if (downsampledLineBuffer != NULL)
	{
		for (filter_y = 0; filter_y < filterWidth_y; filter_y++)
			if (downsampledLineBuffer[filter_y] != NULL)
				free(downsampledLineBuffer[filter_y]);
		free(downsampledLineBuffer);
	}

	if (upsampledLinePiece != NULL)
		free(upsampledLinePiece);

	return RAYLEIGH_ERROR;
}
