/*
 * rhoLUT
 *
 * In many calculations, sqrt(x^2 + y^2 + z^2) appears, where
 * z is constand during the calculation. Moreover, x and y are
 * usually coordinate differences, therefore they are often close
 * to zero.
 * It follows it is advantageous to store rho = sqrt(x^2 + y^2)
 * in a look-up table.
 */


#ifndef __RHOLUT_H
#define __RHOLUT_H

typedef struct {
	double deltaXMin, deltaYMin, deltaXMax, deltaYMax; /* ranges of x and y */
	double samplingDistance;		                   /* sampling of x and y */
													   /* sampling distance is sampling * common_sampling_distance */
	int samplesX, samplesY;                            /* dimensions of the LUT */
	double rhoMin, rhoMax;							   /* min and max values of rho */
	double *rho;                                       /* values of sqrt(x^2 + y^2) */
} t_rhoLUT;

#include "rayleigh.h"

/*
 * Find extremal xy for vectors, where the starting point is in rectangle 1
 * and the ending point in a rectangle 2.
 * These are needed in
 */
RAYLEIGH_EXPORT int rhoLUTFindExtremeXYRel(double x1Min, double x1Max,
					  double y1Min, double y1Max,
					  double x2Min, double x2Max,
					  double y2Min, double y2Max,
					  double *absDeltaXMin, double *absDeltaXMax,
					  double *absDeltaYMin, double *absDeltaYMax);

/*
 * Find extremal xy shifts between the source and the target.
 * Suitable for the propagation task.
 */
RAYLEIGH_EXPORT int rhoLUTFindMinMaxRelShift(t_optfield *source, t_optfield *target, t_point *minRelShift, t_point *maxRelShift);


/*
 * create a rhoLUT
 */
RAYLEIGH_EXPORT int rhoLUTNew(double xMin, double xMax,
				 double yMin, double yMax,
				 double samplingDistance,
				 t_rhoLUT **rhoLUT);

/*
 * create a rhoLUT suitable for the source->target propagation
 */
RAYLEIGH_EXPORT int rhoLUTPropagationNew(t_optfield *source, t_optfield *target,
							t_rhoLUT **rhoLUT,
							t_point *rMinRelShift, t_point *rMaxRelShift,
							int rhoLUTsubsampling);


/* gettters / setters */
RAYLEIGH_EXPORT double rhoLUTGetSamplingDistance(t_rhoLUT *rhoLUT);
RAYLEIGH_EXPORT double rhoLUTGetDeltaXMin(t_rhoLUT *rhoLUT);
RAYLEIGH_EXPORT double rhoLUTGetDeltaYMin(t_rhoLUT *rhoLUT);
RAYLEIGH_EXPORT double rhoLUTGetDeltaXMax(t_rhoLUT *rhoLUT);
RAYLEIGH_EXPORT double rhoLUTGetDeltaYMax(t_rhoLUT *rhoLUT);
RAYLEIGH_EXPORT int rhoLUTGetSamplesX(t_rhoLUT *rhoLUT);
RAYLEIGH_EXPORT int rhoLUTGetSamplesY(t_rhoLUT *rhoLUT);
RAYLEIGH_EXPORT double rhoLUTGetRhoMin(t_rhoLUT *rhoLUT);
RAYLEIGH_EXPORT double rhoLUTGetRhoMax(t_rhoLUT *rhoLUT);

/*
 * dispose a rhoLUT
 */
RAYLEIGH_EXPORT int rhoLUTDispose(t_rhoLUT **rhoLUT);

/*
 * save rhoLUT as an image for debugging purposes
 */
RAYLEIGH_EXPORT int rhoLUTSave(t_rhoLUT *rhoLUT, char *filename);

/*
 * calculate the optimal sampling of the rhoLUT
 */
RAYLEIGH_EXPORT double rhoLUTOptSampling(double precision, double x, double y, double z);

#endif
