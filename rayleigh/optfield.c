#include <string.h>
#include "rayleighCompile.h"
#include "png.h"

char *SampleTypeName[] = {"", "fftw_complex"};

/*
 * set/get optical field sample [0,0] position
 *
 * The "P" versions operate with t_point input/output type.
 */
int optfieldSetCorner(double corner_x, double corner_y, double corner_z, t_optfield *optfield)
{
	if (optfield == NULL)
		return RAYLEIGH_ERROR;

	optfield->corner.x = corner_x;
	optfield->corner.y = corner_y;
	optfield->corner.z = corner_z;

	return RAYLEIGH_OK;
}

int optfieldSetCornerP(t_point *corner, t_optfield *optfield)
{
	if (optfield == NULL)
		return RAYLEIGH_ERROR;

	optfield->corner.x = corner->x;
	optfield->corner.y = corner->y;
	optfield->corner.z = corner->z;

	return RAYLEIGH_OK;
}



int optfieldGetCorner(double *corner_x, double *corner_y, double *corner_z, t_optfield *optfield)
{
	if (optfield == NULL)
		return RAYLEIGH_ERROR;

	*corner_x = optfield->corner.x;
	*corner_y = optfield->corner.y;
	*corner_z = optfield->corner.z;

	return RAYLEIGH_OK;
}

int optfieldGetCornerP(t_point *corner, t_optfield *optfield)
{
	if (optfield == NULL)
		return RAYLEIGH_ERROR;

	corner->x = optfield->corner.x;
	corner->y = optfield->corner.y;
	corner->z = optfield->corner.z;

	return RAYLEIGH_OK;
}

/*
 * set/get optical field sampling
 * setting sampling does not change sample count of the optical field,
 * i.e. its size is changed accordingly
 */
int optfieldSetSampling(int sampling, t_optfield *optfield)
{
	if (optfield == NULL)
		return RAYLEIGH_ERROR;

	optfield->sampling = sampling;
	return RAYLEIGH_OK;
}

int optfieldGetSampling(t_optfield *optfield)
{
	return optfield->sampling;
}


/*
 * set/get optical field sample type
 * setting sampling type does not change buffer size,
 * size of buffer is tested
 * TODO: better handling buffer->data
 */
t_SampleType optfieldSetSampleType(t_SampleType sampleType, t_optfield *optfield)
{
	int samples_x, samples_y;
	int sizeofType;
	RAYLEIGH_INT64 numofsamples;

	/* check if sampleType is valid */
	if (sampleType == SampleTypeFFTWDouble)
		sizeofType = sizeof(fftw_complex);
	else
		return RAYLEIGH_ERROR;

	/* check pointer validity */
	if (optfield == NULL)
		return RAYLEIGH_ERROR;

	/* check if buffer is big enough */
	optfieldGetSamples(&samples_x, &samples_y, optfield);
	numofsamples = (RAYLEIGH_INT64)samples_x * (RAYLEIGH_INT64)samples_y;

	if (numofsamples > 0)
	{
		if (optfield->buffer == NULL)
			return RAYLEIGH_ERROR;
		if (optfield->buffer->data == NULL)
			return RAYLEIGH_ERROR;
		if (numofsamples * (RAYLEIGH_INT64) sizeofType > optfield->buffer->byteSize)
			return RAYLEIGH_ERROR;
	}

	/* set sample type */
	optfield->sampleType = sampleType;
	optfield->sampleTypeSize = sizeofType;

	return RAYLEIGH_OK;
}


t_SampleType optfieldGetSampleType(t_optfield *optfield)
{
	return optfield->sampleType;
}


int optfieldGetSampleTypeSize(t_optfield *optfield)
{
	return optfield->sampleTypeSize;
}

/*
 * set/get optical field sampling distance
 * setting sampling distance does not change sample count of the optical field,
 * i.e. its size is changed accordingly
 * sampling distance is set to the nearest multiple of common_sampling_distance
 */
int optfieldSetSamplingdistance(double sampling_distance, t_optfield *optfield)
{
	if (optfield == NULL)
		return RAYLEIGH_ERROR;

	optfield->sampling = (int) floor(sampling_distance / common_sampling_distance + 0.5);
	if (optfield->sampling < 1) optfield->sampling = 1;

	return RAYLEIGH_OK;
}


double optfieldGetSamplingdistance(t_optfield *optfield)
{
	return common_sampling_distance * optfield->sampling;
}


/*
 * set/get number of samples of the optical field
 * setting number of samples checks if the buffer inside can hold such data
 */
int optfieldSetSamples(int samples_x, int samples_y, t_optfield *optfield)
{
	RAYLEIGH_INT64 numofsamples;

	/* check valid conditions */
	if (optfield == NULL)
		return RAYLEIGH_ERROR;

	if (samples_x < 0 || samples_y < 0)
		return RAYLEIGH_ERROR;

	numofsamples = (RAYLEIGH_INT64) samples_x * (RAYLEIGH_INT64) samples_y;

	/* will the data fit into the buffer? */
	if (numofsamples != 0)
		/* we have some data => there has to be a buffer, large enough */
		if (optfield->buffer == NULL ||
			numofsamples * (RAYLEIGH_INT64) optfieldGetSampleTypeSize(optfield) >
			optfield->buffer->byteSize)
			return RAYLEIGH_ERROR;

	optfield->samples_x = samples_x;
	optfield->samples_y = samples_y;

	return RAYLEIGH_OK;
}

int optfieldGetSamples(int *samples_x, int *samples_y, t_optfield *optfield)
{
	/* check valid conditions */
	if (optfield == NULL)
		return RAYLEIGH_ERROR;

	*samples_x = optfield->samples_x;
	*samples_y = optfield->samples_y;

	return RAYLEIGH_OK;
}

/*
 * SETs number of samples of the optical field through the actual physical size
 * setting number of samples checks if the buffer inside can hold such data
 * GETs actual physical size
 */
/* TODO:
   SETTER optfieldSetSize
 */
int optfieldGetSize(double *size_x, double *size_y, t_optfield *optfield)
{
	int samples_x, samples_y;
	double samplingDistance;

	CHECK(optfieldGetSamples(&samples_x, &samples_y, optfield));
	samplingDistance = optfieldGetSamplingdistance(optfield);

	*size_x = samplingDistance * samples_x;
	*size_y = samplingDistance * samples_y;

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}

/*
 * set/get optical field center position
 */
int optfieldSetCenter(double center_x, double center_y, double center_z, t_optfield *optfield)
{
	double corner_x, corner_y, corner_z;
	double samplingDistance;
	int samples_x, samples_y;

	/* check valid optfield */
	if (optfield == NULL)
		GOEXCEPTION;

	/* get optfield data */
	CHECK(optfieldGetSamples(&samples_x, &samples_y, optfield));
	samplingDistance = optfieldGetSamplingdistance(optfield);

	/* calculate corner position */
	corner_x = center_x -  samplingDistance * (samples_x - 1)/2.0;
	corner_y = center_y -  samplingDistance * (samples_y - 1)/2.0;
	corner_z = center_z;

	/* set and exit */
	optfieldSetCorner(corner_x, corner_y, corner_z, optfield);

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}


int optfieldSetCenterP(t_point *center, t_optfield *optfield)
{
	if (center != NULL)
		return optfieldSetCenter(center->x, center->y, center->z, optfield);
	else
		return RAYLEIGH_ERROR;
}



int optfieldGetCenter(double *center_x, double *center_y, double *center_z, t_optfield *optfield)
{
	double corner_x, corner_y, corner_z;
	double samplingDistance;
	int samples_x, samples_y;

	/* check valid optfield */
	if (optfield == NULL)
		GOEXCEPTION;

	/* get optfield data */
	CHECK(optfieldGetSamples(&samples_x, &samples_y, optfield));
	CHECK(optfieldGetCorner(&corner_x, &corner_y, &corner_z, optfield));
	samplingDistance = optfieldGetSamplingdistance(optfield);

	/* calculate corner position */
	*center_x = corner_x + samplingDistance * (samples_x - 1)/2.0;
	*center_y = corner_y + samplingDistance * (samples_y - 1)/2.0;
	*center_z = corner_z;

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}


int optfieldGetCenterP(t_point *center, t_optfield *optfield)
{
	if (center != NULL)
		return optfieldGetCenter(&(center->x), &(center->y), &(center->z), optfield);
	else
		return RAYLEIGH_ERROR;
}


/* Initialize image parameters and allocate space.
 *
 * PARAMETERS
  * PARAMETERS
 * center_x, center_y, center_z (IN): centre of the plane, in meters
 * samples_x, samples_y (IN): sample count in both x and y directions
 * sampling (IN): sampling distance as an integer multiple of common_sampling_distance
 * sampleType (IN): type of one sample, see SAMPLE_TYPE_* list
 * bufferType (IN): type of buffer, see BUFFER_TYPE_* list
 * buffer (IN): you can provide external buffer; if NULL, a new buffer is created
 * optfield (OUT): output image.
 */
int optfieldNew(double center_x, double center_y, double center_z,
				 int samples_x, int samples_y,
				 int sampling,
				 t_SampleType sampleType,
				 t_BufferType bufferType,
				 t_buffer *buffer,
				 t_optfield **optfield)
{
	RAYLEIGH_INT64 dataSize;

	/* initialize structure */
	CHECK(optfieldNewEmpty(optfield))

	/* set sample type */
	optfieldSetSampleType(sampleType, *optfield);

	/* create buffer */
	dataSize = (RAYLEIGH_INT64)samples_x * (RAYLEIGH_INT64)samples_y * (RAYLEIGH_INT64)optfieldGetSampleTypeSize(*optfield);
	if (buffer == NULL)
		/* no external buffer => create new one */
		CHECK(bufferNew(dataSize, bufferType, &((*optfield)->buffer)))
	else
	{
		/* external buffer provided => check it out */
		if (dataSize > buffer->byteSize)
			GOEXCEPTION;

		(*optfield)->buffer = buffer;
	}

	/* set number of samples */
	CHECK(optfieldSetSamples(samples_x, samples_y, *optfield));

	/* set sampling and redefine center position */
	optfieldSetSampling(sampling, *optfield);
	optfieldSetCenter(center_x, center_y, center_z, *optfield);

	/* initialize data */
	optfieldZeroData(*optfield);

	return RAYLEIGH_OK;

EXCEPTION
	if (optfield != NULL)
		if (*optfield != NULL)
		{
			bufferDispose(&((*optfield)->buffer));
			free(*optfield);
			*optfield = NULL;
		}
	return RAYLEIGH_ERROR;
}

/*
 * creates an empty valid optfield structure with NULL buffer
 */
int optfieldNewEmpty(t_optfield **optfield)
{
	if ((*optfield = (t_optfield*) malloc(sizeof(t_optfield))) == NULL)
		return RAYLEIGH_ERROR;

	(*optfield)->buffer = NULL;
	optfieldSetCorner(0, 0, 0, *optfield);
	optfieldSetSampleType(SampleTypeFFTWDouble, *optfield);
	optfieldSetSamples(0, 0, *optfield);
	optfieldSetSampling(1, *optfield);

	return RAYLEIGH_OK;
}

/*
 * Sets image samples to complex zero
 * TODO: better handling buffer->data
 */
int optfieldZeroData(t_optfield *optfield)
{
	if (optfield == NULL)
		return RAYLEIGH_ERROR;

	if (optfield->buffer->data == NULL)
		return RAYLEIGH_ERROR;

	memset(optfield->buffer->data, 0, bufferGetByteSize(optfield->buffer));

	return RAYLEIGH_OK;
}


/*
 * Sets image data to sequence of integers for easy debugging
 */

int optfieldDebugdata(t_optfield *optfield)
{
	RAYLEIGH_INT64 i, datasize;
	int samples_x, samples_y;
	T_SAMPLE_TYPE_FFTWDOUBLE *data;

	CHECK(optfieldGetSamples(&samples_x, &samples_y, optfield));

	if (optfieldGetSampleType(optfield) == SampleTypeFFTWDouble)
		data = (T_SAMPLE_TYPE_FFTWDOUBLE*) optfield->buffer->data;
	else
		GOEXCEPTION;

	datasize = (RAYLEIGH_INT64) samples_x * (RAYLEIGH_INT64) samples_y;

	for (i=0; i<datasize; i++)
	{
		data[i][0] = (double)(i+1);
		data[i][1] = 0;
	}
	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;

}

/* Copies image (samples_x * samples_y) sample values from src to dst (ie. from src->data to dst_data).
 * Data can be offset from the zero position by offset_x, offset_y.
 * Data can be interleaved, ie. can have gaps, sample distances are set by step_x and step_y.
 * Example 1: common copy from src to dst:
 *            copy_image_data(dst, 0, 0, 1, 1, src, 0, 0, 1, 1, src->resolution_x, src->resolution_y)
 * Example 2: copy every even sample of src to odd samples of dst:
 *            copy_image_data(dst, 1, 1, 2, 2, src, 0, 0, 2, 2, src->resolution_x/2, src->resolution_y/2)
 *            (image: . = sample left, * = sample copied, array position [0,0] is the upper left corner)
 *            * . * . * . * .          . . . . . . . .
 *            . . . . . . . .          . * . * . * . *
 *            * . * . * . * .          . . . . . . . .
 *            . . . . . . . .          . * . * . * . *
 *            * . * . * . * .          . . . . . . . .
 *            . . . . . . . .          . * . * . * . *
 */
int optfieldCopyData(t_optfield *dst, int dst_offset_x, int dst_offset_y, int dst_step_x, int dst_step_y,
					t_optfield *src, int src_offset_x, int src_offset_y, int src_step_x, int src_step_y,
					int samples_x, int samples_y)
{
	int x, y;
	int dst_next_row, src_next_row;
	int dst_index, src_index;
	int src_samples_x, src_samples_y;
	int dst_samples_x, dst_samples_y;
	t_SampleType sampletype;
	int sampletypesize;

	CHECK(optfieldGetSamples(&src_samples_x, &src_samples_y, src));
	CHECK(optfieldGetSamples(&dst_samples_x, &dst_samples_y, dst));

	/* check if first sample index (both in x and y) to be copied is in the valid scope */
	if (src_offset_x < 0 || src_offset_x >= src_samples_x) GOEXCEPTION;
	if (src_offset_y < 0 || src_offset_y >= src_samples_y) GOEXCEPTION;
	if (dst_offset_x < 0 || dst_offset_x >= dst_samples_x) GOEXCEPTION;
	if (dst_offset_y < 0 || dst_offset_y >= dst_samples_y) GOEXCEPTION;

	/* check if last sample index (both in x and y) to be copied is in the valid scope */
	x = src_offset_x + (samples_x - 1) * src_step_x;
	y = src_offset_y + (samples_y - 1) * src_step_y;
	if (x < 0 || x >= src_samples_x) GOEXCEPTION;
	if (y < 0 || y >= src_samples_y) GOEXCEPTION;

	x = dst_offset_x + (samples_x - 1) * dst_step_x;
	y = dst_offset_y + (samples_y - 1) * dst_step_y;
	if (x < 0 || x >= dst_samples_x) GOEXCEPTION;
	if (y < 0 || y >= dst_samples_y) GOEXCEPTION;

	/* indices of the first sample in 1-dimensional data array */
	src_index = src_offset_x + src_samples_x * src_offset_y;
	dst_index = dst_offset_x + dst_samples_x * dst_offset_y;

	/* how many indices to skip when moving to next row */
	/* last term (-step_x) is because this is added at the end of for x-cycle */
	src_next_row = src_samples_x * src_step_y - (samples_x - 1) * src_step_x - src_step_x;
	dst_next_row = dst_samples_x * dst_step_y - (samples_x - 1) * dst_step_x - dst_step_x;

	/* copy data */
	sampletype = optfieldGetSampleType(src);
	sampletypesize = optfieldGetSampleTypeSize(src);

	if (sampletype == SampleTypeFFTWDouble)
	{
		T_SAMPLE_TYPE_FFTWDOUBLE *srcdata, *dstdata;

		srcdata = (T_SAMPLE_TYPE_FFTWDOUBLE *) src->buffer->data;
		dstdata = (T_SAMPLE_TYPE_FFTWDOUBLE *) dst->buffer->data;

		for (y=0; y<samples_y; y++)
		{
			for (x=0; x<samples_x; x++)
			{
				memcpy(dstdata[dst_index], srcdata[src_index], sampletypesize);
				dst_index += dst_step_x;
				src_index += src_step_x;
			}
			dst_index += dst_next_row;
			src_index += src_next_row;
		}
	}
	else
		GOEXCEPTION;

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}


/* the same function as copy_image_data; instead of overwriting original numbers, it adds to actual content
 */
int optfieldAddDataSparse(t_optfield *dst, int dst_offset_x, int dst_offset_y, int dst_step_x, int dst_step_y,
				   t_optfield *src, int src_offset_x, int src_offset_y, int src_step_x, int src_step_y,
				   int samples_x, int samples_y)
{
	int x, y;
	int dst_next_row, src_next_row;
	int dst_index, src_index;
	int src_samples_x, src_samples_y;
	int dst_samples_x, dst_samples_y;
	t_SampleType sampletype;
	int sampletypesize;

	CHECK(optfieldGetSamples(&src_samples_x, &src_samples_y, src));
	CHECK(optfieldGetSamples(&dst_samples_x, &dst_samples_y, dst));

	/* check if first sample index (both in x and y) to be copied is in the valid scope */
	if (src_offset_x < 0 || src_offset_x >= src_samples_x) GOEXCEPTION;
	if (src_offset_y < 0 || src_offset_y >= src_samples_y) GOEXCEPTION;
	if (dst_offset_x < 0 || dst_offset_x >= dst_samples_x) GOEXCEPTION;
	if (dst_offset_y < 0 || dst_offset_y >= dst_samples_y) GOEXCEPTION;

	/* check if last sample index (both in x and y) to be copied is in the valid scope */
	x = src_offset_x + (samples_x - 1) * src_step_x;
	y = src_offset_y + (samples_y - 1) * src_step_y;
	if (x < 0 || x >= src_samples_x) GOEXCEPTION;
	if (y < 0 || y >= src_samples_y) GOEXCEPTION;

	x = dst_offset_x + (samples_x - 1) * dst_step_x;
	y = dst_offset_y + (samples_y - 1) * dst_step_y;
	if (x < 0 || x >= dst_samples_x) GOEXCEPTION;
	if (y < 0 || y >= dst_samples_y) GOEXCEPTION;

	/* indices of the first sample in 1-dimensional data array */
	src_index = src_offset_x + src_samples_x * src_offset_y;
	dst_index = dst_offset_x + dst_samples_x * dst_offset_y;

	/* how many indices to skip when moving to next row */
	/* last term (-step_x) is because this is added at the end of for x-cycle */
	src_next_row = src_samples_x * src_step_y - (samples_x - 1) * src_step_x - src_step_x;
	dst_next_row = dst_samples_x * dst_step_y - (samples_x - 1) * dst_step_x - dst_step_x;

	/* copy data */

	sampletype = optfieldGetSampleType(src);
	sampletypesize = optfieldGetSampleTypeSize(src);

	if (sampletype == SampleTypeFFTWDouble)
	{
		T_SAMPLE_TYPE_FFTWDOUBLE *srcdata, *dstdata;

		srcdata = (T_SAMPLE_TYPE_FFTWDOUBLE *) src->buffer->data;
		dstdata = (T_SAMPLE_TYPE_FFTWDOUBLE *) dst->buffer->data;

		for (y=0; y<samples_y; y++)
		{
			for (x=0; x<samples_x; x++)
			{
				dstdata[dst_index][0] += srcdata[src_index][0];
				dstdata[dst_index][1] += srcdata[src_index][1];
				dst_index += dst_step_x;
				src_index += src_step_x;
			}
			dst_index += dst_next_row;
			src_index += src_next_row;
		}
	}
	else
		GOEXCEPTION;

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}


/* the same function as copy_image_data; instead of overwriting original numbers, it multiplies actual content
 */
int optfieldMulDataSparse(t_optfield *dst, int dst_offset_x, int dst_offset_y, int dst_step_x, int dst_step_y,
				   t_optfield *src, int src_offset_x, int src_offset_y, int src_step_x, int src_step_y,
				   int samples_x, int samples_y)
{
	int x, y;
	int dst_next_row, src_next_row;
	int dst_index, src_index;
	int src_samples_x, src_samples_y;
	int dst_samples_x, dst_samples_y;
	t_SampleType sampletype;
	int sampletypesize;

	CHECK(optfieldGetSamples(&src_samples_x, &src_samples_y, src));
	CHECK(optfieldGetSamples(&dst_samples_x, &dst_samples_y, dst));


	/* check if first sample index (both in x and y) to be copied is in the valid scope */
	if (src_offset_x < 0 || src_offset_x >= src_samples_x) GOEXCEPTION;
	if (src_offset_y < 0 || src_offset_y >= src_samples_y) GOEXCEPTION;
	if (dst_offset_x < 0 || dst_offset_x >= dst_samples_x) GOEXCEPTION;
	if (dst_offset_y < 0 || dst_offset_y >= dst_samples_y) GOEXCEPTION;

	/* check if last sample index (both in x and y) to be copied is in the valid scope */
	x = src_offset_x + (samples_x - 1) * src_step_x;
	y = src_offset_y + (samples_y - 1) * src_step_y;
	if (x < 0 || x >= src_samples_x) GOEXCEPTION;
	if (y < 0 || y >= src_samples_y) GOEXCEPTION;

	x = dst_offset_x + (samples_x - 1) * dst_step_x;
	y = dst_offset_y + (samples_y - 1) * dst_step_y;
	if (x < 0 || x >= dst_samples_x) GOEXCEPTION;
	if (y < 0 || y >= dst_samples_y) GOEXCEPTION;

	/* indices of the first sample in 1-dimensional data array */
	src_index = src_offset_x + src_samples_x * src_offset_y;
	dst_index = dst_offset_x + dst_samples_x * dst_offset_y;

	/* how many indices to skip when moving to next row */
	/* last term (-step_x) is because this is added at the end of for x-cycle */
	src_next_row = src_samples_x * src_step_y - (samples_x - 1) * src_step_x - src_step_x;
	dst_next_row = dst_samples_x * dst_step_y - (samples_x - 1) * dst_step_x - dst_step_x;

	/* copy data */

	sampletype = optfieldGetSampleType(src);
	sampletypesize = optfieldGetSampleTypeSize(src);

	if (sampletype == SampleTypeFFTWDouble)
	{
		T_SAMPLE_TYPE_FFTWDOUBLE *srcdata, *dstdata;

		srcdata = (T_SAMPLE_TYPE_FFTWDOUBLE *) src->buffer->data;
		dstdata = (T_SAMPLE_TYPE_FFTWDOUBLE *) dst->buffer->data;

		for (y=0; y<samples_y; y++)
		{
			for (x=0; x<samples_x; x++)
			{
				double re1, re2, im1, im2;

				re1 = srcdata[src_index][0];
				im1 = srcdata[src_index][1];

				re2 = dstdata[dst_index][0];
				im2 = dstdata[dst_index][1];

				dstdata[dst_index][0] = re1 * re2 - im1 * im2;
				dstdata[dst_index][1] = re1 * im2 + re2 * im1;
				dst_index += dst_step_x;
				src_index += src_step_x;
			}
			dst_index += dst_next_row;
			src_index += src_next_row;
		}
	}
	else
		GOEXCEPTION;

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}




/*
 * Create a deep copy of src optical field into dst optical field. Allocation included.
 */
int optfieldCopy(t_optfield **dst, t_optfield *src)
{
	/* check valid pointer */
	if (dst == NULL)
		GOEXCEPTION;

	/* initialize *dst pointer */
	(*dst) = NULL;

	/* create optfield structure */
	CHECK(optfieldNewEmpty(dst));

	/* copy all the data including buffer pointer; rewrite it to NULL */
	memcpy(*dst, src, sizeof(t_optfield));
	(*dst)->buffer = NULL;

	/* allocate new buffer */
	CHECK(bufferNew(bufferGetByteSize(src->buffer), bufferGetType(src->buffer), &((*dst)->buffer)));

	memcpy((*dst)->buffer->data, src->buffer->data, bufferGetByteSize(src->buffer));

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}


/*
 * Free space occupied by the optical field including its buffer.
 */
int optfieldDispose(t_optfield **optfield)
{
	/* check valid pointers */
	if (optfield == NULL || *optfield == NULL)
		GOEXCEPTION;

	/* try to dispose buffer */
	if ((*optfield)->buffer != NULL)
		CHECK(bufferDispose(&((*optfield)->buffer)));

	/* free the optfield structure */
	free(*optfield);
	*optfield = NULL;

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}

char *defaultform1 = "                    ";
char *defaultform2 = "                    ";
char *defaultform3 = "                    ";

void optfieldPrintLine(t_optfield *optfield, int y, char *form1, char *form2, char *form3)
{
	int x;
	double val;
	int digits, decimals, spaces;
	int samples_x, samples_y;

	if (form1 == NULL || form2 == NULL || form3 == NULL)
	{
		digits=5;
		decimals=2;
		spaces=2;

		memset(defaultform1, ' ', 20);
		memset(defaultform2, ' ', 20);
		memset(defaultform3, ' ', 20);

		sprintf(defaultform1, "%%%d.%df", digits, decimals);
		defaultform1[5] = ' ';
		defaultform1[5+spaces] = 0;
		defaultform2[digits-1] = '.';
		defaultform2[digits+spaces] = 0;
		defaultform3[digits+spaces] = 0;

		form1 = defaultform1;
		form2 = defaultform2;
		form3 = defaultform3;
	}

	optfieldGetSamples(&samples_x, &samples_y, optfield);

	if (y < samples_y)
		for (x=0; x<samples_x; x++)
		{
			val = ((fftw_complex*)optfield->buffer->data)[x + y*samples_x][0];
			if (fabs(val) > 0.5) printf(form1, val);
			else printf(form2, val);
		}
	else
		for (x=0; x<samples_x; x++) printf("%s", form3);
}


void optfieldPrint1(t_optfield *optfield,
					char *form1, char *form2, char *form3)
{
	int y;
	int samples_x, samples_y;

	CHECK(optfieldGetSamples(&samples_x, &samples_y, optfield))

	for (y=0; y<samples_y; y++)
	{
		optfieldPrintLine(optfield, y, form1, form2, form3);
		putchar('\n');
	}

	return;

EXCEPTION
	ELOG0("Problems printing optical field\n");
	exit(0);
}


void optfieldPrint2(t_optfield *lights, t_optfield *screen,
					char *form1, char *form2, char *form3)
{
	int y, y1, y2, x1, x2, maxy;

	CHECK(optfieldGetSamples(&x1, &y1, lights));
	CHECK(optfieldGetSamples(&x2, &y2, screen));

	maxy = y1 < y2 ? y2 : y1;

	for (y=0; y<maxy; y++)
	{
		optfieldPrintLine(lights, y, form1, form2, form3);
		printf("    -->    ");
		optfieldPrintLine(screen, y, form1, form2, form3);
		putchar('\n');
	}

	return;

EXCEPTION
	ELOG0("Problems printing optical field\n");
	exit(0);
}


void optfieldPrint3(t_optfield *lights, t_optfield *kernel, t_optfield *screen,
					char *form1, char *form2, char *form3)
{
	int y, y1, y2, y3, x1, x2, x3, maxy;

	CHECK(optfieldGetSamples(&x1, &y1, lights));
	CHECK(optfieldGetSamples(&x2, &y2, kernel));
	CHECK(optfieldGetSamples(&x3, &y3, screen));

	maxy = y1 < y2 ? y2 : y1;
	maxy = maxy < y3 ? y3 : maxy;

	for (y=0; y<maxy; y++)
	{
		optfieldPrintLine(lights, y, form1, form2, form3);
		printf("    -->    ");
		optfieldPrintLine(kernel, y, form1, form2, form3);
		printf("    -->    ");
		optfieldPrintLine(screen, y, form1, form2, form3);
		putchar('\n');
	}

	return;

EXCEPTION
	ELOG0("Problems printing optical field\n");
	exit(0);
}

void optfieldPrint4(t_optfield *orig, t_optfield *lights, t_optfield *kernel, t_optfield *screen,
					char *form1, char *form2, char *form3)
{
	int y, y1, y2, y3, y4, x1, x2, x3, x4, maxy;

	CHECK(optfieldGetSamples(&x1, &y1, orig));
	CHECK(optfieldGetSamples(&x2, &y2, lights));
	CHECK(optfieldGetSamples(&x3, &y3, kernel));
	CHECK(optfieldGetSamples(&x4, &y4, screen));

	maxy = y1 < y2 ? y2 : y1;
	maxy = maxy < y3 ? y3 : maxy;
	maxy = maxy < y4 ? y4 : maxy;

	for (y=0; y<maxy; y++)
	{
		optfieldPrintLine(orig, y, form1, form2, form3);
		printf("    -->    ");
		optfieldPrintLine(lights, y, form1, form2, form3);
		printf("    -->    ");
		optfieldPrintLine(kernel, y, form1, form2, form3);
		printf("    -->    ");
		optfieldPrintLine(screen, y, form1, form2, form3);
		putchar('\n');
	}

	return;

EXCEPTION
	ELOG0("Problems printing optical field\n");
	exit(0);

}


void optfieldPrintProperties(t_optfield *optfield, char *type)
{
	double center_x, center_y, center_z;
	double size_x, size_y;
	int samples_x, samples_y;

	CHECK(optfieldGetCenter(&center_x, &center_y, &center_z, optfield));
	CHECK(optfieldGetSize(&size_x, &size_y, optfield));
	CHECK(optfieldGetSamples(&samples_x, &samples_y, optfield));

	printf("%s piece: center [%.3f; %.3f; %.3f] mm, size %.3f x %.3f mm, sampling distance %.3f um\n",
		type,
		center_x TOMM, center_y TOMM, center_z TOMM,
		size_x TOMM, size_y TOMM,
		optfieldGetSamplingdistance(optfield) TOUM);
	return;

EXCEPTION
	ELOG0("Problems printing optical field properties\n");
	exit(0);
}


void optfieldTestBasic(void)
{
	t_optfield *src = NULL, *tgt = NULL, *cpy = NULL;

	CHECK(optfieldNew(0, 0, 0, 5, 5, 1000, SampleTypeFFTWDouble, BufferTypeMain, NULL, &src));
	CHECK(optfieldNew(0, 0, 0, 5, 5, 1000, SampleTypeFFTWDouble, BufferTypeMain, NULL, &tgt));

	CHECK(optfieldDebugdata(src));
	CHECK(optfieldZeroData(tgt));
	CHECK(optfieldCopy(&cpy, src));

	optfieldPrintProperties(src, "src");
	optfieldPrintProperties(cpy, "cpy");
	optfieldPrint2(src, cpy, NULL, NULL, NULL);

	putchar('\n');

	CHECK(optfieldCopyData(tgt, 1, 1, 2, 2, src, 0, 0, 2, 2, 2, 2));
	optfieldPrint1(tgt, NULL, NULL, NULL);

	putchar('\n');

	CHECK(optfieldAddDataSparse(tgt, 0, 0, 2, 2, src, 0, 0, 2, 2, 2, 2));
	optfieldPrint1(tgt, NULL, NULL, NULL);

	CHECK(optfieldDispose(&src));
	CHECK(optfieldDispose(&tgt));

	LOG0("Optical fields basic test OK.\n");

	return;

EXCEPTION
	ELOG0("Basic test of optical fields failed\n");
	exit(0);

}


/*
 * Create an optical field by combining amplitude from the first and the phase from the second one.
 * Typical use: load two images to "amplitude" and "phase" optfields, combine them using this function.
 *
 * Amplitude or phase may be NULL (but not both!). In this case, NULL variable is set to amplitude_multiplier
 * or phase_multiplier.
 *
 * input: amplitude - optfield, real part is considered as amplitude
 *        amplitude_multiplier - amplitude gets multiplied by this number
 *        phase - optfield, real part is considered as phase
 *        phase_multiplier - phase gets multiplied by this number
 * output: output - optfield, allocation is done if output == NULL;
 *           otherwise, its dimensions must match the amplitude and phase
 */
/*
 * TODO
 * different data types handling
 */
int optfieldCreateAmpPhase(t_optfield **output, t_optfield **amplitude, double amplitude_multiplier,
						   t_optfield **phase, double phase_multiplier)
{
	int i, j, index;
	double a, p;
	int sampling;
	int a_samples_x = 0, a_samples_y = 0;
	int p_samples_x = 0, p_samples_y = 0;
	int o_samples_x = 0, o_samples_y = 0;
	double center_x, center_y, center_z;
	T_SAMPLE_TYPE_FFTWDOUBLE *adata = NULL, *pdata = NULL, *odata = NULL;

	if (amplitude == NULL && phase == NULL)
		GOEXCEPTION0("At least one from Amplitude and Phase must be non-NULL.\n")

	if (amplitude != NULL) CHECK(optfieldGetSamples(&a_samples_x, &a_samples_y, *amplitude));
	if (phase != NULL)     CHECK(optfieldGetSamples(&p_samples_x, &p_samples_y, *phase));

	if (output == NULL)
		GOEXCEPTION0("NULL output pointer.\n");

	if (amplitude != NULL &&
		phase != NULL &&
		(a_samples_x != p_samples_x || a_samples_y != p_samples_y ||
		 optfieldGetSampling(*amplitude) != optfieldGetSampling(*phase))
		)
		GOEXCEPTION0("Amplitude and phase images must match.\n");

	if (amplitude != NULL)
	{
		CHECK(optfieldGetCenter(&center_x, &center_y, &center_z, *amplitude));
		sampling = optfieldGetSampling(*amplitude);
	}
	else
	{
		CHECK(optfieldGetCenter(&center_x, &center_y, &center_z, *phase));
		sampling = optfieldGetSampling(*phase);
	}

	if (*output == NULL)
	{
		CHECK(optfieldNew(center_x, center_y, center_z,
			a_samples_x, a_samples_y,
			sampling,
			SampleTypeFFTWDouble,
			BufferTypeMain,
			NULL,
			output));
	}
	else
	{
		CHECK(optfieldGetSamples(&o_samples_x, &o_samples_y, *output));

		if (amplitude != NULL &&
			  (a_samples_x != o_samples_x || a_samples_y != o_samples_y)
			  ||
			phase != NULL &&
			  (p_samples_x != o_samples_x || p_samples_y != o_samples_y)
			)
		GOEXCEPTION0("Output does not match amplitude or phase image.\n");
	}

	if ((amplitude != NULL && optfieldGetSampleType(*amplitude) != SampleTypeFFTWDouble) ||
		(amplitude != NULL && phase != NULL && optfieldGetSampleType(*amplitude) != optfieldGetSampleType(*phase)) ||
		(amplitude != NULL && optfieldGetSampleType(*amplitude) != optfieldGetSampleType(*output)))
		GOEXCEPTION0("Unsupported sample type.\n");

	if (amplitude != NULL) adata = (T_SAMPLE_TYPE_FFTWDOUBLE*) (*amplitude)->buffer->data;
	if (phase != NULL)     pdata = (T_SAMPLE_TYPE_FFTWDOUBLE*) (*phase)->buffer->data;
	odata = (T_SAMPLE_TYPE_FFTWDOUBLE*) (*output)->buffer->data;

	if (amplitude != NULL && phase != NULL)
	{
		for (i=0, index=0; i<o_samples_y; i++)
			for (j=0; j<o_samples_x; j++, index++)
			{
				a = adata[index][0];
				p = pdata[index][0] * phase_multiplier;
				odata[index][0] = a * cos(p);
				odata[index][1] = a * sin(p);
			}
	}
	else if (amplitude == NULL)
	{
		for (i=0, index=0; i<o_samples_y; i++)
			for (j=0; j<o_samples_x; j++, index++)
			{
				p = pdata[index][0] * phase_multiplier;
				odata[index][0] = amplitude_multiplier * cos(p);
				odata[index][1] = amplitude_multiplier * sin(p);
			}
	}
	else if (phase == NULL)
	{
		for (i=0, index=0; i<o_samples_y; i++)
			for (j=0; j<o_samples_x; j++, index++)
			{
				a = adata[index][0];
				p = phase_multiplier;
				odata[index][0] = a * cos(p);
				odata[index][1] = a * sin(p);
			}
	}

	return RAYLEIGH_OK;
EXCEPTION
	ELOG0("Error in calling optfieldCreateAmpPhase\n");
	return RAYLEIGH_ERROR;
}


/*
 * Create an optical field by combining real part from the first and the imag part from the second one.
 * Typical use: load two images to "real" and "imag" optfields, combine them using this function.
 *
 * Real or imag may be NULL (but not both!). In this case, NULL variable is set to real_multiplier
 * or imag_multiplier.
 *
 * input: real - optfield, real part is considered only
 *        real_multiplier - real part gets multiplied by this number
 *        imag - optfield, real part is considered only (and serves as an imag part of the output)
 *        imag_multiplier - imag part gets multiplied by this number
 * output: output - optfield, allocation is done if output == NULL;
 *           otherwise, its dimensions must match the amplitude and phase
 */
/*
 * TODO
 * different data types handling
 */
int optfieldCreateRealImag(t_optfield **output, t_optfield **real, double real_multiplier,
						   t_optfield **imag, double imag_multiplier)
{
	int i, j, index;
	int sampling;
	int r_samples_x = 0, r_samples_y = 0;
	int i_samples_x = 0, i_samples_y = 0;
	int o_samples_x = 0, o_samples_y = 0;
	double center_x, center_y, center_z;
	T_SAMPLE_TYPE_FFTWDOUBLE *rdata = NULL, *idata = NULL, *odata = NULL;

	if (real == NULL && imag == NULL)
		GOEXCEPTION0("At least one from Real and Imag must be non-NULL.\n")

	if (real != NULL) CHECK(optfieldGetSamples(&r_samples_x, &r_samples_y, *real));
	if (imag != NULL) CHECK(optfieldGetSamples(&i_samples_x, &i_samples_y, *imag));

	if (output == NULL)
		GOEXCEPTION0("NULL output pointer.\n");

	if (real != NULL &&
		imag != NULL &&
		(r_samples_x != i_samples_x || r_samples_y != i_samples_y ||
		 optfieldGetSampling(*real) != optfieldGetSampling(*imag))
		)
		GOEXCEPTION0("Real and Imag images must match.\n");

	if (real != NULL)
	{
		CHECK(optfieldGetCenter(&center_x, &center_y, &center_z, *real));
		sampling = optfieldGetSampling(*real);
	}
	else
	{
		CHECK(optfieldGetCenter(&center_x, &center_y, &center_z, *imag));
		sampling = optfieldGetSampling(*imag);
	}

	if (*output == NULL)
	{
		CHECK(optfieldNew(center_x, center_y, center_z,
			r_samples_x, r_samples_y,
			sampling,
			SampleTypeFFTWDouble,
			BufferTypeMain,
			NULL,
			output));
	}
	else
	{
		CHECK(optfieldGetSamples(&o_samples_x, &o_samples_y, *output));

		if (real != NULL &&
			  (r_samples_x != o_samples_x || r_samples_y != o_samples_y)
			  ||
			imag != NULL &&
			  (i_samples_x != o_samples_x || i_samples_y != o_samples_y)
			)
		GOEXCEPTION0("Output does not match real or imag image.\n");
	}

	if ((real != NULL && optfieldGetSampleType(*real) != SampleTypeFFTWDouble) ||
		(real != NULL && imag != NULL && optfieldGetSampleType(*real) != optfieldGetSampleType(*imag)) ||
		(real != NULL && optfieldGetSampleType(*real) != optfieldGetSampleType(*output)))
		GOEXCEPTION0("Unsupported sample type.\n");

	if (real != NULL) rdata = (T_SAMPLE_TYPE_FFTWDOUBLE*) (*real)->buffer->data;
	if (imag != NULL) idata = (T_SAMPLE_TYPE_FFTWDOUBLE*) (*imag)->buffer->data;
	odata = (T_SAMPLE_TYPE_FFTWDOUBLE*) (*output)->buffer->data;

	if (real != NULL && imag != NULL)
	{
		for (i=0, index=0; i<o_samples_y; i++)
			for (j=0; j<o_samples_x; j++, index++)
			{
				odata[index][RE] = rdata[index][0] * real_multiplier;
				odata[index][IM] = idata[index][0] * imag_multiplier;
			}
	}
	else if (real == NULL)
	{
		for (i=0, index=0; i<o_samples_y; i++)
			for (j=0; j<o_samples_x; j++, index++)
			{
				odata[index][RE] = real_multiplier;
				odata[index][IM] = idata[index][0] * imag_multiplier;
			}
	}
	else if (imag == NULL)
	{
		for (i=0, index=0; i<o_samples_y; i++)
			for (j=0; j<o_samples_x; j++, index++)
			{
				odata[index][RE] = rdata[index][0] * real_multiplier;
				odata[index][IM] = imag_multiplier;
			}
	}

	return RAYLEIGH_OK;
EXCEPTION
	ELOG0("Error in calling optfieldCreateRealImag\n");
	return RAYLEIGH_ERROR;
}


/*
 * TODO: various data type handling
 */
/* Convert optfield to a real one based on the intensity of the first.
 * output = [ input .* conj(input) ] * factor
 * where factor determines output extent.
 * When maxValue == 0, then factor = 1, i.e. output is the intensity of the input.
 * When maxValue != 0, then factor is set so that the maximum value of the output
 * reaches maxValue.
 *
 * outMaxIntensity is set to the maximum intensity of the input.
 *   In case outMaxIntensity == NULL, no such output is provided.
 *
 * NULL output means that an output optfield must be created.
 * Its parameters are taken from input.
 * Non-NULL output means that the space for the result already exists;
 * data dimensions (samplesX, samplesY) are checked only.
 * Output pointer can be the same as input.
 */
int optfieldIntensity(t_optfield **output, t_optfield *input, double maxValue, double *outMaxIntensity)
{
	int outputAllocated = 0;
	int outputSamplesX, outputSamplesY;
	int inputSamplesX, inputSamplesY;
	RAYLEIGH_INT64 index, samplesCount;
	fftw_complex *inputData, *outputData;
	double maxIntensity = 0;
	double re, im, intensity;

	if (input == NULL)
		GOEXCEPTION0("NULL input pointer.\n");

	if (output == NULL)
		GOEXCEPTION0("NULL output pointer.\n");

	if (*output == NULL)
	{
		CHECK(optfieldCopy(output, input));
		outputAllocated = 1;
	}

	optfieldGetSamples(&inputSamplesX, &inputSamplesY, input);
	optfieldGetSamples(&outputSamplesX, &outputSamplesY, *output);

	if (inputSamplesX != outputSamplesX ||
		inputSamplesY != outputSamplesY)
		GOEXCEPTION0("Input and output samples counts do not match.\n");

	samplesCount = (RAYLEIGH_INT64)inputSamplesX * (RAYLEIGH_INT64)inputSamplesY;

	inputData = (fftw_complex*) input->buffer->data;
	outputData = (fftw_complex*) (*output)->buffer->data;

	for (index = 0; index < samplesCount; index++)
	{
		re = inputData[index][RE];
		im = inputData[index][IM];
		intensity = re*re + im*im;
		outputData[index][RE] = intensity;
		outputData[index][IM] = 0;
		if (intensity > maxIntensity) maxIntensity = intensity;
	}

	if (maxValue != 0)
		optfieldScale(output, *output, 1/maxIntensity);

	if (outMaxIntensity != NULL)
		*outMaxIntensity = maxIntensity;

	return RAYLEIGH_OK;

EXCEPTION

	if (outputAllocated)
		optfieldDispose(output);

	return RAYLEIGH_ERROR;
}




/*
 * TODO: various data type handling
 */
/* Multiplication by a real number: output = input * factor.
 * NULL output means that an output optfield must be created.
 * Its parameters are taken from input.
 * Non-NULL output means that the space for the result already exists;
 * data dimensions (samplesX, samplesY) are checked only.
 * Output pointer can be the same as input.
 */
int optfieldScale(t_optfield **output, t_optfield *input, double factor)
{
	int outputAllocated = 0;
	int outputSamplesX, outputSamplesY;
	int inputSamplesX, inputSamplesY;
	RAYLEIGH_INT64 index, samplesCount;
	fftw_complex *inputData, *outputData;

	if (input == NULL)
		GOEXCEPTION0("NULL input pointer.\n");

	if (output == NULL)
		GOEXCEPTION0("NULL output pointer.\n");

	if (*output == NULL)
	{
		CHECK(optfieldCopy(output, input));
		outputAllocated = 1;
	}

	optfieldGetSamples(&inputSamplesX, &inputSamplesY, input);
	optfieldGetSamples(&outputSamplesX, &outputSamplesY, *output);

	if (inputSamplesX != outputSamplesX ||
		inputSamplesY != outputSamplesY)
		GOEXCEPTION0("Input and output samples counts do not match.\n");

	samplesCount = (RAYLEIGH_INT64)inputSamplesX * (RAYLEIGH_INT64)inputSamplesY;

	inputData = (fftw_complex*) input->buffer->data;
	outputData = (fftw_complex*) (*output)->buffer->data;

	for (index = 0; index < samplesCount; index++)
	{
		outputData[index][RE] = inputData[index][RE] * factor;
		outputData[index][IM] = inputData[index][IM] * factor;
	}

	return RAYLEIGH_OK;

EXCEPTION

	if (outputAllocated)
		optfieldDispose(output);

	return RAYLEIGH_ERROR;
}


/*
 * TODO: various data type handling
 */
/* Elementwise summation: output = inputA + inputB.
 * inputA and inputB must have the same data dimensions (samplesX, samplesY)
 * Other input parameters are not checked.
 * NULL output means that an output optfield must be created.
 * Its parameters are taken from inputA.
 * Non-NULL output means that the space for the result already exists;
 * data dimensions (samplesX, samplesY) are checked only.
 * Output pointer can be the same as inputA or inputB.
 */
int optfieldAdd(t_optfield **output, t_optfield *inputA, t_optfield *inputB)
{
	int outputAllocated = 0;
	int outputSamplesX, outputSamplesY;
	int inputSamplesAX, inputSamplesAY;
	int inputSamplesBX, inputSamplesBY;
	RAYLEIGH_INT64 index, samplesCount;
	fftw_complex *inputDataA, *inputDataB, *outputData;

	if (inputA == NULL)
		GOEXCEPTION0("NULL input A pointer.\n");

	if (inputB == NULL)
		GOEXCEPTION0("NULL input B pointer.\n");

	if (output == NULL)
		GOEXCEPTION0("NULL output pointer.\n");

	optfieldGetSamples(&inputSamplesAX, &inputSamplesAY, inputA);
	optfieldGetSamples(&inputSamplesBX, &inputSamplesBY, inputB);

	if (inputSamplesAX != inputSamplesBX ||
		inputSamplesAY != inputSamplesBY)
		GOEXCEPTION0("Input samples counts do not match.\n");

	if (*output == NULL)
	{
		CHECK(optfieldCopy(output, inputA));
		outputAllocated = 1;
	}

	optfieldGetSamples(&outputSamplesX, &outputSamplesY, *output);

	if (inputSamplesAX != outputSamplesX ||
		inputSamplesAY != outputSamplesY)
		GOEXCEPTION0("Input and output samples counts do not match.\n");

	samplesCount = (RAYLEIGH_INT64)inputSamplesAX * (RAYLEIGH_INT64)inputSamplesAY;

	inputDataA = (fftw_complex*) inputA->buffer->data;
	inputDataB = (fftw_complex*) inputB->buffer->data;
	outputData = (fftw_complex*) (*output)->buffer->data;

	for (index = 0; index < samplesCount; index++)
	{
		outputData[index][RE] = inputDataA[index][RE] + inputDataB[index][RE];
		outputData[index][IM] = inputDataB[index][IM] + inputDataB[index][IM];
	}

	return RAYLEIGH_OK;

EXCEPTION

	if (outputAllocated)
		optfieldDispose(output);

	return RAYLEIGH_ERROR;
}


/*
 * TODO: various data type handling
 */
/* Elementwise multiplication: output = inputA .* inputB.
 * inputA and inputB must have the same data dimensions (samplesX, samplesY)
 * Other input parameters are not checked.
 * NULL output means that an output optfield must be created.
 * Its parameters are taken from inputA.
 * Non-NULL output means that the space for the result already exists;
 * data dimensions (samplesX, samplesY) are checked only.
 * Output pointer can be the same as inputA or inputB.
 */
int optfieldMul(t_optfield **output, t_optfield *inputA, t_optfield *inputB)
{
	int outputAllocated = 0;
	int outputSamplesX, outputSamplesY;
	int inputSamplesAX, inputSamplesAY;
	int inputSamplesBX, inputSamplesBY;
	RAYLEIGH_INT64 index, samplesCount;
	fftw_complex *inputDataA, *inputDataB, *outputData;
	double reA, imA, reB, imB;

	if (inputA == NULL)
		GOEXCEPTION0("NULL input A pointer.\n");

	if (inputB == NULL)
		GOEXCEPTION0("NULL input B pointer.\n");

	if (output == NULL)
		GOEXCEPTION0("NULL output pointer.\n");

	optfieldGetSamples(&inputSamplesAX, &inputSamplesAY, inputA);
	optfieldGetSamples(&inputSamplesBX, &inputSamplesBY, inputB);

	if (inputSamplesAX != inputSamplesBX ||
		inputSamplesAY != inputSamplesBY)
		GOEXCEPTION0("Input samples counts do not match.\n");

	if (*output == NULL)
	{
		CHECK(optfieldCopy(output, inputA));
		outputAllocated = 1;
	}

	optfieldGetSamples(&outputSamplesX, &outputSamplesY, *output);

	if (inputSamplesAX != outputSamplesX ||
		inputSamplesAY != outputSamplesY)
		GOEXCEPTION0("Input and output samples counts do not match.\n");

	samplesCount = (RAYLEIGH_INT64)inputSamplesAX * (RAYLEIGH_INT64)inputSamplesAY;

	inputDataA = (fftw_complex*) inputA->buffer->data;
	inputDataB = (fftw_complex*) inputB->buffer->data;
	outputData = (fftw_complex*) (*output)->buffer->data;

	for (index = 0; index < samplesCount; index++)
	{
		reA = inputDataA[index][RE];
		imA = inputDataA[index][IM];
		reB = inputDataB[index][RE];
		imB = inputDataB[index][IM];
		outputData[index][RE] = reA * reB - imA * imB;
		outputData[index][IM] = reA * imB + reB * imA;
	}

	return RAYLEIGH_OK;

EXCEPTION

	if (outputAllocated)
		optfieldDispose(output);

	return RAYLEIGH_ERROR;
}


/*
 * Find maxima/minima in the optfield
 * input: image
 * output: outMin - minimum in both the real and the imaginary part
 *		   outMax - maximum in both the real and the imaginary part
 */
int optfieldFindMinmax(t_optfield *image, fftw_complex outMin, fftw_complex outMax,
									   fftw_complex outMaxAbsSqSample,
									   double *outMaxAbsSq, RAYLEIGH_INT64 *outMaxAbsSqIndex)
{
	int samplesX, samplesY;
	RAYLEIGH_INT64 dataSize, i;
	fftw_complex min, max, maxAbsSqSample;
	double absSq, maxAbsSq;
	RAYLEIGH_INT64 maxAbsSqIndex;

	CHECK(optfieldGetSamples(&samplesX, &samplesY, image));
	dataSize = (RAYLEIGH_INT64)samplesX * (RAYLEIGH_INT64)samplesY;

	if (optfieldGetSampleType(image) == SampleTypeFFTWDouble)
	{
		T_SAMPLE_TYPE_FFTWDOUBLE *data;

		data = (T_SAMPLE_TYPE_FFTWDOUBLE*) (image->buffer->data);
		max[RE] = min[RE] = data[0][RE];
		max[IM] = min[IM] = data[0][IM];
		maxAbsSqSample[RE] = maxAbsSqSample[IM] = 0;
		maxAbsSq = 0;
		maxAbsSqIndex = 0;

		for (i=0; i<dataSize; i++)
		{
			if (data[i][RE] > max[RE]) max[RE] = data[i][RE];
			else if (data[i][RE] < min[RE]) min[RE] = data[i][RE];

			if (data[i][IM] > max[IM]) max[IM] = data[i][IM];
			else if (data[i][IM] < min[IM]) min[IM] = data[i][IM];

			absSq = data[i][RE] * data[i][RE] +
					data[i][IM] * data[i][IM];
			if (absSq > maxAbsSq)
			{
				maxAbsSq = absSq;
				maxAbsSqSample[RE] = data[i][RE];
				maxAbsSqSample[IM] = data[i][IM];
				maxAbsSqIndex = i;
			}
		}
	}
	else
		GOEXCEPTION0("Unsupported sample type in finding min/max\n");

	if (outMin != NULL)
	{
		outMin[RE] = min[RE];
		outMin[IM] = min[IM];
	}

	if (outMax != NULL)
	{
		outMax[RE] = max[RE];
		outMax[IM] = max[IM];
	}

	if (outMaxAbsSqSample != NULL)
	{
		outMaxAbsSqSample[RE] = maxAbsSqSample[RE];
		outMaxAbsSqSample[IM] = maxAbsSqSample[IM];
	}

	if (outMaxAbsSq != NULL)
	{
		*outMaxAbsSq = maxAbsSq;
	}

	if (outMaxAbsSqIndex != NULL)
	{
		*outMaxAbsSqIndex = maxAbsSqIndex;
	}

	return RAYLEIGH_OK;

EXCEPTION

	ELOG0("Error when calling optfieldFindMinmax.\n");
	return RAYLEIGH_ERROR;
}
