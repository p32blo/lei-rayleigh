/* 
 * Propagation of lightsArray to an optfield
 * 
 */

#ifndef __LIGHTSARRAYPROPAGATE_H
#define __LIGHTSARRAYPROPAGATE_H

#include "rayleigh.h"
#include "rhoLUT.h"
#include "waveLUT.h"

/*
 * user function:
 * Propagate lightsArray to the target
 * Actually just calls lightsPropagate()
 */
RAYLEIGH_EXPORT int lightsArrayPropagate(t_lightsArray *lightsArray, t_optfield *target);

/*
 * internal functions:
 * propagate point lights array to the target plane
 */

/* methods without lookup table acceleration */
RAYLEIGH_EXPORT int lightsPropagateAliased(t_light *lights, int lightsCount, t_optfield *target);
RAYLEIGH_EXPORT int lightsPropagate(t_light *lights, int lightsCount, t_optfield *target);

/* method using double LUT or 
 * waveLUT only (if rhoLUT == NULL)
 * acceleration 
 */
RAYLEIGH_EXPORT int lightsPropagateDLUT(t_light *lights, int lightsCount, t_optfield *target, t_rhoLUT *rhoLUT, t_waveLUT *waveLUT);

/* method that calls DLUT or WLUT type and prepares correct lookup tables */
RAYLEIGH_EXPORT int lightsPropagateDLUTAuto(t_light *lights, int lightsCount, t_optfield *target, 
							t_PropagationAcceleration propagationAcceleration,
							double waveLUTUpsampling, double zSamplingDistance, 
							double limitRhoSamplingDistance, double rhoSamplingFactor,
							int baseUpsampling, t_FilterType filterType, void *filterTypeParams);


#endif