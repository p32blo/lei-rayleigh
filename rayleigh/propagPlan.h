/*
 * Complex propagation tasks are driven by a propagation plan.
 * A propagation plan is a list of simple propagation tasks.
 *
 */

#ifndef __PROPAGPLAN
#define __PROPAGPLAN

#include "rayleigh.h"

/*
 * the result of partition is a "partition list" that describes
 * where to get data, where to put them and how big space to use for calculation
 */
typedef struct tp {
	int sourceStart_x, sourceStart_y;							/* start offset for source data */
	int sourceStep_x, sourceStep_y;								/* interleaving steps */
	int sourceSamples_x, sourceSamples_y;						/* samples of the source piece */

	int targetStart_x, targetStart_y;							/* the same for destination */
	int targetStep_x, targetStep_y;
	int targetSamples_x, targetSamples_y;

	int convolutionSamples_x, convolutionSamples_y;				/* fft-friendly space to use for convolution */

	struct tp *next;											/* next piece in linked chain */
} t_propagPlan;

/*
 * structures for samples/count of different pieces
 */
typedef struct {
	int count_x, count_y, samples_x, samples_y;
} t_piecesDataInfo;

typedef struct {
	int count_x, count_y, samples_x, samples_y;
	t_piecesDataInfo dataPiece_big, dataPiece_small;
} t_piecesSamplingInfo;

typedef struct {
	int samplingCount_x, samplingCount_y;
	t_piecesSamplingInfo samplingPiece_big, samplingPiece_small;
} t_piecesInfo;

/*
 * Free propagation plan
 */
RAYLEIGH_EXPORT int propagPlanDispose(t_propagPlan **propagPlan);

/*
 * Create a propagation plan
 */
RAYLEIGH_EXPORT int propagPlanNew(t_optfield *source, t_optfield *orig_image_target, RAYLEIGH_INT64 maximum_FFT_size, t_propagPlan **propagPlan);

/*
 * Add a piece to a propagation plan
 */
RAYLEIGH_EXPORT int propagPlanAdd(t_propagPlan **propag_list,
	int source_start_x, int source_start_y,
	int source_step_x, int source_step_y,
	int source_samples_x, int source_samples_y,
	int target_start_x, int target_start_y,
	int target_step_x, int target_step_y,
	int target_samples_x, int target_samples_y,
	int convolution_samples_x, int convolution_samples_y);

/*
 * Manually set a piece in a propagation plan
 */
RAYLEIGH_EXPORT int propagPieceSet(t_propagPlan *piece,
	int source_start_x, int source_start_y,
	int source_step_x, int source_step_y,
	int source_samples_x, int source_samples_y,
	int target_start_x, int target_start_y,
	int target_step_x, int target_step_y,
	int target_samples_x, int target_samples_y,
	int convolution_samples_x, int convolution_samples_y,
	t_propagPlan *pieceNext);

/*
 * find largest samples count in x and y directions
 * find largest buffer in term of total samples number
 */
RAYLEIGH_EXPORT int propagPlanFindLargestBuffers(t_propagPlan *propagPlan,
								 int *maxConvolutionSamples_x, int *maxConvolutionSamples_y,
								 RAYLEIGH_INT64 *maxConvolutionSamples_xy);

/*
 * count number of propagation operations on propagPlan
 */
RAYLEIGH_EXPORT int propagPlanPropagationCount(t_propagPlan *propagPlan);

#endif
