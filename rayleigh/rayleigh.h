/*
 * rayleigh.h
 *
 * Common header for Rayleigh light propagation software.
 * It is useful to read this .h file as it covers the structure of the software.
 *
 * Rayleigh software provides tools for monochromatic coherent light propagation.
 * There are some tools included like lens simulations, laser beam simulation and so on
 * but the main task is to take a 2D array of complex amplitudes (samples of
 * light field in a plane) and to calculate another 2D array of complex
 * amplitudes (propagated light field).
 *
 *
 */

#ifndef __RAYLEIGH_H
#define __RAYLEIGH_H

#ifdef __cplusplus
extern "C" {
#endif


#pragma warning(disable : 4996)
#pragma warning(disable : 4102)
#pragma warning(disable : 4100)

/*
 * common libraries
 */
#include <stdlib.h>
#include <stdint.h>

#include <stdio.h>
#include <string.h>

#include <math.h>
#include <float.h>

#ifdef RAYLEIGH_STATIC
	#define RAYLEIGH_EXPORT extern
#else
	#ifdef COMPILE_DLL
		#define RAYLEIGH_EXPORT extern __declspec(dllexport)
	#else
		#define RAYLEIGH_EXPORT extern __declspec(dllimport)
	#endif
#endif

/*
 * system specific 64bit integer type
 * TODO: extend for other compilers and platforms
 */
#define RAYLEIGH_INT64	int64_t

/*
 * Common return values. Most functions return if everything was OK or not.
 * However, nothing else is done. It is up to programmer to check this output
 * code and to decide what to do in the case of any error.
 */
#define RAYLEIGH_OK		0
#define RAYLEIGH_ERROR	1

/*
 * Some functions apply FFT to the calculated array (e.g. the propagation kernel)
 * if they are asked to do so.
 */

typedef enum {
	FFTApplyOff = 0,
	FFTApplyOn = 1
} t_FFTApply;

/*
 * There are some implementations that accelerate propagation.
 * - precise:    no acceleration at all
 * - double LUT: uses both LUT for rho=sqrt(x^2+y^2) and for u=f(rho,z)
 * - waveLUT:    uses just the LUT u=f(rho,z), the value of rho=sqrt(x^2+y^2) is evaluated on the fly
 */
typedef enum {
	PropagationAccPrecise = 0,
	PropagationAccDoubleLUT = 1,
	PropagationAccWaveLUT = 2
} t_PropagationAcceleration;

RAYLEIGH_EXPORT char *propagationMethodAccName[];


/*
 * SORRY, I AM IN THE MIDDLE OF REWRITING EXCEPTION HANDLING
 */

/*
 * There is a simple exception handling inside many functions of the Rayleigh
 * software. It is made by a few macros and a simple program structure:
 *      CHECK(f1(....));
 *       normal code 1
 *      CHECK(f2(....));
 *       normal code 2
 *      ...
 *      EXCEPTION
 *		 exception code
 */
//#define INITEXCEPTION(s)	int _rayleigh_retval=RAYLEIGH_ERROR; char *_rayleigh_funcname=s;
#define RAYLEIGH_RETVAL		_rayleigh_retval
#define INITEXCEPTION		int RAYLEIGH_RETVAL=RAYLEIGH_ERROR;
#define SETRETVALUE(s)		{ RAYLEIGH_RETVAL = s; }
#define ENDFUNCTION			goto rayleigh_stop_ok;
#define GOEXCEPTION			goto rayleigh_stop;
#define GOEXCEPTION0(s)		{ ELOG0(s); GOEXCEPTION }
#define GOEXCEPTION1(s, p1) { ELOG1(s, p1); GOEXCEPTION }
#define GOEXCEPTION2(s, p1, p2) { ELOG2(s, p1, p2); GOEXCEPTION }
#define CHECK(f)			{ if ((f) != RAYLEIGH_OK) GOEXCEPTION; }
#define CHECK0(f, s)		{ if ((f) != RAYLEIGH_OK) GOEXCEPTION0(s); }
#define CHECK1(f, s, p)		{ if ((f) != RAYLEIGH_OK) GOEXCEPTION1(s, p); }
#define EXCEPTION			rayleigh_stop:
#define EXCEPTION0(s)		rayleigh_stop_ok: \
							RAYLEIGH_RETVAL=RAYLEIGH_OK; \
							rayleigh_stop: \
								if(RAYLEIGH_RETVAL != RAYLEIGH_OK) \
									ELOG1("Error when calling %s.\n", s);
/*
#define EXCEPTION1(s, p)	_rayleigh_retval=RAYLEIGH_OK; \
							rayleigh_stop: \
								if(_rayleigh_retval != RAYLEIGH_OK) \
									ELOG2("Error when calling %s.\n", s, p);
*/
#define IFEXCEPTION			if (_rayleigh_retval != RAYLEIGH_OK)
#define ENDEXCEPTION		return  _rayleigh_retval;

/*
 * Logging to stdout or stderr should be handled using these
 * simple macros.
 */

#ifdef LOGS
	#define LOG0(s)					fprintf(stdout, (s));
	#define LOG1(s, p1)				fprintf(stdout, (s), (p1));
	#define LOG2(s, p1, p2)			fprintf(stdout, (s), (p1), (p2));
	#define LOG3(s, p1, p2, p3)		fprintf(stdout, (s), (p1), (p2), (p3));
	#define LOG4(s, p1, p2, p3, p4)		fprintf(stdout, (s), (p1), (p2), (p3), (p4));
	#define LOG5(s, p1, p2, p3, p4, p5)		fprintf(stdout, (s), (p1), (p2), (p3), (p4), (p5));
	#define LOG6(s, p1, p2, p3, p4, p5, p6)		fprintf(stdout, (s), (p1), (p2), (p3), (p4), (p5), (p6));

	#define ELOG0(s)				fprintf(stderr, (s));
	#define ELOG1(s, p1)			fprintf(stderr, (s), (p1));
	#define ELOG2(s, p1, p2)		fprintf(stderr, (s), (p1), (p2));
	#define ELOG3(s, p1, p2, p3)	fprintf(stderr, (s), (p1), (p2), (p3));
	#define ELOG4(s, p1, p2, p3, p4)		fprintf(stderr (s), (p1), (p2), (p3), (p4));
	#define ELOG5(s, p1, p2, p3, p4, p5)		fprintf(stderr, (s), (p1), (p2), (p3), (p4), (p5));
	#define ELOG6(s, p1, p2, p3, p4, p5, p6)		fprintf(stderr, (s), (p1), (p2), (p3), (p4), (p5), (p6));
#else
	#define LOG0(s)
	#define LOG1(s, p1)
	#define LOG2(s, p1, p2)
	#define LOG3(s, p1, p2, p3)
	#define LOG4(s, p1, p2, p3, p4)
	#define LOG5(s, p1, p2, p3, p4, p5)
	#define LOG6(s, p1, p2, p3, p4, p5, p6)

	#define ELOG0(s)
	#define ELOG1(s, p1)
	#define ELOG2(s, p1, p2)
	#define ELOG3(s, p1, p2, p3)
	#define ELOG4(s, p1, p2, p3, p4)
	#define ELOG5(s, p1, p2, p3, p4, p5)
	#define ELOG6(s, p1, p2, p3, p4, p5, p6)
#endif

/* debugging variable
 * some functions save debugging files, this is a unified way how to name them
 */
RAYLEIGH_EXPORT char *debugFnamePrefix;


/*
 * All the sampling distances have to be an integer multiple of
 * common_sampling_distance; it is better to do it this way as it
 * is then easy to decide e.g. whether sampling distance 1 is twice as
 * big as sampling distance 2. It is hard to do this if sampling distances
 * are defined as floating point values.
 * For example, common sampling distance could be set as 1 nm and
 * sampling distance of a particular image as 7000 (i.e. 7 um).
 * Or, it could be set as 7 um and sampling distance of
 * a particular image as 1 (i.e. 7 um as well).
 * The former one is however more flexible, it allows easily
 * work with sampling rates 1 um, 0.5 um, 7 um etc. in one optical setup.
 */
extern double common_sampling_distance;

/*
 * Rayleigh works with monochromatic coherent light only. However,
 * wavelength can be changed anywhere in the calculation. It is necessary
 * to set wave k_number = 2*PI/lambda as well.
 */
extern double lambda;
extern double k_number;


/*
 * Complex type is taken from FFTW.
 * It is declared as an array of two double numbers.
 * Indices for accessing real and imaginary parts of complex type:
 */
#define RE		0
#define IM		1

/*
 * Common length measures
 * Use is eg.: 1 MM   0.5 UM   (3 + 5) MM  3 MM + 50 UM
 * Beware of the bug: 1 + 2 MM means 1.002 m; for 3 MM, use (1 + 2) MM
 *
 * All the distances in Rayleigh are expressed in meters
 *
 */
#define KM		*1.0e+3
#define M
#define MM		*1.0e-3
#define UM		*1.0e-6
#define NM		*1.0e-9

/*
 * mainly for text output; e.g.
 * printf("Distance is %f mm", distance TOMM);
 */
#define TOKM		*1.0e-3
#define TOM
#define TOMM		*1.0e+3
#define TOUM		*1.0e+6
#define TONM		*1.0e+9

/*
 * DEG to RAD
 */

#define RAD
#define DEG		*(PI/180.0)

#define TORAD
#define TODEG		*(180.0/PI)

/*
 * Common constants
 */

#define PI		3.1415926535897932384626433832795
#define PI2		6.283185307179586476925286766559


/*
 * Point or vector in 3D space
 */
typedef struct {
	double x, y, z;
} t_point;

/*
 * Point light
 */
typedef struct {
	t_point position;   /* position of the light */
	double amplitude;   /* relative amplitude of the light; */
						/* amplitude of 1 has no direct relation to any physical value */
	double phase;       /* phase of the light */
	double phasorRe, phasorIm; /* phasor of the light, i.e. phasorRe = amplitude * cos(phase) etc. */
} t_light;

/* Include tools needed */

/* For FFT, the FFTW library is used. See http://www.fftw.org/ */
#include "fftw3.h"

/* Utility functions, no interesting function is there */
#include "utils.h"

/* Samples of light field, i.e. complex amplitudes, are held in
 * 2D arrays. However, it might be useful for decent memory management
 * to allocate blocks of memory (buffers) first and use them for
 * different purpuse through the computation.
 * Any "optical field" is then stored in a buffer.
 * Buffer.h covers buffers declaration and simple functions for their
 * management.
 */
#include "buffer.h"

/* Optical field itself is just a struct that covers its parameters
 * and points to a buffer with complex amplitudes.
 * Optfield.h covers optfield declaration and simple functions for their
 * management.
 */
#include "optfield.h"

/* Picture.h covers functions for optical fields input and output.
 */
#include "picture.h"

/* Some image functions use filtering, e.g. gaussian blurring,
 * there is also resampling included in filter.h
 */
#include "filter.h"

/* The core of the Rayleigh software, propagation functions.
*/
#include "propagPlan.h"

/* A few functions for lens simulation.
 */
#include "lens.h"

/* Simple optical grating pattern generator.
 */
#include "grating.h"

/* If it is necessary to illuminate e.g. a picture,
 * a few functions for e.g. spherical wave or gaussian beam
 * are included in lightsource.h.
 */
#include "lightsource.h"

/* Point light cloud */
#include "rhoLUT.h"
#include "waveLUT.h"
#include "kernelCalculate.h"
#include "lightsArray.h"
#include "lightsArrayPropagate.h"
#include "optfieldPropagate.h"

/* Debug functions for profiling.
 */
#include "time_profiling.h"


/*
 * base utility functions
 *
 */

/*
 * set lambda, wawenumber (2pi/lambda) and common sampling distance
 */
RAYLEIGH_EXPORT void rayleighSetEnvironment(double _lambda, double _commonSamplingDistance);
RAYLEIGH_EXPORT double rayleighGetKNumber(void);
RAYLEIGH_EXPORT double rayleighGetLambda(void);
RAYLEIGH_EXPORT double rayleighGetCommonSamplingDistance(void);

#ifdef __cplusplus
}
#endif

#endif
