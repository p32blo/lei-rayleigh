/*
 * Generating pattern of black or white pixels. Used as diffraction grating pattern generator.
 */

#ifndef __GRATING_H
#define __GRATING_H

#include "rayleigh.h"

/*
 * generate a 2-D pattern composed of square waves
 */
RAYLEIGH_EXPORT int gratingInitPattern(t_optfield *grating, char *grating_pattern, int grating_upsampling);

#endif
