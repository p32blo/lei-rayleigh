/*
 * Manipulating array of (point) light sources.
 * Mainly for testing purposes.
 */

#ifndef __LIGHTSARRAY_H
#define __LIGHTSARRAY_H

#include "rayleigh.h"

/* File types for lightsArray */
typedef enum {
	LightsArrayIOAuto = 0,      /* automatic filetype detection */
	LightsArrayIORawXYZ = 1,    /* raw text XYZ data */
	LightsArrayIORawXYZA = 2,   /* raw text XYZ data */
	LightsArrayIORawXYZAP = 3,  /* raw text XYZ amplitude phase */
	LightsArrayIORawXYZRGB = 4, /* raw text XYZ red green blue data */
	LightsArrayIOPLY = 5       /* text based PLY format */
} t_LightsArrayIO;

/*
 * IF CHANGED, CHANGE numCountExpected[] AS WELL !!
 */

/* For lines generation */
typedef enum {
	GenereteEndsOff = 0,
	GenereteEndsOn  = 1
} t_LightsArrayOptions;

/*
 * simple point light cloud for testing purposes
 */
typedef struct {
	t_light *lights;     /* point light sources */
	int lightsCount;     /* actual number of point lights in the array */
	int maxLightsCount;  /* maximum number of point lights, i.e. amount of allocated memory */
} t_lightsArray;


/*
 * create an empty lightsArray
 */
RAYLEIGH_EXPORT int lightsArrayNew(t_lightsArray **lights, int maxLightsCount);

/*
 * create a new lightsArray; if a nonempty *lights is provided,
 * copy its contents (up to maxLightsCount) to a newly created one.
 */
RAYLEIGH_EXPORT int lightsArrayRealloc(t_lightsArray **lights, int maxLightsCount);

/*
 * copy up to N lights fro src_lights to dst_lights
 * if dst_lights==NULL, make a ne one
 */
RAYLEIGH_EXPORT int lightsArrayCopyN(t_lightsArray **dst_lights, t_lightsArray *src_lights, int maxLightsCount);

/*
 * dispose a point lights cloud
 */
RAYLEIGH_EXPORT int lightsArrayDispose(t_lightsArray **lights);


/*
 * print the contents of the point lights cloud
 */
RAYLEIGH_EXPORT int lightsArrayList(t_lightsArray *lights);

/*
 * getters/setters
 */
RAYLEIGH_EXPORT int lightsArrayGetLightsCount(t_lightsArray *lights);
RAYLEIGH_EXPORT int lightsArrayGetMaxLightsCount(t_lightsArray *lights);

/*
 * Read a lightsArray from a file
 */
RAYLEIGH_EXPORT int lightsArrayLoad(t_lightsArray **lights, char *filename, t_LightsArrayIO filetype);

/*
 * Save a lightsArray to a file
 */
RAYLEIGH_EXPORT int lightsArraySave(t_lightsArray *lights, char *filename, t_LightsArrayIO filetype);

/*
 * Randomize lights phase
 */
RAYLEIGH_EXPORT int lightsArrayAddRndPhase(t_lightsArray *lights, double maxPhaseRad);

/*
 * Generate a point light source
 * and store it to a lightsArray
 */
RAYLEIGH_EXPORT int lightsArrayGeneratePoint(t_lightsArray **lights,
						double ax, double ay, double az, double aa, double ap);

/*
 * Generate a line composed of point lights
 * and store it to a lightsArray
 */
RAYLEIGH_EXPORT int lightsArrayGenerateLine(t_lightsArray **lights,
					   double ax, double ay, double az, double aa, double ap,
					   double bx, double by, double bz, double ba, double bp,
					   int randomSeed, double randomPhase, int n, t_LightsArrayOptions generateEnds);

/*
 * generate a lights array for testing purposes
 */
RAYLEIGH_EXPORT int lightsArrayGenerateTest(t_lightsArray **lights, double dist, double z1, double z2);

/*
 * sort lights by z coordinate
 */
RAYLEIGH_EXPORT int lightsSort(t_light *lights, int lightsCount);

/*
 * find extremal X, Y, Z, amplitude, phase, phasorIm, phasorRe of lights in the provided array
 * if any of the output pointers is NULL, it is not set
 */
RAYLEIGH_EXPORT int lightsFindExtreme(t_light *lights, int lightsCount,
					  t_light *minValues, t_light *maxValues);


/*
 * testing functions
 */
void lightsArrayTestIO(void);

#endif
