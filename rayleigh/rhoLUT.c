#include "rayleighCompile.h"

/* gettters / setters
 * (just getters in fact, no setters make sense)
 */
double rhoLUTGetSamplingDistance(t_rhoLUT *rhoLUT)
{
	return rhoLUT->samplingDistance;
}

double rhoLUTGetDeltaXMin(t_rhoLUT *rhoLUT)
{
	return rhoLUT->deltaXMin;
}

double rhoLUTGetDeltaYMin(t_rhoLUT *rhoLUT)
{
	return rhoLUT->deltaYMin;
}

double rhoLUTGetDeltaXMax(t_rhoLUT *rhoLUT)
{
	return rhoLUT->deltaXMax;
}

double rhoLUTGetDeltaYMax(t_rhoLUT *rhoLUT)
{
	return rhoLUT->deltaYMax;
}

int rhoLUTGetSamplesX(t_rhoLUT *rhoLUT)
{
	return rhoLUT->samplesX;
}

int rhoLUTGetSamplesY(t_rhoLUT *rhoLUT)
{
	return rhoLUT->samplesY;
}

double rhoLUTGetRhoMin(t_rhoLUT *rhoLUT)
{
	return rhoLUT->rhoMin;
}

double rhoLUTGetRhoMax(t_rhoLUT *rhoLUT)
{
	return rhoLUT->rhoMax;
}


/*
 * Get rho for a given double (x,y) point using various interpolations.
 */

double rhoLUTGetRhoConstant(double x, double y, t_rhoLUT *rhoLUT)
{
	return rhoLUT->rho[(int)(floor(x+0.5)) + (int)(floor(y+0.5))*rhoLUT->samplesX];
}

double rhoLUTGetRhoBilinear(double x, double y, t_rhoLUT *rhoLUT)
{
	double xFrac, yFrac;
	int xInt, yInt;
	double rho00, rho01, rho10, rho11;

	xFrac = x - floor(x);
	xInt = (int) floor(x);

	yFrac = y - floor(y);
	yInt = (int) floor(y);

	rho00 = rhoLUT->rho[xInt + yInt*rhoLUT->samplesX];
	rho01 = rhoLUT->rho[xInt + (yInt+1)*rhoLUT->samplesX];
	rho10 = rhoLUT->rho[xInt + 1 + yInt*rhoLUT->samplesX];
	rho11 = rhoLUT->rho[xInt + 1 + (yInt+1)*rhoLUT->samplesX];
	/*  simple bilinear interpolation:
	 *
		return rho00*(1-xFrac)*(1-yFrac) +
			   rho01*(1-xFrac)*(yFrac) +
			   rho10*(xFrac)*(1-yFrac) +
			   rho11*(xFrac)*(yFrac);
	*/

	/* Using Horner's method: */
	return bilinearInterp(rho00, rho01, rho10, rho11, xFrac, yFrac);
}


/*
 * auxiliary function:
 * find maximum and minimum value of |P1-P2|,
 * where P1 is from [p1Min, p1Max]
 * and P2 is from [p2Min, p2Max]
 */
void findExtremeRange(double p1Min, double p1Max,
					 double p2Min, double p2Max,
					 double *absDeltaPMin, double *absDeltaPMax)
{
	double tmp;
	double deltaPMin, deltaPMax;
	double aMin, aMax;

	/* find signed extremes deltaPMin, deltaPMax */
	deltaPMax = deltaPMin = p1Min - p2Min;

	tmp = p1Max - p2Min;
	if (tmp < deltaPMin) deltaPMin = tmp;
	if (tmp > deltaPMax) deltaPMax = tmp;

	tmp = p1Min - p2Max;
	if (tmp < deltaPMin) deltaPMin = tmp;
	if (tmp > deltaPMax) deltaPMax = tmp;

	tmp = p1Max - p2Max;
	if (tmp < deltaPMin) deltaPMin = tmp;
	if (tmp > deltaPMax) deltaPMax = tmp;

	/* determine absolute value extremes */
	/* assume deltaPMin < deltaPMax */
	aMin = fabs(deltaPMin);
	aMax = fabs(deltaPMax);

	if (deltaPMin > 0) /* i.e. also deltaPMax > 0 */
	{
		*absDeltaPMin = aMin;
		*absDeltaPMax = aMax;
	}
	else if (deltaPMax < 0) /* i.e. also deltaPMin < 0 */
	{
		*absDeltaPMax = aMin;
		*absDeltaPMin = aMax;
	}
	else
	{
		*absDeltaPMin = 0;
		*absDeltaPMax = aMin > aMax ? aMin : aMax;
	}
}




/*
 * Find extremal xy for vectors, where the starting point is in rectangle 1
 * and the ending point in a rectangle 2.
 * rectangle 1 (x1Min to x1Max) x (y1Min to y1Max)
 * rectangle 2 (x2Min to x2Max) x (y2Min to y2Max)
 *
 * output:
 *   min and max X difference (absolute value)
 *   min and max Y difference (absolute value)
 *   min and max rho
 * minimum
 */
int rhoLUTFindExtremeXYRel(double x1Min, double x1Max,
					  double y1Min, double y1Max,
					  double x2Min, double x2Max,
					  double y2Min, double y2Max,
					  double *absDeltaXMin, double *absDeltaXMax,
					  double *absDeltaYMin, double *absDeltaYMax)
{
	/* sanity check */
	if (absDeltaXMin == NULL || absDeltaXMax == NULL ||
		absDeltaYMin == NULL || absDeltaYMax == NULL)
		GOEXCEPTION;

	findExtremeRange(x1Min, x1Max, x2Min, x2Max, absDeltaXMin, absDeltaXMax);
	findExtremeRange(y1Min, y1Max, y2Min, y2Max, absDeltaYMin, absDeltaYMax);

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}


int rhoLUTFindMinMaxRelShift(t_optfield *source, t_optfield *target, t_point *minRelShift, t_point *maxRelShift)
{
	int sourceX, sourceY, targetX, targetY;
	double reducedSourceSizeX, reducedSourceSizeY;
	double reducedTargetSizeX, reducedTargetSizeY;
	double minXRel, minYRel, maxXRel, maxYRel;
	t_point sourceCorner, targetCorner;

	/* Get properties of the source, check if the source is valid.
	 * reducedSourceSize is the distance betwwen the first and the last sample
	 * in a row/column.
	 * Please note that this is not the same as optfieldGetSize; it supposes that an
	 * optfield sample represents a non
	 */
	CHECK0(optfieldGetSamples(&sourceX, &sourceY, source), "Weird source optfield.\n");
	optfieldGetCorner(&sourceCorner.x, &sourceCorner.y, &sourceCorner.z, source);
	reducedSourceSizeX = (sourceX-1) * optfieldGetSamplingdistance(source);
	reducedSourceSizeY = (sourceY-1) * optfieldGetSamplingdistance(source);

	/* Do the same for the target */
	CHECK0(optfieldGetSamples(&targetX, &targetY, target), "Weird target optfield.\n");
	optfieldGetCorner(&targetCorner.x, &targetCorner.y, &targetCorner.z, target);
	reducedTargetSizeX = (targetX - 1) * optfieldGetSamplingdistance(target);
	reducedTargetSizeY = (targetY - 1) * optfieldGetSamplingdistance(target);

	/* Find the biggest shift between all the corners of the source and of the target.
	 * Note the precise function of the rhoLUTFindExtremeXYRel: if, e.g.
	 * sourceCorner = 0, sourceCorner + reducedSourceSize = 10,
	 * targetCorner = 0.5, sourceCorner + reducedSourceSize = 10.5,
	 * samplingDistance = 1,
	 * then minRel will be 0 because there is a point S in
	 * the countinuous source (e.g. 5,5,sz), and a point T in the
	 * continuous target (e.g. 5,5,tz) so that T-S = (0,0,tz-sz).
	 * However, there are no two actual samples of the source and the target
	 * that give the same shift vector.
	 *
	 * This is no issue when rho is to be interpolated as then it is better
	 * to have rho for (0,0) vector in the rhoLUT.
	 */
	rhoLUTFindExtremeXYRel(
		sourceCorner.x, sourceCorner.x + reducedSourceSizeX,
		sourceCorner.y, sourceCorner.y + reducedSourceSizeY,
		targetCorner.x, targetCorner.x + reducedTargetSizeX,
		targetCorner.y, targetCorner.y + reducedTargetSizeY,
		&minXRel, &maxXRel,
		&minYRel, &maxYRel);

	if (minRelShift != NULL)
	{
		minRelShift->x = minXRel;
		minRelShift->y = minYRel;
		minRelShift->z = targetCorner.z - sourceCorner.z;
	}

	if (maxRelShift != NULL)
	{
		maxRelShift->x = maxXRel;
		maxRelShift->y = maxYRel;
		maxRelShift->z = targetCorner.z - sourceCorner.z;
	}

	return RAYLEIGH_OK;

EXCEPTION
	ELOG0("Error in rhoLUTFindMinMaxRelShift.\n");
	return RAYLEIGH_ERROR;
}



/*
 * create a rho LUT
 */
int rhoLUTNew(double xMin, double xMax,
				 double yMin, double yMax,
				 double samplingDistance,
				 t_rhoLUT **rhoLUT)
{
	int samplesY, samplesX;
	int i, j, index;
	double x, y, yy;
	double rhoMin, rhoMax, rho;

	INITEXCEPTION

	/* sanity check */
	if (rhoLUT == NULL || *rhoLUT != NULL)
		GOEXCEPTION0("NULL rhoLUT pointer.\n");

	if (xMin < 0 || xMax < 0 ||
		yMin < 0 || yMax < 0 ||
		xMax - xMin < 0 ||
		yMax - yMin < 0)
		GOEXCEPTION0("Error in the range of either X or Y.\n");

	/* create rhoLUT structure */
	*rhoLUT = (t_rhoLUT*) malloc(sizeof(t_rhoLUT));
	if (*rhoLUT == NULL)
		GOEXCEPTION0("Error allocating rhoLUT structure.\n");

	/* fill the rhoLUT parameters */
	(*rhoLUT)->samplesX = samplesX = (int) ceil((xMax-xMin) / samplingDistance)+1;
	(*rhoLUT)->samplesY = samplesY = (int) ceil((yMax-yMin) / samplingDistance)+1;
	(*rhoLUT)->samplingDistance = samplingDistance;
	(*rhoLUT)->deltaXMin = xMin;
	(*rhoLUT)->deltaXMax = xMin + (samplesX - 1)*samplingDistance;
	(*rhoLUT)->deltaYMin = yMin;
	(*rhoLUT)->deltaYMax = yMin + (samplesY - 1)*samplingDistance;

	/* allocate the rhoLUT data */
	(*rhoLUT)->rho = (double*) malloc((RAYLEIGH_INT64)samplesX * (RAYLEIGH_INT64)samplesY * sizeof(double));
	if ((*rhoLUT)->rho == NULL)
		GOEXCEPTION0("Error allocating rhoLUT data.\n");;

	rhoMax = 0;
	rhoMin = xMin*xMin + yMin*yMin;

	/* fill the rhoLUT data */
	for (i=0, index = 0; i<samplesY; i++)
	{
		y = i * samplingDistance + yMin;
		yy = y * y;
		for (j=0; j<samplesX; j++, index++)
		{
			x = j * samplingDistance + xMin;
			(*rhoLUT)->rho[index] = rho = sqrt(x*x + yy);
			if (rho < rhoMin)
				rhoMin = rho;
			if (rho > rhoMax)
				rhoMax = rho;
		}
	}

	(*rhoLUT)->rhoMin = rhoMin;
	(*rhoLUT)->rhoMax = rhoMax;

EXCEPTION0("rhoLUTCreate")

	IFEXCEPTION
	if (rhoLUT != NULL)
	{
		if (*rhoLUT != NULL && (*rhoLUT)->rho != NULL)
			free((*rhoLUT)->rho);

		free(*rhoLUT);
	}

ENDEXCEPTION;
}


/*
 * dispose a rhoLUT
 */
int rhoLUTDispose(t_rhoLUT **rhoLUT)
{
	if (rhoLUT == NULL || *rhoLUT == NULL)
		GOEXCEPTION0("NULL pointer when calling rhoLUTDispose.\n");

	if ((*rhoLUT)->rho != NULL)
		free((*rhoLUT)->rho);

	free(*rhoLUT);
	*rhoLUT = NULL;
	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}

/*
 * save rhoLUT as an image for debugging purposes
 */
int rhoLUTSave(t_rhoLUT *rhoLUT, char *filename)
{
	t_optfield *tmp = NULL;
	int i, j, index;

	INITEXCEPTION;

	if (rhoLUT == NULL)
		GOEXCEPTION0("NULL rhoLUT pointer.\n");

	CHECK(optfieldNew(0, 0, 0, rhoLUT->samplesX, rhoLUT->samplesY, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &tmp));

	for (i=0, index=0; i<rhoLUT->samplesY; i++)
		for (j=0; j<rhoLUT->samplesX; j++, index++)
		{
			((fftw_complex*)tmp->buffer->data)[index][RE] = rhoLUT->rho[index];
			((fftw_complex*)tmp->buffer->data)[index][IM] = 0;
		}

	CHECK(optfieldSavePNG(tmp, filename, PictureIORealImagIntensity, 1.0, 0, NULL))

EXCEPTION0("saveRhoLUT");

	if (tmp != NULL)
		optfieldDispose(&tmp);

ENDEXCEPTION;
}


int rhoLUTPropagationNew(t_optfield *source, t_optfield *target,
							t_rhoLUT **rhoLUT,
							t_point *rMinRelShift, t_point *rMaxRelShift,
							int rhoLUTsubsampling)
{
	t_point minRelShift, maxRelShift;
	double rhoLUTSamplingDistance;

	if (rhoLUT == NULL)
	{
		GOEXCEPTION0("NULL rhoLUT pointer.\n");
	}

	/* Calculate minRelShift and maxRelShift,
	 * ur use provided ones.
	 */
	if (rMaxRelShift == NULL)
	{
		/* solving cases: rMaxRelShift == NULL, rMinRelShift == NULL
		 *                rMaxRelShift == NULL, rMinRelShift != NULL
		 */

		CHECK0(rhoLUTFindMinMaxRelShift(source, target, &minRelShift, &maxRelShift), \
			"Cannot find geometrical shift for the source and target.\n");

		if (rMinRelShift != NULL)
			memcpy((void*)(&minRelShift), (void*)(rMinRelShift), sizeof(t_point));

	}
	else
	{
		/* solving cases: rMaxRelShift != NULL, rMinRelShift == NULL
		 *                rMaxRelShift != NULL, rMinRelShift != NULL
		 */

		memcpy((void*)(&maxRelShift), (void*)(rMaxRelShift), sizeof(t_point));

		if (rMinRelShift != NULL)
			memcpy((void*)(&minRelShift), (void*)(rMinRelShift), sizeof(t_point));
		else
		{
			CHECK0(rhoLUTFindMinMaxRelShift(source, target, &minRelShift, NULL), \
				"Cannot find geometrical shift for the source and target.\n");
		}
	}


	/* TODO
	 * determine fine enough rhoLUT sampling distance
	 */
	rhoLUTSamplingDistance = optfieldGetSamplingdistance(target)*rhoLUTsubsampling;

	/* rhoLUT has to be a few samples bigger for correct bilinear interpolation.
	 * Strictly, here should be a switch and add a few samples if bilinear
	 * interpolation is in use, but who cares. Moreover, the rhoLUT prepared in a
	 * unified way will work for whatever interpolation method later.
	 * Moreover, strictly said, one more sample should be enough. We add two just
	 * to make sure that no rounding errors cause an error.
	 */
	LOG0("Creating rho LUT...\n");
	CHECK0(
		rhoLUTNew(minRelShift.x, maxRelShift.x + rhoLUTSamplingDistance*2, \
					 minRelShift.y, maxRelShift.y + rhoLUTSamplingDistance*2, \
					 rhoLUTSamplingDistance, \
					 rhoLUT), \
		 "Failed to create the rhoLUT.\n");

	LOG2("Created rho LUT %d x %d\n", (*rhoLUT)->samplesX, (*rhoLUT)->samplesY);

	return RAYLEIGH_OK;

EXCEPTION
		ELOG0("Error in rhoLUTPropagationCreate.\n");
		return RAYLEIGH_ERROR;
}


/*
 *
 * Auxiliary functions for the rhoLUT sampling estimates.
 *
 * They are intended to be purely inernal.
 *
 */

/* Rho calculation for easier formula writing. */
double _rho(double x, double y)
{
	return sqrt(x*x + y*y);
}

/* Local frequency of the Rayleigh-Sommerfeld kernel
 * with respect to rho (i.e. radial frequency)
 */
double _localFreqRS(double rho, double z)
{
	return rho/(sqrt(rho*rho + z*z) * lambda);
}


/*
 * Calculate the (precise) error of the rhoLUT lookup using bilinear interpolation
 * with respect to the Rayleigh-Sommerfeld error.
 * Input:
 * x, y - the corner of the rhoLUT cell
 * z    - propagation distance
 * samplingReference - size of the rhoLUT cell (i.e. rhoLUT sampling).
 *
 * Output:
 * A value that tells how much is the value kernel(rhoLUT(x,y),z) off the real
 * value kernel(rho(x,y),z).
 * Value of "1" means that the rhoLUT caused an error one fringe wide. This is
 * unacceptable; values well below 1 are therefore needed.
 */
double rhoLUTErr(double x, double y, double z, double samplingReference)
{
	double rhop;
	double rhoa, rho00, rho01, rho10, rho11;
	double lf;

	/* calculate precise rho in the center of the cell */
	rhop = _rho(x+samplingReference/2, y+samplingReference/2);

	/* calculate precise rho in the cell's corners */
	rho00 = _rho(x, y);
	rho01 = _rho(x, y+samplingReference);
	rho10 = _rho(x+samplingReference, y);
	rho11 = _rho(x+samplingReference, y+samplingReference);

	/* calculate bilinear approximation of the rho in the center of the cell */
	rhoa = (rho00+rho01+rho10+rho11)/4;

	/* calculate the local frequency at the center of the cell */
	lf = _localFreqRS(rhop, z);

	return (rhoa - rhop)*lf;
}


/*
 * Calculate the optimal sampling of the rhoLUT.
 * Bilinear interpolation is assumed.
 * Input: wanted precision in multiples of the fringe spacing;
 *        e.g. 0.01 means that approximated rho will be off the
 *        exact position up to 0.01*local spacing of the Rayleigh-Sommerfeld
 *        pattern.
 *        x, y: minimum (x,y) of the rhoLUT
 *        z: propagation distance.
 *
 * See Maxima document "presnost_rho_kombinovane.wxm" (in Czech) for details.
 */
double rhoLUTOptSampling(double precision, double x, double y, double z)
{
	/* An error is evaluated for a particular sampling (samplingReference)
	 * using rhoLUTerr() function (err in the Maxima document).
	 * Then it is possible to guess how the error behaves in a quite wide range
	 * of parameters; moreover, it is possible to guess the parameters (namely sampling)
	 * to reach given error ("precision").
	 * The "samplingReference" is set to 1 um as it gives good results.
	 *
	 * The formula below contains powers 0.5 (square roots) because the log-log
	 * graph of the rhoLUTErr has derivative approximately 2. Multiplications and
	 * divisions follow from un-log (exponential) of a linear equation in log-log space.
	 */
	const static double samplingReference = 1e-6;
	return sqrt(precision) * samplingReference/sqrt(rhoLUTErr(x, y, z, samplingReference));
}
