/*
 * waveLUT
 *
 * The impulse response of the free space propagation is usually
 * described using a (kernel) function with radial symmetry,
 * e.g. the Rayleigh-Sommerfeld kernel
 *   u(x, y, z) = -1/2pi (jk - 1/r) exp(jkr)/r z/r
 * where
 *   r = sqrt(rho^2 + z^2)
 *   rho = sqrt(x^2 + y^2)
 * The waveLUT samples u(rho, z) = rho(x, y, z) for a given range of z and rho.
 *
 * It is assumed that waveLUT will be used together with rhoLUT.
 */

#ifndef __WAVELUT_H
#define __WAVELUT_H

#include "rayleigh.h"

typedef struct {
	int samplesRho, samplesZ;                           /* dimensions of the LUT */
	double zMin, zMax, zSamplingDistance;               /* parameters of Z sampling */
														/* zMax = zMin + zSamplingDistance * (samplesZ-1) */
	double rhoMin, rhoMax, rhoSamplingDistance;         /* parameters of rho sampling */
	double limitRhoSamplingDistance;                    /* phasor values are low-passed so that they can be captured
														 * using limitRhoSamplingDistance. This is quite useful;
														 * limitRhoSamplingDistance usually equals sampling distance
														 * of the optical field (e.g. a hologram) while
														 * real rhoSamplingDistance can be much higher to limit
														 * error introduced by LUT instead of direct calculation.
														*/
	fftw_complex *phasor;                               /* values of u(rho, z) */
	int *samplesRhoNonZero;                             /* for every z, only rhoSamples values are nonzero
														 *   due to proper antialiasing of the waveLUT */
} t_waveLUT;


/*
 * create the waveLUT
 */
RAYLEIGH_EXPORT int waveLUTNew(double rhoMin, double rhoMax,
				  double rhoSamplingDistance,
				  double zMin, double zMax,
				  double zSamplingDistance,
				  double limitRhoSamplingDistance,
				  double rhoSamplingFactor,
				  int baseUpsampling, t_FilterType filterType, void *filterTypeParams,
				  t_waveLUT **waveLUT);


/*
 * calculate samplesRho and samplesZ for given parameters
 */
RAYLEIGH_EXPORT int waveLUTCalculateSamples(double rhoMin, double rhoMax,
							double rhoSamplingDistance,
							double zMin, double zMax,
							double zSamplingDistance,
							double limitRhoSamplingDistance,
							double rhoSamplingFactor,
							int *samplesRho, int *samplesZ);

/*
 * dispose a waveLUT
 */
RAYLEIGH_EXPORT int waveLUTDispose(t_waveLUT **waveLUT);

/*
 * save waveLUT as an image for debugging purposes
 */
RAYLEIGH_EXPORT int waveLUTSave(t_waveLUT *waveLUT, char *filename);
RAYLEIGH_EXPORT int waveLUTSaveInterp(t_waveLUT *waveLUT, char *filename, t_FilterType filterType, void *filterTypeParams, int rhoUpsampling);

/*
 * getters/setters
 */

RAYLEIGH_EXPORT int waveLUTGetSamplesRho(t_waveLUT *waveLUT);
RAYLEIGH_EXPORT int waveLUTGetSamplesZ(t_waveLUT *waveLUT);
RAYLEIGH_EXPORT double waveLUTGetZMin(t_waveLUT *waveLUT);
RAYLEIGH_EXPORT double waveLUTGetZMax(t_waveLUT *waveLUT);
RAYLEIGH_EXPORT double waveLUTGetZSamplingDistance(t_waveLUT *waveLUT);
RAYLEIGH_EXPORT double waveLUTGetRhoMin(t_waveLUT *waveLUT);
RAYLEIGH_EXPORT double waveLUTGetRhoMax(t_waveLUT *waveLUT);
RAYLEIGH_EXPORT double waveLUTGetRhoSamplingDistance(t_waveLUT *waveLUT);
RAYLEIGH_EXPORT double waveLUTGetRLimitRhoSamplingDistance(t_waveLUT *waveLUT);

#endif
