/*
 * waveLUT
 *
 * The impulse response of the free space propagation is usually
 * described using a (kernel) function with radial symmetry,
 * e.g. the Rayleigh-Sommerfeld kernel
 *   u(x, y, z) = -1/2pi (jk - 1/r) exp(jkr)/r z/r
 * where
 *   r = sqrt(rho^2 + z^2)
 *   rho = sqrt(x^2 + y^2)
 * The waveLUT samples u(rho, z) = rho(x, y, z) for a given range of z and rho.
 *
 * It is assumed that waveLUT will be used together with rhoLUT.
 */


#include "rayleighCompile.h"
#include <float.h>

/*
 * getters/setters
 * (just getters in fact, there is no sense to make setters)
 */

int waveLUTGetSamplesRho(t_waveLUT *waveLUT)
{
	return waveLUT->samplesRho;
}

int waveLUTGetSamplesZ(t_waveLUT *waveLUT)
{
	return waveLUT->samplesZ;
}

double waveLUTGetZMin(t_waveLUT *waveLUT)
{
	return waveLUT->zMin;
}

double waveLUTGetZMax(t_waveLUT *waveLUT)
{
	return waveLUT->zMax;
}

double waveLUTGetZSamplingDistance(t_waveLUT *waveLUT)
{
	return waveLUT->zSamplingDistance;
}

double waveLUTGetRhoMin(t_waveLUT *waveLUT)
{
	return waveLUT->rhoMin;
}

double waveLUTGetRhoMax(t_waveLUT *waveLUT)
{
	return waveLUT->rhoMax;
}

double waveLUTGetRhoSamplingDistance(t_waveLUT *waveLUT)
{
	return waveLUT->rhoSamplingDistance;
}

double waveLUTGetRLimitRhoSamplingDistance(t_waveLUT *waveLUT)
{
	return waveLUT->limitRhoSamplingDistance;
}


/*
 * Quite internal function, but useful sometimes.
 *
 * Determines how many waveLUT samples are necessary for given parameters.
 * Currently, only Rayleigh-Sommerfeld kernel is assumed
 *
 * Description of input parameters: see waveLUTNew
 * Output parameters: samplesRho, samplesZ - samples count for each dimension
 */

int waveLUTCalculateSamples(double rhoMin, double rhoMax,
							double rhoSamplingDistance,
							double zMin, double zMax,
							double zSamplingDistance,
							double limitRhoSamplingDistance,
							double rhoSamplingFactor,
							int *samplesRho, int *samplesZ)
{
	double z, zz, maxCorrectRho;
	int maxSample;

	INITEXCEPTION;

	/* determine number of z samples */
	*samplesZ = (int) ceil((zMax - zMin) / zSamplingDistance) + 1;
	if (*samplesZ < 1)
		GOEXCEPTION0("Bad zMin, zMax or zSamplingDistance (leading to negative Z samples count).\n");

	/* determine maximum width of the row, i.e. number of samples */
	/* this is the absolute maximum needed due to the maximum rho value and rhoSamplingDistance */
	*samplesRho = (int) ceil((rhoMax - rhoMin) / rhoSamplingDistance) + 1;

	if (*samplesRho < 1)
		GOEXCEPTION0("Bad rhoMin, rhoMax or rhoSamplingDistance (leading to negative rho samples count).\n");

	/* However, fringes are not allowed to alias; ve have to determine maximum sample that does not.
	   Be a little careful as zmin and zmax are allowed to cross zero.
	*/
	z = fabs(zMax);
	zz = fabs(zMin);
	if (zz > z)
		z = zz;

	/* find maximum rho that can be sampled with limitRhoSamplingDistance */
	/*
	 * theory:
	 * A point light source phasor is mainly exp(j 2pi/lambda sqrt(rho^2 + z^2))
	 * i.e. its local frequency is given as
	 * f = 1/lambda \partial/partial\rho \sqrt{\rho^2 + z^2}
	 *   = \rho/(lambda sqrt{\rho^2 + z^2})
	 * As the sampling distance is \Delta = limitRhoSamplingDistance,
	 * the maximum frequency that can be sampled is 1/2\Delta, maximum rho
	 * is a solution of the equation
	 * 1/2\Delta = \rho/(lambda sqrt{\rho^2 + z^2})
	 */
	maxCorrectRho = lambda * z / sqrt(4*limitRhoSamplingDistance*limitRhoSamplingDistance - lambda*lambda);

	/* maxCorrectRho can be INF or NaN, i.e. no limit of sampling was found */
	/* If it is not the case, we have to determine consequences. */
	if (isfinite(maxCorrectRho))
	{
		/* Maximum sample that is not affected by aliasing. */
		/* Note that rho is sampled from rhoMin. */
		/* rhoLUTWidthFactor controls how higher local frequencies are clipped;
		   rhoLUTWidthFactor = 1 tells that any higher local frequencies should be strictly cut off.
		   However, a proper filtering should sample the phasor using fine sampling distance,
		   and apply an antialiasing (low-pass) filter. It is then likely that samples for
		   rho > maxCorrectRho will gradually fade to zero.
		   For practical purposes, rhoLUTWidthFactor should be between 1 and 2.
		   */
		maxSample = (int)floor((maxCorrectRho * rhoSamplingFactor - rhoMin) / rhoSamplingDistance);

		/* Is the rhoSamplingDistance high enough so that at least some samples
		 * can ba calculated? For example, if rhoMin is high and samplingDistance big,
		 * no alias-free sample can be taken.
		 */
		if (maxSample <= 0)
			GOEXCEPTION0("rhoMin or rhoSamplingDistance too big when calling fastcghCreateWaveLUT\n");

		if (maxSample < *samplesRho)
			*samplesRho = maxSample;
	}

	EXCEPTION0("waveLUTCalculateSamples");
	ENDEXCEPTION;
}


/*
 * create the waveLUT
 *
 * input: rhoMin, rhoMax - range of rho (i.e. sqrt(x^2 + y^2) to be sampled, 0 <= rhoMin <= rhoMax
 *           In fact, rhoMax is given as a theoretical value only. It is further re-evaluated
 *           so that actual rhoMax is equal to rhoMin + rhoSamplingDistance*I for certain integer I.
 *        rhoSamplingDistance - sampling distance of rho
 *        zMin, zMax - range of z to be sampled, zMin <= zMax
 *           As the LUT contains values of z*exp(jkr)/r**2, where r is calculated as
 *           (target - source) position, zMin and zMax must obey this rule.
 *           For example, for building waveLUT for point lights in z range (-2, -1)
 *           and hologram place z = 0, the values must be
 *           zMin = 0 - (-1) = 1, zMax = 0 - (-2) = 2
 *        zSamplingDistance - sampling distance of z
 *        limitRhoSamplingDistance - limit sampling distance of rho for antialiasing filter
 *          (In an optical field, the sampling of x and y cannot lead to aliasing, i.e. the waveLUT
 *           has to be antialiased. However, it is desirable to sample rho in the waveLUT table
 *           more densely than x, y in the optical field due to more precise lookup.)
 *        rhoSamplingFactor - For rhoSamplingFactor = 1, phasor for any rho bigger is set to 0, i.e.
 *           high local frequencies of the phasor are discarded. However, a proper treatment should be:
 *           1. sample phasor with high sampling rate
 *           2. apply a lowpass filter
 *           3. downsample to rhoSamplingDistance
 *           In this case, samples with high local frequency gradually fade to 0.
 *           rhoSamplingFactor multiplies number of samples stored for a single z value; e.g.
 *           for rhoSamplingFactor = 2, twice as much phasor samples are calculated; it is probable
 *           that in the upper half the values will be close to zero.
 *        baseUpsampling - rho upsampling for the antialiasing filter
 *        filterType - type of the antialiasing filter
 *        filterTypeParams - optional parameters of the antialiasing filter if some are required
 * output: waveLUT
 *        values of z*exp(jkr)/r**2, where r is calculated as (target - source) position
 */
int waveLUTNew(double rhoMin, double rhoMaxTheoretical,
				  double rhoSamplingDistance,
				  double zMin, double zMax,
				  double zSamplingDistance,
				  double limitRhoSamplingDistance,
				  double rhoSamplingFactor,
				  int baseUpsampling, t_FilterType filterType, void *filterTypeParams,
				  t_waveLUT **waveLUT)
{
	int samplesZ, samplesRho, index, i, j, maxSample, delta;
	// int maxUpSample, upsampling, upsampledWidth;
	int filterShift, filterWidthHalf;
	double *filterData = NULL;
	double rhoMax;
	fftw_complex *upsampledLine = NULL, *filteredUpsampledLine = NULL;
	double z, zz;
	double rhoUpSamplingDistance;
	double maxCorrectRho;
	double tmp_re, tmp_im, baseRho;
	double signed_k_number;

	INITEXCEPTION;

	/* sanity check */
	if (waveLUT == NULL)
		GOEXCEPTION0("Bad waveLUT pointer.\n");

	if (rhoMin < 0 || rhoMaxTheoretical < 0 || rhoMaxTheoretical - rhoMin < 0)
		GOEXCEPTION0("Bad rhoMin or rhoMax.\n");

	/* zMin and zMax can be negative! */
	if (zMin > zMax)
		GOEXCEPTION0("Bad zMin or zMax.\n");

	CHECK(waveLUTCalculateSamples(rhoMin, rhoMaxTheoretical, rhoSamplingDistance,
		zMin, zMax, zSamplingDistance,
		limitRhoSamplingDistance, rhoSamplingFactor,
		&samplesRho, &samplesZ));

	rhoMax = rhoMin + rhoSamplingDistance * (samplesRho-1);

	/* If necessary, allocate memory for the structure of the LUT.
	 * Otherwise check if the provided waveLUT structure is large enough to
	 * store all the data.
	 */
	if (*waveLUT == NULL)
	{
		/* Create a new waveLUT from the scratch. */
		*waveLUT = (t_waveLUT*) malloc(sizeof(t_waveLUT));
		if (*waveLUT == NULL)
			GOEXCEPTION0("Error allocating waveLUT.\n");

		/* initialize pointers */
		(*waveLUT)->samplesRhoNonZero = NULL;
		(*waveLUT)->phasor = NULL;

		(*waveLUT)->samplesRho = samplesRho;
		(*waveLUT)->samplesZ = samplesZ;

		/* every LUT row has a certain number of non-zero values; allocate memory for this data */
		(*waveLUT)->samplesRhoNonZero = (int*) malloc(samplesZ * sizeof(int));
		if ((*waveLUT)->samplesRhoNonZero == NULL)
			GOEXCEPTION0("Error allocating samplesRhoNonZero.\n");

		/* allocate the LUT itself */
		(*waveLUT)->phasor = (fftw_complex*) malloc((RAYLEIGH_INT64)samplesRho * (RAYLEIGH_INT64)samplesZ * sizeof(fftw_complex));
		if ((*waveLUT)->phasor == NULL)
			GOEXCEPTION0("Error allocating phasor LUT.\n");
	}
	else
	{
		/* WaveLUT was provided.
		 * Check if it was prepared for this situation.
		 */
		if ((*waveLUT)->samplesRho != samplesRho)
			GOEXCEPTION0("Provided waveLUT structure does not have samplesRho as expected.\n");

		if ((*waveLUT)->samplesZ != samplesZ)
			GOEXCEPTION0("Provided waveLUT structure does not have samplesZ as expected.\n");
	}


	/* set up LUT parameters in the structure */
	(*waveLUT)->zMax = zMax;
	(*waveLUT)->zMin = zMin;
	(*waveLUT)->zSamplingDistance = zSamplingDistance;
	(*waveLUT)->rhoMin = rhoMin;
	(*waveLUT)->rhoMax = rhoMax;
	(*waveLUT)->rhoSamplingDistance = rhoSamplingDistance;
	(*waveLUT)->limitRhoSamplingDistance = limitRhoSamplingDistance;

	memset((void*)(*waveLUT)->phasor, 0, (RAYLEIGH_INT64)samplesRho * (RAYLEIGH_INT64)samplesZ * sizeof(fftw_complex));

	filterData = (double*) malloc(MAX_FILTER_WIDTH * sizeof(double));
	if (filterData == NULL)
		GOEXCEPTION0("Error allocating filter data.\n");

	/* not used
	upsampling = (int)floor(baseUpsampling * limitSamplingDistance / rhoSamplingDistance + 0.5);
	filterCalculate(filterType, filterTypeParams, &filterShift, upsampling, filterData, &filterWidthHalf);
	filterScale(filterData, 2*filterWidthHalf+1, upsampling, filterData);

	upsampledWidth = upsampling * width + 2*filterWidthHalf;
	upsampledLine = (fftw_complex*) malloc(upsampledWidth * sizeof(fftw_complex));
	if (upsampledLine == NULL)
		GOEXCEPTION;

	filteredUpsampledLine = (fftw_complex*) malloc(upsampledWidth * sizeof(fftw_complex));
	if (filteredUpsampledLine == NULL)
		GOEXCEPTION;
	*/

	/* Before filling the waveLUT, each row (for a single z distance)
	 * is calculated baseUpsamplig-times upsampled, lowpass filtered
	 * and downsampled
	 */

	/* rho sampling distance for preliminary sampling */
	rhoUpSamplingDistance = limitRhoSamplingDistance / baseUpsampling;

	/* calculate lowpass filter kernel */
	filterCalculate(filterType, filterTypeParams, &filterShift, baseUpsampling, filterData, &filterWidthHalf);

	/* it is not necessary to scale the filter as it is already normalized
	   filterScale(filterData, 2*filterWidthHalf+1, baseUpsampling, filterData);
	*/

	/* calculate the waveLUT */
	for (i=0, index=0; i<samplesZ; i++)
	{
		/* determine z out of the row number */
		z = zMin + i*zSamplingDistance;

		/* the sign of z determines forward/backward propagation */
		if (z < 0)
			signed_k_number = -k_number;
		else
			signed_k_number = k_number;

		/* determine maximum sample that does not alias */
		zz = z*z;
		maxCorrectRho = lambda * fabs(z) / sqrt(4*limitRhoSamplingDistance*limitRhoSamplingDistance - lambda*lambda);
		if (isfinite(maxCorrectRho))
		{
			maxSample = (int)floor((maxCorrectRho * rhoSamplingFactor - rhoMin) / rhoSamplingDistance);
			if (maxSample > samplesRho)
				maxSample = samplesRho;
			if (maxSample < 0)
				GOEXCEPTION1("rhoSamplingDistance too big for z=%f.\n", z);
		}
		else
			maxSample = samplesRho;

		/* set the number of non-zero samples */
		(*waveLUT)->samplesRhoNonZero[i] = maxSample;

		/* calculate the row itself */

		/* This version based on convolution still does not work.
		   !!!!
		   First, calculate upsampled version


		maxUpSample = maxSample*upsampling + 2*filterWidthHalf;
		for (j=0; j<maxUpSample; j++)
		{
			rho = (j-filterWidthHalf)*rhoUpSamplingDistance;
			r = sqrt(rho*rho + zz);
			kr = k_number*r;
			upsampledLine[j][RE] = cos(kr)/r;
			upsampledLine[j][IM] = sin(kr)/r;
		}

		filterComplex1D(upsampledLine, maxUpSample, filterData, filterWidthHalf, upsampling, &((*waveLUT)->phasor[index]));
		index += width;
		*/

		/* this would be simple unfiltered version
		 *
		for (j=0; j<maxSample; j++, index++)
		{
			rho = j*rhoSamplingDistance;
			r = sqrt(rho*rho + zz);
			kr = k_number*r;
			(*waveLUT)->phasor[index][RE] = cos(kr)/r;
			(*waveLUT)->phasor[index][IM] = sin(kr)/r;
		}
		index += width - maxSample;
		*/

		/* the most simple solution of filtered propagation */
		for (j=0; j<maxSample; j++, index++)
		{
			tmp_re = 0;
			tmp_im = 0;
			baseRho = j*rhoSamplingDistance + rhoMin;
			for (delta = -filterWidthHalf; delta <= filterWidthHalf; delta++)
			{
				double rho, rr, r, kr, amp;

				rho = baseRho + delta * rhoUpSamplingDistance;
				rr = rho*rho + zz;
				r = sqrt(rr);
				kr = signed_k_number*r;
/*
 * point light source
				amp = z / (lambda *  rr);
				tmp_re += amp * cos(kr) * filterData[delta + filterWidthHalf];
				tmp_im += amp * sin(kr) * filterData[delta + filterWidthHalf];
*/

/*
 * rayleigh-sommerfeld kernel
 */
				amp = z / (lambda *  rr);
				tmp_re += amp * sin(kr) * filterData[delta + filterWidthHalf];
				tmp_im -= amp * cos(kr) * filterData[delta + filterWidthHalf];

			}
			(*waveLUT)->phasor[index][RE] = tmp_re;
			(*waveLUT)->phasor[index][IM] = tmp_im;
		}

		index += samplesRho - maxSample;

	}

EXCEPTION0("waveLUTNew")

	if (RAYLEIGH_RETVAL != RAYLEIGH_OK)
	{
		if (waveLUT != NULL)
		{
			if (*waveLUT != NULL)
			{
				if ((*waveLUT)->phasor != NULL)
					free((*waveLUT)->phasor);
				if ((*waveLUT)->samplesRhoNonZero != NULL)
					free((*waveLUT)->samplesRhoNonZero);
				free(*waveLUT);
			}
		}
	}

	if (filterData != NULL)
		free(filterData);

	if (upsampledLine != NULL)
		free(upsampledLine);

	if (filteredUpsampledLine != NULL)
		free(filteredUpsampledLine);

ENDEXCEPTION;
}

/*
 * dispose a waveLUT
 */
int waveLUTDispose(t_waveLUT **waveLUT)
{
	if (waveLUT == NULL || *waveLUT == NULL)
		GOEXCEPTION0("NULL pointer when calling waveLUTDispose.\n");

	if ((*waveLUT)->phasor != NULL)
		free((*waveLUT)->phasor);

	if ((*waveLUT)->samplesRhoNonZero != NULL)
		free((*waveLUT)->samplesRhoNonZero);

	free(*waveLUT);
	*waveLUT = NULL;
	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}



/*
 * save waveLUT as an image for debugging purposes
 */
int waveLUTSave(t_waveLUT *waveLUT, char *filename)
{
	t_optfield *tmp = NULL;
	int i, j, index;

	INITEXCEPTION;

	if (waveLUT == NULL)
		GOEXCEPTION0("NULL waveLUT pointer.\n");

	CHECK(optfieldNew(0, 0, 0, waveLUT->samplesRho, waveLUT->samplesZ, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &tmp));

	for (i=0, index=0; i<waveLUT->samplesZ; i++)
		for (j=0; j<waveLUT->samplesRho; j++, index++)
		{
			((fftw_complex*)tmp->buffer->data)[index][RE] = waveLUT->phasor[index][RE];
			((fftw_complex*)tmp->buffer->data)[index][IM] = waveLUT->phasor[index][IM];
		}

		CHECK(optfieldSavePNG(tmp, filename, PictureIORealImagIntensity, 1.0, 0, NULL));

EXCEPTION0("saveWaveLUT");

	if (tmp != NULL)
		optfieldDispose(&tmp);

ENDEXCEPTION;
}


/*
 * save the interpolated waveLUT as an image for debugging purposes
 */
int waveLUTSaveInterp(t_waveLUT *waveLUT, char *filename, t_FilterType filterType, void *filterTypeParams, int rhoUpsampling)
{
	t_optfield *tmp = NULL;
	int upsSamplesRho;
	int i, j, index;
	fftw_complex w;
	double rho, rhoFrac;
	int rhoInt;

	INITEXCEPTION;

	if (waveLUT == NULL)
		GOEXCEPTION0("NULL waveLUT pointer.\n");

	if (filterType != FilterTypeConstant &&
		filterType != FilterTypeTriangular &&
		filterType != FilterTypeCubicStd)
	{
		GOEXCEPTION0("Unsupported filter type.\n");
	}

	upsSamplesRho = rhoUpsampling*waveLUT->samplesRho;

	CHECK0(optfieldNew(0, 0, 0, upsSamplesRho, waveLUT->samplesZ, 1, SampleTypeFFTWDouble, BufferTypeMain, NULL, &tmp),
		"Error initializing temporary optfield.\n");

	for (i=0, index=0; i<waveLUT->samplesZ; i++)
		for (j=0; j<upsSamplesRho; j++, index++)
		{
			rho = (double)j/(double)rhoUpsampling;

			rhoFrac = floor(rho);
			rhoInt = (int)rhoFrac;
			rhoFrac = rho - rhoFrac;

			if (filterType == FilterTypeConstant)
			{
				w[RE] = waveLUT->phasor[(int)floor(rho+0.5)][RE];
				w[IM] = waveLUT->phasor[(int)floor(rho+0.5)][IM];
			}
			else if (filterType == FilterTypeTriangular)
			{
				w[RE] = waveLUT->phasor[rhoInt][RE]
						+ rhoFrac*(waveLUT->phasor[rhoInt+1][RE] - waveLUT->phasor[rhoInt][RE]);
				w[IM] = waveLUT->phasor[rhoInt][IM]
						+ rhoFrac*(waveLUT->phasor[rhoInt+1][IM] - waveLUT->phasor[rhoInt][IM]);
			}
			else /* if (filterType == FilterTypeCubicStd) */
			{
				int im1, i0, i1, i2;

				if (rhoInt == 0)
				{
					im1 = 1; i0 = 0; i1 = 1; i2 = 2;
				}
				else if (rhoInt < waveLUT->samplesRho-2)
				{
					im1 = rhoInt-1; i0 = rhoInt; i1 = rhoInt+1; i2 = rhoInt+2;
				}
				else if (rhoInt < waveLUT->samplesRho-1)
				{
					im1 = rhoInt-1; i0 = rhoInt; i1 = rhoInt+1; i2 = rhoInt+1;
				}
				else
				{
					im1 = rhoInt-1; i0 = rhoInt; i1 = rhoInt; i2 = rhoInt;
				}

				w[RE] = cubicInterp(waveLUT->phasor[im1][RE],
									waveLUT->phasor[i0][RE],
									waveLUT->phasor[i1][RE],
									waveLUT->phasor[i2][RE],
									rhoFrac);
				w[IM] = cubicInterp(waveLUT->phasor[im1][IM],
									waveLUT->phasor[i0][IM],
									waveLUT->phasor[i1][IM],
									waveLUT->phasor[i2][IM],
									rhoFrac);
			}

			((fftw_complex*)tmp->buffer->data)[index][RE] = w[RE];
			((fftw_complex*)tmp->buffer->data)[index][IM] = w[IM];

		}

	CHECK0(optfieldSavePNG(tmp, filename, PictureIORealImagIntensity, 1.0, 0, NULL),
		"Error writing the image.\n");

EXCEPTION0("saveWaveLUTInterp");

	if (tmp != NULL)
		optfieldDispose(&tmp);

ENDEXCEPTION;
}
