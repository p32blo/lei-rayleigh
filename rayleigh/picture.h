/*
 * Loading and saving optical fields.
 * Loading and saving pictures.
 */

#ifndef __PICTURE_H
#define __PICTURE_H

#include "rayleigh.h"

/* Gamma correction for saving viewable pictures */
extern double gamma_correction;

/* Output types for viewable images */
typedef enum {
	PictureIOIntensity = 0,        /* grayscale, intensity */
	PictureIOPhase = 1,            /* RGB, phase image: R = 0, Y = 60, G, C, B, M = 300 degrees */
	PictureIOIntensityPhase = 2,   /* RGB, intensity with hue as phase above */
	PictureIOText = 3,             /* plain text output */
	PictureIOPhaseGray = 4,        /* grayscale, linear phase */
	PictureIOReal = 5,             /* grayscale, real part */
	PictureIOImag = 6,             /* grayscale, imaginary part */
	PictureIORealImagIntensity = 7 /* RGB, red=real part, green=imaginary part, blue=intensity */
} t_PictureIOType;

/* Loads/saves complex optical field.
 * Two files are saved: image descriptor (txt file), image samples (binary).
 * Comment can be added to txt file.
 */
RAYLEIGH_EXPORT int optfieldSaveDF(t_optfield* optfield, char* filename, char *comment);

/*
 * TODO: implement this
 */
RAYLEIGH_EXPORT int optfieldLoadDF(t_optfield** optfield, char* filename);

/*
 * Saves optical field as a viewable picture
 * Saves txt file with accompanied information as well
 * optfield (IN)	optical field to be saved
 * filename (IN)	prefix of the filename; suffix is added automatically
 * outputtype (IN)	how to encode complex values; choose PictureIOIntensity etc.
 * gamma (IN)		gamma for encoding; use 1.0 for measurements, 2.2 for viewable picture
 *                  out = in^1/gamma
 * providedMaxIntensity (IN)
 *					calculated intensity is normalizet so that maximum picture luminosity = providedMaxIntensity
 * if maxintensity == 0, then picture is normalized to maximum intensity found, otherwise intensities are normalized to this one
 */
RAYLEIGH_EXPORT int optfieldSavePNG(t_optfield *optfield, char *filename, t_PictureIOType outputtype, double gamma, double providedMaxIntensity, char *comment);

/*
 * Loads PNG and converts it to the optical field.
 * optfield (OUT)	loaded optical field
 * filename (IN)	exact input filename (including suffixes)
 * sampling (IN)	sampling of input picture; if 0, dpi found in PNG is used
 * inputtype (IN)	how to convert from PNG to complex value; just PictureIOIntensity is supported now
 * gamma (IN)       gamma correction to apply; out = in^gamma
 */
RAYLEIGH_EXPORT int optfieldLoadPNG(t_optfield **image, char *filename, int sampling, t_SampleType sampleType, t_BufferType bufferType, t_PictureIOType inputtype, double gamma);

RAYLEIGH_EXPORT int optfieldPrint(t_optfield *optfield, char *filename, char *format);

#ifdef _RAYLEIGH_DEBUG
void pictureTest(void);
#endif

#endif
