/*
 * Application of the lens to the optical field
 */

#ifndef __LENS_H
#define __LENS_H

#define LENS_NO_APODIZE	1
#define LENS_APODIZE	0

#define LENS_THICK_SIMPLE		0
#define LENS_THICK_ADDCONSTANT	1

/*
 * Apply an ideal convex lens that works exactly according the imaging equation:
 * 1/s + 1/v = 1/f
 * where s and v are object and image distances, f is focal distance
 */
RAYLEIGH_EXPORT int lensIdeal(double object_x, double object_y, double object_z,
			  double lens_center_x, double lens_center_y, double lens_center_z,
			  double image_x, double image_y, double image_z,
			  t_optfield *image,
			  double mask_center_x, double mask_center_y, double mask_radius,
			  int apodize);


/*
 * Apply a thick (aspherical) lens phase distortion.
 * image: image to apply the distortion
 * tc: lens "thickness", or rather "thickness" on the z axis
 * innerPropagation: LENS_THICK_SIMPLE - calculates phase distortions on the first and the second surface only
 *                   LENS_THICK_ADDCONSTANT - adds a phase shift proportional to tc and n (see below)
 * r1: radius of the first surface; positive means the curvature center is to the right of the lens
 * r1_a4, r1_16, r1_conic: aspheric coefficients; all zeros indicate spherical surface
 * r2, r2_a4, r2_a6, r2_conic: dtto for the second surface
 * n: index of refraction of the lens material
 * lensCenterX, lensCenterY: XY coordinates of the lens center
 * maskCenterX, maskCenterY: XY coordinates of the lens mask (aperture) center
 * maskRadius: aperture radius
 * apodize: LENS_NO_APODIZE - hard clip (lens unmasked/lens masked)
 *          LENS_APODIZE - gradual change between masked and unmasked
 */
RAYLEIGH_EXPORT int lensThickAspheric(t_optfield *image,
					  int innerPropagation,
					  double tc,
					  double r1, double r1_a4, double r1_a6, double r1_a8, double r1_a10, double r1_conic,
					  double r2, double r2_a4, double r2_a6, double r2_a8, double r2_a10, double r2_conic,
					  double n,
					  double lensCenterX, double lensCenterY,
					  double maskCenterX, double maskCenterY, double maskRadius,
					  int apodize);

RAYLEIGH_EXPORT int lensThickAsphericDoublet(t_optfield *image,
					  int innerPropagation,
					  double tc1, double tc2,
					  double r1, double r1_a4, double r1_a6, double r1_a8, double r1_a10, double r1_conic,
					  double r2, double r2_a4, double r2_a6, double r2_a8, double r2_a10, double r2_conic,
					  double r3, double r3_a4, double r3_a6, double r3_a8, double r3_a10, double r3_conic,
					  double n1, double n2,
					  double lensCenterX, double lensCenterY,
					  double maskCenterX, double maskCenterY, double maskRadius,
					  int apodize);


/* test functions */
void lens_test_ideal(void);
void lens_test_aspheric(void);
void lens_test_asphericDoublet(void);

#endif
