#include "rayleighCompile.h"

#define PATTERN_BLACK	'x'
#define PATTERN_NONE	'.'
#define PATTERN_NL		'|'

/*
 * Generate a 2-D pattern composed of square waves
 * Output: grating - an optfield composed of zeros and ones
 * Input:  grating_pattern - a string that represents a grating structure
 *           e.g. "x." generates rows composed of 0101010101...
 *           e.g. "x|." generates columns composed of 0101010101...
 *           e.g. "x.|.x" generates a checkboard
 *         grating_upsampling - every 'x' and '.' in the grating_pattern is grating_upsampling-times repeated
 */

int gratingInitPattern(t_optfield *grating, char *grating_pattern, int grating_upsampling)
{
	int pattern_length;
	int rows = 1, columns = 0, tmp_columns = 0;
	int i, pattern_offset;
	int x_data, y_data, x_pattern, y_pattern, x_up, y_up, row_offset, offset, val, valid_rows, valid_columns;
	int samples_x, samples_y;
	T_SAMPLE_TYPE_FFTWDOUBLE *data;
	int sampleTypeSize;

	CHECK(optfieldGetSamples(&samples_x, &samples_y, grating));

	if (optfieldGetSampleType(grating) != SampleTypeFFTWDouble)
		GOEXCEPTION;

	data = (T_SAMPLE_TYPE_FFTWDOUBLE*) grating->buffer->data;

	pattern_length = (int) strlen(grating_pattern);
	if (pattern_length < 2 || grating_pattern[0] == PATTERN_NL)
		GOEXCEPTION;

	for (i=0; i<pattern_length; i++)
	{
		if (grating_pattern[i] == PATTERN_BLACK ||
			grating_pattern[i] == PATTERN_NONE)
		{
			tmp_columns++;
			continue;
		}

		if (grating_pattern[i] == PATTERN_NL)
		{
			if (rows == 1)
				columns = tmp_columns;
			rows++;
			if (tmp_columns != columns)
				GOEXCEPTION;
			tmp_columns = 0;
			continue;
		}

		GOEXCEPTION;
	}

	if (rows == 1)
		columns = tmp_columns;
	else if (tmp_columns != columns)
		GOEXCEPTION;


	y_data = 0;
	row_offset = 0;
	pattern_offset = 0;
	sampleTypeSize = optfieldGetSampleTypeSize(grating);

	for (y_pattern = 0; y_pattern < rows; y_pattern++)
	{
		x_data = 0;
		offset = row_offset;

		/* reading one line of the pattern */
		for (x_pattern = 0; x_pattern < columns; x_pattern++)
		{
			val = grating_pattern[pattern_offset++] == PATTERN_BLACK ? 0 : 1;

			/* immediate x upsampling of the pattern */
			for (x_up = 0; x_up < grating_upsampling; x_up++)
			{
				if (x_data >= samples_x) break;
				data[offset][RE] = val;
				data[offset][IM] = 0;
				offset++;
				x_data++;
			}
		}

		/* copying pattern by doubling its size */
		valid_columns = x_data;
		while (valid_columns * 2 <= samples_x)
		{
			memcpy(&(data[row_offset + valid_columns]),
				&(data[row_offset]), valid_columns * sampleTypeSize);
			valid_columns *= 2;
		}

		/* copy the rest of the line */
		if (valid_columns < samples_x)
			memcpy(&(data[row_offset + valid_columns]),
				&(data[row_offset]), (samples_x - valid_columns) * sampleTypeSize);

		/* duplicate lines to upsample vertical */
		for (y_up = 1; y_up < grating_upsampling; y_up++)
		{
			if (y_up >= samples_y)
				/* the whole grating has been generated! */
				return RAYLEIGH_OK;

			memcpy(&(data[row_offset + samples_x]),
				&(data[row_offset]), samples_x * sampleTypeSize);

			row_offset += samples_x;
			y_data++;
		}

		/* one line is now completed, move to the next one */
		row_offset += samples_x;
		y_data++;

		/* done one line of the pattern; now do the rest of them */

		/* skip the NEW_LINE character in pattern */
		pattern_offset++;
	}

	/* the whole pattern is now in the memory filling the whole width of the image */
	/* repeat it by dublicating to fill the height as far as possible */

	/* we know that filling has to be done; if the pattern filled the image before, the function would return OK */
	valid_rows = rows * grating_upsampling;
	while (valid_rows * 2 <= samples_y)
	{
		memcpy(&(data[valid_rows * samples_x]),
			&(data[0]),
			valid_rows * samples_x * sampleTypeSize);
		valid_rows *= 2;
	}

	/* fill the rest of the image */
	if (valid_rows < samples_y)
		memcpy(&(data[valid_rows * samples_x]),
			&(data[0]),
			(samples_y - valid_rows) * samples_x * sampleTypeSize);


	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}
