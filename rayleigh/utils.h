/*
 * Various utility functions
 */

#ifndef __RAYLEIGHUTILS_H
#define __RAYLEIGHUTILS_H

#include "rayleigh.h"

/*
 * dst = op1 + op2
 */
void vectorAdd(t_point *dst, t_point *op1, t_point *op2);

/*
 * dst = op1 - op2
 */
void vectorSub(t_point *dst, t_point *op1, t_point *op2);

/*
 * dst = k * op1
 */
void vectorMul(t_point *dst, double k, t_point *op1);

/*
 * vector length
 */
double vectorLength(t_point vector);

/*
 * dot product of two vectors
 */
double vectorDot(t_point *u, t_point *v);

/*
 * product of 3x3 matrices
 */
int matrix33Multiply(double **result, double *a, double *b);


/*
 * transpose of a 3x3 matrix
 */
int matrix33Transpose(double** result, double *a);

/*
 * create 3x3 matrix for x, y and z-axis rotation
 */
int matrix33RotateX(double **result, double angle);
int matrix33RotateY(double **result, double angle);
int matrix33RotateZ(double **result, double angle);

/*
 * create 3x3 matrix for xyz-axis rotation
 */
int matrix33Rotate(double **result, double angleX, double angleY, double angleZ);


/*
 * The greatest common divisor
 */
int gcd(int a, int b);

/*
 * Integer division a/b rounded up
 * Does not work with negative integers, doesn't check it
 */
int div_roundup(int a, int b);

/*
 * Find a number higher than n that is FFTW-friendly
 */
int makeFFTFriendly(int n);


/*
 * calculate fft-friendly size for convolution
 */
void calculateConvolutionSamples(int sourceSamples_x, int sourceSamples_y,
			int targetSamples_x, int targetSamples_y,
			int *convolutionSamples_x, int *convolutionSamples_y);


#ifdef _RAYLEIGH_DEBUG
void test_util_doubletofraction(void);
void test_util_vectormatrix(void);
#endif


#endif
