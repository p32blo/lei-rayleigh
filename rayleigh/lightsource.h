/*
 * Creation of complex amplitudes of the light in a given optical field array.
 */

#ifndef __LIGHTSOURCE_H
#define __LIGHTSOURCE_H

#include "rayleigh.h"

/*
 * Generates a spherical wavefront of a given curvature radius ("laser_spot_curvature_radius")
 * illuminating the optical field array ("light")
 * under a given angle ("angle_x, angle_y").
 * This wavefront is attenuated using a gaussian profile of a given radius ("laser_spot_sigma")
 * centered in a given point ("laser_spot_x, laser_spot_y").
 */
RAYLEIGH_EXPORT int optfieldLightSimple(t_optfield *light,
					   double laser_spot_sigma, double laser_spot_curvature_radius,
					   double laser_spot_x, double laser_spot_y,
					   double angle_x, double angle_y,
					   double amplitudeMultiplier);

/*
 * Generates a rigorous gaussian beam starting at point "cavity" and
 * travelling in a direction towards "spot". Beam divergence is given
 * indirectly through "waistRadius" parameter. See http://en.wikipedia.org/wiki/Gaussian_beam
 * for the explanation.
 */
RAYLEIGH_EXPORT int optfieldLightGaussBeam(t_optfield *light,
					  double waistRadius, double peakAmplitude,
					  double cavity_x, double cavity_y, double cavity_z,
					  double spot_x, double spot_y, double spot_z);


#endif
