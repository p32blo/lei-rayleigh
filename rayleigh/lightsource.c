/*
 * Creation of complex amplitudes of the light in a given optical field array.
 */

#include "rayleighCompile.h"
#include <float.h>

/*
 * Generates a spherical wavefront of a given curvature radius ("laser_spot_curvature_radius")
 * illuminating the optical field array ("light")
 * under a given angle ("angle_x, angle_y").
 * This wavefront is attenuated using a gaussian profile of a given radius ("laser_spot_sigma")
 * centered in a given point ("laser_spot_x, laser_spot_y").
 */
int optfieldLightSimple(t_optfield *light,
					   double laser_spot_sigma, double laser_spot_curvature_radius,
					   double laser_spot_x, double laser_spot_y,
					   double angle_x, double angle_y,
					   double amplitudeMultiplier)
{
	int i, j, index;
	double x, y, z;
	double tmp_x, tmp_z;
	double tmp_amplitude, amplitude, tmp, phase, r2;
	double cos_angle_x, cos_angle_y;
	double sin_angle_x, sin_angle_y;
	int samples_x, samples_y;
	double sampling_distance;
	double corner_x, corner_y, corner_z;
	T_SAMPLE_TYPE_FFTWDOUBLE *data;

	if (light == NULL || light->buffer->data == NULL)
		return RAYLEIGH_ERROR;

	/* TODO
	 * handle different types
	 */
	if (light->sampleType != SampleTypeFFTWDouble)
		return RAYLEIGH_ERROR;

	data = (T_SAMPLE_TYPE_FFTWDOUBLE*) light->buffer->data;

	CHECK(optfieldGetSamples(&samples_x, &samples_y, light));
	CHECK(optfieldGetCorner(&corner_x, &corner_y, &corner_z, light));
	sampling_distance = optfieldGetSamplingdistance(light);

	// laser_spot_sigma = laser_spot_sigma MM;
	r2 = laser_spot_curvature_radius * laser_spot_curvature_radius;
	cos_angle_x = cos(angle_x);
	cos_angle_y = cos(angle_y);
	sin_angle_x = sin(angle_x);
	sin_angle_y = sin(angle_y);

	index = 0;
	for (i = 0; i < samples_y; i++)
	{
		tmp = i*sampling_distance + corner_y - laser_spot_y;
		// z = 0;
		y = tmp * cos_angle_x; // - z * sin_angle_z
		tmp_z = tmp * sin_angle_x; // + z * cos_angle_z

		tmp = y / laser_spot_sigma;
		tmp_amplitude = amplitudeMultiplier * exp(-tmp*tmp);
		for (j = 0; j < samples_x; j++)
		{
			tmp_x = j*sampling_distance + corner_x - laser_spot_x;
			x = tmp_x * cos_angle_y - tmp_z * sin_angle_y;
			z = tmp_x * sin_angle_y + tmp_z * cos_angle_y;

			tmp = x / laser_spot_sigma;
			amplitude = tmp_amplitude * exp(-tmp*tmp);

			z += laser_spot_curvature_radius;

			phase = -k_number * sqrt(x*x + y*y + z*z);

			data[index][RE] += amplitude * cos(phase);
			data[index][IM] += -amplitude * sin(phase);
			index++;
		}
	}

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}


/*
 * Generates a rigorous gaussian beam starting at point "cavity" and
 * travelling in a direction towards "spot". Beam divergence is given
 * indirectly through "waistRadius" parameter. See http://en.wikipedia.org/wiki/Gaussian_beam
 * for the explanation.
 */
int optfieldLightGaussBeam(t_optfield *light,
					  double waistRadius, double peakAmplitude,
					  double cavity_x, double cavity_y, double cavity_z,
					  double spot_x, double spot_y, double spot_z)
{
	int i, j, index;
	double world_x, world_y, world_z; /* world space x, y, z */
	double lr, lz;     /* r and z in laser space */
	double dx, dy, dz; /* normalized direction of the laser beam */
	double vx, vy, vz; /* vector {point on screen} - {cavity} */
	double cx, cy, cz; /* projection of (point on screen) to the laser beam */
	double tmp;
	double zr; /* waist radius in the Rayleigh range */
	double z_zr; /* z / zr */
	double wz; /* waist radius in a distance z */
	double rz; /* radius of curvature in the distance z */
	double amplitude, phase;

	int samples_x, samples_y;
	double sampling_distance;
	double corner_x, corner_y, corner_z;
	T_SAMPLE_TYPE_FFTWDOUBLE *data;

	if (light == NULL || light->buffer->data == NULL)
		return RAYLEIGH_ERROR;

	/* TODO
	 * handle different types
	 */
	if (light->sampleType != SampleTypeFFTWDouble)
		return RAYLEIGH_ERROR;

	data = (T_SAMPLE_TYPE_FFTWDOUBLE*) light->buffer->data;

	CHECK(optfieldGetSamples(&samples_x, &samples_y, light));
	CHECK(optfieldGetCorner(&corner_x, &corner_y, &corner_z, light));
	sampling_distance = optfieldGetSamplingdistance(light);

	index = 0;

	/* normalized direction of {spot}-{cavity} */
	dx = spot_x - cavity_x;
	dy = spot_y - cavity_y;
	dz = spot_z - cavity_z;
	tmp = sqrt(dx*dx + dy*dy + dz*dz);
	dx /= tmp;
	dy /= tmp;
	dz /= tmp;

	world_z = corner_z;
	for (i = 0; i < samples_y; i++)
	{
		world_y = i*sampling_distance + corner_y;
		for (j = 0; j < samples_x; j++)
		{
			world_x = j*sampling_distance + corner_x;

			/* vector {point on screen}-{cavity} */
			vx = world_x - cavity_x;
			vy = world_y - cavity_y;
			vz = world_z - cavity_z;

			/* z in laser space is equal to dot(v, d) */
			lz = vx*dx + vy*dy + vz*dz;

			/* r in laser space forms an right-angled triangle with lz and v */
			/* this calculation is numerically unstable !
			   lr = sqrt(vx*vx + vy*vy + vz*vz - lz*lz);
			*/

			/* calculate vector {point on the beam} - {point on the screen} */
			cx = dx * lz - vx;
			cy = dy * lz - vy;
			cz = dz * lz - vz;

			/* r is just length of the previous vector */
			lr = sqrt(cx*cx + cy*cy + cz*cz);


			/* calculate beam parameters in the point world_x, world_y, world_z (or lz, lr) */
			zr = PI*waistRadius*waistRadius/lambda;
			z_zr = lz / zr;
			wz = waistRadius*sqrt(1 + z_zr*z_zr);
			rz = lz * (1 + 1/(z_zr*z_zr));

			amplitude = peakAmplitude * waistRadius/wz * exp(-lr*lr/(wz*wz));
			phase = (k_number*(lz + lr*lr/(2*rz)) + atan(z_zr));

			data[index][RE] += amplitude * cos(phase);
			data[index][IM] += amplitude * sin(phase);

			if (!isfinite(data[index][RE]))
			{
				printf("!!!\n");
				exit(0);
			}
			if (!isfinite(data[index][IM]))
			{
				printf("!!!\n");
				exit(0);
			}


			index++;
		}
	}

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}
