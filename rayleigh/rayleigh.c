#include "rayleighCompile.h"

/* default optical environment */
#define _LAMBDA		(532 NM)
double lambda = _LAMBDA;
double k_number = 2*PI/_LAMBDA;
double common_sampling_distance = _LAMBDA;

/* debugging variable
 * some functions save debugging files, this is a unified way how to name them
 */
char *debugFnamePrefix = NULL;

char *propagationMethodAccName[] = {"precise", "double LUT", "wave LUT"};

/*
 * set lambda, wawenumber (2pi/lambda) and common sampling distance
 */
void rayleighSetEnvironment(double _lambda, double _commonSamplingDistance)
{
	lambda = _lambda;
	k_number = PI2/_lambda;
	common_sampling_distance = _commonSamplingDistance;
}

double rayleighGetKNumber(void)
{
	return k_number;
}

double rayleighGetLambda(void)
{
	return lambda;
}

double rayleighGetCommonSamplingDistance(void)
{
	return common_sampling_distance;
}
