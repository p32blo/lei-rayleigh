/*
 * Many processes in Rayleigh use filtering.
 * This suite collects common filtering tasks.
 */
#ifndef __FILTER_H
#define __FILTER_H

/* Just 1D filters are supported.
 * For simplicity, their taps are often stored in a fixed size array.
 */
#define MAX_FILTER_WIDTH	1000	/* about 100 is reasonable, 1000 is just to have enough space */

#include "rayleigh.h"

/* There are several filter types to choose from:
 * constant: nonzero part of the rectangular filter
 * triangular: nonzero part of the tri-filter (rect * rect)
 * lanczos2: nonzero part of the Lanczos filter sinc(x) sinc(x/2), -2<x<2
 * lanczos3: nonzero part of the Lanczos filter sinc(x) sinc(x/3), -3<x<3
 */
typedef enum {
	FilterTypeDefault = -1,
	FilterTypeConstant = 1,
	FilterTypeTriangular = 2,
	FilterTypeLanczos2 = 3,
	FilterTypeLanczos3 = 4,
	FilterTypeCubicStd = 5
} t_FilterType;

/* Filtertype names for the number->string conversion */
extern char* filterTypeName[];


/* Bilinear (triangular) interpolation:
 *
 * (r01)-------(r11)
 *   |           |
 *   |           |     y
 *   |           |
 * (r00)-------(r10)
 *
 *         x
 */
#define bilinearInterp(r00, r01, r10, r11, x, y) (((r11-r10-r01+r00)*x+r01-r00)*y+(r10-r00)*x+r00)

/* (bi)Cubic interpolation.
 * See also bicubic.m Octave script.
 * Generally:
 *   out = am1*w(-x-1, a) + a0*w(-x, a) + a1*w(-x+1,a) + a2*w(-x+2,a)
 * where w(x, a) is a bicubic kernel:
 *
 *	function out = w(x, a)
 *		ax = abs(x);
 *		if (ax <= 1)
 *			out = ((a+2)*ax - (a+3))*ax*ax + 1;
 *		elseif (ax < 2)
 *			out = ((a*ax - 5*a)*ax + 8*a)*ax - 4*a;
 *		else
 *			out = 0;
 *		end
 *	end
 *
 * For 0 <= x <= 1:
 * w(-1-x, a) = a*x^3-2*a*x^2+a*x            // i.e. |-1-x|=1+x > 1
 * w(-x, a)   = (a+2)*x^3 - (a+3)*x^2 + 1    // i.e. |x| = x <= 1
 * w(-x+1, a) = (-a-2)*x^3+(2*a+3)*x^2-a*x   // i.e. |-x+1| = 1-x <= 1
 * w(-x+2, a) = a*x^2 - a*x^3                // i.e. |-x+2| = 2-x > 1
 *
 * Specifically for a=-0.5
 * w(-1-x, -0.5) = -0.5*x^3 +     x^2 - 0.5*x
 * w(-x, -0.5)   =  1.5*x^3 - 2.5*x^2         + 1.0
 * w(-x+1, -0.5) = -1.5*x^3 + 2.0*x^2 + 0.5*x
 * w(-x+2, -0.5) =  0.5*x^3 - 0.5*x^2
 *
 * i.e.
 * out = -0.5*(  (am1 - a2 + 3*a1 - 3*a0)*x^3
 *             + (-2*am1 + a2 - 4*a1 + 5*a0)*x^2
 *			   + (am1 - a1)*x)
 *			   + a0
 */

#define cubicInterp(am1, a0, a1, a2, x) (-0.5*(((((am1) - (a2) + 3*(a1) - 3*(a0))*(x) + (-2*(am1) + (a2) - 4*(a1) + 5*(a0)))*(x) + ((am1) - (a1)))*(x)) + (a0))


/*
 * Calculate filter taps. The function is not intended to be called by a common user.
 */
void filterFillData(int fwh, int upsampling, double *filterData, t_FilterType filterType, void *filterTypeParams);


/*
 * Find suitable upsampling parameter for propagation calculation. For details
 * see Lobaz: Memory-efficient reference calculation of light propagation using the convolution method,
 * Optics Express, Vol. 21 Issue 3, pp.2795-2806 (2013)
 */
void filterFindUpsampling(t_point shift,
			double samplingDistance_x, double samplingDistance_y,
			t_FilterType filterType, void *filterTypeParams,
			int *finalUpsampling_x, int *finalUpsampling_y,
			double *filterSamplingDistance_x, double *filterSamplingDistance_y,
			double propagationPrecision);


/*
 * Calculate an interpolation filter for a given upsampling.
 */
void filterCalculate(
			t_FilterType filterType, void *filterTypeParams,
			int *filterShift, int upsampling, double *filterData, int *filterWidthHalf);

/*
 * Calculate the frequency response of a filter.
 */
int filterCalcFreqResponse(fftw_complex **freqResponse,
						   double *filterData, int filterWidthHalf, int upsampling,
						   int samples, int cutdown);

/*
 * Filter a line of complex numbers.
 */
int filterComplex1D(fftw_complex *input, int inSamples,
					double *filterData, int filterWidthHalf,
					int downsampling,
					fftw_complex *output);

/*
 * multiply filter taps with a real number
 */
int filterScale(double *infilter, int filterWidth, double scale, double *outfilter);

/*
 * Save filter taps (real part only) to a text file.
 */
int filterRealSave(double *filter, int samples, char *filename);

/*
 * Save filter taps to a text file.
 */
int filterComplexSave(fftw_complex *filter, int samples, char *filename);

/*
 * Resample an optical field
 */
RAYLEIGH_EXPORT int optfieldResample(t_optfield **dst, t_optfield *src, int upsampling_x, int upsampling_y, t_FilterType filterType, void *filterTypeParams);

/*
 * testing filter.c
 */
void optfieldResampleTest(void);

#endif
