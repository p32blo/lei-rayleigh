#include "rayleighCompile.h"

/*
 * Common vector operations
 *
 */

/*
 * dst = op1 + op2
 */
void vectorAdd(t_point *dst, t_point *op1, t_point *op2)
{
	dst->x = op1->x + op2->x;
	dst->y = op1->y + op2->y;
	dst->z = op1->z + op2->z;
}


/*
 * dst = op1 - op2
 */
void vectorSub(t_point *dst, t_point *op1, t_point *op2)
{
	dst->x = op1->x - op2->x;
	dst->y = op1->y - op2->y;
	dst->z = op1->z - op2->z;
}


/*
 * dot product of two vectors
 */
double vectorDot(t_point *u, t_point *v)
{
	return u->x*v->x + u->y*v->y + u->z*v->z;
}


/*
 * dst = k * op1
 */
void vectorMul(t_point *dst, double k, t_point *op1)
{
	dst->x = k * op1->x;
	dst->y = k * op1->y;
	dst->z = k * op1->z;
}


/*
 * vector length
 */
double vectorLength(t_point vector)
{
	return sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
}



/*
 * product of 3x3 matrices
 * order of matrix elements: row0col0 row0col1 row0col2 row1col0 ... row2col2
 */
int matrix33Multiply(double **result, double *a, double *b)
{
	int row, col, tmp, index;
	double *temp = NULL;

	if (result == NULL)
		return RAYLEIGH_ERROR;

	temp = (double *) malloc(3*3 * sizeof(double));
	if (temp == NULL)
		return RAYLEIGH_ERROR;

	memset(temp, 0, 3*3 * sizeof(double));

	for(row = 0, index=0; row < 3; row++)
	{
		for(col = 0; col < 3; col++, index++)
		{
			for(tmp = 0; tmp < 3; tmp++)
				temp[index] += a[row*3 + tmp] * b [tmp*3 + col];
		}
	}

	*result = temp;
	return RAYLEIGH_OK;
}


/*
 * transpose of a 3x3 matrix
 */
int matrix33Transpose(double** result, double *a)
{
	int row, col;
	double *temp;

	if (result == NULL)
		return RAYLEIGH_ERROR;

	temp = (double *) malloc(3*3 * sizeof(double));
	if (temp == NULL)
		return RAYLEIGH_ERROR;

	for(row = 0; row < 3; row++)
	{
		for(col = 0; col < 3; col++)
		{
			temp[row * 3 + col] = a[col * 3 + row];
		}
	}

	*result = temp;
	return RAYLEIGH_OK;
}

/*
 * create 3x3 matrix for z-axis rotation
 */
int matrix33RotateZ(double **result, double angle)
{
	double *temp;

	if (result == NULL)
		return RAYLEIGH_ERROR;

	temp = (double *) malloc(3*3 * sizeof(double));
	if (temp == NULL)
		return RAYLEIGH_ERROR;

	temp[0] = cos(angle);  temp[1] = -sin(angle);  temp[2] = 0;
	temp[3] = -temp[1];    temp[4] = temp[0];      temp[5] = 0;
	temp[6] = 0;           temp[7] = 0;            temp[8] = 1;

	*result = temp;
	return RAYLEIGH_OK;
}


/*
 * create 3x3 matrix for x-axis rotation
 */
int matrix33RotateX(double **result, double angle)
{
	double *temp;

	if (result == NULL)
		return RAYLEIGH_ERROR;

	temp = (double *) malloc(3*3 * sizeof(double));
	if (temp == NULL)
		return RAYLEIGH_ERROR;

	temp[0] = 1;           temp[1] = 0;            temp[2] = 0;
	temp[3] = 0;           temp[4] = cos(angle);   temp[5] = -sin(angle);
	temp[6] = 0;           temp[7] = -temp[5];     temp[8] = temp[4];

	*result = temp;
	return RAYLEIGH_OK;
}


/*
 * create 3x3 matrix for x-axis rotation
 */
int matrix33RotateY(double **result, double angle)
{
	double *temp;

	if (result == NULL)
		return RAYLEIGH_ERROR;

	temp = (double *) malloc(3*3 * sizeof(double));
	if (temp == NULL)
		return RAYLEIGH_ERROR;

	temp[0] = cos(angle);  temp[1] = 0;            temp[2] = sin(angle);
	temp[3] = 0;           temp[4] = 1;            temp[5] = 0;
	temp[6] = -temp[2];    temp[7] = 0;            temp[8] = temp[0];

	*result = temp;
	return RAYLEIGH_OK;
}


/*
 * create 3x3 matrix for xyz-axis rotation
 */
int matrix33Rotate(double **result, double angleX, double angleY, double angleZ)
{
	double *a, *b, *c;

	matrix33RotateX(&a, angleX);
	matrix33RotateY(&b, angleY);
	matrix33Multiply(&c, a, b);
	free(a);
	free(b);

	matrix33RotateZ(&a, angleZ);
	matrix33Multiply(&b, c, a);

	free(c);
	free(a);

	*result = b;

	return RAYLEIGH_OK;
}


/*
 * greatest common divisor
 */
int gcd(int a, int b)
{
  int c;
  while ( a != 0 ) {
	 c = a; a = b%a;  b = c;
  }
  return b;
}



/* integer division a/b rounded up */
/* does not work with negative integers, doesn't check it */
int div_roundup(int a, int b)
{
	int x = a / b;
	if (x*b < a) x++;
	return x;
}


/* conversion from (double)value to (int)nominator / (int) denominator
 * maximum nominator or denominator as well as tolerance can be specified below
 * returned fraction is altered so that it is not zero
 */

#define DOUBLE_TO_FRACTION_MAXERROR		0.00001
#define DOUBLE_TO_FRACTION_MAXNUMBER	1024

void double_to_fraction(double value, int *nominator, int *denominator)
{
	int best_a = 1, best_b = 1; /* temporary best nominator and denominator */
	int neg = 1;                /* for rare case if value is negative */
	int a = 1, b = 1;           /* temporary nominator and denominator iterative search*/
	double x;                   /* approximation of the "value" */
	double best_error, error;   /* errors of our approximation */

	/* algorithm works por positive values; negative case will be treated at the end */
	if (value < 0)
	{
		neg = -1;
		value = -value;
	}

	best_error = value;

	/* we try to increase a or b step by step to reach "value" */
	do
	{
		x = a / (double) b;
		error = fabs(x - value);
		if (error < best_error)
		{
			best_a = a;
			best_b = b;
			best_error = error;
		}
		if (error < DOUBLE_TO_FRACTION_MAXERROR) break;
		if (a >= DOUBLE_TO_FRACTION_MAXNUMBER || b >= DOUBLE_TO_FRACTION_MAXNUMBER) break;
		if (x < value)
			a++;
		else
			b++;
#pragma warning(push)
#pragma warning(disable : 4127)
	} while (1);
#pragma warning(pop)

	*nominator = best_a;
	*denominator = best_b;

	a = best_a - 1;
	a = a < 1 ? 1 : a;

	for (; a <= best_a+1; a++)
	{
		b = best_b - 1;
		b = b < 1 ? 1 : b;
		for (; b <= best_b+1; b++)
		{
			x = a / (double) b;
			error = fabs(x - value);
			if (error < best_error)
			{
				*nominator = a;
				*denominator = b;
				best_error = error;
			}
		}
	}

	if (neg == -1)
		*nominator = - *nominator;

	return;
}

void test_util_doubletofraction(void)
{
	int a, b;
	double x;
	x = 1.0; double_to_fraction(x, &a, &b); printf("%.5lf = %2d / %2d\n", x, a, b);
	x = 1.1; double_to_fraction(x, &a, &b); printf("%.5lf = %2d / %2d\n", x, a, b);
	x = 1.2; double_to_fraction(x, &a, &b); printf("%.5lf = %2d / %2d\n", x, a, b);
	x = 1.25; double_to_fraction(x, &a, &b); printf("%.5lf = %2d / %2d\n", x, a, b);
	x = 1.33333; double_to_fraction(x, &a, &b); printf("%.5lf = %2d / %2d\n", x, a, b);
	x = 1.5; double_to_fraction(x, &a, &b); printf("%.5lf = %2d / %2d\n", x, a, b);
	x = 1.7; double_to_fraction(x, &a, &b); printf("%.5lf = %2d / %2d\n", x, a, b);
	x = 1.9; double_to_fraction(x, &a, &b); printf("%.5lf = %2d / %2d\n", x, a, b);
	x = 1.999; double_to_fraction(x, &a, &b); printf("%.5lf = %2d / %2d\n", x, a, b);

	return;
}

double ma[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
double mb[] = {-1, -2, -3, 4, 5, 6, 7, 8, 9};

void matrix33Print(double *a)
{
	int row, col;
	for (row=0; row < 3; row++)
	{
		for (col=0; col < 3; col++)
			printf("%8.1f", a[row*3 + col]);
		putchar('\n');
	}
}

void test_util_vectormatrix(void)
{
	t_point a = {1, 2, 3};
	t_point b = {5, 6, 7};
	t_point res;
	double *mc, *md;

	printf("a:        %lf %lf %lf\n", a.x, a.y, a.z);
	printf("b:        %lf %lf %lf\n\n", b.x, b.y, b.z);

	printf("|a|:      %lf\n", vectorLength(a));
	printf("a . b:    %lf\n", vectorDot(&a, &b));

	vectorAdd(&res, &a, &b);
	printf("a + b:    %lf %lf %lf\n", res.x, res.y, res.z);

	vectorSub(&res, &a, &b);
	printf("a - b:    %lf %lf %lf\n", res.x, res.y, res.z);

	vectorMul(&res, 2, &a);
	printf("2a:       %lf %lf %lf\n", res.x, res.y, res.z);

	matrix33Print(ma);

	matrix33Multiply(&mc, ma, mb);
	printf("ma * mb:\n");
	matrix33Print(mc);

	matrix33Transpose(&md, ma);
	printf("md^T:\n");
	matrix33Print(md);

	free(mc);
	free(md);

	matrix33RotateZ(&mc, 30*PI/180);
	printf("rotation Z 30 deg:\n");
	matrix33Print(mc);
	free(mc);

	matrix33RotateX(&mc, 30*PI/180);
	printf("rotation X 30 deg:\n");
	matrix33Print(mc);
	free(mc);

	matrix33RotateY(&mc, 30*PI/180);
	printf("rotation Y 30 deg:\n");
	matrix33Print(mc);
	free(mc);
}

/*
 * calculate fft-friendly size for convolution
 */
void calculateConvolutionSamples(int sourceSamples_x, int sourceSamples_y,
			int targetSamples_x, int targetSamples_y,
			int *convolutionSamples_x, int *convolutionSamples_y)
{
	*convolutionSamples_x = sourceSamples_x + targetSamples_x - 1;
	*convolutionSamples_x = makeFFTFriendly(*convolutionSamples_x);

	*convolutionSamples_y = sourceSamples_y + targetSamples_y - 1;
	*convolutionSamples_y = makeFFTFriendly(*convolutionSamples_y);
}


/* searching for FFTW-friendly array sizes
 * FFTW is optimized for arrays of size 2^a 3^b 5^c 7^d
 * so we will try to search for number n nearest higher number of such type
 */
#define MAX_FRIENDLY_VALUE 100000       /* maximal size of an array edge (for 2D FFT) */
#define FRIENDLY_VALUES	1000			/* array of FFTW-friendly sizes; 1000 is enough even for maximum size of 100 000 */

int primes[] = {2, 3, 5, 7};            /* FFTW-freindly primes */
int primes_count = 4;
int friendly_value[FRIENDLY_VALUES];    /* array of friendly sizes; initialization is automatic by the first use of make_fft_friendly */
int friendly_values = 0;                /* size of friendly_value[] array */
int *max_prime_power;                   /* temporary array of maximal prime powers */


/* internal recursive function that tries to consecutively increase power of specified prime
 * the powers of the other primes are increased recursively
 *
 * input: prime to start with, value built until now
 */
void check_powers(int first_prime, int value)
{
	int power;

	if (first_prime >= primes_count)
		return;

	/* iterative check of all the powers of specified prime */
	for (power = 0; power < max_prime_power[first_prime]; power++)
	{
		if (value <= MAX_FRIENDLY_VALUE)
		{
			if (power > 0) /* in case we are checking number e.g. 2^0 * 3^0 * 5^0 * 7^n, we want to
							* write it just once, when checking diffrent powers of 7 */
				friendly_value[friendly_values++] = value;
			if (first_prime < primes_count-1) /* there may be < primes_count as well, but recursive call will return immediately */
				check_powers(first_prime+1, value);
		}
		else
			break; /* as there is no chance to get value lower */
		value *= primes[first_prime]; /* this needs to be last as we want to check powers x^0 */
	}
}

/* internal function for quicksort
 */
int compare_int(const void *arg1, const void *arg2 )
{
	return *((int*)arg1) < *((int*)arg2) ? -1 :
		   *((int*)arg1) > *((int*)arg2) ?  1 : 0;
}

/* internal function for binary search;
 * returns nearest bigger number to that of specified */
int search_nearest(int value, int *array, int first, int last)
{
	int middle;

	/* simple cases first */
	if (first == last) return array[first];
	if (value <= array[first]) return array[first];
	if (value >= array[last]) return array[last];
	if (first == last - 1) return array[last];

	/* recursive binary search */
	middle = (first + last) / 2;
	if (value == array[middle]) return array[middle];
	if (value < array[middle])
		return search_nearest(value, array, first+1, middle); /* there has to be middle, not middle-1, as we don't want to miss
															   * nearest higher number */
	else
		return search_nearest(value, array, middle+1, last); /* the same logic applies here */
}


/* for a given n, it finds nearest bigger number of the form 2^a * 3^b * 5^c * 7^d
 * (or other, as specified above)
 */
int makeFFTFriendly(int n)
{
	static int initialized = 0;

	if (initialized == 0)
	{
		int value = 1;
		int prime = 0;

		max_prime_power = (int*) malloc(primes_count * sizeof(int));

		/* let's find maximum powers so that prime^power < MAX_FRIENDLY_VALUE */
		for (prime = 0; prime < primes_count; prime++)
		{
			max_prime_power[prime] = 0;
			value = 1;
			while (value < MAX_FRIENDLY_VALUE)
			{
				value *= primes[prime];
				max_prime_power[prime]++;
			}
		}

		/* check all the combinations of powers and write them to friendly_values[] */
		check_powers(0, 1);

		free(max_prime_power);
		qsort((void*) friendly_value, friendly_values, sizeof(int), &compare_int);
		initialized = 1;
	}

	/* if initialized, we just have to search nearest one */
	return search_nearest(n, friendly_value, 0, friendly_values-1);
}
/* test for make_fft_friendly
	int i;
	make_fft_friendly(9);
	printf("%d\n", friendly_values);
	for (i = 0; i < friendly_values; i++) printf("%8d", friendly_value[i]);
	putchar('\n');
	for (i = 0; i < 100; i++) printf("%d -> %d\n", i, make_fft_friendly(i));
	exit(0);
*/
