#include "rayleighCompile.h"
//#include "cycle.h"
#include <stdio.h>
//#include <Windows.h>
//#include <WinBase.h>
#include <time.h>

typedef struct {
	t_timer timer;
	int valid;
} t_timerArray;

#define TIMER_ARRAY_LENGTH	50
int timerArrayLength = TIMER_ARRAY_LENGTH;
t_timerArray timerArray[TIMER_ARRAY_LENGTH];
int timerArrayInitialized = 0, timerArrayUsed = 0;

#define RAYLEIGH_TIMER_DEBUG


#ifdef _WINDOWS_

typedef struct {
	LARGE_INTEGER lastPosition;
	LARGE_INTEGER elapsed;
	LARGE_INTEGER frequency;
#ifdef RAYLEIGH_TIMER_DEBUG
	int running;
#endif
	char *name;
	int timerArrayPosition;
} t_winTimer;


int timerNew(t_timer *timer, char *timerName)
{
	t_winTimer *winTimer = NULL;
	INITEXCEPTION;

	if (timerArrayInitialized == 0)
		timerArrayInit();

	if (timer != NULL)
	{
		winTimer = (t_winTimer*) malloc(sizeof(t_winTimer));
		if (winTimer == NULL)
			GOEXCEPTION0("Error allocating timer structure.\n");
		winTimer->name = NULL;
		if (timerName != NULL)
		{
			winTimer->name = (char*) malloc(sizeof(char)*strlen(timerName)+1);
			if (winTimer->name == NULL)
				GOEXCEPTION0("Error allocating space for timer name.\n");
			strcpy(winTimer->name, timerName);
		}
		winTimer->elapsed.QuadPart = 0;
#ifdef RAYLEIGH_TIMER_DEBUG
		winTimer->running = 0;
#endif
		QueryPerformanceFrequency(&(winTimer->frequency));
		*timer = (t_timer)winTimer;
		timerArrayAdd(*timer);
	}

EXCEPTION0("timerNew");

	IFEXCEPTION {
		if (timerName != NULL &&
			winTimer != NULL &&
			winTimer->name == NULL)
		{
			free(winTimer);
		}
	}

ENDEXCEPTION;
}


void timerDispose(t_timer *timer)
{
	t_winTimer *winTimer;
	if (timer != NULL)
	{
		timerArrayRemove(*timer);
		winTimer = (t_winTimer*)(*timer);
		if (winTimer->name != NULL)
			free(winTimer->name);
		free(*timer);
		*timer = NULL;
	}
}


void timerStart(t_timer timer)
{
	t_winTimer *winTimer;

	winTimer = (t_winTimer*)timer;
#ifdef RAYLEIGH_TIMER_DEBUG
	if (timer == NULL)
	{
		ELOG0("NULL timer when starting.\n");
		exit(0);
	}

	if (winTimer->running == 1)
	{
		ELOG1("Error starting timer %s.\n", winTimer->name);
		exit(0);
	}
	winTimer->running = 1;
#endif
	QueryPerformanceCounter((LARGE_INTEGER*)&(winTimer->lastPosition));
}


void timerPause(t_timer timer)
{
	t_winTimer *winTimer;
	LARGE_INTEGER finish;

	winTimer = (t_winTimer*)timer;
	QueryPerformanceCounter(&finish);

#ifdef RAYLEIGH_TIMER_DEBUG
	if (timer == NULL)
	{
		ELOG0("NULL timer when pausing.\n");
		exit(0);
	}
	if (winTimer->running == 0)
	{
		ELOG1("Error pausing timer %s.\n", winTimer->name);
		exit(0);
	}
	winTimer->running = 0;
#endif

	winTimer->elapsed.QuadPart += finish.QuadPart - (winTimer->lastPosition.QuadPart);
	winTimer->lastPosition.QuadPart = finish.QuadPart;
}


double timerReadSec(t_timer timer)
{
	t_winTimer *winTimer;

	winTimer = (t_winTimer*)timer;
#ifdef RAYLEIGH_TIMER_DEBUG
	if (timer == NULL)
	{
		ELOG0("NULL timer when reading.\n");
		exit(0);
	}
	if (winTimer->running == 1)
	{
		ELOG1("Error reading timer %s. Timer running.\n", winTimer->name);
		exit(0);
	}
#endif

	return (double)winTimer->elapsed.QuadPart / (double)winTimer->frequency.QuadPart;

}


void timerSetArrayPosition(t_timer timer, int position)
{
	t_winTimer *winTimer;
	winTimer = (t_winTimer*)timer;
#ifdef RAYLEIGH_TIMER_DEBUG
	if (timer == NULL)
	{
		ELOG0("NULL timer in timerSetArrayPosition.\n");
		exit(0);
	}
#endif
	winTimer->timerArrayPosition = position;
}

int timerGetArrayPosition(t_timer timer)
{
	t_winTimer *winTimer;
	winTimer = (t_winTimer*)timer;
#ifdef RAYLEIGH_TIMER_DEBUG
	if (timer == NULL)
	{
		ELOG0("NULL timer in timerGetArrayPosition.\n");
		exit(0);
	}
#endif
	return winTimer->timerArrayPosition;
}


char* timerGetName(t_timer timer)
{
	t_winTimer *winTimer;
	winTimer = (t_winTimer*)timer;
#ifdef RAYLEIGH_TIMER_DEBUG
	if (timer == NULL)
	{
		ELOG0("NULL timer in timerGetName.\n");
		exit(0);
	}
#endif
	return winTimer->name;
}


void timerArrayInit()
{
	int i;
	for (i=0; i < timerArrayLength; i++)
	{
		timerArray[i].valid = 0;
		timerArray[i].timer = NULL;
	}
	timerArrayInitialized = 1;
	timerArrayUsed = 0;
}


void timerArrayDispose(void)
{
	int i;
	for (i=0; i<timerArrayLength; i++)
	{
		if (timerArray[i].valid == 1)
			timerDispose(&(timerArray[i].timer));
		if (timerArrayUsed == 0)
			break;
	}
}


void timerArrayAdd(t_timer timer)
{
	int i, position = -1;
	for (i = 0; i < timerArrayLength; i++)
		if (timerArray[i].valid == 0)
		{
			timerArray[i].valid = 1;
			timerArray[i].timer = timer;
			timerSetArrayPosition(timer, i);
			position = i;
			timerArrayUsed++;
			break;
		}
}

void timerArrayRemove(t_timer timer)
{
	int i;
	i = timerGetArrayPosition(timer);
	if (i>=0 && i < timerArrayLength)
	{
		timerArray[i].valid = 0;
		timerArrayUsed--;
	}
}


void timerArrayList()
{
	int i, j;
	j = timerArrayUsed;
	for (i = 0; i < timerArrayLength; i++)
	{
		if (timerArray[i].valid == 1)
		{
			LOG2("%16g %s\n", timerReadSec(timerArray[i].timer), timerGetName(timerArray[i].timer));
			j--;
			if (j==0)
				break;
		}
	}
}



#else

clock_t startAnsiTime;
void startClock(void)
{
	startAnsiTime = clock();
}

double measureTime(void)
{
	clock_t finish;
	finish = clock();
	return (double)(finish - startAnsiTime) / (double) CLOCKS_PER_SEC;
}

#endif
