#include "rayleighCompile.h"

/* default size of lightsArray */
#define LIGHTSARRAY_DEFAULT_MAXLIGHTSCOUNT		1000

/* If reallocation is taken, the size of lightsArray is increased
 * to LIGHTSARRAY_DEFAULT_REALLOC * (actual size)
 */
#define LIGHTSARRAY_DEFAULT_REALLOC				1.5

int numCountExpected[6] = {0, 3, 4, 5, 6, 6};

int loadPLYHeader(FILE *fin);

/*
 * create a testing point lights cloud
 */
int lightsArrayNew(t_lightsArray **lights, int maxLightsCount)
{
	/* sanity check */
	if (lights == NULL)
		GOEXCEPTION0("NULL pointer when calling lightsArrayCreate\n");

	if (maxLightsCount < 0)
		GOEXCEPTION0("Wrong number of lights r when calling lightsArrayCreate\n");

	if (maxLightsCount == 0)
		maxLightsCount = LIGHTSARRAY_DEFAULT_MAXLIGHTSCOUNT;

	*lights = (t_lightsArray*) malloc(sizeof(t_lightsArray));
	if (*lights == NULL)
		GOEXCEPTION0("Error allocating structure memory when calling lightsArrayCreate\n");

	(*lights)->lights = (t_light*) malloc(maxLightsCount * sizeof(t_light));
	if ((*lights)->lights == NULL)
		GOEXCEPTION0("Error allocating array memory when calling lightsArrayCreate\n");

	/* allocate */
	(*lights)->maxLightsCount = maxLightsCount;
	(*lights)->lightsCount = 0;

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}

/*
 * dispose a point lights cloud
 */
int lightsArrayDispose(t_lightsArray **lights)
{
	if (lights == NULL || *lights == NULL)
		GOEXCEPTION0("NULL pointer when calling lightsArrayDispose.\n");

	free(*lights);
	*lights = NULL;
	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}


/*
 * create a new lightsArray; if a nonempty *lights is provided,
 * copy its contents (up to maxLightsCount) to a newly created one.
 */
int lightsArrayRealloc(t_lightsArray **src_lights, int maxLightsCount)
{
	t_lightsArray *dst_lights = NULL;
	int lightsToCopy;
	INITEXCEPTION

	/* sanity check */
	if (src_lights == NULL)
		GOEXCEPTION0("NULL lights pointer.\n");

	if (maxLightsCount < 0)
		GOEXCEPTION0("Wrong number of lights.\n");

	/* allocate default number of lights */
	if (maxLightsCount == 0)
		maxLightsCount = LIGHTSARRAY_DEFAULT_MAXLIGHTSCOUNT;

	/* create a new lightsArray */
	CHECK0(lightsArrayNew(&dst_lights, maxLightsCount), "Error creating new lightsArray.\n");

	/* Check if the src_lights contains lights.
	 * If so, copy them to the dst_lights and dispose src_lights.
	 */
	if (*src_lights == NULL)
	{
		/* nothing to copy */
	}
	else
	{
		lightsToCopy = lightsArrayGetLightsCount(*src_lights);
		if (lightsToCopy > maxLightsCount)
			lightsToCopy = maxLightsCount;

		CHECK0(lightsArrayCopyN(&dst_lights, *src_lights, maxLightsCount), "Error copying old lights.\n");
		CHECK0(lightsArrayDispose(src_lights), "Error disposing old lightsArray.\n");
	}

	*src_lights = dst_lights;

	EXCEPTION0("lightsArrayRealloc")
	ENDEXCEPTION
}



/*
 * copy up to N lights fro src_lights to dst_lights
 * if dst_lights==NULL, make a ne one
 */
int lightsArrayCopyN(t_lightsArray **dst_lights, t_lightsArray *src_lights, int maxLightsCount)
{
	int lightsToCopy;
	INITEXCEPTION

	/* sanity check */
	if (dst_lights == NULL)
		GOEXCEPTION0("NULL destination pointer.\n");

	if (src_lights == NULL)
		GOEXCEPTION0("NULL source pointer.\n");

	if (maxLightsCount < 0)
		GOEXCEPTION0("Negative lights count.\n");

	/* determine number of lights to copy */
	lightsToCopy = lightsArrayGetLightsCount(src_lights);
	if (lightsToCopy > maxLightsCount)
		lightsToCopy = maxLightsCount;

	/* allocate new lightsArray of further restrict number of lights to copy */
	if (*dst_lights == NULL)
	{
		CHECK0(lightsArrayNew(dst_lights, lightsToCopy), "Error creating new lightsArray.\n");
	}
	else
	{
		int dstMaxLightsCount;
		dstMaxLightsCount = lightsArrayGetMaxLightsCount(*dst_lights);
		if (lightsToCopy > dstMaxLightsCount) lightsToCopy = dstMaxLightsCount;
	}

	/* copy the data */
	memcpy((*dst_lights)->lights, src_lights->lights, lightsToCopy * sizeof(t_light));

	(*dst_lights)->lightsCount = lightsToCopy;

	EXCEPTION0("lightsArrayCopyN")
	ENDEXCEPTION
}


/*
 * print the contents of the point lights cloud
 */
int lightsArrayList(t_lightsArray *lights)
{
	int i;

	/* sanity check */
	if (lights == NULL)
		return RAYLEIGH_OK;

	if (lights->lightsCount < 0 || lights->lightsCount > lights->maxLightsCount)
		GOEXCEPTION;

	/* print lights */
	for (i=0; i<lights->lightsCount; i++)
		printf("%6d [%6.3f %6.3f %6.3f] %6.3f %6.3f\n",
		  i,
		  (lights->lights)[i].position.x,
		  (lights->lights)[i].position.y,
		  (lights->lights)[i].position.z,
		  (lights->lights)[i].amplitude,
		  (lights->lights)[i].phase
		  );

	return RAYLEIGH_OK;

EXCEPTION
	return RAYLEIGH_ERROR;
}


/*
 * getters/setters
 */
int lightsArrayGetLightsCount(t_lightsArray *lights)
{
	return lights->lightsCount;
}

int lightsArrayGetMaxLightsCount(t_lightsArray *lights)
{
	return lights->maxLightsCount;
}

/* TODO
 *
 * read a PNG file and convert it to a point lights cloud
 */
int lightsReadImage(t_lightsArray *lights, char *intensityFileName, char *depthFileName, double minDepth, double maxDepth)
{
/*	t_optfield *intensity = NULL, *depth = NULL;
	int retvalue = RAYLEIGH_ERROR;
	int samples_x, samples_y;
	int i, j, imageIndex, lightIndex;
	double deltaDepth;


	deltaDepth = maxDepth - minDepth;

	CHECK(optfieldLoadPNG(&intensity, intensityFileName, 1, SampleTypeFFTWDouble, BufferTypeMain, PictureIOIntensity));
	CHECK(optfieldLoadPNG(&depth, depthFileName, 1, SampleTypeFFTWDouble, BufferTypeMain, PictureIOIntensity));

	optfieldGetSamples(&samples_x, &samples_y, intensity);
	optfieldGetSamples(&i, &j, intensity);

	if (i != samples_x || j != samples_y)
	{
		ELOG2("Images %s and %s do not have the same resolution.\n", intensityFileName, depthFileName);
		GOEXCEPTION;
	}

	imageIndex = 0;
	lightIndex = lights->lightsCount;

	for (i=0; i<samples_y; i++)
		for (j=0; j<samples_x; j++, imageIndex++)
		{
			depth = ((fftw_complex*)depth->buffer->data)[imageIndex][RE];
			if (depth != 0)
			{
				depth = minDepth + depth*deltaDepth;

			}
		}


	retvalue = RAYLEIGH_OK;
EXCEPTION

	if (intensity != NULL)
		optfieldDispose(&intensity);

	if (depth != NULL)
		optfieldDispose(&depth);

	return retvalue;
	*/
	return RAYLEIGH_ERROR;
}


/*
 * Generate a point light source
 * and store it to a lightsArray.
 * If necessary, it reallocates the lightsArray to make it bigger.
 *
 * input/output: lights - lightsArray
 * input: ax, ay, az - light position
 * input: aa, ap - amplitude and phase
 *
 * return: RAYLEIGH_OK/RAYLEIGH_ERROR
 */
int lightsArrayGeneratePoint(t_lightsArray **inout_lights,
						double ax, double ay, double az, double aa, double ap)
{
	int lightIndex;
	t_lightsArray *lights;

	INITEXCEPTION;

	/* sanity check */
	if (inout_lights == NULL)
		GOEXCEPTION0("NULL lights pointer when calling lightsGeneratePoint\n");

	lights = *inout_lights;

	/* Is there enough memory in the lightsArray?
	 * If not, reallocate.
	 */
	if (lights->lightsCount + 1 > lights->maxLightsCount)
	{
		/* whithout realloationm it would be necessary to end up with an error
		   GOEXCEPTION0("Not enough space in lights array when calling lightsGeneratePoint\n");
		 */
		int newMaxLightsCount;
		newMaxLightsCount = (int)(lightsArrayGetMaxLightsCount(lights) * LIGHTSARRAY_DEFAULT_REALLOC);
		if (newMaxLightsCount <= lightsArrayGetMaxLightsCount(lights))
			newMaxLightsCount = lightsArrayGetMaxLightsCount(lights) + 1;
		CHECK0(lightsArrayRealloc(inout_lights, newMaxLightsCount),
			"Error adding a point when reallocating the lightsArray.\n");
		lights = *inout_lights;
	}

	/* starting index of a light to generate */
	lightIndex = lights->lightsCount;

	/* allocate space in the lightsArray */
	lights->lightsCount += 1;

	/* generate light */
	(lights->lights)[lightIndex].amplitude = aa;
	(lights->lights)[lightIndex].phase = ap;
	(lights->lights)[lightIndex].phasorRe = aa * cos(ap);
	(lights->lights)[lightIndex].phasorIm = aa * sin(ap);
	(lights->lights)[lightIndex].position.x = ax;
	(lights->lights)[lightIndex].position.y = ay;
	(lights->lights)[lightIndex].position.z = az;

EXCEPTION0("lightsGeneratePoint")
ENDEXCEPTION
}





/*
 * Generate a line composed of point lights
 * and store it to a lightsArray
 *
 * input/output: lights - lightsArray
 * input: ax, ay, az, bx, by, bz - line end points
 * input: aa, ap, ba, bp - line end amplitudes and phases
 * input: randomSeed - seed for a random number generator
 * input: randomPhase - amplitude of the random number added to a light phase
 * input: n - number of points to generate between end points
 * input: generateEnds - 1 = generate end point light sources; 0 = do not
 *
 * return: RAYLEIGH_OK/RAYLEIGH_ERROR
 */
int lightsArrayGenerateLine(t_lightsArray **lights,
					   double ax, double ay, double az, double aa, double ap,
					   double bx, double by, double bz, double ba, double bp,
					   int randomSeed, double randomPhase, int n, t_LightsArrayOptions generateEnds)
{
	int i, lightIndex;
	int nEffective;   /* actual number of points to generate */
	double t, nDenom;
	double bx_ax, by_ay, bz_az, ba_aa, bp_ap;
	double randMult;
	double amp, pha;

	INITEXCEPTION;

	/* sanity check */
	if (lights == NULL)
		GOEXCEPTION0("NULL lights pointer when calling lightsGeneratePoint\n");

	if (n < 2)
		GOEXCEPTION0("Error: number of points < 2 when calling lightsGeneratePoint\n");

	/* determine number of points to generate */
	nEffective = n;
	i = 0;
	if (generateEnds == GenereteEndsOff)
	{
		nEffective -= 2;
		i = 1;
	}

	/* shall we create a new lightsArray? */
	if (*lights == NULL)
	{
		CHECK0(lightsArrayNew(lights, nEffective), "Error creating new lightsArray.\n");
	}
	else
	{
		/* shall we reallocate the lightsArray? */
		if ((*lights)->lightsCount + nEffective > (*lights)->maxLightsCount)
		{
			CHECK0(lightsArrayRealloc(lights, (*lights)->lightsCount + nEffective),
			 "Error reallocating lightsArray.\n");
		}
	}

	/* starting index of a light to generate */
	lightIndex = (*lights)->lightsCount;

	/* allocate space in the lightsArray */
	(*lights)->lightsCount += nEffective;

	/* parameters for linear interpolation */
	nDenom = (double) (n - 1);
	bx_ax = bx - ax;
	by_ay = by - ay;
	bz_az = bz - az;
	ba_aa = ba - aa;
	bp_ap = bp - ap;

	/* initialize random numbers generator */
	srand(randomSeed);
	randMult = PI2 / RAND_MAX * randomPhase;

	/* generate lights; index i has been set before */
	for (; i < nEffective; i++)
	{
		t = i/nDenom;
		((*lights)->lights)[lightIndex].amplitude = amp = aa + ba_aa * t;
		((*lights)->lights)[lightIndex].phase = pha = ap + bp_ap * t + randMult * rand();
		((*lights)->lights)[lightIndex].phasorRe = amp * cos(pha);
		((*lights)->lights)[lightIndex].phasorIm = amp * sin(pha);
		((*lights)->lights)[lightIndex].position.x = ax + bx_ax * t;
		((*lights)->lights)[lightIndex].position.y = ay + by_ay * t;
		((*lights)->lights)[lightIndex].position.z = az + bz_az * t;
		lightIndex++;
	}

EXCEPTION0("lightsArrayGenerateLine");
ENDEXCEPTION
}

void lightAddPhase(t_light *light, double phase)
{
	double a, p;

	a = light->amplitude;
	p = light->phase;
	p += phase;
	light->phase = p;
	light->phasorRe = a * cos(p);
	light->phasorIm = a * sin(p);
}


int lightsArrayAddRndPhase(t_lightsArray *lights, double maxPhaseRad)
{
	int lightsCount, i;
	double rnd;

	INITEXCEPTION;

	lightsCount = lightsArrayGetLightsCount(lights);
	for (i=0; i < lightsCount; i++)
	{
		rnd = ((double)rand() / RAND_MAX) * maxPhaseRad;
		lightAddPhase(&(lights->lights[i]), rnd);
	}

	EXCEPTION0("lightsArrayAddRndPhase");
	ENDEXCEPTION;
}

/*
 * auxiliary comparison function for quick sort
 */
int lightCompareZ(const void *arg1, const void *arg2)
{
	if (((t_light*)arg1)->position.z < ((t_light*)arg2)->position.z)
		return -1;
	if (((t_light*)arg1)->position.z > ((t_light*)arg2)->position.z)
		return +1;

	return 0;
}


/*
 * sort lights by z coordinate
 */
int lightsSort(t_light *lights, int lightsCount)
{
	if (lights == NULL)
		return RAYLEIGH_ERROR;

	qsort((void*) lights, (size_t) lightsCount, sizeof(t_light), lightCompareZ);

	return RAYLEIGH_OK;
}


/*
 * find extremal X, Y, Z coordinates of lights in the provided array
 */
int lightsFindExtreme(t_light *lights, int lightsCount,
					  t_light *minValues, t_light *maxValues)
{
	int i;
	t_light min, max;

	/* sanity check */
	if (lights == NULL)
		return RAYLEIGH_ERROR;

	if (lightsCount == 0)
		return RAYLEIGH_OK;

	/* set initial min/max values */
	memcpy((void*)&min, (void*) lights, sizeof(t_light));
	memcpy((void*)&max, (void*) lights, sizeof(t_light));

	for (i=1; i<lightsCount; i++)
	{
		if (min.position.x > lights[i].position.x) min.position.x = lights[i].position.x;
		if (max.position.x < lights[i].position.x) max.position.x = lights[i].position.x;
		if (min.position.y > lights[i].position.y) min.position.y = lights[i].position.y;
		if (max.position.y < lights[i].position.y) max.position.y = lights[i].position.y;
		if (min.position.z > lights[i].position.z) min.position.z = lights[i].position.z;
		if (max.position.z < lights[i].position.z) max.position.z = lights[i].position.z;
		if (min.amplitude  > lights[i].amplitude)  min.amplitude  = lights[i].amplitude;
		if (max.amplitude  < lights[i].amplitude)  max.amplitude  = lights[i].amplitude;
		if (min.phase      > lights[i].phase)      min.phase      = lights[i].phase;
		if (max.phase      < lights[i].phase)      max.phase      = lights[i].phase;
		if (min.phasorRe   > lights[i].phasorRe)   min.phasorRe   = lights[i].phasorRe;
		if (max.phasorRe   < lights[i].phasorRe)   max.phasorRe   = lights[i].phasorRe;
		if (min.phasorIm   > lights[i].phasorIm)   min.phasorIm   = lights[i].phasorIm;
		if (max.phasorIm   < lights[i].phasorIm)   max.phasorIm   = lights[i].phasorIm;
	}

	if (minValues != NULL)
		memcpy((void*)minValues, (void*)&min, sizeof(t_light));

	if (maxValues != NULL)
		memcpy((void*)maxValues, (void*)&max, sizeof(t_light));

	return RAYLEIGH_OK;
}


/*
 * generate a lights array for testing purposes
 * - a rectangle <-dist, -dist> to <dist, dist>, z position z1
 * - a cross <-dist, -dist> to <dist, dist>, z position z2
 */
int lightsArrayGenerateTest(t_lightsArray **lights, double dist, double z1, double z2)
{
	CHECK(lightsArrayGenerateLine(lights, -dist, -dist, z1, 1, 0,
							   +dist, -dist, z1, 1, 0,
							   1, 1, 100, 1));
	CHECK(lightsArrayGenerateLine(lights, -dist, +dist, z1, 1, 0,
							   +dist, +dist, z1, 1, 0,
							   2, 1, 100, 1));
	CHECK(lightsArrayGenerateLine(lights, -dist, -dist, z1, 1, 0,
							   -dist, +dist, z1, 1, 0,
							   3, 1, 100, 1));
	CHECK(lightsArrayGenerateLine(lights, +dist, -dist, z1, 1, 0,
							   +dist, +dist, z1, 1, 0,
							   4, 1, 100, 1));
	CHECK(lightsArrayGenerateLine(lights, 0, -dist, z2, 1, 0,
							   0, +dist, z2, 1, 0,
							   6, 1, 100, 1));
	CHECK(lightsArrayGenerateLine(lights, -dist, 0, z2, 1, 0,
							   +dist, 0, z2, 1, 0,
							   7, 1, 100, 1));

	return RAYLEIGH_OK;

EXCEPTION

	return RAYLEIGH_ERROR;
}




/*
 * Read a lightsArray from a file
 */
int lightsArrayLoad(t_lightsArray **lights, char *filename, t_LightsArrayIO filetype)
{
	FILE *fin = NULL;
	int lineWidth = 1000;
	char line[1000];
	double x, y, z, a, p, r, g, b;
	int numsRead, linesNotRead = 0;
	INITEXCEPTION


	if (lights == NULL)
		GOEXCEPTION0("NULL lights pointer.\n");

	if (//filetype == LIGHTSARRAY_AUTO ||
		filetype == LightsArrayIORawXYZ ||
		filetype == LightsArrayIORawXYZA ||
		filetype == LightsArrayIORawXYZAP ||
		filetype == LightsArrayIORawXYZRGB ||
		filetype == LightsArrayIOPLY
		)
	{
		fin = fopen(filename, "r");
		if (fin == NULL) GOEXCEPTION1("Error opening file %s.\n", filename);
	}
	else
		GOEXCEPTION0("Unsupported file type.\n");

	if (*lights == NULL)
		CHECK0(lightsArrayNew(lights, 0), "Error creating new lightsArray.\n");

	x = y = z = 0;

	if (filetype == LightsArrayIOPLY)
		if (loadPLYHeader(fin) != RAYLEIGH_OK)
			GOEXCEPTION0("Unknown PLY structure. File not PLY?\n");

#pragma warning(push)
#pragma warning(disable : 4127)  /* yes, the while condition is constant, do not generate a warning */
	while(1)
#pragma warning(pop)
	{
		if (fgets(line, lineWidth, fin) == NULL)
			break;

		/* set initial amplitude and phase values */
		p = 0;
		a = 1;

		switch (filetype)
		{
		case LightsArrayIORawXYZ:
			numsRead = sscanf(line, "%lf %lf %lf", &x, &y, &z);
			break;
		case LightsArrayIORawXYZA:
			numsRead = sscanf(line, "%lf %lf %lf %lf", &x, &y, &z, &a);
			break;
		case LightsArrayIORawXYZAP:
			numsRead = sscanf(line, "%lf %lf %lf %lf %lf", &x, &y, &z, &a, &p);
			break;
		case LightsArrayIORawXYZRGB:
			numsRead = sscanf(line, "%lf %lf %lf %lf %lf %lf", &x, &y, &z, &r, &g, &b);
			a = 0.30*r + 0.59*g + 0.11*b;
			break;
		case LightsArrayIOPLY:
			numsRead = sscanf(line, "%lf %lf %lf %lf %lf %lf", &x, &y, &z, &r, &g, &b);
			a = 0.30*r + 0.59*g + 0.11*b;
			break;
		default:
			;
		}

		/* Was reading OK? */
		if (numsRead != numCountExpected[filetype])
		{
			linesNotRead++;
		}
		else
		{
			CHECK0(lightsArrayGeneratePoint(lights, x, y, z, a, p), "Error adding a point.\n");
		}
	}

	if (linesNotRead != 0)
		ELOG2("There was problem reading %d lines of the file %s.\n", linesNotRead, filename);



EXCEPTION0("lightsArrayLoad")

	if (fin != NULL)
		fclose(fin);

ENDEXCEPTION
}


/*
 * Save a lightsArray to a file
 */
int lightsArraySave(t_lightsArray *lights, char *filename, t_LightsArrayIO filetype)
{
	FILE *fout = NULL;
	int i, lightsCount;
	char numFormat[] = "%.15g";
	char format[100];
	double x, y, z, a, p, maxAmp;
	int ampInt;
	t_light maxValue;
	INITEXCEPTION;

	if (filetype == LightsArrayIOAuto ||
		filetype == LightsArrayIORawXYZ ||
		filetype == LightsArrayIORawXYZA ||
		filetype == LightsArrayIORawXYZAP ||
		filetype == LightsArrayIORawXYZRGB ||
		filetype == LightsArrayIOPLY
		)
	{
		fout = fopen(filename, "w");
		if (fout == NULL) GOEXCEPTION1("Error opening file %s.\n", filename);
	}
	else
		GOEXCEPTION0("Unsupported file type.\n");

	/* in default case, choose a filetype */
	if (filetype == LightsArrayIOAuto)
		filetype = LightsArrayIORawXYZAP;


	maxValue.amplitude = 0;

	if (lights != NULL)
	{
		lightsCount = lightsArrayGetLightsCount(lights);

		switch (filetype)
		{
		case LightsArrayIORawXYZ:
			sprintf(format, "%s %s %s\n", numFormat, numFormat, numFormat);
			break;
		case LightsArrayIORawXYZA:
			sprintf(format, "%s %s %s %s\n", numFormat, numFormat, numFormat, numFormat);
			break;
		case LightsArrayIORawXYZAP:
			sprintf(format, "%s %s %s %s %s\n", numFormat, numFormat, numFormat, numFormat, numFormat);
			break;
		case LightsArrayIORawXYZRGB:
			sprintf(format, "%s %s %s %s %s %s\n", numFormat, numFormat, numFormat, numFormat, numFormat, numFormat);
			CHECK0(lightsFindExtreme(lights->lights, lights->lightsCount, NULL, &maxValue),
				"Error when searching for the maximum amplitude.\n");
			break;
		case LightsArrayIOPLY:
			sprintf(format, "%s %s %s %%d %%d %%d\n", numFormat, numFormat, numFormat);
			fprintf(fout, "ply\n"
						  "format ascii 1.0\n"
						  "element vertex %d\n"
						  "property float x\n"
						  "property float y\n"
						  "property float z\n"
						  "property uchar red\n"
						  "property uchar green\n"
						  "property uchar blue\n"
						  "end_header\n", lightsCount);
			CHECK0(lightsFindExtreme(lights->lights, lights->lightsCount, NULL, &maxValue),
				"Error when searching for the maximum amplitude.\n");
		default:
			;
		}

		maxAmp = maxValue.amplitude;
		if (maxAmp <= 0) maxAmp = 1;

		for (i=0; i<lightsCount; i++)
		{
			x = lights->lights[i].position.x;
			y = lights->lights[i].position.y;
			z = lights->lights[i].position.z;
			a = lights->lights[i].amplitude;
			p = lights->lights[i].phase;

			switch (filetype)
			{
			case LightsArrayIORawXYZ:
				fprintf(fout, format, x, y, z);
				break;
			case LightsArrayIORawXYZA:
				fprintf(fout, format, x, y, z, a);
				break;
			case LightsArrayIORawXYZAP:
				fprintf(fout, format, x, y, z, a, p);
				break;
			case LightsArrayIORawXYZRGB:
				a = a / maxAmp * 255;
				fprintf(fout, format, x, y, z, a, a, a);
				break;
			case LightsArrayIOPLY:
				ampInt = (int) floor(a / maxAmp * 255 + 0.5);
				if (ampInt < 0) ampInt = 0;
				else if (ampInt > 255) ampInt = 255;
				fprintf(fout, format, x, y, z, ampInt, ampInt, ampInt);
				break;
			default:
				;
			}
		}

	}


	EXCEPTION0("lightsArraySave");

	if (fout != NULL)
		fclose(fout);

	ENDEXCEPTION
}


void lightsArrayTestIO(void)
{
	t_lightsArray *testObject = NULL;

	lightsArrayLoad(&testObject, "test_raw_xyz.txt", LightsArrayIORawXYZ);
	lightsArraySave(testObject, "out_raw_xyz.txt", LightsArrayIORawXYZ);
	lightsArrayDispose(&testObject);
	lightsArrayLoad(&testObject, "test_raw_xyza.txt", LightsArrayIORawXYZA);
	lightsArraySave(testObject, "out_raw_xyza.txt", LightsArrayIORawXYZA);
	lightsArrayDispose(&testObject);
	lightsArrayLoad(&testObject, "test_raw_xyzap.txt", LightsArrayIORawXYZAP);
	lightsArraySave(testObject, "out_raw_xyzap.txt", LightsArrayIORawXYZAP);
	lightsArrayDispose(&testObject);
	lightsArrayLoad(&testObject, "test_raw_xyzrgb.txt", LightsArrayIORawXYZRGB);
	lightsArraySave(testObject, "out_raw_xyzrgb.txt", LightsArrayIORawXYZRGB);
	lightsArraySave(testObject, "out_ply.ply", LightsArrayIOPLY);
	lightsArrayDispose(&testObject);
	(void) getchar();
}


int loadPLYHeader(FILE *fin)
{

	int lineWidth = 1000;
	char line[1000], propertyType[11], propertyName[11];
	int headerOK = 0;
	int numVerts = 0;
	int out;

	INITEXCEPTION;

	if (fgets(line, lineWidth, fin) == NULL) GOEXCEPTION;
	if (fgets(line, lineWidth, fin) == NULL) GOEXCEPTION;
	if (fgets(line, lineWidth, fin) == NULL) GOEXCEPTION;
	if (fgets(line, lineWidth, fin) == NULL) GOEXCEPTION;
	if (fgets(line, lineWidth, fin) == NULL) GOEXCEPTION;
	if (fgets(line, lineWidth, fin) == NULL) GOEXCEPTION;
	if (fgets(line, lineWidth, fin) == NULL) GOEXCEPTION;
	if (fgets(line, lineWidth, fin) == NULL) GOEXCEPTION;
	if (fgets(line, lineWidth, fin) == NULL) GOEXCEPTION;
	if (fgets(line, lineWidth, fin) == NULL) GOEXCEPTION;

	EXCEPTION0("loadPLYHeader");

	ENDEXCEPTION;
}
