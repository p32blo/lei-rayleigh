#include "rayleighCompile.h"

/*
 * Simple user function, just calls an appropriate propagation function
 */
int lightsArrayPropagate(t_lightsArray *lightsArray, t_optfield *target)
{
	return lightsPropagate(lightsArray->lights, lightsArrayGetLightsCount(lightsArray), target);
}



/*
 * Propagate point lights array to the target plane
 * This simple function does not do any aliasing check; use it
 * for debugging purposes or in case where no aliasing occurs
 */
int lightsPropagateAliased(t_light *lights, int lightsCount, t_optfield *target)
{
	int light_i, image_x, image_y, index;
	t_point targetPosition, r;
	double temp_exponent, temp_r2, temp_r2z, temp_r2yz, temp;
	double cornerX, cornerY, cornerZ;
	int samplesX, samplesY;
	double samplingDistance;
	fftw_complex *out;

	/* nothing to propagate? */
	if (lights == NULL)
		return RAYLEIGH_OK;

	/* get target details and check for target pointer sanity */
	CHECK(optfieldGetSamples(&samplesX, &samplesY, target));
	CHECK(optfieldGetCorner(&cornerX, &cornerY, &cornerZ, target));
	samplingDistance = optfieldGetSamplingdistance(target);

	if (target->buffer == NULL)
		GOEXCEPTION0("Target buffer NULL when calling lightsPropagate\n");

	out = (fftw_complex*) target->buffer->data;

	if (out == NULL)
		GOEXCEPTION0("Target buffer data NULL when calling lightsPropagate\n");

	for (light_i=0; light_i<lightsCount; light_i++)
	{
		targetPosition.z = cornerZ;
		r.z = targetPosition.z - lights[light_i].position.z;
		temp_r2z = r.z * r.z;

		index = 0;

		LOG3("\r             Light %d of %d (%.2f%% done)", light_i+1, lightsCount, (100.0*light_i) / lightsCount);

		for (image_y = 0; image_y < samplesY; image_y++)
		{
			LOG1("\rLine %d /", image_y);
			targetPosition.y = cornerY + image_y * samplingDistance;
			r.y = targetPosition.y - lights[light_i].position.y;
			temp_r2yz = temp_r2z + r.y*r.y;

			for (image_x = 0; image_x < samplesX; image_x++)
			{
				targetPosition.x = cornerX + image_x * samplingDistance;
				r.x = targetPosition.x - lights[light_i].position.x;
				temp_r2 = temp_r2yz + r.x*r.x;

				/* U(x, y) = z/(jl) L exp(jk|r|)/r^2
				 * x, y, z = target position
				 * l = lambda (wavelength)
				 * k = 2pi/lambda (wavenumber)
				 * L = light amplitude
				 * r = vector target - source, r^2 = temp_r2
				 */

				temp_exponent = lights[light_i].phase + k_number * sqrt(temp_r2);

				temp = r.z / lambda * lights[light_i].amplitude / temp_r2;
/*
 orig R-S je * 1/j:
				image->data[index][IM] +=  -temp * cos(temp_exponent);
				image->data[index][RE] +=   temp * sin(temp_exponent);
*/

				out[index][RE] +=  temp * cos(temp_exponent);
				out[index][IM] +=  temp * sin(temp_exponent);

				index++;
			}

		}
	}

	LOG3("\r             Light %d of %d (%.2f%% done)", light_i, lightsCount, (100.0*light_i) / lightsCount);
	return RAYLEIGH_OK;

EXCEPTION

	return RAYLEIGH_ERROR;
}



/*
 * Propagate point lights array to the target plane
 * A simple aliasing check is introduced: only local spatial frequencies
 * less than limit for given sampling are considered.
 */
int lightsPropagate(t_light *lights, int lightsCount, t_optfield *target)
{
	int light_i, image_x, image_y;
	t_point targetPosition, r;
	double temp_exponent, temp_r2, temp_r2z, temp_r2yz, temp;
	double cornerX, cornerY, cornerZ;
	int samplesX, samplesY;
	double samplingDistance;
	double maxCorrectRho;
	int minX, maxX, minY, maxY, indexY;
	RAYLEIGH_INT64 index;
	fftw_complex *out;

	/* nothing to propagate? */
	if (lights == NULL)
		return RAYLEIGH_OK;

	/* get target details and check for target pointer sanity */
	CHECK(optfieldGetSamples(&samplesX, &samplesY, target));
	CHECK(optfieldGetCorner(&cornerX, &cornerY, &cornerZ, target));
	samplingDistance = optfieldGetSamplingdistance(target);

	if (target->buffer == NULL)
		GOEXCEPTION0("Target buffer NULL when calling lightsPropagate\n");

	out = (fftw_complex*) target->buffer->data;

	if (out == NULL)
		GOEXCEPTION0("Target buffer data NULL when calling lightsPropagate\n");

	for (light_i=0; light_i<lightsCount; light_i++)
	{
		LOG3("\r             Light %d of %d (%.2f%% done)", light_i+1, lightsCount, (100.0*light_i) / lightsCount);

		/* Z coordinate of (target - source) vector */
		targetPosition.z = cornerZ;
		r.z = targetPosition.z - lights[light_i].position.z;
		temp_r2z = r.z * r.z;

		/* determine diameter of area that does not alias */
		/* rho equals to sqrt(x^2 + y^2), r equals to sqrt(rho^2 + z^2) */
		maxCorrectRho = lambda * fabs(r.z) / sqrt(4*samplingDistance*samplingDistance - lambda*lambda);
		if (isfinite(maxCorrectRho))
		{
			minY = (int)((lights[light_i].position.y - cornerY - maxCorrectRho)/samplingDistance + 0.5);
			maxY = (int)((lights[light_i].position.y - cornerY + maxCorrectRho)/samplingDistance + 0.5);

			if (minY < 0)
				minY = 0;
			if (maxY >= samplesY)
				maxY = samplesY - 1;
		}
		else
		{
			minY = 0;
			maxY = samplesY - 1;
		}

		indexY = minY * samplesX;

		for (image_y = minY; image_y <= maxY; image_y++, indexY += samplesX)
		{
			LOG1("\rLine %d /", image_y);
			targetPosition.y = cornerY + image_y * samplingDistance;
			r.y = targetPosition.y - lights[light_i].position.y;
			temp_r2yz = temp_r2z + r.y*r.y;

			if (isfinite(maxCorrectRho))
			{
				double xHalfWidth = sqrt(maxCorrectRho*maxCorrectRho - r.y*r.y);
				if (isfinite(xHalfWidth))
				{
					minX = (int)((lights[light_i].position.x - cornerX - xHalfWidth)/samplingDistance + 0.5);
					maxX = (int)((lights[light_i].position.x - cornerX + xHalfWidth)/samplingDistance + 0.5);
					if (minX < 0)
						minX = 0;
					if (maxX >= samplesX)
						maxX = samplesX - 1;
				}
				else
					continue;
			}
			else
			{
				minX = 0;
				maxX = samplesX - 1;
			}

			for (image_x = minX, index = indexY + minX; image_x <= maxX; image_x++, index++)
			{
				targetPosition.x = cornerX + image_x * samplingDistance;
				r.x = targetPosition.x - lights[light_i].position.x;
				temp_r2 = temp_r2yz + r.x*r.x;

				/* U(x, y) = z/(jl) L exp(jk|r|)/r^2
				 * x, y, z = target position
				 * l = lambda (wavelength)
				 * k = 2pi/lambda (wavenumber)
				 * L = light amplitude
				 * r = vector target - source, r^2 = temp_r2
				 */

				temp_exponent = lights[light_i].phase + k_number * sqrt(temp_r2);

				temp = r.z / lambda * lights[light_i].amplitude / temp_r2;
/*
 orig R-S je * 1/j:
				image->data[index][IM] +=  -temp * cos(temp_exponent);
				image->data[index][RE] +=   temp * sin(temp_exponent);
*/

				out[index][RE] +=  temp * cos(temp_exponent);
				out[index][IM] +=  temp * sin(temp_exponent);
			}
		}
	}

	LOG3("\r             Light %d of %d (%.2f%% done)\n", light_i, lightsCount, (100.0*light_i) / lightsCount);
	return RAYLEIGH_OK;

EXCEPTION

	return RAYLEIGH_ERROR;
}


int lightsPropagateDLUTAuto(t_light *lights, int lightsCount, t_optfield *target,
							t_PropagationAcceleration propagationAcceleration,
							double waveLUTUpsampling, double zSamplingDistance,
							double limitRhoSamplingDistance, double rhoSamplingFactor,
							int baseUpsampling, t_FilterType filterType, void *filterTypeParams)
{
	double samplingDistance;
	t_point cornerPosition;
	t_waveLUT *waveLUT = NULL;
	t_rhoLUT *rhoLUT = NULL;
	t_light minLights, maxLights;
	double width, height;
	double minXRel, maxXRel, minYRel, maxYRel;

	INITEXCEPTION;

	/* sanity check */
	if (lights == NULL)
		ENDFUNCTION;

	if (lightsCount < 0)
		GOEXCEPTION("Negative lights count.\n");

	if (target == NULL)
		GOEXCEPTION0("NULL target optfield.\n");

	if (waveLUTUpsampling < 0)
		GOEXCEPTION0("WaveLUTUpsampling must be positive, should be at least 4.\n");


	CHECK0(optfieldGetCorner(&cornerPosition.x, &cornerPosition.y, &cornerPosition.z, target),
		"Error accessing target optfield.\n");
	samplingDistance = optfieldGetSamplingdistance(target);
	optfieldGetSize(&width, &height, target);

	/* check/set default values */
	if (waveLUTUpsampling == 0) waveLUTUpsampling = 8;
	if (zSamplingDistance == 0) zSamplingDistance = samplingDistance;
	if (limitRhoSamplingDistance == 0) limitRhoSamplingDistance = samplingDistance;
	if (rhoSamplingFactor == 0) rhoSamplingFactor = 1;
	if (baseUpsampling == 0) baseUpsampling = 1;
	if (filterType == FilterTypeDefault) filterType = FilterTypeTriangular;

	/* for creating perfectly symmetric rhoLUT table */
	width -= samplingDistance;
	height -= samplingDistance;

	/* prepare LUTs*/
	lightsFindExtreme(lights, lightsCount, &minLights, &maxLights);
	rhoLUTFindExtremeXYRel(minLights.position.x, maxLights.position.x,
		minLights.position.y, maxLights.position.y,
		cornerPosition.x, cornerPosition.x + width,
		cornerPosition.y, cornerPosition.y + height,
		&minXRel, &maxXRel,
		&minYRel, &maxYRel);

	if (propagationAcceleration == PropagationAccDoubleLUT)
	{
		rhoLUTNew(minXRel, maxXRel, minYRel, maxYRel, samplingDistance, &rhoLUT);
		waveLUTNew(rhoLUT->rhoMin, rhoLUT->rhoMax, samplingDistance/waveLUTUpsampling,
			cornerPosition.z-maxLights.position.z, cornerPosition.z-minLights.position.z, zSamplingDistance,
			limitRhoSamplingDistance, rhoSamplingFactor,
			baseUpsampling, filterType, filterTypeParams,
			&waveLUT);
	}
	else if (propagationAcceleration == PropagationAccWaveLUT)
	{
		waveLUTNew(sqrt(minXRel*minXRel + minYRel*minXRel),
			sqrt(maxXRel*maxXRel + maxYRel*maxXRel),
			samplingDistance/waveLUTUpsampling,
			cornerPosition.z-maxLights.position.z, cornerPosition.z-minLights.position.z, zSamplingDistance,
			limitRhoSamplingDistance, rhoSamplingFactor,
			baseUpsampling, filterType, filterTypeParams,
			&waveLUT);
	}
	else
	{
		GOEXCEPTION0("Unsupported propagation acceleration method.\n");
	}

	/* propagate */
	lightsPropagateDLUT(lights, lightsCount, target, rhoLUT, waveLUT);

EXCEPTION0("lightsPropagateDLUT")

	/* clean up */
	if (waveLUT != NULL)
		waveLUTDispose(&waveLUT);
	if (rhoLUT != NULL)
		rhoLUTDispose(&rhoLUT);

ENDEXCEPTION;
}


/*
 * Propagate lights array to the target plane (i.e. calculate the object wave)
 * using double LUT method (i.e. both rhoLUT and waveLUT).
 * Both rhoLUT and waveLUT must be provided.
 *
 * input:
 *    lights - array of lights
 *    lightsCount - number of lights in the lights array
 *    rhoLUT - precalculated lookup table for rho
 *    waveLUT - precalculated lookup table for the phasor of a point light source
 * input/output:
 *    target
 */
int	lightsPropagateDLUT(t_light *lights, int lightsCount, t_optfield *target, t_rhoLUT *rhoLUT, t_waveLUT *waveLUT)
{
	int i, j, l, index;
	int samples_x, samples_y;
	double corner_x, corner_y, corner_z;
	double samplingDistance;
	double xLight, yLight, zLight, rho;
	double phasorRe, phasorIm;
	double rhoY2 = 0, x, y;
	int indexZ, indexWave, maxSamples, maxSamplesOptfield;
	int minY, maxY, minX, maxX;
	int centerY, centerX;
	int rhoSampleY, rhoSampleYIndex = 0, rhoSampleYIndexSkip = 0;
	int rhoSampleX, rhoSampleXIndex = 0, rhoSampleXIndexSkip;
	int partX, partXMin, partXMax;
	int partY, partYMin, partYMax;
	fftw_complex *out;

	INITEXCEPTION;

	/* sanity check */
	if (lights == NULL)
		GOEXCEPTION0("NULL lights pointer.\n");
	if (waveLUT == NULL)
		GOEXCEPTION0("NULL waveLUT pointer.\n");
	if (target == NULL)
		GOEXCEPTION0("NULL target pointer.\n");
	/*
	 * rhoLUT can be NULL. In that case, calculate rho directly
	 */

	/* nothing to propagate */
	if (lightsCount < 1)
		ENDFUNCTION;


	/* get target parameters */
	CHECK0(optfieldGetSamples(&samples_x, &samples_y, target),
		"Error accessing target.\n");
	samplingDistance = optfieldGetSamplingdistance(target);
	optfieldGetCorner(&corner_x, &corner_y, &corner_z, target);

	/* set output buffer */
	out = (fftw_complex*) target->buffer->data;

	/* iterate through all lights */
	for (l = 0; l < lightsCount; l++)
	{
		/* get lights[l] parameters */
		xLight = (lights+l)->position.x;
		yLight = (lights+l)->position.y;
		zLight = (lights+l)->position.z;
		phasorRe = (lights+l)->phasorRe;
		phasorIm = (lights+l)->phasorIm;

		/* calculate which row of waveLUT will be used */
		indexZ = (int) floor((corner_z - zLight - waveLUT->zMin)/waveLUT->zSamplingDistance + 0.5);
		if (indexZ < 0 || indexZ >= waveLUTGetSamplesZ(waveLUT))
			GOEXCEPTION2("Z coordinate out of range of the waveLUT (light %d, z = %lf.\n", l, zLight);

		/* get rho samples of that row */
		maxSamples = waveLUT->samplesRhoNonZero[indexZ];

		/* recalculate maxSamples to the target sampling */
		/* this light source will affect just a rectangle
		   2*maxSamplesOptfield x 2*maxSamplesOptfield
		   in the target array
		 */
		maxSamplesOptfield = (int)((maxSamples * waveLUT->rhoSamplingDistance + waveLUT->rhoMin) / samplingDistance);

		/* calculate first index in waveLUT */
		indexZ *= waveLUT->samplesRho;

		/* calculate position (in samples) of the light in the target array */
		centerY = (int)floor((yLight - corner_y)/samplingDistance + 0.5);
		centerX = (int)floor((xLight - corner_x)/samplingDistance + 0.5);

		/* get Y range of light influence in target array */
		minY = centerY - maxSamplesOptfield;
		if (minY < 0) minY = 0;
		maxY = centerY + maxSamplesOptfield;
		if (maxY >= samples_y) maxY = samples_y-1;

		/* Do the same for X. */
		/* We will define minX-maxX range for every row, but this time
		 * we just check if the minX-maxX lies off the target window
		 */
		minX = centerX - maxSamplesOptfield;
		if (minX < 0) minX = 0;
		maxX = centerX + maxSamplesOptfield;
		if (maxX >= samples_x) maxX = samples_x-1;

		/* if the light influence does not intersect with
		 * target array, continue with next light
		 */
		if (maxX < 0 || maxY < 0 ||
			minX >= samples_x || minY >= samples_y)
			continue;

		/* Iterate all influenced rows of the target array.
		 * Split the iteration to two parts: for row <= centerY, and row > centerY.
		 * This is because in the first part, we need to take rhoLUT samples
		 * in decreasing sequence, in the other part in increasing distance.
		 * As both parts are almost the same, let's make a loop (partY variable)
		 * that just switches the parametrers partYMin, partYMax and rhoSampleYIndexSkip
		 * after finishing the first part.
		 *
		 * Note that this loop split is necessary only if we use rhoLUT.
		 *
		 */
		partYMin = minY;
		partYMax = centerY;
		if (partYMax > maxY)
			partYMax = maxY;

		for (partY = 0; partY < 2; partY++)
		{
			if (rhoLUT != NULL)
			{
				/* We use rhoLUT. Calculate the Y index for the lookup.
				 *
				 * Calculate Y distance from the Y position of the light
				 * in the "propagation theory language" (target - source):
				 * This would be in meters: yrel = corner_y + minY * samplingDistance - yLight;
				 * This is in samples:
				 */
				rhoSampleY = partYMin - centerY;

				/* We need to find what row (Y coord) of rhoLUT to use.
				 * The situation is a bit more complicated because rhoLUT may begin at nonzero value.
				 * The simplest is to solve it for positive and negative possibilities.
				 * Note: the code could be rewritten in a similar sense as the X loop, i.e.
				 * without explicit checking. Who cares...
				 */
				if (rhoSampleY < 0)
				{
					/* Negative case, i.e. minY < centerY; this happens e.g. when the light source Y is in the
					 * hologram area.
					 * As the rhoLUT describes symmetric function, the index to rhoLUT will be
					 * derived from -rhoSampleY. Then, compensate for nonzero starting rho. */
					rhoSampleYIndex = -rhoSampleY - (int)floor(rhoLUT->deltaYMin / rhoLUT->samplingDistance + 0.5);

					/* This should never happen, however... */
					if (rhoSampleYIndex < 0) rhoSampleYIndex = 0;

					/* rhoSampleYIndex now contains row index of rhoLUT.
					 * As rhoLUT is a 1-D array in principle, convert it to the beginning index
					 * of a correct row. */
					rhoSampleYIndex *= rhoLUT->samplesX;

					/* After processing one line we should skip to the next row.
					 * Y index will be increased, i.e. rhoSampleY should be decreased.
					 */
					rhoSampleYIndexSkip = -rhoLUT->samplesX;
				}
				else
				{
					/* The same applies as for negative case. In fact, this applies for partY == 1. */
					rhoSampleYIndex = rhoSampleY - (int)floor(rhoLUT->deltaYMin / rhoLUT->samplingDistance + 0.5);
					rhoSampleYIndex *= rhoLUT->samplesX;
					rhoSampleYIndexSkip = rhoLUT->samplesX;
				}

				/* End of rhoLUT index preparation. */
			}

			/* iterate all influenced rows */
			for (j = partYMin; j <= partYMax; j++)
			{
				/* Decide X range of iteration - inside of a circle
				 * with center (centerX, centerY) and radius maxSamples
				 */
				minX = (int)floor(sqrt((double)maxSamplesOptfield * (double)maxSamplesOptfield - (double)(j - centerY) * (double)(j - centerY)));
				maxX = centerX + minX;
				minX = centerX - minX;
				if (minX < 0)
					minX = 0;
				if (maxX >= samples_x)
					maxX = samples_x - 1;

				/*
				 * Prepare for columns iteration.
				 * It depends if we use rhoLUT or not.
				 */

				if (rhoLUT == NULL)
				{
					/* We don't use the rhoLUT. Prepare y component of the rho. */
					y = corner_y + j * samplingDistance;
					rhoY2 = (yLight - y) * (yLight - y);
				}
				else
				{
					/* We use rhoLUT.
					 * Calculate X distance from the X position of the light
					 * in the "propagation theory language" (target - source).
					 * This would be in meters: xrel = corner_x + minX * samplingDistance - yLight;
					 * This is in samples:
					 */
					rhoSampleX = minX - centerX;

					/* We need to find what column (X coord) of rhoLUT to use.
					 * The situation is a bit more complicated because rhoLUT may begin at nonzero value.
					 * The simplest is to solve it for positive and negative possibilities.
					 */

					if (rhoSampleX < 0)
					{
						/* Negative case, i.e. minX < centerX; this happens e.g. when the light source X is in the
						 * hologram area.
						 * As the rhoLUT describes symmetric function, the index to rhoLUT will be
						 * derived from -rhoSampleX. Then, compensate for nonzero starting rho. */
						rhoSampleXIndex = -rhoSampleX - (int)floor(rhoLUT->deltaXMin / rhoLUT->samplingDistance + 0.5);

						/* This should never happen, however... */
						if (rhoSampleXIndex < 0) rhoSampleXIndex = 0;
					}
					else
					{
						/* The same applies as for negative case. However, in the positive case
						 * is minX > centerX, i.e. we will never cross centerX during calculation.
						 */
						rhoSampleXIndex = rhoSampleX - (int)floor(rhoLUT->deltaXMin / rhoLUT->samplingDistance + 0.5);
						if (rhoSampleXIndex < 0) rhoSampleXIndex = 0;
					}
				}


				/* iterate all influenced columns of the target array */
				/* divide iteration to two parts: from minX to centerX, and from centerX+1 to maxX */

				partXMin = minX;
				partXMax = centerX;
				if (partXMax > maxX)
					partXMax = maxX;

				rhoSampleXIndexSkip = -1;
				index = j * samples_x + minX;

				for (partX = 0; partX < 2; partX++)
				{
					for (i = partXMin; i <= partXMax; i++, index++)
					{
						if (rhoLUT == NULL)
						{
							/* We don't use rhoLUT, calculate rho directly. */
							x = corner_x + i * samplingDistance;
							rho = sqrt((xLight - x) * (xLight - x) + rhoY2);
						}
						else
						{
							rho = rhoLUT->rho[rhoSampleXIndex + rhoSampleYIndex];
							rhoSampleXIndex += rhoSampleXIndexSkip;
						}

						indexWave = (int)floor((rho - waveLUT->rhoMin)/waveLUT->rhoSamplingDistance + 0.5);
						indexWave += indexZ;

						/* there is no need to check if the indexWave < maxSamples as (i,j) is
						 * inside the right area */

						out[index][RE] += waveLUT->phasor[indexWave][RE] * phasorRe - waveLUT->phasor[indexWave][IM]*phasorIm;
						out[index][IM] += waveLUT->phasor[indexWave][IM] * phasorRe + waveLUT->phasor[indexWave][RE]*phasorIm;
					}

					/* if the loop ran for partX == 0, then rhoSampleXIndex ended at -1;
					   however, for the partX == 1 iteration, rhoSampleXIndex should start at +1 */
					if (rhoSampleXIndex == -1)
						rhoSampleXIndex += 2;

					/* switch loop conditions for partX == 1 */
					rhoSampleXIndexSkip = 1;
					partXMin = centerX + 1;
					if (partXMin < 0)
						partXMin = 0;
					partXMax = maxX;
				}
				/* goto next Y for rhoLUT */
				rhoSampleYIndex += rhoSampleYIndexSkip;
			}
			/* switch loop conditions for partY == 1 */
			partYMin = centerY + 1;
			if (partYMin < 0)
				partYMin = 0;
			partYMax = maxY;
		}
	}

EXCEPTION0("lightsPropagateDLUT")
ENDEXCEPTION;
}


/*
 * Propagate lights array to the target plane (i.e. calculate the object wave)
 * using double LUT method (i.e. both rhoLUT and waveLUT).
 * Both rhoLUT and waveLUT must be provided.
 *
 * input:
 *    lights - array of lights
 *    lightsCount - number of lights in the lights array
 *    rhoLUT - precalculated lookup table for rho
 *    waveLUT - precalculated lookup table for the phasor of a point light source
 * input/output:
 *    target
 */
int	lightsPropagateDLUTyaloha(t_light *lights, int lightsCount, t_optfield *target, t_rhoLUT *rhoLUT, t_waveLUT *waveLUT)
{
	int i, j, l, index;
	int samples_x, samples_y;
	double corner_x, corner_y, corner_z;
	double samplingDistance;
	double xLight, yLight, zLight, rho;
	double phasorRe, phasorIm;
	int indexZ, indexWave, maxSamples, maxSamplesOptfield;
	int minY, maxY, minX, maxX;
	int centerY, centerX;
	int rhoSampleY, rhoSampleYIndex, rhoSampleYIndexSkip;
	int rhoSampleX, rhoSampleXIndex, rhoSampleXIndexSkip;
	int partX, partXMin, partXMax;
	int partY, partYMin, partYMax;
	fftw_complex *out;

	INITEXCEPTION;

	/* sanity check */
	if (lights == NULL)
		GOEXCEPTION0("NULL lights pointer.\n");
	/* nothing to propagate */
	if (lightsCount < 1)
		ENDFUNCTION;
	if (rhoLUT == NULL)
		GOEXCEPTION0("NULL rhoLUT pointer.\n");
	if (waveLUT == NULL)
		GOEXCEPTION0("NULL waveLUT pointer.\n");
	if (target == NULL)
		GOEXCEPTION0("NULL target pointer.\n");

	/* get target parameters */
	CHECK0(optfieldGetSamples(&samples_x, &samples_y, target),
		"Error accessing target.\n");
	samplingDistance = optfieldGetSamplingdistance(target);
	optfieldGetCorner(&corner_x, &corner_y, &corner_z, target);

	/* set output buffer */
	out = (fftw_complex*) target->buffer->data;

	/* iterate through all lights */
	for (l = 0; l < lightsCount; l++)
	{
		/* get lights[l] parameters */
		xLight = (lights+l)->position.x;
		yLight = (lights+l)->position.y;
		zLight = (lights+l)->position.z;
		phasorRe = (lights+l)->phasorRe;
		phasorIm = (lights+l)->phasorIm;

		/* calculate which row of waveLUT will be used */
		indexZ = (int) floor((corner_z - zLight - waveLUT->zMin)/waveLUT->zSamplingDistance + 0.5);

		/* get rho samples of that row */
		maxSamples = waveLUT->samplesRhoNonZero[indexZ];

		/* recalculate maxSamples to the target sampling */
		/* this light source will affect just a rectangle
		   2*maxSamplesOptfield x 2*maxSamplesOptfield
		   in the target array
		 */
		maxSamplesOptfield = (int)((maxSamples * waveLUT->rhoSamplingDistance + waveLUT->rhoMin) / samplingDistance);

		/* calculate first index in waveLUT */
		indexZ *= waveLUT->samplesRho;

		/* calculate position (in samples) of the light in the target array */
		centerY = (int)floor((yLight - corner_y)/samplingDistance + 0.5);
		centerX = (int)floor((xLight - corner_x)/samplingDistance + 0.5);

		/* get Y range of light influence in target array */
		minY = centerY - maxSamplesOptfield;
		if (minY < 0) minY = 0;
		maxY = centerY + maxSamplesOptfield;
		if (maxY >= samples_y) maxY = samples_y-1;

		/* Do the same for X. */
		/* We will define minX-maxX range for every row, but this time
		 * we just check if the minX-maxX lies off the target window
		 */
		minX = centerX - maxSamplesOptfield;
		if (minX < 0) minX = 0;
		maxX = centerX + maxSamplesOptfield;
		if (maxX >= samples_x) maxX = samples_x-1;

		/* if the light influence does not intersect with
		 * target array, continue with next light
		 */
		if (maxX < 0 || maxY < 0 ||
			minX >= samples_x || minY >= samples_y)
			continue;

		/* Iterate all influenced rows of the target array.
		 * Split the iteration to two parts: for row <= centerY, and row > centerY.
		 * This is because in the first part, we need to take rhoLUT samples
		 * in decreasing sequence, in the other part in increasing distance.
		 * As both parts are almost the same, let's make a loop (partY variable)
		 * that just switches the parametrers partYMin, partYMax and rhoSampleYIndexSkip
		 * after finishing the first part.
		 */
		partYMin = minY;
		partYMax = centerY;
		if (partYMax > maxY)
			partYMax = maxY;

		for (partY = 0; partY < 2; partY++)
		{
			/* Calculate Y distance from the Y position of the light
			 * in the "propagation theory language" (target - source):
			 * This would be in meters: yrel = corner_y + minY * samplingDistance - yLight;
			 * This is in samples:
			 */
			rhoSampleY = partYMin - centerY;

			/* We need to find what row (Y coord) of rhoLUT to use.
			 * The situation is a bit more complicated because rhoLUT may begin at nonzero value.
			 * The simplest is to solve it for positive and negative possibilities.
			 * Note: the code could be rewritten in a similar sense as the X loop, i.e.
			 * without explicit checking. Who cares...
			 */
			if (rhoSampleY < 0)
			{
				/* Negative case, i.e. minY < centerY; this happens e.g. when the light source Y is in the
				 * hologram area.
				 * As the rhoLUT describes symmetric function, the index to rhoLUT will be
				 * derived from -rhoSampleY. Then, compensate for nonzero starting rho. */
				rhoSampleYIndex = -rhoSampleY - (int)floor(rhoLUT->deltaYMin / rhoLUT->samplingDistance + 0.5);

				/* This should never happen, however... */
				if (rhoSampleYIndex < 0) rhoSampleYIndex = 0;

				/* rhoSampleYIndex now contains row index of rhoLUT.
				 * As rhoLUT is a 1-D array in principle, convert it to the beginning index
				 * of a correct row. */
				rhoSampleYIndex *= rhoLUT->samplesX;

				/* After processing one line we should skip to the next row.
				 * Y index will be increased, i.e. rhoSampleY should be decreased.
				 */
				rhoSampleYIndexSkip = -rhoLUT->samplesX;
			}
			else
			{
				/* The same applies as for negative case. In fact, this applies for partY == 1. */
				rhoSampleYIndex = rhoSampleY - (int)floor(rhoLUT->deltaYMin / rhoLUT->samplingDistance + 0.5);
				rhoSampleYIndex *= rhoLUT->samplesX;
				rhoSampleYIndexSkip = rhoLUT->samplesX;
			}

			/* iterate all influenced rows */
			for (j = partYMin; j <= partYMax; j++)
			{
											/*
											 * old program
											 * /
											/* simple version, produces wrong results due to multiplication and division with samplingDistance
											y = corner_y + j * samplingDistance;
											yrel = fabs(y - yLight);
											indexRow = rhoLUT->width * (int)floor(yrel/rhoLUT->samplingDistance + 0.5);
											*/

											/* better version, documentation see x version below
											yrel = fabs(corner_y/rhoLUT->samplingDistance
														+ j * (samplingDistance/rhoLUT->samplingDistance)
														- yLight/rhoLUT->samplingDistance);
											indexRow = rhoLUT->width * (int)floor(yrel + 0.5);
											*/

				/*
				 * best version, no index interpolation
				 */

				/* X distance (in samples) from the X position of the light
				 * in the "propagation theory language":
				 * X component of the (target - source)
				 */
				/*
				xrel = (int)floor(
							  corner_x/rhoLUT->samplingDistance
							  + minX * (samplingDistance/rhoLUT->samplingDistance)
							  - xLight/rhoLUT->samplingDistance
							  - rhoLUT->deltaXMin / rhoLUT->samplingDistance
							  + 0.5);
							  */
				;
				/* Decide X range of iteration - inside of a circle
				 * with center (centerX, centerY) and radius maxSamples
				 */
				minX = (int)floor(sqrt((double)maxSamplesOptfield * (double)maxSamplesOptfield - (double)(j - centerY) * (double)(j - centerY)));
				maxX = centerX + minX;
				minX = centerX - minX;
				if (minX < 0)
					minX = 0;
				if (maxX >= samples_x)
					maxX = samples_x - 1;

				/* Prepare for columns iteration.
				 * Calculate X distance from the X position of the light
				 * in the "propagation theory language" (target - source).
				 * This would be in meters: xrel = corner_x + minX * samplingDistance - yLight;
				 * This is in samples:
				 */
				rhoSampleX = minX - centerX;

				/* We need to find what column (X coord) of rhoLUT to use.
				 * The situation is a bit more complicated because rhoLUT may begin at nonzero value.
				 * The simplest is to solve it for positive and negative possibilities.
				 */

				if (rhoSampleX < 0)
				{
					/* Negative case, i.e. minX < centerX; this happens e.g. when the light source X is in the
					 * hologram area.
					 * As the rhoLUT describes symmetric function, the index to rhoLUT will be
					 * derived from -rhoSampleX. Then, compensate for nonzero starting rho. */
					rhoSampleXIndex = -rhoSampleX - (int)floor(rhoLUT->deltaXMin / rhoLUT->samplingDistance + 0.5);

					/* This should never happen, however... */
					if (rhoSampleXIndex < 0) rhoSampleXIndex = 0;
				}
				else
				{
					/* The same applies as for negative case. However, in the positive case
					 * is minX > centerX, i.e. we will never cross centerX during calculation.
					 */
					rhoSampleXIndex = rhoSampleX - (int)floor(rhoLUT->deltaXMin / rhoLUT->samplingDistance + 0.5);
					if (rhoSampleXIndex < 0) rhoSampleXIndex = 0;
				}


				/* iterate all influenced columns of the target array */
				/* divide iteration to two parts: from minX to centerX, and from centerX+1 to maxX */

				partXMin = minX;
				partXMax = centerX;
				if (partXMax > maxX)
					partXMax = maxX;

				rhoSampleXIndexSkip = -1;
				index = j * samples_x + minX;

				for (partX = 0; partX < 2; partX++)
				{
					for (i = partXMin; i <= partXMax; i++, index++)
					{
						/* simple version, produces wrong results due to multiplication and division with samplingDistance
						   x and xrel are doubles

						x = corner_x + i * samplingDistance;
						xrel = fabs(x - xLight);
						rho = rhoLUT->rho[(int)floor(xrel/rhoLUT->samplingDistance+0.5) + indexRow];
						*/


						/* a better version; however, even when the term connected to i is exactly 1,
						   due to a limited precision xrel goes in the sequence 257.5 256.5 255.49999999999997 254.49999999999997 ...

						xrel = fabs(corner_x/rhoLUT->samplingDistance
									+ i * (samplingDistance/rhoLUT->samplingDistance)
									- xLight/rhoLUT->samplingDistance)
							   ;
						rho = rhoLUT->rho[(int)floor(xrel+0.5) + indexRow];
						*/

						/* best version, no index interpolation */
						rho = rhoLUT->rho[rhoSampleXIndex + rhoSampleYIndex];
						rhoSampleXIndex += rhoSampleXIndexSkip;

						indexWave = (int)floor((rho - waveLUT->rhoMin)/waveLUT->rhoSamplingDistance + 0.5);
						indexWave += indexZ;

						/* there is no need to check if the indexWave < maxSamples as (i,j) is
						 * inside the right area */

						out[index][RE] += waveLUT->phasor[indexWave][RE] * phasorRe - waveLUT->phasor[indexWave][IM]*phasorIm;
						out[index][IM] += waveLUT->phasor[indexWave][IM] * phasorRe + waveLUT->phasor[indexWave][RE]*phasorIm;
					}

					/* if the loop ran for partX == 0, then rhoSampleXIndex ended at -1;
					   however, for the partX == 1 iteration, rhoSampleXIndex should start at +1 */
					if (rhoSampleXIndex == -1)
						rhoSampleXIndex += 2;

					/* switch loop conditions for partX == 1 */
					rhoSampleXIndexSkip = 1;
					partXMin = centerX + 1;
					if (partXMin < 0)
						partXMin = 0;
					partXMax = maxX;
				}
				/* goto next Y for rhoLUT */
				rhoSampleYIndex += rhoSampleYIndexSkip;
			}
			/* switch loop conditions for partY == 1 */
			partYMin = centerY + 1;
			if (partYMin < 0)
				partYMin = 0;
			partYMax = maxY;
		}
	}

EXCEPTION0("lightsPropagateDLUT")
ENDEXCEPTION;
}
