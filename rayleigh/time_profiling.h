/*
 * Measuring time for testing and profiling
 * light propagation.
 */

/* choose between NO_TICKS_MEASURE and TICKS_MEASURE to enable/disable
   ticks measuring and display */
#ifndef __TIMEPROFILING_H

typedef enum  {
	TimePropagation = 0,
	TimeRhoCalculation = 1,
	TimeWaveLUTCalculation = 2,
	TimeConvolution = 3,
	TimeFFT = 4,
/* TimeComplete must be the last! */
	TimeComplete = 5} t_timeType;

extern char* timeTypeName[];

typedef void *t_timer;

RAYLEIGH_EXPORT int timerNew(t_timer *timer, char *timerName);
RAYLEIGH_EXPORT void timerDispose(t_timer *timer);
RAYLEIGH_EXPORT void timerStart(t_timer timer);
RAYLEIGH_EXPORT void timerPause(t_timer timer);
RAYLEIGH_EXPORT double timerReadSec(t_timer timer);

RAYLEIGH_EXPORT void timerSetArrayPosition(t_timer timer, int position);
RAYLEIGH_EXPORT int timerGetArrayPosition(t_timer timer);
RAYLEIGH_EXPORT char* timerGetName(t_timer timer);

RAYLEIGH_EXPORT void timerArrayInit(void);
RAYLEIGH_EXPORT void timerArrayDispose(void);
RAYLEIGH_EXPORT void timerArrayAdd(t_timer timer);
RAYLEIGH_EXPORT void timerArrayRemove(t_timer timer);
RAYLEIGH_EXPORT void timerArrayList();


#endif
