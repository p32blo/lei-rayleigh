/*
 * Loading and saving optical fields.
 * Loading and saving pictures.
 */

#include <string.h>
#include "rayleighCompile.h"
#include "png.h"

#define DEBUG_NOTIFY_SAVING

char SUFFIX_INFO[]						= ".nfo";

char DF_SUFFIX_DATA[]					= ".df";

char PICTURE_SUFFIX_INTENSITY[]			= "_i";
char PICTURE_SUFFIX_PHASE[]				= "_p";
char PICTURE_SUFFIX_INTENSITYPHASE[]	= "_b";
char PICTURE_SUFFIX_PHASEGRAY[]			= "_f";
char PICTURE_SUFFIX_TEXT[]				= "_t.txt";
char PICTURE_SUFFIX_REAL[]				= "_re";
char PICTURE_SUFFIX_IMAG[]				= "_im";
char PICTURE_SUFFIX_REALIMAG[]			= "_ri";

char PICTURE_SUFFIX_PNG8[]		= ".8.png";
char PICTURE_SUFFIX_PNG16[]		= ".16.png";


/* internal functions */
double atan2pos(double y, double x);
int save_complexfieldinfo(t_optfield* image, char* filename, char* comment);
void compute_phase_map(int phase, int *int_phase_r, int *int_phase_g, int *int_phase_b, int max_int_value);





/* Save the light field as DF file.
 * Two files are saved: image descriptor (txt file), image samples (binary).
 * Comment can be added to txt file.
 */

int optfieldSaveDF(t_optfield* image, char* filename, char *comment)
{
	FILE *fout = NULL;
	char *tmp_filename = NULL;
	RAYLEIGH_INT64 res;
	int samples_x, samples_y;
	double samplingdistance;
	double center_x, center_y, center_z;

	if ((tmp_filename = (char*) malloc(strlen(filename)+strlen(DF_SUFFIX_DATA)+1)) == NULL)
		GOEXCEPTION;

	sprintf(tmp_filename, "%s%s", filename, DF_SUFFIX_DATA);
	if ((fout = fopen(tmp_filename, "wb")) == NULL)
		GOEXCEPTION;

#ifdef DEBUG_NOTIFY_SAVING
	LOG1("Saving %s\n", tmp_filename);
#endif

	CHECK(optfieldGetSamples(&samples_x, &samples_y, image));
	CHECK(optfieldGetCenter(&center_x, &center_y, &center_z, image));
	samplingdistance = optfieldGetSamplingdistance(image);


	fputc(4, fout);
	fwrite("DFHD", 4, 1, fout);
	fwrite(&lambda, sizeof(double), 1, fout);
	fwrite(&samplingdistance, sizeof(double), 1, fout);
	fwrite(&samplingdistance, sizeof(double), 1, fout);
	res = samples_x; fwrite(&res, sizeof(RAYLEIGH_INT64), 1, fout);
	res= samples_y; fwrite(&res, sizeof(RAYLEIGH_INT64), 1, fout);
	fwrite(&center_x, sizeof(double), 1, fout);
	fwrite(&center_y, sizeof(double), 1, fout);
	fputc(4, fout);
	fwrite("DFPT", 4, 1, fout);

	if (optfieldGetSampleType(image) == SampleTypeFFTWDouble)
		fwrite(image->buffer->data,
			bufferGetByteSize(image->buffer), 1, fout);
	else
		GOEXCEPTION;

	fclose(fout);

	CHECK(save_complexfieldinfo(image, tmp_filename, comment));

	free(tmp_filename);
	return RAYLEIGH_OK;

EXCEPTION

	if (fout != NULL) fclose(fout);
	if (tmp_filename != NULL) free(tmp_filename);
	return RAYLEIGH_ERROR;
}


int save_complexfieldinfo(t_optfield* image, char* filename, char* comment)
{
	FILE *fout = NULL;
	char *tmp_filename = NULL;
	fftw_complex min, max, max_abs;
	double max_abs_size;
	RAYLEIGH_INT64 max_abs_index;
	int samples_x, samples_y;
	double center_x, center_y, center_z, size_x, size_y;

	if ((tmp_filename = (char*) malloc(strlen(filename)+strlen(SUFFIX_INFO)+1)) == NULL)
		GOEXCEPTION;

	sprintf(tmp_filename, "%s%s", filename, SUFFIX_INFO);
	if ((fout = fopen(tmp_filename, "w")) == NULL)
	{
		ELOG1("Error opening output file %s\n", tmp_filename);
		GOEXCEPTION;
	}

	CHECK(optfieldGetSamples(&samples_x, &samples_y, image));
	CHECK(optfieldGetSize(&size_x, &size_y, image));
	CHECK(optfieldGetCenter(&center_x, &center_y, &center_z, image));
	fprintf(fout, "RAW complex optical field (double re, double im)\n");
	fprintf(fout, "Filename: \"%s\"\n", filename);
	fprintf(fout, "Resolution: %d * %d (columns * rows)\n", samples_x, samples_y);
	fprintf(fout, "Sample type: %s (%d bytes/sample)\n",
		SampleTypeName[optfieldGetSampleType(image)], optfieldGetSampleTypeSize(image));

	fprintf(fout, "Raw data size: %d bytes\n", bufferGetByteSize(image->buffer));

	fprintf(fout, "Size: %.5g * %.5g mm (x * y)\n", size_x TOMM, size_y TOMM);
	fprintf(fout, "Sampling distance: %.3g um (%d x %.3g nm)\n",
		optfieldGetSamplingdistance(image) TOUM, optfieldGetSampling(image), common_sampling_distance TONM);
	fprintf(fout, "Center position: (%.5g; %.5g; %.5g) [mm]\n",
		center_x TOMM, center_y TOMM, center_z TOMM);

	optfieldFindMinmax(image, min, max, max_abs, &max_abs_size, &max_abs_index);

	fprintf(fout, "Range RE: %g to %g\n", min[RE], max[RE]);
	fprintf(fout, "Range IM: %g to %g\n", min[IM], max[IM]);
	fprintf(fout, "Range intensity: maximum %g at position (%d; %d) with re=%g; im=%g\n",
		max_abs_size,
		max_abs_index % samples_x, max_abs_index / samples_x,
		max_abs[RE], max_abs[IM]);

	fprintf(fout, "Comment: %s\n", comment);

	fclose(fout);
	free(tmp_filename);
	return RAYLEIGH_OK;

EXCEPTION
	if (fout != NULL) fclose(fout);
	if (tmp_filename != NULL) free(tmp_filename);
	return RAYLEIGH_ERROR;

}



/*
 * Positive atan2, ie. output is between 0 and 2pi
 */
double atan2pos(double y, double x)
{
 double tmp = atan2(y, x);
 return tmp>=0 ? tmp : tmp+PI2;
}

#define MAX_INT_8BIT	255
#define MAX_INT_16BIT	65535

#define OUTPUT(number, fout)	if (max_int_value == MAX_INT_8BIT) \
									fputc(number, fout); \
								else \
								{ \
									fputc((number) & 0xFF, fout); \
									fputc(((number) & 0xFF00) >> 8, fout); \
								}

int save_picture_info(char* filename,
					  int bits_per_channel, int channels,
					  double gamma,
					  char *file_type, char *picture_type,
					  double max_intensity,
					  t_optfield *image,
					  char *comment)
{
	char *infofilename = NULL;
	FILE *fout = NULL;
	int samples_x, samples_y;
	double center_x, center_y, center_z, size_x, size_y;

	if ((infofilename = (char *)malloc(strlen(filename) + strlen(SUFFIX_INFO) + 1)) == NULL)
		GOEXCEPTION;

	sprintf(infofilename, "%s%s", filename, SUFFIX_INFO);
	if ((fout = fopen(infofilename, "w")) == NULL)
	{
		ELOG1("Error opening output file %s\n", infofilename);
		GOEXCEPTION;
	}

	CHECK(optfieldGetSamples(&samples_x, &samples_y, image));
	CHECK(optfieldGetSize(&size_x, &size_y, image));
	CHECK(optfieldGetCenter(&center_x, &center_y, &center_z, image));

	fprintf(fout, "PICTURE INFO\n");
	fprintf(fout, "Filename: \"%s\"\n", filename);
	fprintf(fout, "Type: %s %s\n", file_type, picture_type);
	fprintf(fout, "%d channels, %d bits/channel (IPM PC)\n", channels, bits_per_channel);
	fprintf(fout, "Gamma: %.2g\n", gamma);
	fprintf(fout, "Assumed maximal intensity: %.8g\n", max_intensity);
	fprintf(fout, "Resolution: %d * %d (columns * rows)\n", samples_x, samples_y);
	fprintf(fout, "Size: %.5g * %.5g mm (x * y)\n",size_x TOMM, size_y TOMM);
	fprintf(fout, "Sampling distance: %.3g um (%d x %.3g nm)\n",
		optfieldGetSamplingdistance(image) TOUM, optfieldGetSampling(image), common_sampling_distance TONM);
	fprintf(fout, "Position: (%.8g; %.8g; %.8g) [mm]\n",
		center_x TOMM, center_y TOMM, center_z TOMM);
	if (comment != NULL)
		fprintf(fout, "Comment: %s\n", comment);


	fclose(fout);
	free(infofilename);
	return RAYLEIGH_OK;

EXCEPTION
	if (infofilename != NULL) free(infofilename);
	if (fout != NULL) fclose(fout);
	return RAYLEIGH_ERROR;
}



#define	BUFF_OUTPUT(Intensity, Ptr) {	*(Ptr)++ = ((Intensity) & 0xFF00) >> 8; \
										*(Ptr)++ = (Intensity) & 0xFF; \
									}


/*
 * save as 16bit PNG file
 */
int optfieldSavePNG(t_optfield *image, char *filename, t_PictureIOType outputtype, double gamma, double providedMaxIntensity, char *comment)
{
	char *filename_out = NULL;
	FILE *image_out = NULL;
	int x, y;
	int samples_x, samples_y;
	RAYLEIGH_INT64 index, max_abs_index;
	int phase, int_phase_r, int_phase_g, int_phase_b;
	fftw_complex amplitude;
	double intensity, max_intensity;
	double gamma_correction;
	int int_intensity;
	size_t filename_length, suffix_length;
	char *suffix;
	int max_int_value = MAX_INT_16BIT;
	int bits_per_channel, channels;
	char *file_type = "PNG";

	int           color_type;
	png_struct    *png_ptr = NULL;
	png_info      *info_ptr = NULL;
	png_byte      *png_pixels = NULL;
	png_byte      *pix_ptr = NULL;
	png_uint_32   row_bytes;
	int retval = RAYLEIGH_ERROR;
	T_SAMPLE_TYPE_FFTWDOUBLE *data;

	fftw_complex min_value, max_value, max_abs;
	double norm_value_min, norm_value_max;

	CHECK(optfieldGetSamples(&samples_x, &samples_y, image));

	if (optfieldGetSampleType(image) != SampleTypeFFTWDouble)
	{
		ELOG1("Sample type %s not supported for PNG output.", SampleTypeName[optfieldGetSampleType(image)]);
		GOEXCEPTION;
	}

	data = (T_SAMPLE_TYPE_FFTWDOUBLE*) image->buffer->data;

	gamma_correction = 1/gamma;

	/*
	 * find maximum values
	 */
	optfieldFindMinmax(image, min_value, max_value, max_abs, &max_intensity, &max_abs_index);

	/*
	 * when saving real/imag parts, it is good to have 0 at mid-grey,
	 * i.e. min_value and max_value should be the same for both real and imag
	 * and min_value should be -max_value
	 */
	norm_value_min = min_value[RE] < min_value[IM] ? min_value[RE] : min_value[IM];
	norm_value_max = max_value[RE] > max_value[IM] ? max_value[RE] : max_value[IM];

	if (fabs(norm_value_min) > fabs(norm_value_max))
		norm_value_max = fabs(norm_value_min);

	norm_value_min = -norm_value_max;


	if (providedMaxIntensity != 0)
		max_intensity = providedMaxIntensity;


	suffix = PICTURE_SUFFIX_PNG16;
	bits_per_channel = 16;
	suffix_length = strlen(suffix);

	/*
	 * generate file names
	 */
	filename_length = strlen(filename) + 1;

	if (outputtype == PictureIOIntensity)
	{
		if ((filename_out =
			(char*) malloc(suffix_length+filename_length+strlen(PICTURE_SUFFIX_INTENSITY))) == NULL)
			{
				ELOG0("Memory error while saving image.\n");
				GOEXCEPTION;
			}
		sprintf(filename_out, "%s%s%s", filename, PICTURE_SUFFIX_INTENSITY, suffix);
		channels = 1;
		color_type = PNG_COLOR_TYPE_GRAY;
		save_picture_info(filename_out, bits_per_channel, channels, gamma, file_type, "intensity", max_intensity, image, comment);
	}
	else if (outputtype == PictureIOPhase)
	{
		if ((filename_out =
			(char*) malloc(suffix_length+filename_length+strlen(PICTURE_SUFFIX_PHASE))) == NULL)
			{
				ELOG0("Memory error while saving image.\n");
				GOEXCEPTION;
			}

		sprintf(filename_out, "%s%s%s", filename, PICTURE_SUFFIX_PHASE, suffix);
		channels = 3;
		color_type = PNG_COLOR_TYPE_RGB;
		save_picture_info(filename_out, bits_per_channel, channels, gamma, file_type, "phase", max_intensity, image, comment);
	}
	else if (outputtype == PictureIOIntensityPhase)
	{
		if ((filename_out =
			(char*) malloc(suffix_length+filename_length+strlen(PICTURE_SUFFIX_INTENSITYPHASE))) == NULL)
			{
				ELOG0("Memory error while saving image.\n");
				GOEXCEPTION;
			}
		sprintf(filename_out, "%s%s%s", filename, PICTURE_SUFFIX_INTENSITYPHASE, suffix);
		channels = 3;
		color_type = PNG_COLOR_TYPE_RGB;
		save_picture_info(filename_out, bits_per_channel, channels, gamma, file_type, "intensity/phase", max_intensity, image, comment);
	}
	else if (outputtype == PictureIOPhaseGray)
	{
		if ((filename_out =
			(char*) malloc(suffix_length+filename_length+strlen(PICTURE_SUFFIX_PHASEGRAY))) == NULL)
			{
				ELOG0("Memory error while saving image.\n");
				GOEXCEPTION;
			}
		sprintf(filename_out, "%s%s%s", filename, PICTURE_SUFFIX_PHASEGRAY, suffix);
		channels = 1;
		color_type = PNG_COLOR_TYPE_GRAY;
		save_picture_info(filename_out, bits_per_channel, channels, gamma, file_type, "phase (one channel)", max_intensity, image, comment);
	}
	else if (outputtype == PictureIORealImagIntensity)
	{
		if ((filename_out =
			(char*) malloc(suffix_length+filename_length+strlen(PICTURE_SUFFIX_REAL))) == NULL)
			{
				ELOG0("Memory error while saving image.\n");
				GOEXCEPTION;
			}
		sprintf(filename_out, "%s%s%s", filename, PICTURE_SUFFIX_REALIMAG, suffix);
		channels = 3;
		color_type = PNG_COLOR_TYPE_RGB;
		save_picture_info(filename_out, bits_per_channel, channels, gamma, file_type, "real+imag", max_intensity, image, comment);
	}
	else if (outputtype == PictureIOReal)
	{
		if ((filename_out =
			(char*) malloc(suffix_length+filename_length+strlen(PICTURE_SUFFIX_REAL))) == NULL)
			{
				ELOG0("Memory error while saving image.\n");
				GOEXCEPTION;
			}
		sprintf(filename_out, "%s%s%s", filename, PICTURE_SUFFIX_REAL, suffix);
		channels = 1;
		color_type = PNG_COLOR_TYPE_GRAY;
		save_picture_info(filename_out, bits_per_channel, channels, gamma, file_type, "real part (one channel)", max_intensity, image, comment);
	}
	else if (outputtype == PictureIOImag)
	{
		if ((filename_out =
			(char*) malloc(suffix_length+filename_length+strlen(PICTURE_SUFFIX_IMAG))) == NULL)
			{
				ELOG0("Memory error while saving image.\n");
				GOEXCEPTION;
			}
		sprintf(filename_out, "%s%s%s", filename, PICTURE_SUFFIX_IMAG, suffix);
		channels = 1;
		color_type = PNG_COLOR_TYPE_GRAY;
		save_picture_info(filename_out, bits_per_channel, channels, gamma, file_type, "imaginary part (one channel)", max_intensity, image, comment);
	}
	else
		GOEXCEPTION0("Unknown picture type.\n")


#ifdef DEBUG_NOTIFY_SAVING
	LOG1("Saving %s\n", filename_out);
#endif

	if ((image_out = fopen(filename_out, "wb")) == NULL)
	{
		ELOG0("Error opening output image.\n");
		GOEXCEPTION;
	}

	/*
	 * prepare space for one image line
	 */
	row_bytes = samples_x * channels * 2;
	if ((png_pixels = (png_byte *) malloc (row_bytes * sizeof (png_byte))) == NULL)
	{
		ELOG0("Memory error while saving image.\n");
		GOEXCEPTION;
	}

	/*
	 * PNG stuff
	 */
	if ((png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL)) == NULL)
	{
		ELOG0("PNG init error while saving image.\n");
		GOEXCEPTION;
	}

	if ((info_ptr = png_create_info_struct (png_ptr)) == NULL)
	{
		ELOG0("PNG init error while saving image.\n");
		GOEXCEPTION;
	}

	if (setjmp (png_jmpbuf(png_ptr)))
	{
		ELOG0("PNG init error while saving image.\n");
		GOEXCEPTION;
	}

	png_init_io (png_ptr, image_out);

	png_set_IHDR(png_ptr, info_ptr, samples_x, samples_y, bits_per_channel, color_type,
		PNG_INTERLACE_NONE,	PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

	/*
	 * Setting gamma would be great; however, as PNG gAMA chunk causes problems it is wiser not to use it.
	 * see e.g. http://hsivonen.iki.fi/png-gamma/
	 *
	 * png_set_gAMA(png_ptr, info_ptr, gamma_correction);
	 */

	png_write_info (png_ptr, info_ptr);


	/*
	 * write image itself
	 */
	index = (RAYLEIGH_INT64)(samples_y-1) * (RAYLEIGH_INT64)samples_x;
	for (y=samples_y-1; y>=0; y--)
	{
		for (x=0, pix_ptr = png_pixels; x<samples_x; x++)
		{
			amplitude[RE] = data[index][RE];
			amplitude[IM] = data[index][IM];
			index++;

			if (outputtype == PictureIOIntensity)
			{
				intensity = amplitude[RE]*amplitude[RE] + amplitude[IM]*amplitude[IM];
				intensity /= max_intensity;
				intensity = max_int_value * pow(intensity, gamma_correction);
				if (intensity > max_int_value) int_intensity = max_int_value;
				else if (intensity < 0) int_intensity = 0;
				else int_intensity = (int) (intensity);

				BUFF_OUTPUT(int_intensity, pix_ptr)
			}

			if (outputtype == PictureIOPhase)
			{
				phase = (int) (atan2pos(amplitude[IM], amplitude[RE]) / PI2 * 6 * max_int_value);
				compute_phase_map(phase, &int_phase_r, &int_phase_g, &int_phase_b, max_int_value);
				BUFF_OUTPUT(int_phase_b, pix_ptr)
				BUFF_OUTPUT(int_phase_g, pix_ptr)
				BUFF_OUTPUT(int_phase_r, pix_ptr);
			}

			if (outputtype == PictureIOPhaseGray)
			{
				phase = (int) (atan2pos(amplitude[IM], amplitude[RE]) / PI2 * max_int_value);
				BUFF_OUTPUT(phase, pix_ptr)
			}

			if (outputtype == PictureIOReal)
			{
				intensity = (amplitude[RE]-norm_value_min)/(norm_value_max - norm_value_min);
				intensity *= max_int_value;
				if (intensity > max_int_value) int_intensity = max_int_value;
				else if (intensity < 0) int_intensity = 0;
				else int_intensity = (int) (intensity);
				BUFF_OUTPUT(int_intensity, pix_ptr);
			}

			if (outputtype == PictureIOImag)
			{
				intensity = (amplitude[IM]-norm_value_min)/(norm_value_max - norm_value_min);
				intensity *= max_int_value;
				if (intensity > max_int_value) int_intensity = max_int_value;
				else if (intensity < 0) int_intensity = 0;
				else int_intensity = (int) (intensity);
				BUFF_OUTPUT(int_intensity, pix_ptr);
			}

			if (outputtype == PictureIOIntensityPhase)
			{
				phase = (int) (atan2pos(amplitude[IM], amplitude[RE]) / PI2 * 6 * max_int_value);
				compute_phase_map(phase, &int_phase_r, &int_phase_g, &int_phase_b, max_int_value);
				intensity = amplitude[RE]*amplitude[RE] + amplitude[IM]*amplitude[IM];
				intensity /= max_intensity;
				intensity = pow(intensity, gamma_correction);
				intensity = 0.75*intensity + 0.25;
				if (intensity < 0) intensity = 0; else if (intensity > 1) intensity = 1;

				BUFF_OUTPUT((int)(int_phase_b*intensity), pix_ptr);
				BUFF_OUTPUT((int)(int_phase_g*intensity), pix_ptr);
				BUFF_OUTPUT((int)(int_phase_r*intensity), pix_ptr);
			}

			if (outputtype == PictureIORealImagIntensity)
			{
				intensity = (amplitude[RE]-norm_value_min)/(norm_value_max - norm_value_min);
				intensity *= max_int_value;
				if (intensity > max_int_value) int_intensity = max_int_value;
				else if (intensity < 0) int_intensity = 0;
				else int_intensity = (int) (intensity);
				BUFF_OUTPUT(int_intensity, pix_ptr);

				intensity = (amplitude[IM]-norm_value_min)/(norm_value_max - norm_value_min);
				intensity *= max_int_value;
				if (intensity > max_int_value) int_intensity = max_int_value;
				else if (intensity < 0) int_intensity = 0;
				else int_intensity = (int) (intensity);
				BUFF_OUTPUT(int_intensity, pix_ptr);

				intensity = amplitude[RE]*amplitude[RE] + amplitude[IM]*amplitude[IM];
				intensity /= max_intensity;
				intensity = max_int_value * pow(intensity, gamma_correction);
				if (intensity > max_int_value) int_intensity = max_int_value;
				else if (intensity < 0) int_intensity = 0;
				else int_intensity = (int) (intensity);

				BUFF_OUTPUT(int_intensity, pix_ptr)
			}
		}
		index -= 2*samples_x;
		png_write_row(png_ptr, png_pixels);
	}

	/*
	 * done!
	 *
	 * close files
	 */

	png_write_end (png_ptr, info_ptr);
	retval = RAYLEIGH_OK;

EXCEPTION
	png_destroy_write_struct (&png_ptr, (png_infopp) NULL);
	if (png_pixels != (unsigned char*) NULL) free (png_pixels);
	if (image_out != NULL) fclose(image_out);
	if (filename_out != NULL) free(filename_out);

	return retval;
}




int optfieldLoadPNG(t_optfield **image, char *filename, int sampling, t_SampleType sampleType, t_BufferType bufferType, t_PictureIOType inputtype, double gamma)
{
	FILE *fp = NULL;
	int is_png;
	png_structp png_ptr = NULL;
	png_infop info_ptr = NULL;
	png_uint_32 width, height;
	int bit_depth, color_type, interlace_type, compression_method, filter_method;
	png_bytep* row_pointers = NULL;
	char header[4];
	int header_bytes = 4;
	unsigned int x, y;
	int src_offset;
	RAYLEIGH_INT64 dst_offset;
	int pixel;
	T_SAMPLE_TYPE_FFTWDOUBLE *data;

	/* sanity check */
	if (gamma < 0)
		GOEXCEPTION0("Wrong gamma.\n");

	/* input reference variable check */
	if (image == NULL)
		GOEXCEPTION;

	/* check input parameters */
	if (sampling < 1)
	{
		ELOG2("Wrong sampling distance (%.3g) when saving PNG %s.\n", sampling * common_sampling_distance, filename);
		GOEXCEPTION;
	}

	if (sampleType != SampleTypeFFTWDouble)
	{
		ELOG2("Sample type %s not supported (error when saving PNG file %s).\n", SampleTypeName[sampleType], filename);
		GOEXCEPTION;
	}

	if (bufferType != BufferTypeMain)
	{
		ELOG2("Buffer type %s not supported (error when saving PNG file %s).\n", bufferTypeName[bufferType], filename);
		GOEXCEPTION;
	}

	/* try to open file */
	fp = fopen(filename, "rb");
	if (!fp)
	{
		ELOG1("Cannot open file %s\n", filename);
		GOEXCEPTION;
	}

	/* try to check file type */
	fread(header, 1, header_bytes, fp);
	is_png = !png_sig_cmp((png_bytep)header, 0, header_bytes);

	if (!is_png)
	{
		ELOG1("file %s is not a PNG file\n", filename);
		GOEXCEPTION;
	}

	/* create structure for PNG reading */
	png_ptr = png_create_read_struct
		(PNG_LIBPNG_VER_STRING, (png_voidp)NULL,
		NULL, NULL);
	if (!png_ptr)
	{
		ELOG0("Internal PNG reading error\n.");
		GOEXCEPTION;
	}

	/* create info struct for PNG reading */
	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr)
	{
		ELOG0("internal PNG reading error\n");
		GOEXCEPTION;
	}

	/* begin PNG reading */
	png_init_io(png_ptr, fp);
	png_set_sig_bytes(png_ptr, header_bytes);
	png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

	png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, &interlace_type, &compression_method, &filter_method);

	/* now we know what do we read */
	/*
	printf("%d %d   %d %d   %d   %d %d\n",
		width, height,
		bit_depth, color_type,
		interlace_type,
		compression_method, filter_method);
	*/

	if (color_type != PNG_COLOR_TYPE_GRAY)
	{
		ELOG1("Unsupported color model of PNG file %s.\n", filename);
		GOEXCEPTION;
	}


	/* initialize complex image */
		/* first possibility: we have to initialize both image structure and data block */

	if (*image == NULL)
	{
		if (optfieldNew(0, 0, 0,
			width, height, sampling, sampleType, bufferType, NULL, image) != RAYLEIGH_OK)
		{
			ELOG3("Error initializing image of size %d x %d from file %s.\n", width, height, filename);
			GOEXCEPTION;
		}
	}

	data = (T_SAMPLE_TYPE_FFTWDOUBLE*) (*image)->buffer->data;

	/* TODO */
	/* second possibility: provided image structure */

	/* allocate buffer for PNG reading */
	row_pointers = (png_bytep*) malloc(height * sizeof(png_bytep*));
	if (row_pointers == NULL)
	{
		ELOG0("Internal PNG reading error\n");
		GOEXCEPTION;
	}

	row_pointers = png_get_rows(png_ptr, info_ptr);


	if (bit_depth == 16)
	{
		dst_offset = (RAYLEIGH_INT64)width * (RAYLEIGH_INT64)(height-1);
		for (y=0; y<height; y++, dst_offset-=2*width)
			for (x=0, src_offset = 0; x<width; x++, src_offset += 2, dst_offset++)
			{
				pixel = (((int) *(row_pointers[y] + src_offset)) << 8) +
						((int) *(row_pointers[y] + (src_offset + 1)));
				data[dst_offset][RE] = pow(pixel / 65535.0, 0.5*gamma);
				data[dst_offset][IM] = 0;
			}
	}
	else if (bit_depth == 8)
	{
		dst_offset = 0;
		for (y=0; y<height; y++)
			for (x=0, src_offset = 0; x<width; x++, src_offset ++, dst_offset++)
			{
				pixel = (int) *(row_pointers[y] + (src_offset));
				data[dst_offset][RE] = pow(pixel / 255.0, 0.5*gamma);
				data[dst_offset][IM] = 0;
			}
	}



	/* free row_pointers PNG data */

	png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
	/* for (y=0; y<height; y++)
		free(row_pointers[y]); */
	/* free(row_pointers); */
	fclose(fp);



	return RAYLEIGH_OK;

EXCEPTION
	if (fp != NULL) fclose(fp);
	if (png_ptr != NULL) png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
	if (*image != NULL) optfieldDispose(image);
	/* if (row_pointers != NULL) free(row_pointers); */
	return RAYLEIGH_ERROR;
}



/*
 * compute rgb visualization of the angle
 *
 * PARAMETERS
 *
 * phase (IN): angle between 0 and 360o, representad in interval 0-6*255
 * int_phase_r, int_phase_g, int_phase_b (OUT): rgb values
 * max_int_value (IN): maximum int value; 255 for 8bit output, 65535 for 16bit output
 */
void compute_phase_map(int phase, int *int_phase_r, int *int_phase_g, int *int_phase_b, int max_int_output)
{
	if (phase < max_int_output)
	{
		*int_phase_r = max_int_output;
		*int_phase_g = phase;
		*int_phase_b = 0;
	}
	else if (phase < 2*max_int_output)
	{
		*int_phase_r = 2*max_int_output - phase;
		*int_phase_g = max_int_output;
		*int_phase_b = 0;
	}
	else if (phase < 3*max_int_output)
	{
		*int_phase_r = 0;
		*int_phase_g = max_int_output;
		*int_phase_b = phase - 2*max_int_output;
	}
	else if (phase < 4*max_int_output)
	{
		*int_phase_r = 0;
		*int_phase_g = 4*max_int_output - phase;
		*int_phase_b = max_int_output;
	}
	else if (phase < 5*max_int_output)
	{
		*int_phase_r = phase - 4*max_int_output;
		*int_phase_g = 0;
		*int_phase_b = max_int_output;
	}
	else
	{
		*int_phase_r = max_int_output;
		*int_phase_g = 0;
		*int_phase_b = 6*max_int_output - phase;
	}

	if (*int_phase_r < 0) *int_phase_r = 0; else if (*int_phase_r > max_int_output) *int_phase_r = max_int_output;
	if (*int_phase_g < 0) *int_phase_g = 0; else if (*int_phase_g > max_int_output) *int_phase_g = max_int_output;
	if (*int_phase_b < 0) *int_phase_b = 0; else if (*int_phase_b > max_int_output) *int_phase_b = max_int_output;
}

/*
int optfieldPrint(t_optfield *optfield, char *filename, char *format)
{
	int samples_x, samples_y;
	int closefile = 0;
	FILE *fout = NULL;

	if (filename == NULL)
		fout = stdout;
	else
	{
		closefile = 1;
		if ((fout = fopen(filename, "wb")) == NULL)
			GOEXCEPTION;
	}

	CHECK(optfieldGetSamples(&samples_x, &samples_y, optfield));

	return RAYLEIGH_OK;

EXCEPTION
	if (closefile)
		fclose(fout);
	return RAYLEIGH_ERROR;
}
*/

void pictureTest(void)
{
	t_optfield *image = NULL;
	char filename[100];

	CHECK(optfieldNew(0, 1 MM, 2 MM, 300, 400, 10, SampleTypeFFTWDouble, BufferTypeMain, NULL, &image));
	CHECK(optfieldDebugdata(image));
	CHECK(optfieldSaveDF(image, "test_original", "original debug data"));
	CHECK(optfieldSavePNG(image, "test_original", PictureIOIntensity, 1.0, 0, NULL));
	CHECK(optfieldSavePNG(image, "test_original", PictureIOIntensityPhase, 1.0, 0, NULL));
	CHECK(optfieldSavePNG(image, "test_original", PictureIOPhase, 1.0, 0, NULL));
	CHECK(optfieldSavePNG(image, "test_original", PictureIOPhaseGray, 1.0, 0, NULL));
	CHECK(optfieldDispose(&image));

	sprintf(filename, "test_original%s%s", PICTURE_SUFFIX_INTENSITY, PICTURE_SUFFIX_PNG16);

	//CHECK(optfieldLoadDF(&image, "test_original.df"));
	CHECK(optfieldLoadPNG(&image, filename, 10, SampleTypeFFTWDouble, BufferTypeMain, PictureIOIntensity, 1.0));
	CHECK(optfieldSaveDF(image, "test_copy", "copied debug data"));
	CHECK(optfieldDispose(&image));

	CHECK(optfieldLoadPNG(&image, filename, 10, SampleTypeFFTWDouble, BufferTypeMain, PictureIOIntensity, 1.0));
	CHECK(optfieldSavePNG(image, "test_copy", PictureIOIntensity, 1.0, 0, NULL));
	CHECK(optfieldDispose(&image));
	return;

EXCEPTION
	if (image != NULL) optfieldDispose(&image);
	ELOG0("picture test failed\n");
	exit(0);
}
