/*
 * Propagation kernel evaluation.
 */
#ifndef __PROPAGATIONKERNEL_H
#define __PROPAGATIONKERNEL_H

/* Calculate R-S type propagation kernel using double LUT (waveLUT+rhoLUT) method
 * without any interpolation.
 */
RAYLEIGH_EXPORT int kernelCalculateDLUTFast(t_optfield *kernel, t_optfield *source, t_optfield *target,
							t_rhoLUT *rhoLUT, t_waveLUT *waveLUT);

/* Calculate R-S type propagation kernel using double LUT (waveLUT+rhoLUT) method
 * with LUT entries interpolation.
 */
RAYLEIGH_EXPORT int kernelCalculateDLUT(t_optfield *kernel, t_optfield *source, t_optfield *target,
							 t_rhoLUT *rhoLUT, const int rhoLUTFilterType,
							 t_waveLUT *waveLUT, const int waveLUTFilterType);

/* Calculate R-S type propagation kernel using waveLUT only
 * with LUT entries interpolation.
 */
RAYLEIGH_EXPORT int kernelCalculateWaveLUT(t_optfield *kernel, t_optfield *source, t_optfield *target,
						   t_waveLUT *waveLUT, const int waveLUTFilterType);


/*
 * Calculate Rayleigh-Sommerfeld kernel for the light propagation
 */
RAYLEIGH_EXPORT int kernelCalculateRSFT(t_point *shift, t_propagPlan *propagationPiece,
					double samplingDistance_x, double samplingDistance_y,
					int filterShift_x, double *filterData_x, int filterWidthHalf_x, double filterSamplingDistance_x,
					int filterShift_y, double *filterData_y, int filterWidthHalf_y, double filterSamplingDistance_y,
					t_optfield *kernel, t_FFTApply applyFFT);

/*
 * Calculate angular spectrum kernel for the light propagation
 */
RAYLEIGH_EXPORT int kernelCalculateAS(t_point *global_shift,
					t_propagPlan *propagationPiece,
					double samplingDistance_x, double samplingDistance_y,
					double *interpFilterData_x, int interpFilterWidthHalf_x, int interpUpsampling_x,
					double *interpFilterData_y, int interpFilterWidthHalf_y, int interpUpsampling_y,
					t_optfield *kernel);

#endif
